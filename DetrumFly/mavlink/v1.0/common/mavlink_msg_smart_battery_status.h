// MESSAGE SMART_BATTERY_STATUS PACKING

#define MAVLINK_MSG_ID_SMART_BATTERY_STATUS 93

typedef struct __mavlink_smart_battery_status_t
{
 uint16_t id; /*<  Battery ID*/
 uint16_t production_date ; /*< Battery production date*/
 uint16_t temperature; /*< Battery temperature*/
 uint16_t design_voltage; /*< Design voltage for smart battery*/
 uint16_t design_capacity; /*< Design capacity for smart battery*/
 uint16_t full_capacity; /*< Full capacity for smart battery*/
 uint16_t available_battery; /*< Available battery for smart battery*/
 uint16_t remaining_battery; /*< Remaining battery energy: (0%: 0, 100%: 100)*/
 uint16_t cycle_count; /*< Cycle count for smart battery*/
 uint16_t voltage_single[4]; /*< Battery voltage of cells, in millivolts (1 = 1 millivolt). Cells above the valid cell count for this battery should have the UINT16_MAX value.*/
 uint16_t voltage; /*< Current voltage for smart battery*/
 uint8_t life_time; /*< Smart battery life time*/
} mavlink_smart_battery_status_t;

#define MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN 29
#define MAVLINK_MSG_ID_93_LEN 29

#define MAVLINK_MSG_ID_SMART_BATTERY_STATUS_CRC 49
#define MAVLINK_MSG_ID_93_CRC 49

#define MAVLINK_MSG_SMART_BATTERY_STATUS_FIELD_VOLTAGE_SINGLE_LEN 4

#define MAVLINK_MESSAGE_INFO_SMART_BATTERY_STATUS { \
	"SMART_BATTERY_STATUS", \
	12, \
	{  { "id", NULL, MAVLINK_TYPE_UINT16_T, 0, 0, offsetof(mavlink_smart_battery_status_t, id) }, \
         { "production_date ", NULL, MAVLINK_TYPE_UINT16_T, 0, 2, offsetof(mavlink_smart_battery_status_t, production_date ) }, \
         { "temperature", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_smart_battery_status_t, temperature) }, \
         { "design_voltage", NULL, MAVLINK_TYPE_UINT16_T, 0, 6, offsetof(mavlink_smart_battery_status_t, design_voltage) }, \
         { "design_capacity", NULL, MAVLINK_TYPE_UINT16_T, 0, 8, offsetof(mavlink_smart_battery_status_t, design_capacity) }, \
         { "full_capacity", NULL, MAVLINK_TYPE_UINT16_T, 0, 10, offsetof(mavlink_smart_battery_status_t, full_capacity) }, \
         { "available_battery", NULL, MAVLINK_TYPE_UINT16_T, 0, 12, offsetof(mavlink_smart_battery_status_t, available_battery) }, \
         { "remaining_battery", NULL, MAVLINK_TYPE_UINT16_T, 0, 14, offsetof(mavlink_smart_battery_status_t, remaining_battery) }, \
         { "cycle_count", NULL, MAVLINK_TYPE_UINT16_T, 0, 16, offsetof(mavlink_smart_battery_status_t, cycle_count) }, \
         { "voltage_single", NULL, MAVLINK_TYPE_UINT16_T, 4, 18, offsetof(mavlink_smart_battery_status_t, voltage_single) }, \
         { "voltage", NULL, MAVLINK_TYPE_UINT16_T, 0, 26, offsetof(mavlink_smart_battery_status_t, voltage) }, \
         { "life_time", NULL, MAVLINK_TYPE_UINT8_T, 0, 28, offsetof(mavlink_smart_battery_status_t, life_time) }, \
         } \
}


/**
 * @brief Pack a smart_battery_status message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param id  Battery ID
 * @param production_date  Battery production date
 * @param temperature Battery temperature
 * @param design_voltage Design voltage for smart battery
 * @param design_capacity Design capacity for smart battery
 * @param full_capacity Full capacity for smart battery
 * @param life_time Smart battery life time
 * @param available_battery Available battery for smart battery
 * @param remaining_battery Remaining battery energy: (0%: 0, 100%: 100)
 * @param cycle_count Cycle count for smart battery
 * @param voltage_single Battery voltage of cells, in millivolts (1 = 1 millivolt). Cells above the valid cell count for this battery should have the UINT16_MAX value.
 * @param voltage Current voltage for smart battery
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_smart_battery_status_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       uint16_t id, uint16_t production_date , uint16_t temperature, uint16_t design_voltage, uint16_t design_capacity, uint16_t full_capacity, uint8_t life_time, uint16_t available_battery, uint16_t remaining_battery, uint16_t cycle_count, const uint16_t *voltage_single, uint16_t voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN];
	_mav_put_uint16_t(buf, 0, id);
	_mav_put_uint16_t(buf, 2, production_date );
	_mav_put_uint16_t(buf, 4, temperature);
	_mav_put_uint16_t(buf, 6, design_voltage);
	_mav_put_uint16_t(buf, 8, design_capacity);
	_mav_put_uint16_t(buf, 10, full_capacity);
	_mav_put_uint16_t(buf, 12, available_battery);
	_mav_put_uint16_t(buf, 14, remaining_battery);
	_mav_put_uint16_t(buf, 16, cycle_count);
	_mav_put_uint16_t(buf, 26, voltage);
	_mav_put_uint8_t(buf, 28, life_time);
	_mav_put_uint16_t_array(buf, 18, voltage_single, 4);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#else
	mavlink_smart_battery_status_t packet;
	packet.id = id;
	packet.production_date  = production_date ;
	packet.temperature = temperature;
	packet.design_voltage = design_voltage;
	packet.design_capacity = design_capacity;
	packet.full_capacity = full_capacity;
	packet.available_battery = available_battery;
	packet.remaining_battery = remaining_battery;
	packet.cycle_count = cycle_count;
	packet.voltage = voltage;
	packet.life_time = life_time;
	mav_array_memcpy(packet.voltage_single, voltage_single, sizeof(uint16_t)*4);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_SMART_BATTERY_STATUS;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif
}

/**
 * @brief Pack a smart_battery_status message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param id  Battery ID
 * @param production_date  Battery production date
 * @param temperature Battery temperature
 * @param design_voltage Design voltage for smart battery
 * @param design_capacity Design capacity for smart battery
 * @param full_capacity Full capacity for smart battery
 * @param life_time Smart battery life time
 * @param available_battery Available battery for smart battery
 * @param remaining_battery Remaining battery energy: (0%: 0, 100%: 100)
 * @param cycle_count Cycle count for smart battery
 * @param voltage_single Battery voltage of cells, in millivolts (1 = 1 millivolt). Cells above the valid cell count for this battery should have the UINT16_MAX value.
 * @param voltage Current voltage for smart battery
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_smart_battery_status_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           uint16_t id,uint16_t production_date ,uint16_t temperature,uint16_t design_voltage,uint16_t design_capacity,uint16_t full_capacity,uint8_t life_time,uint16_t available_battery,uint16_t remaining_battery,uint16_t cycle_count,const uint16_t *voltage_single,uint16_t voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN];
	_mav_put_uint16_t(buf, 0, id);
	_mav_put_uint16_t(buf, 2, production_date );
	_mav_put_uint16_t(buf, 4, temperature);
	_mav_put_uint16_t(buf, 6, design_voltage);
	_mav_put_uint16_t(buf, 8, design_capacity);
	_mav_put_uint16_t(buf, 10, full_capacity);
	_mav_put_uint16_t(buf, 12, available_battery);
	_mav_put_uint16_t(buf, 14, remaining_battery);
	_mav_put_uint16_t(buf, 16, cycle_count);
	_mav_put_uint16_t(buf, 26, voltage);
	_mav_put_uint8_t(buf, 28, life_time);
	_mav_put_uint16_t_array(buf, 18, voltage_single, 4);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#else
	mavlink_smart_battery_status_t packet;
	packet.id = id;
	packet.production_date  = production_date ;
	packet.temperature = temperature;
	packet.design_voltage = design_voltage;
	packet.design_capacity = design_capacity;
	packet.full_capacity = full_capacity;
	packet.available_battery = available_battery;
	packet.remaining_battery = remaining_battery;
	packet.cycle_count = cycle_count;
	packet.voltage = voltage;
	packet.life_time = life_time;
	mav_array_memcpy(packet.voltage_single, voltage_single, sizeof(uint16_t)*4);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_SMART_BATTERY_STATUS;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif
}

/**
 * @brief Encode a smart_battery_status struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param smart_battery_status C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_smart_battery_status_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_smart_battery_status_t* smart_battery_status)
{
	return mavlink_msg_smart_battery_status_pack(system_id, component_id, msg, smart_battery_status->id, smart_battery_status->production_date , smart_battery_status->temperature, smart_battery_status->design_voltage, smart_battery_status->design_capacity, smart_battery_status->full_capacity, smart_battery_status->life_time, smart_battery_status->available_battery, smart_battery_status->remaining_battery, smart_battery_status->cycle_count, smart_battery_status->voltage_single, smart_battery_status->voltage);
}

/**
 * @brief Encode a smart_battery_status struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param smart_battery_status C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_smart_battery_status_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_smart_battery_status_t* smart_battery_status)
{
	return mavlink_msg_smart_battery_status_pack_chan(system_id, component_id, chan, msg, smart_battery_status->id, smart_battery_status->production_date , smart_battery_status->temperature, smart_battery_status->design_voltage, smart_battery_status->design_capacity, smart_battery_status->full_capacity, smart_battery_status->life_time, smart_battery_status->available_battery, smart_battery_status->remaining_battery, smart_battery_status->cycle_count, smart_battery_status->voltage_single, smart_battery_status->voltage);
}

/**
 * @brief Send a smart_battery_status message
 * @param chan MAVLink channel to send the message
 *
 * @param id  Battery ID
 * @param production_date  Battery production date
 * @param temperature Battery temperature
 * @param design_voltage Design voltage for smart battery
 * @param design_capacity Design capacity for smart battery
 * @param full_capacity Full capacity for smart battery
 * @param life_time Smart battery life time
 * @param available_battery Available battery for smart battery
 * @param remaining_battery Remaining battery energy: (0%: 0, 100%: 100)
 * @param cycle_count Cycle count for smart battery
 * @param voltage_single Battery voltage of cells, in millivolts (1 = 1 millivolt). Cells above the valid cell count for this battery should have the UINT16_MAX value.
 * @param voltage Current voltage for smart battery
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_smart_battery_status_send(mavlink_channel_t chan, uint16_t id, uint16_t production_date , uint16_t temperature, uint16_t design_voltage, uint16_t design_capacity, uint16_t full_capacity, uint8_t life_time, uint16_t available_battery, uint16_t remaining_battery, uint16_t cycle_count, const uint16_t *voltage_single, uint16_t voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN];
	_mav_put_uint16_t(buf, 0, id);
	_mav_put_uint16_t(buf, 2, production_date );
	_mav_put_uint16_t(buf, 4, temperature);
	_mav_put_uint16_t(buf, 6, design_voltage);
	_mav_put_uint16_t(buf, 8, design_capacity);
	_mav_put_uint16_t(buf, 10, full_capacity);
	_mav_put_uint16_t(buf, 12, available_battery);
	_mav_put_uint16_t(buf, 14, remaining_battery);
	_mav_put_uint16_t(buf, 16, cycle_count);
	_mav_put_uint16_t(buf, 26, voltage);
	_mav_put_uint8_t(buf, 28, life_time);
	_mav_put_uint16_t_array(buf, 18, voltage_single, 4);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS, buf, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS, buf, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif
#else
	mavlink_smart_battery_status_t packet;
	packet.id = id;
	packet.production_date  = production_date ;
	packet.temperature = temperature;
	packet.design_voltage = design_voltage;
	packet.design_capacity = design_capacity;
	packet.full_capacity = full_capacity;
	packet.available_battery = available_battery;
	packet.remaining_battery = remaining_battery;
	packet.cycle_count = cycle_count;
	packet.voltage = voltage;
	packet.life_time = life_time;
	mav_array_memcpy(packet.voltage_single, voltage_single, sizeof(uint16_t)*4);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS, (const char *)&packet, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS, (const char *)&packet, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_smart_battery_status_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint16_t id, uint16_t production_date , uint16_t temperature, uint16_t design_voltage, uint16_t design_capacity, uint16_t full_capacity, uint8_t life_time, uint16_t available_battery, uint16_t remaining_battery, uint16_t cycle_count, const uint16_t *voltage_single, uint16_t voltage)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_uint16_t(buf, 0, id);
	_mav_put_uint16_t(buf, 2, production_date );
	_mav_put_uint16_t(buf, 4, temperature);
	_mav_put_uint16_t(buf, 6, design_voltage);
	_mav_put_uint16_t(buf, 8, design_capacity);
	_mav_put_uint16_t(buf, 10, full_capacity);
	_mav_put_uint16_t(buf, 12, available_battery);
	_mav_put_uint16_t(buf, 14, remaining_battery);
	_mav_put_uint16_t(buf, 16, cycle_count);
	_mav_put_uint16_t(buf, 26, voltage);
	_mav_put_uint8_t(buf, 28, life_time);
	_mav_put_uint16_t_array(buf, 18, voltage_single, 4);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS, buf, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS, buf, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif
#else
	mavlink_smart_battery_status_t *packet = (mavlink_smart_battery_status_t *)msgbuf;
	packet->id = id;
	packet->production_date  = production_date ;
	packet->temperature = temperature;
	packet->design_voltage = design_voltage;
	packet->design_capacity = design_capacity;
	packet->full_capacity = full_capacity;
	packet->available_battery = available_battery;
	packet->remaining_battery = remaining_battery;
	packet->cycle_count = cycle_count;
	packet->voltage = voltage;
	packet->life_time = life_time;
	mav_array_memcpy(packet->voltage_single, voltage_single, sizeof(uint16_t)*4);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS, (const char *)packet, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_SMART_BATTERY_STATUS, (const char *)packet, MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE SMART_BATTERY_STATUS UNPACKING


/**
 * @brief Get field id from smart_battery_status message
 *
 * @return  Battery ID
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_id(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  0);
}

/**
 * @brief Get field production_date  from smart_battery_status message
 *
 * @return Battery production date
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_production_date (const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  2);
}

/**
 * @brief Get field temperature from smart_battery_status message
 *
 * @return Battery temperature
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_temperature(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  4);
}

/**
 * @brief Get field design_voltage from smart_battery_status message
 *
 * @return Design voltage for smart battery
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_design_voltage(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  6);
}

/**
 * @brief Get field design_capacity from smart_battery_status message
 *
 * @return Design capacity for smart battery
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_design_capacity(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  8);
}

/**
 * @brief Get field full_capacity from smart_battery_status message
 *
 * @return Full capacity for smart battery
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_full_capacity(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  10);
}

/**
 * @brief Get field life_time from smart_battery_status message
 *
 * @return Smart battery life time
 */
static inline uint8_t mavlink_msg_smart_battery_status_get_life_time(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  28);
}

/**
 * @brief Get field available_battery from smart_battery_status message
 *
 * @return Available battery for smart battery
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_available_battery(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  12);
}

/**
 * @brief Get field remaining_battery from smart_battery_status message
 *
 * @return Remaining battery energy: (0%: 0, 100%: 100)
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_remaining_battery(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  14);
}

/**
 * @brief Get field cycle_count from smart_battery_status message
 *
 * @return Cycle count for smart battery
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_cycle_count(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  16);
}

/**
 * @brief Get field voltage_single from smart_battery_status message
 *
 * @return Battery voltage of cells, in millivolts (1 = 1 millivolt). Cells above the valid cell count for this battery should have the UINT16_MAX value.
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_voltage_single(const mavlink_message_t* msg, uint16_t *voltage_single)
{
	return _MAV_RETURN_uint16_t_array(msg, voltage_single, 4,  18);
}

/**
 * @brief Get field voltage from smart_battery_status message
 *
 * @return Current voltage for smart battery
 */
static inline uint16_t mavlink_msg_smart_battery_status_get_voltage(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint16_t(msg,  26);
}

/**
 * @brief Decode a smart_battery_status message into a struct
 *
 * @param msg The message to decode
 * @param smart_battery_status C-struct to decode the message contents into
 */
static inline void mavlink_msg_smart_battery_status_decode(const mavlink_message_t* msg, mavlink_smart_battery_status_t* smart_battery_status)
{
#if MAVLINK_NEED_BYTE_SWAP
	smart_battery_status->id = mavlink_msg_smart_battery_status_get_id(msg);
	smart_battery_status->production_date  = mavlink_msg_smart_battery_status_get_production_date (msg);
	smart_battery_status->temperature = mavlink_msg_smart_battery_status_get_temperature(msg);
	smart_battery_status->design_voltage = mavlink_msg_smart_battery_status_get_design_voltage(msg);
	smart_battery_status->design_capacity = mavlink_msg_smart_battery_status_get_design_capacity(msg);
	smart_battery_status->full_capacity = mavlink_msg_smart_battery_status_get_full_capacity(msg);
	smart_battery_status->available_battery = mavlink_msg_smart_battery_status_get_available_battery(msg);
	smart_battery_status->remaining_battery = mavlink_msg_smart_battery_status_get_remaining_battery(msg);
	smart_battery_status->cycle_count = mavlink_msg_smart_battery_status_get_cycle_count(msg);
	mavlink_msg_smart_battery_status_get_voltage_single(msg, smart_battery_status->voltage_single);
	smart_battery_status->voltage = mavlink_msg_smart_battery_status_get_voltage(msg);
	smart_battery_status->life_time = mavlink_msg_smart_battery_status_get_life_time(msg);
#else
	memcpy(smart_battery_status, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_SMART_BATTERY_STATUS_LEN);
#endif
}
