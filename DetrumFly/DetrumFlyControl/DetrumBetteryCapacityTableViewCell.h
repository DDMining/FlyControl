//
//  DetrumBetteryCapacityTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/29.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumBetteryCapacityTableViewCell : DetrumBaseTableViewCell
@property (strong, nonatomic) NSArray *batterys;
@property (weak, nonatomic) IBOutlet UIView *firstBatteryView;
@property (weak, nonatomic) IBOutlet UILabel *firstBatteryLb;

@property (weak, nonatomic) IBOutlet UIView *secondBatteryView;
@property (weak, nonatomic) IBOutlet UILabel *secondBatteryLb;

@property (weak, nonatomic) IBOutlet UIView *threeBatteryView;
@property (weak, nonatomic) IBOutlet UILabel *threeBatteryLb;

@property (weak, nonatomic) IBOutlet UIView *fourBatteryView;
@property (weak, nonatomic) IBOutlet UILabel *fourBatteryLb;

/** 电池寿命 */
@property (weak, nonatomic) IBOutlet UILabel *batteryLife;
/** 循环次数 */
@property (weak, nonatomic) IBOutlet UILabel *loopsNum;
/** 飞行时间 */
@property (weak, nonatomic) IBOutlet UILabel *flyTime;
- (void)initCapacityView;
@end
