//
//  DtFlyBaseViewController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//


#import "Costants.h"
#import <UIKit/UIKit.h>
#import "UserDataMarco.h"
#import "RectExtends.h"
#import "SVProgressHUD.h"
#import <ReactiveCocoa.h>
#import "DetrumFlyMarco.h"
//#import <Toast/UIView+Toast.h>
#import "UIView+QuicklyChangeFrame.h"
#import "InterfaceNetManager.h"

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

typedef void (^backActionBlock)();

@interface DetrumBaseViewController : UIViewController
@property (nonatomic, strong) UIButton *leftNavBtn;
@property (nonatomic, strong) UIButton *rightNavBtn;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, copy) backActionBlock backBlock;

/**
 * 自定义Nav左右按钮(左以图片的形式，右边文字可设置文字大小和宽度)
 * @parama 右边按钮的title
 * @parama 右边按钮的SEL
 * @parama 左边按钮的SEL
 * @parama 左边按钮的图片
 */
- (void)showNavBarWithRightTitle:(NSString *)rightTitle
                  andRightAction:(SEL)raction
                      andLeftImg:(NSString *)imageName
                   andLeftAction:(SEL)laction
                     andFontSize:(CGFloat)size
                   andRightWidth:(CGFloat)width;

/**
 * 自定义Nav左右按钮(左以图片的形式，右边可图片可文字)
 * @parama 右边按钮的图片或者title，取其一
 * @parama 右边按钮的SEL
 * @parama 左边按钮的SEL
 * @parama 左边按钮的图片
 */
- (void)showNavBarLeftImgAndRightTitle:(NSString*)title
                          andRightImge:(UIImage *)rImg
                        andRightAction:(SEL)raction
                         andLeftAction:(SEL)laction
                           andLeftImge:(UIImage *)img;
/**
 *  @brief  自定义Nav左右按钮(左以图片的形式，右边可图片可文字，且可以定义选中状态的图片)
 *
 *  @param title      右边控件的文字
 *  @param rImg       右边控件的图片
 *  @param selectImg  右边控件的选中图片
 *  @param raction    右边控件的点击事件
 *  @param laction    左边控件的点击事件
 *  @param img        左边控件的图片
 *  @param selectLimg 左边控件的选中图片
 */
- (void)showNavBarLeftImgAndRightTitle:(NSString*)title
                          andRightImge:(UIImage *)rImg
                     andRightSeleteImg:(UIImage *)selectImg
                        andRightAction:(SEL)raction
                         andLeftAction:(SEL)laction
                           andLeftImge:(UIImage *)img
                      andLeftSeleteImg:(UIImage *)selectLimg ;
/**
 * 自定义Nav左右按钮(左右按钮可加Badge)
 * @parama 左右图标的图片
 * @parama 左右按钮的SEL
 * @parama 左右显示badge的数量
 * @parama 设置NavagationBar的颜色
 */
- (void)showNavBarWithRightImage:(UIImage *)rimage
                    LeftBtnImage:(UIImage *)limage
               andRightBtnAction:(SEL)raction
                andLeftBtnAction:(SEL)laction
                   andRightBadge:(NSInteger)rnum
                     andLeftBage:(NSInteger)lnum
                           title:(NSString *)title
                        barColor:(UIColor *)color;

/**
 * 自定义Nav左右按钮(左以图片的形式，右边文字,或者右边没有按钮显示)
 * @parama 右边按钮的title
 * @parama 右边按钮的SEL
 * @parama 左边按钮的SEL
 * @parama 左边按钮的图片
 */
- (void)showNavBarWithRightTitle:(NSString *)rightTitle
                  andRightAction:(SEL)raction
                   andLeftAction:(SEL)laction
               andRightBtnIsShow:(BOOL) isShow;


- (void)showSvprogress:(NSString *)str;

- (void)dismiss ;

@end
