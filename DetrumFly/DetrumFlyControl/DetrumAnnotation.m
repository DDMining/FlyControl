//
//  DetrumAnnotation.m
//  DetrumFlyControl
//
//  Created by xcq on 16/1/31.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumAnnotation.h"

@implementation DetrumAnnotation

- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coor {
    if (self = [super init]) {
        self.coordinate = coor;
        self.speed = 0.f;
        self.height = 0.f;
    }
    return self;
}

- (void)awakeFromNib {
    if (self.height ) {
        
    }
}

@end
