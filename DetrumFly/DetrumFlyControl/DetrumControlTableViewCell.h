//
//  DetrumControlTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^calibrationAction)();

typedef  NS_ENUM(NSInteger , DetrumControlType) {
    DetrumControlTypeHaveBtn,
    DetrumControlTypeHaveIndicator,
    DetrumControlTypeCustom,
};

@interface DetrumControlTableViewCell : UITableViewCell
@property (assign, nonatomic) DetrumControlType type;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *parameterLb;
@property (weak, nonatomic) IBOutlet UIButton *calibrationBtn;
@property (weak, nonatomic) IBOutlet UIImageView *indicatorImg;
@property (copy, nonatomic) calibrationAction action;
- (void)setControlType:(DetrumControlType)type;
@end
