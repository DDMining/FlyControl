//
//  NSTimer+FastCreate.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (FastCreate)
+ (nonnull NSTimer *)timerWithTimeInterval:(NSInteger)timeInterval target:(nonnull id)target action:(nonnull SEL)action repeat:(BOOL)isReqeart;

- (void)startTimer;

- (void)destructionTimer;

- (void)pause;

- (void)delayWithTime:(NSTimeInterval)timer ;

- (void)resume;
@end
