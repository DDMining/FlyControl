//
//  DetrumControlDeviceView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/20.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^exitAction)();
typedef void(^cellSeleteAction)(NSString *seleteTypeName , NSInteger index);

@interface DetrumControlDeviceView : UIView <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIButton *closeBtn;
@property (strong, nonatomic) UILabel *titleLb;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) NSMutableArray *parameters;
@property (copy, nonatomic) exitAction action;
@property (copy, nonatomic) cellSeleteAction seleteAction;
@end
