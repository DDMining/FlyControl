//
//  HomePageViewController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseViewController.h"

@interface HomePageViewController : DetrumBaseViewController <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *checkModelView;
@property (weak, nonatomic) IBOutlet UIButton *userHaveDeviceBtn;
@property (weak, nonatomic) IBOutlet UIButton *moreFlyTypeBtn;
/** 无人机名称 */
@property (weak, nonatomic) IBOutlet UILabel *UAVName;
/** 无人机展示滚动图 */
@property (weak, nonatomic) IBOutlet UIScrollView *UAVScrollView;
/** 无人机滚动图Page Control*/
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
/** 无人机介绍 */
@property (weak, nonatomic) IBOutlet UIView *UAVDetailView;
@property (strong, nonatomic) UILabel *scrollLabel;

@property (weak, nonatomic) IBOutlet UIButton *joinBtn;
@end
