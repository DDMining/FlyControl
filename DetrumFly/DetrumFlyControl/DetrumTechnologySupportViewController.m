//
//  DetrumTechnologySupportViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumTechnologySupportViewController.h"

@interface DetrumTechnologySupportViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *depotRepairImg;
@property (weak, nonatomic) IBOutlet UIImageView *contactServiceImg;
@property (weak, nonatomic) IBOutlet UIImageView *emailImg;

@end
@interface DetrumTechnologySupportViewController ()

@end

@implementation DetrumTechnologySupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"技术支持";
    [self showNavBarWithRightTitle:nil
                    andRightAction:nil
                     andLeftAction:nil
                 andRightBtnIsShow:NO];
    [self initUI];
}

- (void)initUI {
    _depotRepairImg.userInteractionEnabled = YES;
    _contactServiceImg.userInteractionEnabled = YES;
    _emailImg.userInteractionEnabled = YES;
    [_depotRepairImg addGestureRecognizer:[[UIGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(depotRepair:)]];
    [_contactServiceImg addGestureRecognizer:[[UIGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(contactService:)]];
    [_emailImg addGestureRecognizer:[[UIGestureRecognizer alloc] initWithTarget:self
                                                                         action:@selector(sendEmailRepair:)]];
}

- (void)depotRepair:(UIGestureRecognizer *)gesture {
    
}

- (void)contactService:(UIGestureRecognizer *)gesture {
    
}

- (void)sendEmailRepair:(UIGestureRecognizer *)gesture {
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
