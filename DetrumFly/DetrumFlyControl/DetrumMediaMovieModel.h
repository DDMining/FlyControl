//
//  DetrumMediaMovieModel.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Realm/Realm.h>
#import "DetrumMediaImgModel.h"

@interface DetrumMediaMovieModel : RLMObject
/** 主键*/
@property  NSInteger id;
@property  NSString *mediaUrl;
@property  NSString *dateStr;
@property  NSDate *date;
@property  NSString *currenLocation;
@property  NSInteger duration;
@property  NSData *thumbnail;
@property  NSInteger index;
@property  NSInteger type;
@property  NSInteger clickType;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<DetrumMediaMovieModel>
RLM_ARRAY_TYPE(DetrumMediaMovieModel)
