//
//  DetrumCourseReversalView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "RectExtends.h"
#import "DetrumCourseReversalCell.h"
#import "DetrumCourseReversalView.h"
#import "UIView+QuicklyChangeFrame.h"
#import "DetrumSlideAndInputTableViewCell.h"

#define kSlideAndInputIndentifier @"sliderAndInput"
#define kImgIndentifer @"courseTip"

@implementation DetrumCourseReversalView

- (void)awakeFromNib {
    [self viewDidLoad];
}

- (void)viewDidLoad {
//    [self setAnimation];
    if (_tableView.delegate == nil) {
        _tableView.tableFooterView = [UIView new];
        _tableView.delegate = self;
        _tableView.dataSource= self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumSlideAndInputTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kSlideAndInputIndentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumCourseReversalCell"
                                               bundle:nil]
         forCellReuseIdentifier:kImgIndentifer];
    }
    if (titleArr == nil) {
        titleArr = [NSMutableArray arrayWithArray:@[@"上升高度",@"悬停高度",@"悬停时间"]];
    }
}

- (IBAction)backEvent:(id)sender {
    [self backAnimaiton];
}

#pragma mark - tableView delegate & data
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 3) {
        DetrumSlideAndInputTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSlideAndInputIndentifier forIndexPath:indexPath];
        cell.name.text = [titleArr objectAtIndex:indexPath.row];
        return cell;
    } else {
        DetrumCourseReversalCell *cell = [tableView dequeueReusableCellWithIdentifier:kImgIndentifer forIndexPath:indexPath];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 3) {
        return 36;
    } else {
        return 85;
    }
}

@end
