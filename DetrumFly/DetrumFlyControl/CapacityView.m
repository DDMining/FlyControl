//
//  CapacityView.m
//  DetrumDrawTest
//
//  Created by xcq on 16/3/9.
//  Copyright © 2016年 Fine. All rights reserved.
//

#import "CapacityView.h"
#define PHOTO_HEIGHT 150

@implementation CapacityView

- (instancetype)initWithFrame:(CGRect)frame
                       percen:(CGFloat)percen
                    fillColor:(UIColor *)fillColor
                       radiu:(CGFloat)radiu {
    if (self = [super initWithFrame:frame]) {
        _percent = percen;
        _fillColor = fillColor;
        _radiu = radiu;
        _isHorizontal = NO;
        [self initLayer];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
                       percen:(CGFloat)percen
                    fillColor:(UIColor *)fillColor
                        radiu:(CGFloat)radiu
                 isHorizontal:(BOOL)bol {
    if (self = [super initWithFrame:frame]) {
        _percent = percen;
        _fillColor = fillColor;
        _radiu = radiu;
        _isHorizontal = bol;
        [self initLayer];
    }
    return self;

}

- (void)initLayer {
    layer = [CALayer layer];
    layer.backgroundColor = _fillColor.CGColor;
    [self.layer addSublayer:layer];
    [self setNeedsLayout];
}

- (void)setPercent:(CGFloat)percent {
    _percent = percent;
    if (_percent < 0) {
        _percent = 0.f;
    } else if (_percent > 1.f){
        _percent = 1.f;
    }
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (_isHorizontal) {
        layer.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame) * _percent , CGRectGetHeight(self.frame));
        return;
    }
    layer.frame = CGRectMake(0, CGRectGetHeight(self.frame) - (CGRectGetHeight(self.frame) * _percent), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame) * _percent);
}
 @end
