//
//  DetrumCollectionViewCell.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "UserDataMarco.h"
#import "DetrumCollectionViewCell.h"

@implementation DetrumCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    addObserver(@selector(refreshSelectStatus), kMediaNotification);
}

- (void)refreshSelectStatus {
    [_photoView imgClickFalse];
}

- (void)setDataBaseObj:(RLMObject *)dataBaseObj {
    if (dataBaseObj == nil) {
        return;
    }
    _dataBaseObj = dataBaseObj;
    if (_photoView.image == nil) {
        [_photoView initDataBaseModel:[[_dataBaseObj valueForKey:@"type"] integerValue]
                            clickType:[[_dataBaseObj valueForKey:@"clickType"] integerValue]
                                mdoel:_dataBaseObj];
    } else {
        [_photoView resetType:[[_dataBaseObj valueForKey:@"type"] integerValue] clickType:[[_dataBaseObj valueForKey:@"clickType"] integerValue] model:_dataBaseObj];
    }
}

- (void)setModel:(DetrumMediaModel *)model {
    if (model == nil) {
        return;
    }
    _model = model;
    //TODO:赋值
    if (_photoView.image == nil) {
        [self layoutIfNeeded];
        [_photoView initWithType:model.type
                       clickType:model.clickType
                           model:_model];
    } else {
        [_photoView resetType:model.type
                    clickType:model.clickType
                        model:model];
    }
}

- (void)setImgSelected:(BOOL)selected {
    _model.isSeleted = selected;
}

@end
