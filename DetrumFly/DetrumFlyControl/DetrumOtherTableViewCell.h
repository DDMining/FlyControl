//
//  DetrumOtherTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/8.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"
@interface DetrumOtherTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *midName;
@property (weak, nonatomic) IBOutlet UILabel *lastName;

@end
