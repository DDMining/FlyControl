//
//  DetrumBaseView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumBaseView : UIView
@property (weak, nonatomic) UIView *animationViw;
@property (strong, nonatomic) CATransition *animation;
- (void)setAnimation;
- (void)backAnimaiton;
@end
