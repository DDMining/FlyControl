//
//  UserAlerts.h
//  OpenOOIM
//
//  Created on xcq 14-9-25.
//  Copyright (c) 2014年 All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UserAlerts : NSObject

+ (UserAlerts *)instance;

- (void)showErrorMsg:(NSString *)title message:(NSString *)msg delegate:(id<UIAlertViewDelegate>)delegate andTag:(NSInteger)tag;
- (void)showErrorMsg:(NSString *)title message:(NSString *)msg delegate:(id<UIAlertViewDelegate>)delegate;
- (void)showErrorMsg:(NSString *)title message:(NSString *)msg;
- (void)showErrorMsg:(NSString *)msg;
- (void)showErrorMsg:(NSString *)msg tag:(NSInteger)tag target:(id)target;
- (void)confirmMsg:(NSString *)msg tag:(NSInteger)tag target:(id)target;

@end
