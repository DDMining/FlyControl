//
//  DetrumGestureSensitivityView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "UIView+QuicklyChangeFrame.h"
#import "DetrumGestureSensitivityView.h"
#import "DetrumClassificationViewCell.h"
#import "DetrumTwoInputViewCell.h"
#import "DetrumMutilpInputViewTbCell.h"
#import "DetrumSliderTableViewCell.h"
#import "DetrumInputTableViewCell.h"

#define KClassIfiCellIdentifier @"classInfo"
#define kTwoInutIdentifier @"twoInput"
#define kMutilpIdentifier @"mutilpInput"
#define kSliderIdentifier @"slider"
#define kInputIdentifier @"input"

@implementation DetrumGestureSensitivityView
- (void)awakeFromNib {
    [self viewDidLoad];
}

- (void)viewDidLoad {
    if (_tableView.delegate == nil) {
        _tableView.tableFooterView = [UIView new];
        _tableView.delegate = self;
        _tableView.dataSource= self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumClassificationViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:KClassIfiCellIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumTwoInputViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kTwoInutIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumMutilpInputViewTbCell"
                                               bundle:nil]
         forCellReuseIdentifier:kMutilpIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumSliderTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kSliderIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumInputTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kInputIdentifier];
        
    }
    if (titleArr == nil) {
        titleArr = [NSMutableArray arrayWithArray:@[@"姿态",@"滚转",@"感度",@""]];
    }
}

- (IBAction)backEvent:(id)sender {
    [self backAnimaiton];
}

#pragma mark - tableView Delegate & dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:{
            DetrumClassificationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KClassIfiCellIdentifier forIndexPath:indexPath];
            cell.name.text = @"姿态";
            return cell;
        }
            break;
        case 1:{
            DetrumTwoInputViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTwoInutIdentifier forIndexPath:indexPath];
            cell.firstInpuViewName.text = @"滚动";
            cell.secondInputViewName.text = @"俯仰角限制";
            return cell;
        }
            break;
            
        case 2:{
            DetrumClassificationViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KClassIfiCellIdentifier forIndexPath:indexPath];
            cell.name.text = @"感度";
            return cell;
        }
            break;
        case 3:{
            DetrumMutilpInputViewTbCell *cell = [tableView dequeueReusableCellWithIdentifier:kMutilpIdentifier forIndexPath:indexPath];
            
            return cell;
        }
            break;
        case 4:{
            DetrumSliderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSliderIdentifier forIndexPath:indexPath];
            cell.name.text = @"刹车";
            return cell;
        }
            break;
        case 5:{
            DetrumInputTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kInputIdentifier forIndexPath:indexPath];
            cell.name.text = @"垂直";
            return cell;
        }
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 3) {
        return 109;
    } else {
        return 36;
    }
}

@end
