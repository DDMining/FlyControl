//
//  DetrumCollectionReusableHeadView.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/19.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <YYKit.h>
#import "DetrumCollectionReusableHeadView.h"

@implementation DetrumCollectionReusableHeadView

- (void)awakeFromNib {
    // Initialization code
}

- (void)setDateText:(NSString *)text {
    NSDateFormatter *fommatter = [[NSDateFormatter alloc] init];
    [fommatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *goalDate = [fommatter dateFromString:text];
    if (goalDate.isToday) {
        _date.text = @"今天";
    } else if (goalDate.isYesterday) {
        _date.text = @"昨天";
    } else {
        _date.text = text;
    }
}   

@end
