//
//  DetrumCoordinateCorrect.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/7/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetrumCoordinateCorrect : NSObject
// 地图经纬度偏移量纠正
- (NSMutableDictionary *)WGS84ToGCJ02:(double)lat lon:(double)lon;

@end
