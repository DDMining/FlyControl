//
//  DetrumSetContentView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/24.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumFlyControlManager.h"
@interface DetrumSetContentView : UIView <UITableViewDataSource,UITableViewDelegate> {

}
@property (weak, nonatomic) DetrumBaseView *popView;
@property (strong, nonatomic) DetrumFlyControlManager *manager;
@property (weak, nonatomic) IBOutlet UILabel *setTypeName;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *ContentView;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *arrowLeadValue;
@property (assign, nonatomic) CGFloat BtnWidth;
@property (assign, nonatomic) DetrumSetType currenType;
@property (strong, nonatomic) NSArray *values;
- (void)setCurrenType:(DetrumSetType)currenType;
@end
