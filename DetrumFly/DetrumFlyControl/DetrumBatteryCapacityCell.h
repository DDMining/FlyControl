//
//  DetrumBatteryCapacityCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/7.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"


@interface DetrumBatteryCapacityCell : DetrumBaseTableViewCell
@property (strong, nonatomic) NSArray *batterys;
@property (assign, nonatomic) CGFloat batteryValue;
@property (weak, nonatomic) IBOutlet UIView *batteryElectricity;

@property (weak, nonatomic) IBOutlet UIView *firstBattryView;
@property (weak, nonatomic) IBOutlet UIView *twoBatteryView;
@property (weak, nonatomic) IBOutlet UIView *threeBatteryView;
@property (weak, nonatomic) IBOutlet UIView *fourBatteryView;


@property (weak, nonatomic) IBOutlet UILabel *firstBatteryValueLb;
@property (weak, nonatomic) IBOutlet UILabel *twoBatteryValueLb;
@property (weak, nonatomic) IBOutlet UILabel *threeBatteryValueLb;
@property (weak, nonatomic) IBOutlet UILabel *fourBatteryValueLb;

/** 充放电次数 */
@property (weak, nonatomic) IBOutlet UILabel *rechargeNum;
/** 电池寿命 */
@property (weak, nonatomic) IBOutlet UILabel *battryLife;
/** 电量 */
@property (weak, nonatomic) IBOutlet UILabel *quantityOfElectricity;

@end
