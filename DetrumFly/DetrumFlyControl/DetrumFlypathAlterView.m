//
//  DetrumFlypathAlterView.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/8/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumFlypathAlterView.h"

@implementation DetrumFlypathAlterView

+(instancetype)shareInstance{
    DetrumFlypathAlterView *alterView = [[[NSBundle mainBundle]loadNibNamed:@"DetrumFlypathAlterView" owner:self options:nil] firstObject];
    alterView.flypathCloseBtn.layer.borderColor = [UIColor grayColor].CGColor;
    alterView.flypathCloseBtn.layer.borderWidth = 1;
    alterView.flypathCloseBtn.layer.cornerRadius = 15;
    
    return alterView;
}


-(void)show:(UIView *)view{
    
    self.center = view.center;
    self.transform = CGAffineTransformMakeScale(0.2, 0.2);
    [view addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
    
}


- (IBAction)close:(id)sender {
    [UIImageView animateWithDuration:0.3 animations:^{
        self.transform = CGAffineTransformScale(self.transform, 0.2, 0.2);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}




@end
