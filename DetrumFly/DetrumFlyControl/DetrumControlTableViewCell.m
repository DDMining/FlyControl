//
//  DetrumControlTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumControlTableViewCell.h"

@implementation DetrumControlTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
    }
}

- (IBAction)calibrationAction:(id)sender {
    _action();
}

- (void)setControlType:(DetrumControlType)type {
    _type = type;
    if (_type == DetrumControlTypeCustom) {
        _calibrationBtn.hidden = YES;
        _indicatorImg.hidden = YES;
        _parameterLb.hidden = NO;
    } else if (_type == DetrumControlTypeHaveBtn) {
        _calibrationBtn.hidden = NO;
        _parameterLb.hidden = NO;
        _indicatorImg.hidden = YES;
    } else {
        _calibrationBtn.hidden = YES;
        _parameterLb.hidden = YES;
        _indicatorImg.hidden = NO;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
