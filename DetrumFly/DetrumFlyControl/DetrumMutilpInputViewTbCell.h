//
//  DetrumMutilpInputViewTbCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumMutilpInputViewTbCell : DetrumBaseTableViewCell
/** 俯仰角 */
@property (weak, nonatomic) IBOutlet UISlider *pitchTf;
/** 横滚 */
@property (weak, nonatomic) IBOutlet UISlider *rollTf;
/** 航向*/
@property (weak, nonatomic) IBOutlet UISlider *headingTf;

@end
