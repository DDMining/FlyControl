//
//  NSDictionary+GetData.h
//  JxCarios
//
//  Created by xcq on 15/2/2.
//  Copyright (c) 2015年 xiongchuanqi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (GetData)

- (id)customObjectForkey:(id)key;
@end
