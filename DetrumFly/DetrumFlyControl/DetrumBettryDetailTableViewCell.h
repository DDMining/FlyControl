//
//  DetrumBettryDetailTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/29.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumBettryDetailTableViewCell : DetrumBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *voltageLb;
@property (weak, nonatomic) IBOutlet UILabel *currenBatteryLb;
@property (weak, nonatomic) IBOutlet UILabel *batteryCapacityLb;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLb;

@end
