//
//  DetrumFlypathAlterView.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/8/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumFlypathAlterView : UIView

@property (weak, nonatomic) IBOutlet UILabel *flypathHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *flypathContentLabel;
@property (weak, nonatomic) IBOutlet UIButton *flypathCloseBtn;

+ (instancetype)shareInstance;
- (void)show:(UIView *)view;


@end
