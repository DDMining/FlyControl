//
//  NSString+Regex.h
//  大博智能
//
//  Created on 14-10-24.
//  Copyright (c) 2014年 All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Regex)

- (NSString *)trim;
- (NSComparisonResult)compareWithDate:(NSString *)date;

@end
