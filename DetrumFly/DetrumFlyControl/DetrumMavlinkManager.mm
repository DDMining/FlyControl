//
//  DetrumMavlinkManager.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/4/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumDataProcessing.h"
#import "DetrumFlyMarco.h"
#import <SVProgressHUD.h>
//#import <UIView+Toast.h>
#import "Interface.h"
#import "FileIO.h"
#import "Mavlib.hpp"
#import "DetrumMavlinkManager.h"

//#define kPort @"4000"
#define kPort @"14550"

#define kTimer 1

#define kTimeOut 5000

#define kForceSendOrder @"6d61766c696e6b207374617274202d64202f6465762f7474795332202d62203537363030202d7320537973537461747573202d722031303030"

static const Byte DFHead[1] = {0x14};
//DEST
static const Byte DFDest[1] = {0x6};
//sour
static const Byte DFSour[1] = {0xB};
//Type
static const Byte DFType[1] = {0x1};
//dataLength
static Byte DFLength[2] = {};

static DetrumMavlinkManager *manager = nil;
@interface DetrumMavlinkManager () <GCDAsyncSocketDelegate,GCDAsyncUdpSocketDelegate>{
    Mavlib mavlink;
    NSMutableArray *datas;
}

@end
@implementation DetrumMavlinkManager

+ (instancetype)shareInstance {
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        manager = [DetrumMavlinkManager new];
    });
    return manager;
}

- (BOOL)connetWifi {
    if (_msgs == nil) {
        _msgs = [NSMutableArray new];
    }
    if (!_dataSource) {
        _dataSource = [NSMutableArray new];
        _parameter = [NSMutableDictionary new];
    }
    BOOL bol;
    if (udpSocket == nil)
    {
//        ip = [[UIDevice new] getGatewayIPAddress];
//        port = kPort;
        mavlink = Mavlib();
        bol = [self setupTcpSocket];
    }
    _isConnect = bol;
    return bol;
}


- (BOOL)setupTcpSocket {
//    WIFI  APM_WIFI
//    密码12345678
//    TCP:192.168.0.99
//    端口：6789
    
    NSError *error;
    tcpSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    BOOL bol = [tcpSocket connectToHost:@"192.168.0.99" onPort:6789  error:&error];
    _isConnect = bol;
    if (datas == nil) {
        datas = [NSMutableArray new];
    }
    return bol;
}

- (BOOL)setupSocket
{
    udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    NSError *error = nil;
    
    if (![udpSocket bindToPort:0 error:&error])
    {
        DFLog(@"Error receiving: %@", error);
        return NO;
    }
    if (![udpSocket beginReceiving:&error])
    {
        DFLog(@"Error receiving: %@", error);
//        [self logError:FORMAT(@"Error receiving: %@", error)];
        return NO;
    }
    [SVProgressHUD showSuccessWithStatus:@"成功连接飞控"];
    return YES;
    DFLog(@"ready");
}

- (void)sendHeartBeat {
    char buff[MAVLINK_MAX_PACKET_LEN];
    int length = mavlink.sendHeartbeat(buff);
    [self sendData:buff length:length];
}

- (void)sendSetMode:(int)mode baseMode:(int)baseMode{
    char buff[MAVLINK_MAX_PACKET_LEN];
    int length = mavlink.sendSetMode(buff, 1, baseMode, mode);
//    int length = mavlink.guidedModeTakeoff(buff, height);
    [self sendData:buff length:length];
    
}

- (void)guidedModeTakeoffHeight:(float)height{
    char buff[MAVLINK_MAX_PACKET_LEN];
    int length = mavlink.guidedModeTakeoff(buff, height);
    [self sendData:buff length:length];
}

- (void)sendkForceSendOrder {
    NSData *data = [DetrumDataProcessing dataWithHexString:kForceSendOrder];
    [tcpSocket writeData:data withTimeout:kTimeOut tag:1];
}

- (void)sendParamer {
    //sendParametersRequest
    char buff[MAVLINK_MAX_PACKET_LEN];
    int length = mavlink.sendParametersRequest(buff,0);
    [self sendData:buff length:length];
}

- (void)sendData:(char [MAVLINK_MAX_PACKET_LEN])buff length:(int)length {
    if (!_isConnect) {
        DFLog(@"wifi未连接!!!");
    } else {
        //TCP
        NSData *data = [NSData dataWithBytes:buff length:length];
        [tcpSocket writeData:data withTimeout:kTimeOut tag:1];
    }
}

- (void)setUpTimer {
    [self initTimer];
}

- (void)initTimer {
    if (_timer == nil) {
        _second = kGetVerifcationSecond;
        _timer = [NSTimer timerWithTimeInterval:kTimer target:self selector:@selector(sendHeartBeat) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        [_timer fire];
    } else {
        [self destructionTimer];
    }
}


- (void)destructionTimer {
    [_timer invalidate]; _timer = nil;
    _second = kGetVerifcationSecond;
}

#pragma mark - 发送mavlink数据
- (void)sendQueryList {
    //数据提
    char buffer[MAVLINK_MAX_PACKET_LEN];
    int lenght = mavlink.sendParametersRequest(buffer,0);
    //封装的mavlink报数据
//    NSData *data = [self spliceDFHeaderOfMavlink:buffer andLength:lenght];
//    NSData *data = DetrumDataProcessing 
    [self sendData:buffer length:lenght];
//    [udpSocket sendData:data toHost:ip port:[port intValue] withTimeout:-1 tag:101];
}

- (void)sendParamterSet:(NSString *)str andValue:(CGFloat)value{
    char buffer[MAVLINK_MAX_PACKET_LEN];

    const char *color_char = [str cStringUsingEncoding:NSASCIIStringEncoding];
    char *buf = new char[strlen(color_char)+1];
    strcpy(buf, color_char);
    int lenght = mavlink.sendParameterSetting(buffer, 0, buf, value);
//    NSData *data = [self spliceDFHeaderOfMavlink:buffer andLength:lenght];
    [self sendData:buffer length:lenght];

}

//开始校准
- (void)startCalibration:(int)cal_type{
    char buffer[MAVLINK_MAX_PACKET_LEN];
    int length = mavlink.startCalibration(buffer, cal_type);
    [self sendData:buffer length:length];
}

// 结束校准
- (void)stopCalibration{
    char buffer[MAVLINK_MAX_PACKET_LEN];
    int length = mavlink.stopCalibration(buffer);
    [self sendData:buffer length:length];
}

- (void)sendParamRaw:(int)compID paramName:(NSString *)paramName value:(double)value valueType:(int) valueType{
    char buffer[MAVLINK_MAX_PACKET_LEN];
    const char *name = [paramName UTF8String];
    int length = mavlink.sendParamRaw(buffer, compID, name, value, valueType);
    [self sendData:buffer length:length];
}




//- (void)setSendSetMode:

//发送mavlink完整包
- (NSData *)spliceDFHeaderOfMavlink:(char [])mavlinkBuffer andLength:(NSUInteger)length {
    NSString *hexStr = [NSString stringWithFormat:@"%lX",(unsigned long)length];
    if (hexStr.length == 1) {
        hexStr = [@"000" stringByAppendingString:hexStr];
    } else if (hexStr.length == 2) {
        hexStr = [@"00" stringByAppendingString:hexStr];
    } else if (hexStr.length == 3) {
        hexStr = [@"0" stringByAppendingString:hexStr];
    }
    NSMutableData *mutableData = [DetrumDataProcessing stringToByte:hexStr];
    
    Byte *lengByte = (Byte *)[mutableData bytes];
    DFLength[0] = *(lengByte+0);
    DFLength[1] = *(lengByte+1);
    Byte headBuff[] = {DFHead[0],DFDest[0],DFSour[0],DFType[0],DFLength[0],DFLength[1],0x00,0x00};
    
    NSMutableData *headBuffData = [NSMutableData dataWithBytes:headBuff length:8];
    [headBuffData appendBytes:mavlinkBuffer length:length];
    
    unsigned short tep = csum((unsigned char *)[headBuffData bytes], headBuffData.length);
    NSString *tepStr = [NSString stringWithFormat:@"%X",tep];
    if (tepStr.length == 1) {
        tepStr = [@"000" stringByAppendingString:tepStr];
    } else if (tepStr.length == 2) {
        tepStr = [@"00" stringByAppendingString:tepStr];
    } else if (tepStr.length == 3) {
        tepStr = [@"0" stringByAppendingString:tepStr];
    }
    NSMutableData *tempData = [DetrumDataProcessing stringToByte:tepStr];
    
    Byte *tepByte = (Byte *)[tempData bytes];
    
    Byte headBuff2[] = {DFHead[0],DFDest[0],DFSour[0],DFType[0],DFLength[0],DFLength[1],*(tepByte+1),*(tepByte+0)};
    
    NSMutableData *data = [NSMutableData dataWithBytes:headBuff2 length:8];
    [data appendBytes:mavlinkBuffer length:length];
    return data;
}

- (id)resolveDFHeaderOfMavlink:(CGFloat)data {
   
//    [[UIApplication sharedApplication].keyWindow makeToast:[NSString stringWithFormat:@"心跳包数据:%f",data]];
    return nil;
}


#pragma mark - TCP代理方法

- (void)socket:(GCDAsyncSocket*)sock didConnectToHost:(NSString*)host port:(uint16_t)port {
    NSLog(@"socket连接成功");
    [sock readDataWithTimeout:kTimeOut tag:1];
}

- (void)socket:(GCDAsyncSocket*)sock didWriteDataWithTag:(long)tag {
    NSLog(@"message did write");
    [sock readDataWithTimeout:kTimeOut tag:tag];
}

//断开方法
- (void)socketDidDisconnect:(GCDAsyncSocket*)sock withError:(NSError*)err {
    NSLog(@"连接失败 %@", err);
    if (!_isConnect) {
        [self setupTcpSocket];
    }
}

- (void)socket:(GCDAsyncSocket*)sock didReadData:(NSData *)data withTag:(long)tag {
    int msgID;
//    NSLog(@"data:%@",data);
    NSUInteger len = [data length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [data bytes], len);
    
    for (int i = 0; i<len; i++) {
//        NSLog(@"%d",byteData[i]);
        //获取当前解析的消息类型
        msgID = mavlink.ReceivedBytes(byteData[i]);
        
        if (msgID == 253) {
            char buffer[100];
            mavlink.getStatusText(buffer);
            NSString *statusText = [NSString stringWithCString:buffer encoding:NSASCIIStringEncoding];
            NSLog(@"statusText:%@",statusText);
            
        }
        
        if (msgID == 22) {
            NSLog(@"msgID:%d",msgID);
            char valueType[10];
            double value;
            char buff[100];
            value = mavlink.getParaValueInfo(valueType, buff);
            NSLog(@"valueType:%s,value:%f,buff:%s",valueType,value,buff);
        }
        
        [self processData:msgID];
    }
    [sock readDataWithTimeout:kTimeOut tag:tag];
    free(byteData);
}

+ (NSString *)stringFromHexString:(NSString *)hexString { //
    
    char *myBuffer = (char *)malloc((int)[hexString length] / 2 + 1);
    bzero(myBuffer, [hexString length] / 2 + 1);
    for (int i = 0; i < [hexString length] - 1; i += 2) {
        unsigned int anInt;
        NSString * hexCharStr = [hexString substringWithRange:NSMakeRange(i, 2)];
        NSScanner * scanner = [[NSScanner alloc] initWithString:hexCharStr];
        [scanner scanHexInt:&anInt];
        myBuffer[i / 2] = (char)anInt;
        }
    NSString *unicodeString = [NSString stringWithCString:myBuffer encoding:4];
    NSLog(@"------字符串=======%@",unicodeString);
    return unicodeString; 
}

- (void)save {
    [tcpSocket disconnect];
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *plistPath = [paths objectAtIndex:0];
    //得到完整的文件名
    NSString *filename = [plistPath stringByAppendingPathComponent:@"10"];
    //
    NSLog(@"%@",filename);
    [FileIO saveFileAtPath:filename data:[NSKeyedArchiver archivedDataWithRootObject:datas]];
}

#pragma mark - UDP代理方法
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    // You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    // You could add checks here
}

#pragma mark - 接收数据的方法
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    int msgID;
    NSString *str = [DetrumDataProcessing hexStringFromData:data];
    //分解出的mavlink数据体 payload
    NSData *payload = [DetrumDataProcessing stringToByte:[str substringFromIndex:16]];
    //    UInt64 length = strtoul([[str substringWithRange:NSMakeRange(8, 4)] UTF8String], 0, 16);
    char *dataBuff;
    dataBuff = (char *)[payload bytes];
    //获取当前解析的消息类型
//    msgID = mavlink.ReceivedBytes(dataBuff, (int)payload.length);
    printf("---------------------- msgId ---------------------- : %d",msgID);
    //取具体数据并存储
    [self processData:msgID];
}

- (void)processData:(int)msgId {
    switch (msgId) {
        case MAVLINK_MSG_ID_HOME_POSITION:
            
            break;
        case MAVLINK_MSG_ID_GPS_RAW_INT:{
            CGFloat time_usec = mavlink.getParameterValue(PARAM_GPS_TIMEUSEC);
            CGFloat lat = mavlink.getParameterValue(PARAM_GPS_LAT);
            CGFloat lon = mavlink.getParameterValue(PARAM_GPS_LON);
            CGFloat alt = mavlink.getParameterValue(PARAM_GPS_ALT);
            CGFloat eph = mavlink.getParameterValue(PARAM_GPS_EPH);
            CGFloat epv = mavlink.getParameterValue(PARAM_GPS_EPV);
            CGFloat vel = mavlink.getParameterValue(PARAM_GPS_VEL);
            CGFloat cog = mavlink.getParameterValue(PARAM_GPS_COG);
            CGFloat fixtype = mavlink.getParameterValue(PARAM_GPS_FIXTYPE);
            CGFloat satellite = mavlink.getParameterValue(PARAM_GPS_SATELLITESVISIBLE);
            [_parameter setObject:@[@(time_usec),@(lat),@(lon),@(alt),@(eph),@(epv),@(vel),@(cog),@(fixtype),@(satellite)] forKey:PARAM_GPS];
            break;
        }
            // 心跳包
        case MAVLINK_MSG_ID_HEARTBEAT: {
            CGFloat flyModel = mavlink.getParameterValue(PARAM_HEARTBEART_FLY_MODEL);
            CGFloat systemStatus = mavlink.getParameterValue(PARAM_HEARTBEART_SYSTEM_STATUS);
            CGFloat type = mavlink.getParameterValue(PARAM_HEARTBEART_TYPE);
            CGFloat autoPilot = mavlink.getParameterValue(PARAM_HEARTBEART_AUTOPILOT);
            CGFloat customMode = mavlink.getParameterValue(PARAM_HEARTBEART_BASE_MODEL);
            CGFloat mavlinkVersion = mavlink.getParameterValue(PARAM_HEARTBEART_MAVLINK_VERSION);
            [_parameter setObject:@[@(flyModel),@(systemStatus),@(type),@(autoPilot),@(customMode),@(mavlinkVersion)] forKey:PARAM_HEARTBEART];;
            break;
        }
            // 参数消息
        case MAVLINK_MSG_ID_PARAM_VALUE: {
            CGFloat paramValue = mavlink.getParameterValue(PARAM_VALUE);
            CGFloat paramCount = mavlink.getParameterValue(PARAM_COUNT);
            CGFloat paramIndex = mavlink.getParameterValue(PARAM_INDEX);
            CGFloat param_id_1 = mavlink.getParameterValue(PARAM_ID_1);
            CGFloat param_id_2 = mavlink.getParameterValue(PARAM_ID_2);
            CGFloat param_id_3 = mavlink.getParameterValue(PARAM_ID_3);
            CGFloat param_id_4 = mavlink.getParameterValue(PARAM_ID_4);
            CGFloat param_id_5 = mavlink.getParameterValue(PARAM_ID_5);
            CGFloat param_id_6 = mavlink.getParameterValue(PARAM_ID_6);
            CGFloat param_id_7 = mavlink.getParameterValue(PARAM_ID_7);
            CGFloat param_id_8 = mavlink.getParameterValue(PARAM_ID_8);
            CGFloat param_id_9 = mavlink.getParameterValue(PARAM_ID_9);
            CGFloat param_id_10 = mavlink.getParameterValue(PARAM_ID_10);
            CGFloat param_id_11 = mavlink.getParameterValue(PARAM_ID_11);
            CGFloat param_id_12 = mavlink.getParameterValue(PARAM_ID_12);
            CGFloat param_id_13 = mavlink.getParameterValue(PARAM_ID_13);
            CGFloat param_id_14 = mavlink.getParameterValue(PARAM_ID_14);
            CGFloat param_id_15 = mavlink.getParameterValue(PARAM_ID_15);
            CGFloat param_id_16 = mavlink.getParameterValue(PARAM_ID_16);
            CGFloat param_type = mavlink.getParameterValue(PARAM_TYPE);
            [_parameter setObject:@[@(paramValue),
                                    @(paramCount),
                                    @(paramIndex),
                                    @(param_id_1),
                                    @(param_id_2),
                                    @(param_id_3),
                                    @(param_id_4),
                                    @(param_id_5),
                                    @(param_id_6),
                                    @(param_id_7),
                                    @(param_id_8),
                                    @(param_id_9),
                                    @(param_id_10),
                                    @(param_id_11),
                                    @(param_id_12),
                                    @(param_id_13),
                                    @(param_id_14),
                                    @(param_id_15),
                                    @(param_id_16),
                                    @(param_type)]
                           forKey:PARAM_VALUE_];
            break;
        }
            // 遥控通道
        case MAVLINK_MSG_ID_RC_CHANNELS: {
            CGFloat chanel1 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN1_RAW);
            CGFloat chanel2 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN2_RAW);
            CGFloat chanel3 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN3_RAW);
            CGFloat chanel4 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN4_RAW);
            CGFloat chanel5 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN5_RAW);
            CGFloat chanel6 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN6_RAW);
            CGFloat chanel7 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN7_RAW);
            CGFloat chanel8 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN8_RAW);
            CGFloat chanel9 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN9_RAW);
            CGFloat chanel10 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN10_RAW);
            CGFloat chanel11 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN11_RAW);
            CGFloat chanel12 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN12_RAW);
            CGFloat chanel13 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN13_RAW);
            CGFloat chanel14 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN14_RAW);
            CGFloat chanel15 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN15_RAW);
            CGFloat chanel16 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN16_RAW);
            CGFloat chanel17 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN17_RAW);
            CGFloat chanel18 = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHAN18_RAW);
            CGFloat chancount = mavlink.getParameterValue(PARAM_RC_CHANNELS_CHANCOUNT);
            CGFloat rssi  = mavlink.getParameterValue(PARAM_RC_CHANNELS_RSSI);
            NSArray *parameters = @[@(chanel1),
                                    @(chanel2),
                                    @(chanel3),
                                    @(chanel4),
                                    @(chanel5),
                                    @(chanel6),
                                    @(chanel7),
                                    @(chanel8),
                                    @(chanel9),
                                    @(chanel10),
                                    @(chanel11),
                                    @(chanel12),
                                    @(chanel13),
                                    @(chanel14),
                                    @(chanel15),
                                    @(chanel16),
                                    @(chanel17),
                                    @(chanel18),
                                    @(chancount),
                                    @(rssi)];
            
            
            [_parameter setObject:parameters forKey:PARAM_RC_CHANNELS];
            NSDictionary *channelsDic = [[NSDictionary alloc]initWithObjectsAndKeys:parameters,PARAM_RC_CHANNELS, nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:PARAM_RC_CHANNELS object:nil userInfo:channelsDic];
                break;
            }
            // IMU
        case MAVLINK_MSG_ID_HIGHRES_IMU:{
            CGFloat xacc = mavlink.getParameterValue(PARAM_IMU_XACC);
            CGFloat yacc = mavlink.getParameterValue(PARAM_IMU_YACC);
            CGFloat zacc = mavlink.getParameterValue(PARAM_IMU_ZACC);
            CGFloat zgyro = mavlink.getParameterValue(PARAM_IMU_ZGYRO);
            CGFloat xgyro = mavlink.getParameterValue(PARAM_IMU_XGYRO);
            CGFloat ygyro= mavlink.getParameterValue(PARAM_IMU_YGYRO);
            CGFloat xmag = mavlink.getParameterValue(PARAM_IMU_XMAG);
            CGFloat ymag = mavlink.getParameterValue(PARAM_IMU_YMAG);
            CGFloat zmag = mavlink.getParameterValue(PARAM_IMU_ZMAG);
            CGFloat abs_pressure = mavlink.getParameterValue(PARAM_IMU_ABS_PRESSURE);
            
            CGFloat diff_pressure = mavlink.getParameterValue(PARAM_IMU_DIFF_PRESSURE);
            CGFloat pressure_alt = mavlink.getParameterValue(PARAM_IMU_PRESSURE_ALT);
            CGFloat temperature = mavlink.getParameterValue(PARAM_IMU_TEMPERATURE);
            CGFloat fields_updated = mavlink.getParameterValue(PARAM_IMU_FIELDS_UPDATED);
            NSArray *parameterArr = @[@(xacc),@(yacc),@(zacc),@(zgyro),@(xgyro),@(ygyro),@(xmag),@(ymag),@(zmag),@(abs_pressure),@(diff_pressure),@(pressure_alt),@(temperature),@(fields_updated)];
            [_parameter setObject:parameterArr forKey:PARAM_IMU];
            break;
        }
            
//        case MAVLINK_MSG_ID_SMART_BATTERY_STATUS:{
//            CGFloat bettry_id = mavlink.getParameterValue(PARAM_BETTERY_ID);
//            CGFloat production_date = mavlink.getParameterValue(PARAM_BETTERY_PRODUCTION_DATE);
//            CGFloat temperature = mavlink.getParameterValue(PARAM_BETTERY_TEMPERATURE);
//            CGFloat design_voltage = mavlink.getParameterValue(PARAM_BETTERY_DESIGN_VOLTAGE);
//            CGFloat design_capacity = mavlink.getParameterValue(PARAM_BETTERY_DESIGN_CAPACITY);
//            CGFloat full_capacity = mavlink.getParameterValue(PARAM_BETTERY_FULL_CAPACITY);
//            CGFloat available_battery = mavlink.getParameterValue(PARAM_BETTERY_AVAILABLE_BATTERY);
//            CGFloat remaining_battery = mavlink.getParameterValue(PARAM_BETTERY_REMAINING_BATTERY);
//            CGFloat cycle_count = mavlink.getParameterValue(PARAM_BETTERY_CYCLE_COUNT);
//            CGFloat voltage_single_1 = mavlink.getParameterValue(PARAM_BETTERY_VOLTAGE_SINGLE_1);
//            CGFloat voltage_single_2 = mavlink.getParameterValue(PARAM_BETTERY_VOLTAGE_SINGLE_2);
//            CGFloat voltage_single_3 = mavlink.getParameterValue(PARAM_BETTERY_VOLTAGE_SINGLE_3);
//            CGFloat voltage_single_4 = mavlink.getParameterValue(PARAM_BETTERY_VOLTAGE_SINGLE_4);
//            CGFloat voltage = mavlink.getParameterValue(PARAM_BETTERY_VOLTAGE);
//            CGFloat life_time = mavlink.getParameterValue(PARAM_BETTERY_LIFE_TIME);
//         
//            NSArray *params = @[@(bettry_id),@(production_date),@(temperature),@(design_voltage),@(design_capacity),@(full_capacity),@(available_battery),@(remaining_battery),@(cycle_count),@(voltage_single_1),@(voltage_single_2),@(voltage_single_3),@(voltage_single_4),@(voltage),@(life_time)];
//            [_parameter setObject:params forKey:PARAM_BETTERY];
//            break;
//        }
            
        case MAVLINK_MSG_ID_VFR_HUD: {
            CGFloat airspeed = mavlink.getParameterValue(PARAM_VFR_HUD_AIRSPEED);
            CGFloat groundspeed = mavlink.getParameterValue(PARAM_VFR_HUD_GROUNDSPEED);
            CGFloat alt = mavlink.getParameterValue(PARAM_VFR_HUD_ALT);
            CGFloat climb = mavlink.getParameterValue(PARAM_VFR_HUD_CLIMB);
            CGFloat heading = mavlink.getParameterValue(PARAM_VFR_HUD_HEADING);
            CGFloat throttle= mavlink.getParameterValue(PARAM_VFR_HUD_THROTTLE);
            NSArray *params = @[@(airspeed),@(groundspeed),@(alt),@(climb),@(heading),@(throttle)];
            [_parameter setObject:params forKey:PARAM_VFR_HUD];
            break;
        }
            
        case MAVLINK_MSG_ID_LOCAL_POSITION_NED: {
            CGFloat timeBootMs = mavlink.getParameterValue(PARAM_LOCAL_POSITION_TIMEBOOTMS);
            CGFloat x = mavlink.getParameterValue(PARAM_LOCAL_POSITION_X);
            CGFloat y = mavlink.getParameterValue(PARAM_LOCAL_POSITION_Y);
            CGFloat z = mavlink.getParameterValue(PARAM_LOCAL_POSITION_Z);
            CGFloat vx = mavlink.getParameterValue(PARAM_LOCAL_POSITION_VX);
            CGFloat vy = mavlink.getParameterValue(PARAM_LOCAL_POSITION_VY);
            CGFloat vz = mavlink.getParameterValue(PARAM_LOCAL_POSITION_VZ);
            NSArray *params = @[@(timeBootMs),@(x),@(y),@(z),@(vx),@(vy),@(vz)];
            [_parameter setObject:params forKey:PARAM_LOCAL];
            break;
        }
            // 系统状态:设备姿态, CPU等实时信息
        case MAVLINK_MSG_ID_SYS_STATUS: {
            CGFloat errorComm = mavlink.getParameterValue(PARAM_SYS_STATUS_ERRORS_COMM);
            CGFloat statusLoad = mavlink.getParameterValue(PARAM_SYS_STATUS_LOAD);
            CGFloat dropRateComm = mavlink.getParameterValue(PARAM_SYS_STATUS_DROP_RATE_COMM);
            CGFloat errorsCount1 = mavlink.getParameterValue(PARAM_SYS_STATUS_ERRORS_COUNT1);
            CGFloat errorsCount2 = mavlink.getParameterValue(PARAM_SYS_STATUS_ERRORS_COUNT1);
            CGFloat errorsCount3 = mavlink.getParameterValue(PARAM_SYS_STATUS_ERRORS_COUNT1);
            CGFloat errorsCount4 = mavlink.getParameterValue(PARAM_SYS_STATUS_ERRORS_COUNT1);
            CGFloat currenBattery = mavlink.getParameterValue(PARAM_SYS_STATUS_CURRENT_BATTERY);
            CGFloat voltageBattery = mavlink.getParameterValue(PARAM_SYS_STATUS_VOLTAGE_BATTERY);
            CGFloat batteryRemaining = mavlink.getParameterValue(PARAM_SYS_STATUS_BATTERY_REMAINING);

            CGFloat onboardSensorsPresent = mavlink.getParameterValue(PARAM_SYS_STATUS_ONBOARD_SENSORS_PRESENT);
            CGFloat onboardSensorsEnabled = mavlink.getParameterValue(PARAM_SYS_STATUS_ONBOARD_SENSORS_ENABLED);
            CGFloat onboardSensorsHealth = mavlink.getParameterValue(PARAM_SYS_STATUS_ONBOARD_SENSORS_HEALTH);
          
            NSArray *params = @[@(onboardSensorsPresent),
                                @(onboardSensorsEnabled),
                                @(onboardSensorsHealth),
                                @(statusLoad),
                                @(voltageBattery),
                                @(currenBattery),
                                @(dropRateComm),
                                @(errorComm),
                                @(errorsCount1),
                                @(errorsCount2),
                                @(errorsCount3),
                                @(errorsCount4),
                                @(batteryRemaining)];
            [_parameter setObject:params forKey:PARAM_STATUE];
            
            break;
        }
        case MAVLINK_MSG_ID_STATUSTEXT:{
            char buffer[100];
            mavlink.getStatusText(buffer);
            NSString *statusText = [NSString stringWithCString:buffer encoding:NSASCIIStringEncoding];
            [_parameter setObject:statusText forKey:PARAM_STATUE_TEXT];
            NSDictionary *statueTextDic = [[NSDictionary alloc]initWithObjectsAndKeys:statusText,PARAM_STATUE_TEXT, nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:PARAM_STATUE_TEXT object:nil userInfo:statueTextDic];
//            NSLog(@"statueTextDic:%@",statueTextDic);            statueTextDic = nil;
            
            break;
        }
        case MAVLINK_MSG_ID_COMMAND_ACK:
            
            break;
            
        case MAVLINK_MSG_ID_LOG_ENTRY:
            
            break;
            
        case MAVLINK_MSG_ID_LOG_DATA:
            break;
        default:
            break;
    }
}

- (NSArray *)getDataWithType:(NSString *)type {
    NSArray *temp = [_parameter objectForKey:type];
    if (temp == nil) {
        return @[];
    }
    return temp;
}

//校验和算法
unsigned short csum(unsigned char *addr, int count)
{
    register long sum = 0;
    while( count > 1 )
    {
        /* This is the inner loop */
        //            int temp = * (unsigned short *) addr++
        sum += * (unsigned short *) addr;
        addr += 2;
        count -= 2;
    }
    /* Add left-over byte, if any */
    if( count > 0 )
        sum += * (unsigned char *) addr;
    /* Fold 32-bit sum to 16 bits */
    while (sum>>16)
        sum = (sum & 0xffff) + (sum >> 16);
    return ~sum;
}



@end
