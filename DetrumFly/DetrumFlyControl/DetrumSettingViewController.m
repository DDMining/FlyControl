//
//  DetrumSettingViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumSetTableViewCell.h"
#import "DetrumSettingViewController.h"

@interface DetrumSettingViewController () <UITableViewDataSource,UITableViewDelegate>

@end

@implementation DetrumSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    [self showNavBarWithRightTitle:nil
                    andRightAction:nil
                     andLeftAction:nil
                 andRightBtnIsShow:NO];
    [self initUi];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)initUi {
    
    if (self.tableView.delegate == nil) {
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    self.tableView.tableFooterView = [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetrumSetTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"DetrumSetTableViewCell" owner:self options:nil] lastObject];
    switch (indexPath.row) {
        case 0:
            cell.type = DetrumSetTypeHaveSwitch;
            cell.setItemNameTx.text = @"使用移动数据上传";
            break;
        case 1:
            cell.type = DetrumSetTypeHaveIndicator;
            cell.setItemNameTx.text = @"清理视频缓存";
            break;
        case 2:
            cell.type = DetrumSetTypeHaveIndicator;
            cell.setItemNameTx.text = @"清理照片缓存";
            break;
        case 3:
            cell.type = DetrumSetTypeHaveIndicator;
            cell.setItemNameTx.text = @"重置新手指引";
            break;
        case 4:
            cell.type = DetrumSetTypeHaveIndicator;
            cell.setItemNameTx.text = @"隐私";
            break;
        case 5:
            cell.type = DetrumSetTypeHaveIndicator;
            cell.setItemNameTx.text = @"给我们评分";
            break;
        case 6:
            cell.type = DetrumSetTypeHaveBtn;
            cell.setItemNameTx.text = @"退出登陆";
            break;
            
        default:
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 62;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 6) {
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"提示" message:@"您确认要退出当前账号吗?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        UIAlertAction *actionDetermine = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
            [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:DetrumUserLogin];
        }];
        [controller addAction:action];
        [controller addAction:actionDetermine];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
