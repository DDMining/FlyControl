//
//  DetrumMannotationView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/1/31.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumAnnotation.h"
#import <MAMapKit/MAMapKit.h>

//typedef void (^);

@interface DetrumAnnotationView : MAAnnotationView <UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIImageView *annotationImg;
@property (nonatomic, strong) UITapGestureRecognizer *gesture;
@end
