//
//  NSArray+SecurityGetObj.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/9.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "NSArray+SecurityGetObj.h"

@implementation NSArray (SecurityGetObj)

- (id)objectSecurityAtIndex:(int)index {
    if (index >= self.count) {
        return @"";
    }
    id obj = [self objectAtIndex:index];
    if (obj == nil) {
        return @"";
    }
    return obj;
}

@end
