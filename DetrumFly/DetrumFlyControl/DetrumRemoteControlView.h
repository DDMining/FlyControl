//
//  DetrumRemoteControlView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumRemoteControlView : UIView
{
    NSTimer *timer;
    CATransition *animation;
}
@property (weak, nonatomic) IBOutlet UIImageView *japanModeOneImg;
@property (weak, nonatomic) IBOutlet UIImageView *japanModeTwoImg;
@property (weak, nonatomic) IBOutlet UIImageView *usaModeOneImg;
@property (weak, nonatomic) IBOutlet UIImageView *usaModeTwoImg;
@property (strong, nonatomic) UIImageView *pointImg;
@property (weak, nonatomic) UIView *animationView;
@property (assign, nonatomic) NSInteger currenIndex;
@property (assign, nonatomic) NSInteger lastIndex;
@property (strong, nonatomic) NSMutableArray *analyMode;
@end
