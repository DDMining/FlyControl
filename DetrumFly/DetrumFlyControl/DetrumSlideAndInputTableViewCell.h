//
//  DetrumSlideAndInputTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#define kResHeight @"kResHeight"
#define kXY_VEL @"kXY_VEL"
#define kZ_VEL @"kZ_VEL"

#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>
#import "DetrumSetContentView.h"

@interface DetrumSlideAndInputTableViewCell : DetrumBaseTableViewCell <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UITextField *inputTf;
@property (strong, nonatomic)DetrumSetContentView *setView;
@property (assign ,nonatomic)NSInteger sliderRow;


@end
