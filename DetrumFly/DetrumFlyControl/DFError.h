//
//  DFError.h
//  JiuLe
//
//  Created by dynamrc on 16/3/31.
//  Copyright © 2016年 Fine. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DFErrorDomain @"domainError"

@interface DFError : NSError
@property (nonatomic, strong) NSString *failMsg;
+ (instancetype)shareInstanceOfCode:(NSInteger)code;
+ (instancetype)shareInstanceOfErrorMsg:(NSString *)msg;
@end
