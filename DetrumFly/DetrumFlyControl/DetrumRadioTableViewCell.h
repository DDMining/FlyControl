//
//  DetrumRadioTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumRadioTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *firstRadio;
@property (weak, nonatomic) IBOutlet UIButton *lastRadio;
@property (weak, nonatomic) IBOutlet UILabel *firstRadioLb;
@property (weak, nonatomic) IBOutlet UILabel *secondRadioLb;

@end
