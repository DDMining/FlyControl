//
//  DetrumCradleHeadTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"
#import "CQDropDownView.h"

@interface DetrumCradleHeadTableViewCell : DetrumBaseTableViewCell <CQDropDownViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) CQDropDownView * dropDown;
@end
