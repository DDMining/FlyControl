//
//  NSString+Check.h
//  MyCheZan
//
//  Created by lovecar on 14-7-30.
//  Copyright (c) 2014年 chezan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Check)

-(BOOL)checkPhoneNumInput;
- (BOOL)checkEmailInput;

@end
