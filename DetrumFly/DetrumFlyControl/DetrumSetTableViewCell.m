//
//  DetrumSetTableViewCell.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumSetTableViewCell.h"
@interface DetrumSetTableViewCell ()

@end
@implementation DetrumSetTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
 }
 
- (void)setType:(DetrumSetType)type {
    if (type == DetrumSetTypeHaveIndicator) {
        _nextImg.hidden = NO;
        _exitBtn.hidden = YES;
        _switch3g.hidden = YES;
        _setItemNameTx.hidden = NO;
    } else if (type == DetrumSetTypeHaveSwitch){
        _nextImg.hidden= YES;
        _switch3g.hidden = NO;
        _exitBtn.hidden = YES;
        _setItemNameTx.hidden = NO;
    } else {
        _exitBtn.hidden = NO;
        _nextImg.hidden = YES;
        _switch3g.hidden = YES;
        _setItemNameTx.hidden = YES;
    }
}

- (IBAction)exitEvent:(UIButton *)sender {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
