//
//  DetrumSlideAndInputTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "Common.h"
#import "DetrumMarco.h"
#import "DetrumMavlinkManager.h"
#import "DetrumSlideAndInputTableViewCell.h"


@implementation DetrumSlideAndInputTableViewCell

- (void)awakeFromNib {
    // Initialization code
//    _inputTf.text = [NSString stringWithFormat:@"%zd",[[NSUserDefaults standardUserDefaults] integerForKey:kResHeight]];
//    if (self.sliderRow == 1) {
//        self.inputTf.text = [NSString stringWithFormat:@"%zd",[[NSUserDefaults standardUserDefaults] integerForKey:kResHeight]];
//    }else if (self.sliderRow == 3){
//        self.inputTf.text = [NSString stringWithFormat:@"%zd",[[NSUserDefaults standardUserDefaults] integerForKey:kXY_VEL]];
//    }else if (self.sliderRow == 4){
//        self.inputTf.text = [NSString stringWithFormat:@"%zd",[[NSUserDefaults standardUserDefaults] integerForKey:kZ_VEL]];
//    }
    _inputTf.delegate = self;
//    _slider.value = [_inputTf.text intValue];
    
//    _inputTf.enabled = NO;
//    _slider.value = [[NSUserDefaults standardUserDefaults] floatForKey:kResHeight];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (![Common verificationContentIsNum:string]) {
        return NO;
    }
    
    if ([[DetrumMavlinkManager shareInstance] isConnect]) {
       
        [[NSUserDefaults standardUserDefaults] setInteger:[string integerValue] forKey:kResHeight];
        [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROLRESHEIGHT andValue:[string integerValue]];
    }
    return YES;
}

- (IBAction)valueChange:(id)sender {

//    NSLog(@"%ld",(long)self.sliderRow);
    // 限高
    if (self.sliderRow == 1) {
        [[NSUserDefaults standardUserDefaults] setInteger:_slider.value forKey:kResHeight];
        if ([[DetrumMavlinkManager shareInstance] isConnect]) {
            [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROLRESHEIGHT andValue:_slider.value];
        }
        NSLog(@"kResHeight,%f",_slider.value);
    }
    // 飞行水平速度限制
    else if (self.sliderRow == 3){
        [[NSUserDefaults standardUserDefaults] setInteger:_slider.value forKey:kXY_VEL];
        if ([[DetrumMavlinkManager shareInstance] isConnect]) {
            [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROL_XY_VEL andValue:_slider.value];
        }
        NSLog(@"kXY_VEL,%f",_slider.value);
    }
    // 飞行垂直速度限制
    else if (self.sliderRow == 4){
        [[NSUserDefaults standardUserDefaults] setInteger:_slider.value forKey:kZ_VEL];
        if ([[DetrumMavlinkManager shareInstance] isConnect]) {
            [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROL_Z_VEL andValue:_slider.value];
        }
        NSLog(@"kZ_VEL,%f",_slider.value);
    }

    _inputTf.text = [NSString stringWithFormat:@"%.0f",_slider.value];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
