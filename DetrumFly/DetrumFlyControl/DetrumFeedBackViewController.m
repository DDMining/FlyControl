//
//  DetrumFeedBackViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumFeedBackViewController.h"

@interface DetrumFeedBackViewController () <UITextViewDelegate , UIPopoverPresentationControllerDelegate , UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextView *feedBackTv;
@property (weak, nonatomic) IBOutlet UILabel *feedBackTips;
@property (weak, nonatomic) IBOutlet UITextField *feedBackPhone;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@end

@implementation DetrumFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"用户反馈";
    [self showNavBarWithRightTitle:nil andRightAction:nil andLeftAction:nil andRightBtnIsShow:NO];
    _feedBackTv.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    _feedBackTv.hidden = YES;
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    _feedBackTv.hidden = NO;
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (textView.text.length > 0) {
        _feedBackTv.hidden = YES;
    } else {
        _feedBackTv.hidden = NO;
    }
    if (text.length > 200) {
        [SVProgressHUD showErrorWithStatus:@"亲，内容太长啦，限200字"];
        return NO;
    }
    return YES;
}

- (IBAction)sendEvent:(UIButton *)sender {
   
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
