//
//  DetrumFlyControlManager.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import <ExternalAccessory/ExternalAccessory.h>
#import "DetrumFlyControlManager.h"
static DetrumFlyControlManager *manager = nil;
@interface DetrumFlyControlManager ()

@end

@implementation DetrumFlyControlManager

+ (instancetype)shareInstance {
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        manager = [[DetrumFlyControlManager alloc] init];
    });
    return manager;
}

- (void)setCurrenType:(DetrumSetType)type {
    currenType = type;
    
    [self initParameter:currenType];
}

- (void)initParameter:(DetrumSetType)type {
    //TODO:或许会根据网络请求获得设置数据
    
    switch (type) {
        case  DetrumSetTypeFlyControl:
            _dataSource = [NSMutableArray arrayWithArray:@[@{@"name": @"新手模式" ,
                                                             @"type":@(DetrumContentSubViewTypeSwitch)},
                                                           @{@"name": @"限高(20~500m)",
                                                             @"type":@(DetrumContentSubViewTypeSlideAddInputView)},
                                                           @{@"name": @"飞行单位设置" ,
                                                             @"type":@(DetrumContentSubViewTypeRadiu),@"firstRadio":@"m/s",
                                                             @"secondRadio":@"km/h"},
                                                           
//                                                           @{@"name":@"飞行距离设置",
//                                                             @"type":@(DetrumContentSubViewTypeRadiu),@"firstRadio":@"相对起飞点距离",
//                                                             @"secondRadio":@"相对人的距离"},
                                                           @{@"name":@"飞行水平速度限制(20~500m/s)",
                                                             @"type":@(DetrumContentSubViewTypeSlideAddInputView)},
//                                                           @{@"name":@"飞行高度限制(20~500m/s)",
//                                                             @"type":@(DetrumContentSubViewTypeSlideAddInputView)},
                                                           @{@"name":@"飞行垂直速度限制(20~500m/s)",
                                                             @"type":@(DetrumContentSubViewTypeSlideAddInputView)},
                                                           @{@"name":@"返航设置",
                                                             @"type":@(DetrumContentSubViewTypeIndictor)},
                                                           @{@"name":@"姿态感度设置",
                                                             @"type":@(DetrumContentSubViewTypeIndictor)},
                                                           @{@"name":@"电池信息详情",
                                                             @"type":@(DetrumContentSubViewTypeIndictor)},
                                                           @{@"name":@"传感器设置",
                                                             @"type":@(DetrumContentSubViewTypeIndictor)}]];
            _seleteDatas = @{@"返航设置":@"DetrumCourseReversalView",
                             @"姿态感度设置":@"DetrumGestureSensitivityView",
                             @"电池信息详情":@"DetrumBatteryInfoView",
                             @"传感器设置":@"DetrumSensorView"};
            break;
        case  DetrumSetTypeRemoteControl:
            _dataSource = [NSMutableArray arrayWithArray:@[@{@"name": @"云台拨轮速度"},@{@"name":@"按键设置"},@{@"name":@"遥控器校准"}]];
            break;
        case  DetrumSetTypePanTilt:
            _dataSource = [NSMutableArray arrayWithArray:@[@{@"name": @"云台模式"},@{@"name":@"相机"},@{@"name":@"相机"},@{@"name":@"相机回复初始状态"},@{@"name":@"相机回复初始状态"}]];
            break;
        case  DetrumSetTypeGraphicCommunication:
            _dataSource = [NSMutableArray arrayWithArray:@[@{@"name": @"信道设置"},@{@"name":@"temp"},@{@"name":@"图传质量"}]];
            
            break;
        case  DetrumSetTypeWifi:
            _dataSource = [NSMutableArray arrayWithArray:@[@{@"name": @"temp"}]];
            break;
        case  DetrumSetTypeBattery:
            _dataSource = [NSMutableArray arrayWithArray:@[@{@"name": @"temp"},@{@"name": @"temp"},@{@"name": @"temp"}]];
            break;
        case  DetrumSetTypeOther:
             _dataSource = [NSMutableArray arrayWithArray:@[@{@"name": @"参数单位"},
                                                            @{@"name": @"左边纠正"},
                                                            @{@"name": @"显示航线"},
                                                            @{@"name": @"清除航线"},
                                                            @{@"name": @"视频缓存 录制时缓存"},
                                                            @{@"name": @"自动清理缓存(大于5GB)"},
                                                            @{@"name": @"清除缓存"},
                                                            @{@"name": @"其他"},
                                                            @{@"name": @"直播"},
                                                            @{@"name": @"关于"}]];
            _seleteDatas = @{@"关于":@"DetrumAboutView"};
            break;
            
        default:
            break;
    }
}


@end
