//
//  DetrumBettryOneTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/29.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBettryOneTableViewCell.h"

@implementation DetrumBettryOneTableViewCell

- (IBAction)sliderValueChanged:(id)sender {
    CGFloat percentage = _slider.value;
    _sliderValue.text = [NSString stringWithFormat:@"%.1f",percentage];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
