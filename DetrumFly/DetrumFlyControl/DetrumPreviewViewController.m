//
//  DetrumPreviewViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/20.
//  Copyright © 2016年 dynamrc. All rights reserved.
//


#import "VIPhotoView.h"
#import "BaseNavController.h"
#import "DetrumMediaImgModel.h"
#import "DetrumPreviewViewController.h"
#define kImgTag 0x1F4

#define kPhotoImgWidth CGRectGetWidth([UIScreen mainScreen].bounds)
#define kPhotoImgHeight CGRectGetHeight(self.scrollView.frame)

@interface DetrumPreviewViewController () <UIScrollViewDelegate> {
    NSInteger currenIndex;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *downLocalBtn;
@property (weak, nonatomic) IBOutlet UIImageView *deleteBtn;

@end

@implementation DetrumPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预览图片";
    currenIndex = 0;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"blackBox"]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    [self showNavBarLeftImgAndRightTitle:nil
                            andRightImge:nil
                          andRightAction:nil
                           andLeftAction:@selector(backTo)
                             andLeftImge:[UIImage imageNamed:@"return"]];
    
    _downLocalBtn.userInteractionEnabled = YES;
    _deleteBtn.userInteractionEnabled = YES;
    
    [self.downLocalBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(downLocal)]];
    
    [self.deleteBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(deleteImg)]];
    if (_type == JoinPreviewClassTypeSelectMedia) {
        _deleteBtn.hidden = YES;
    }

    [self initScrollView];
}

- (void)downLocal {
    int index = _scrollView.contentOffset.x / kPhotoImgWidth;
    [self saveImageToPhotos:[[_models objectAtIndex:index] valueForKey:@"_fullScreenImage"]];
}

- (void)deleteImg {
    int index =  _scrollView.contentOffset.x / kPhotoImgWidth;
    _action(index, [_models objectAtIndex:index]);
    [_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (index == _models.count - 1) {
        _initIndex = _models.count - 2;
    } else {
        _initIndex = index;
    }
    [_models removeObjectAtIndex:index];
    if (_models.count == 0) {
        [self backTo];
        return;
    }
    [self initScrollView];
}

- (void)saveImageToPhotos:(UIImage*)savedImage
{
     UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

 // 指定回调方法
- (void)image:(UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
 {
     NSString *msg = nil ;
     if(error != NULL){
         msg = @"保存图片失败" ;
         [SVProgressHUD showSuccessWithStatus:msg];
     }else{
         msg = @"保存图片成功" ;
         [SVProgressHUD showSuccessWithStatus:msg];
     }
}

- (void)backTo {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"box"]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

- (void)initScrollView {
    for (int i = 0 , count = (int)_models.count; i < count; i ++) {
        DetrumMediaModel *model = _models[i];
        VIPhotoView *photo = [[VIPhotoView alloc] initWithFrame:CGRectMake(kPhotoImgWidth * i, 0, kPhotoImgWidth, kPhotoImgHeight) andImage:nil];
//        UIImageView *photo = [[UIImageView alloc] initWithFrame:CGRectMake(kPhotoImgWidth * i, 0, kPhotoImgWidth, kPhotoImgHeight)];
//        photo.userInteractionEnabled = YES;
//        UITapGestureRecognizer*doubleTap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleDoubleTap:)];
//        [doubleTap setNumberOfTapsRequired:2];
//        
//        photo.contentMode = UIViewContentModeScaleAspectFit;
        if (_type == JoinPreviewClassTypeLibraryMedia) {
            if (i == _initIndex || i == _initIndex+1 || i == _initIndex - 1) {
                [photo initSetup:model.fullScreenImage];
            }
        } else {
            if (i == _initIndex || i == _initIndex+1 || i == _initIndex - 1) {
                [photo initSetup:[UIImage imageWithCGImage:model.defaul.fullScreenImage]];
            }
        }
        photo.tag = kImgTag + i;
        [_scrollView addSubview:photo];
     }
    _scrollView.pagingEnabled = YES;
    _scrollView.contentSize = CGSizeMake(_models.count * kPhotoImgWidth, 0);
    _scrollView.delegate = self;
    _scrollView.scrollEnabled = YES;
    if (_initIndex > 0) {
        [_scrollView setContentOffset:CGPointMake(_initIndex * kPhotoImgWidth, 0)];
    }
    currenIndex = _initIndex;
}

-(void)handleDoubleTap:(UIGestureRecognizer*)gesture{
    float newScale = [(UIScrollView*)gesture.view.superview zoomScale] *1.5;//每次双击放大倍数
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gesture locationInView:gesture.view]];
    [_scrollView zoomToRect:zoomRect animated:YES];
}

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center
{
    CGRect zoomRect;
    zoomRect.size.height=self.view.frame.size.height/ scale;
    zoomRect.size.width=self.view.frame.size.width/ scale;
    zoomRect.origin.x= center.x- (zoomRect.size.width/2.0);
    zoomRect.origin.y= center.y- (zoomRect.size.height/2.0);
    return zoomRect;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    _scrollView.zoomScale = 1.f;
    
 }

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    int index =  _scrollView.contentOffset.x / kPhotoImgWidth;
    int nextIndex = 0;
   
    if (index < currenIndex) {
        if (index == 0) {
          nextIndex = -1;
        } else {
          nextIndex = index - 1;
        }
    } else {
        if (index == _models.count - 1) {
            nextIndex = -1;
        } else {
            nextIndex = index + 1;
        }
    }
    VIPhotoView *photo = [_scrollView viewWithTag:kImgTag + index];
    VIPhotoView *nextPhoto = [_scrollView viewWithTag:kImgTag + nextIndex];
    if (photo.imageView.image == nil && photo != nil) {
        DetrumMediaModel *asset = _models[index];
        if (_type == JoinPreviewClassTypeLibraryMedia) {
            [photo initSetup:asset.fullScreenImage];
        } else {
            [photo initSetup:[UIImage imageWithCGImage:asset.defaul.fullScreenImage]];
        }
    }
    if (nextPhoto != nil && nextPhoto.imageView.image == nil) {
        if (index > 0 || index < _models.count - 1) {
            DetrumMediaModel *nextModel = _models[nextIndex];
            if (_type == JoinPreviewClassTypeLibraryMedia) {
                [nextPhoto initSetup:nextModel.fullScreenImage];
            } else {
                [nextPhoto initSetup:[UIImage imageWithCGImage:nextModel.defaul.fullScreenImage]];
            }
        }
     }
    currenIndex = index;
}

#pragma clang diagnostic pop


@end
