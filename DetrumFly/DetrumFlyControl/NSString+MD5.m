//
//  NSString+MD5.m
//  SpellBus
//
//  Created by wangfang on 15/1/31.
//  Copyright (c) 2015年 publicNetwork. All rights reserved.
//

#import "NSString+MD5.h"
#import <CommonCrypto/CommonDigest.h>

static NSString *token = @"585bedd4e3d4a3cfa185f710914ad96c";

@implementation NSString (MD5)

- (NSString *)myMD5
{
    NSString *str = [NSString stringWithFormat:@"%@%@%@", token, self, token];
    return [str MD5];
}

#pragma mark 使用MD5加密字符串
- (NSString *)MD5
{
    const char *cStr = [self UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(cStr, strlen(cStr), digest);
    
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [result appendFormat:@"%02x", digest[i]];
    }
    
    return result;
}


@end
