//
//  DetrumPullDownMenuCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumPullDownMenuCell.h"

@implementation DetrumPullDownMenuCell

- (void)awakeFromNib {
    if (_g1View == nil) {
        [self initPullDownView];
    }
}

- (void)initPullDownView {
    CGRect g1ViewRect = CGRectMake(CGRectGetMidX(_g1Btn.frame) - ( 158 / 3 ), CGRectGetMaxY(_g1Btn.frame) + 10 , 158, 23);
    CGRect g2ViewRect = CGRectMake(CGRectGetMidX(_g2Btn.frame) , CGRectGetMaxY(_g2Btn.frame) + 10 , 158, 23);
    
    _g1View = [[CQDropDownView alloc] initWithFrame:g1ViewRect andDefaultTitle:@[@"相机水平朝前",@"相机水平",@"相机朝前",@"相机朝下"]];
    _g1View.delegate = self;
    [_g1View setDirection:CQDropDownDirectionDown];
    _g1View.tag = 0x521;
    [self addSubview:_g1View];
    
    _g2View = [[CQDropDownView alloc] initWithFrame:g2ViewRect andDefaultTitle:@[@"相机俯仰朝向切换",@"相机偏航朝向水平",@"云台模式切换",@"相机朝下"]];
    _g2View.delegate = self;
    [_g2View setDirection:CQDropDownDirectionDown];
    _g2View.tag = 0x522;
    [self addSubview:_g2View];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)CQDropDownDelegateMethod:(CQDropDownView *)sender andSeleteStr:(NSString *)str{
    //TODO:传值什么的
    if (sender.tag == 0x521) {
        NSLog(@"selete 1: %@",str);
    } else {
        NSLog(@"selete 2: %@",str);
    }
}

@end
