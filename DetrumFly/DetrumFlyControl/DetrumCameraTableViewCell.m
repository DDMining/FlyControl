//
//  DetrumCameraTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumCameraTableViewCell.h"

@implementation DetrumCameraTableViewCell

- (void)awakeFromNib {
    // Initialization code

}

- (void)setType:(DetrumCameraType)type {
    _type = type;
    if (_type == DetrumCameraTypeOne) {
        _firstView.hidden = YES;
        _secondView.hidden = NO;
        [_horizontalBtn setTitle:@"水平" forState:UIControlStateNormal];
        [_toDownBtn setTitle:@"朝下" forState:UIControlStateNormal];
    } else {
        _firstView.hidden = NO;
        _secondView.hidden = YES;
        [_toLeft setTitle:@"朝左" forState:UIControlStateNormal];
        [_toFront setTitle:@"朝前" forState:UIControlStateNormal];
        [_toRight setTitle:@"朝右" forState:UIControlStateNormal];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
