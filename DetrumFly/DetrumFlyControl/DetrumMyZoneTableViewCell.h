//
//  DetrumMyZoneTableViewCell.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumMyZoneTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *myZoneTitle;
@property (weak, nonatomic) IBOutlet UIImageView *myZoneImg;
@property (strong, nonatomic) NSString *className;
@end
