//
//  DetrumScrollView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/14.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "RectExtends.h"
#import "UIView+QuicklyChangeFrame.h"
#import "DetrumScrollView.h"

#define customLeading 3

@implementation DetrumScrollView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {

    }
    return self;
}

- (void)awakeFromNib {
    if (panGesture == nil) {
         self.userInteractionEnabled = YES;
        panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGestureEvent:)];
        [_sliderBtn addGestureRecognizer:panGesture];
    }
    if (self.layer.cornerRadius == 0.f) {
        self.layer.cornerRadius = 16.f;
        self.layer.masksToBounds = YES;
    }
}

- (void)swipeGestureEvent:(UIPanGestureRecognizer *)gesture {
    CGPoint translation = [gesture translationInView:self];
    CGPoint velocity = [gesture velocityInView:self];
    CGFloat criticalPoint = CGRectGetWidth(self.frame) * (0.6);
    NSLog(@"translation:%@ \n velocity:%@",NSStringFromCGPoint(translation),NSStringFromCGPoint(velocity));
    if (translation.x < 0) {
        return;
    }
    if (_sliderLeading.constant == CGRectGetWidth(self.frame) - CGRectGetWidth(_sliderBtn.frame) ) {
        return;
    }
    if (gesture.state == UIGestureRecognizerStateBegan) {
        if (CGRectGetMaxX(_sliderBtn.frame) == CGRectGetMaxX(self.frame)) {
            //Not Do Anything
        }
    } else if (gesture.state == UIGestureRecognizerStateChanged) {
        _sliderLeading.constant = (_sliderLeading.constant + translation.x) - 10;
        if (_sliderLeading.constant == CGRectGetWidth(self.frame) - CGRectGetWidth(_sliderBtn.frame) )

            [UIView animateWithDuration:10.f animations:^{
                _sliderLeading.constant = CGRectGetWidth(self.frame) - CGRectGetWidth(_sliderBtn.frame);
                [self setNeedsUpdateConstraints];
            }];
    } else if (gesture.state == UIGestureRecognizerStateEnded) {
        if (CGRectGetMaxX(_sliderBtn.frame) == CGRectGetWidth(self.frame) || CGRectGetMidX(_sliderBtn.frame) >= criticalPoint) {
            [self doAnything];
            [UIView animateWithDuration:10.f animations:^{
                _sliderLeading.constant = CGRectGetWidth(self.frame) - CGRectGetWidth(_sliderBtn.frame);
                [self setNeedsUpdateConstraints];
            }];
        } else {
            

            [UIView animateWithDuration:10.f animations:^{
                _sliderLeading.constant = customLeading;
                [self setNeedsUpdateConstraints];

            }];
        }
    }
    NSLog(@"constant:%f",_sliderLeading.constant);
    NSLog(@"state:%zd",gesture.state);
    
    [gesture setTranslation:CGPointMake(0, 0) inView:self];
}

- (void)doAnything {
    if (_event) {
        _event();
    }
//    [self.superview performSelector:@selector(cancel:) withObject:nil];
}

@end
