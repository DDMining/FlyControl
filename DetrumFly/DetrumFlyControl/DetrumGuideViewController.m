//
//  DetrumGuideViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "UserDataMarco.h"
#import "UIColor+RGBConverHex.h"
#import "DetrumPublicManager.h"
#import "DetrumGuideViewController.h"
#import "Costants.h"
#define kGuideImgCount 3
#define kImgTagCardinalNumber 0x155
#define kBtnWidth 140
@implementation DetrumGuideViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    //TODO:初始化
    [self initScrollView];
}

- (void)initScrollView {
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(kGuideImgCount * CGRectGetWidth(self.view.frame), 0);
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;
    [self.view addSubview:_scrollView];
    [self loadImgs];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame) / 2 - 61 / 2, CGRectGetHeight(self.view.frame) - 30, 61, 37)];
    _pageControl.numberOfPages = kGuideImgCount;
    _pageControl.currentPage = 0 ;
    _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    _pageControl.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_pageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = page;
}

- (void)loadImgs {
     for (int i = 0 ; i < kGuideImgCount; i ++) {
         UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"guide_%d",i]]];
         img.frame = CGRectMake(i * CGRectGetWidth(self.view.bounds),
                               0,
                               CGRectGetWidth(self.view.bounds),
                               CGRectGetHeight(self.view.bounds));
         img.tag = kImgTagCardinalNumber + i;
         [_scrollView addSubview:img];
         if (i  == kGuideImgCount-1) {
             _joinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
             _joinBtn.frame = CGRectMake(CGRectGetMinX(img.frame)+CGRectGetWidth(self.view.frame) / 2 - kBtnWidth / 2, CGRectGetHeight(self.view.frame) - 65, kBtnWidth, 35);
             [_joinBtn addTarget:self
                          action:@selector(goHomePage)
                forControlEvents:UIControlEventTouchUpInside];
             [_joinBtn setTitle:@"立即使用"
                       forState:UIControlStateNormal];
             [_joinBtn setBackgroundColor:[UIColor colorWithHex:0xCCCCCC]];
             [_joinBtn setTitleColor:[UIColor colorWithHex:0xFFFFFF] forState:UIControlStateNormal];
             [_scrollView addSubview:_joinBtn];
         }
     }
}

- (void)goHomePage {
    //TODO:去首页
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DetrumUserFirstRun];
    [UIApplication sharedApplication].keyWindow.rootViewController = [DetrumPublicManager createRootController];
}



@end
