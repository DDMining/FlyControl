//
//  DetrumPreviewViewController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/20.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumMediaModel.h"
#import "DetrumBaseViewController.h"
typedef NS_ENUM(NSInteger , JoinPreviewClassType) {
    JoinPreviewClassTypeSelectMedia,
    JoinPreviewClassTypeLibraryMedia,
};

typedef void (^deleteAction)(NSInteger index, DetrumMediaModel *deleteModel);

@interface DetrumPreviewViewController : DetrumBaseViewController
@property (nonatomic, strong) NSMutableArray *models;
@property (nonatomic, strong) NSMutableArray *alassets;
@property (nonatomic, assign) NSInteger initIndex;
@property (nonatomic, copy) deleteAction action;
@property (nonatomic, assign) JoinPreviewClassType type;
@end
