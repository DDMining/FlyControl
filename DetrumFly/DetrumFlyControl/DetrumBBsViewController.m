//
//  DetrumBBsViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

@import WebKit;
#import <MJExtension/NSObject+MJKeyValue.h>
#import <NSString+YYAdd.h>
#import "DetrumBBsViewController.h"


#define kUrl @"http://www.detrumtech.com/"

@interface DetrumBBsViewController () <WKUIDelegate,WKNavigationDelegate>
@property (nonatomic, strong) WKWebView *webView;
@end

@implementation DetrumBBsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"翔迷论坛";
    
    [self showNavBarWithRightTitle:nil
                    andRightAction:nil
                     andLeftAction:nil
                 andRightBtnIsShow:NO];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:kUrl]];
    [SVProgressHUD showWithStatus:@"加载中.."];
    self.webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds))];
//    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [self.view addSubview:self.webView];
    [self.webView loadRequest:request];
    self.webView.UIDelegate = self;
    self.webView.navigationDelegate = self;
    self.webView.allowsBackForwardNavigationGestures = YES;
//    [self request];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kUrl]]];
}

- (void)request {
    NSString *appID = @"test001";
    NSString *appSecret = @"2770bcb4-7e35-498d-b83e-fe2d5377cf74";
    NSString *targetUrl =@"http://wxtest.yiyaogo.com/partner/index.html#/redirect";
    NSString *SAPARATOR=@"@$@";
    NSMutableDictionary *dataDic = [NSMutableDictionary new];
    [dataDic setObject:targetUrl forKey:@"targetUrl"];
    [dataDic setObject:@"15071327239" forKey:@"mobile"];
    [dataDic setObject:@"name" forKey:@"name"];
    
    NSError *error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dataDic options:NSJSONWritingPrettyPrinted error:&error];
    //Data转换为JSON
    NSString *data = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSInteger time = [NSDate date].timeIntervalSince1970 * 1000;
    data = [data stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    NSString *plain = [NSString stringWithFormat:@"%@%@%@%@%@%@%ld%@%@%@%@",appID,SAPARATOR,appSecret,SAPARATOR,data,SAPARATOR,time,SAPARATOR,appSecret,SAPARATOR,appID];
    NSString *sha = [plain sha512String];
    
    //    NSMutableDictionary *params = [NSMutableDictionary new];;
    //    [params setObject:appID forKey:@"appid"];
    //    [params setObject:[self getSha512String:plain] forKey:@"siganature"];
    //    [params setObject:[NSString stringWithFormat:@"%ld",time] forKey:@"timestamp"];
    //    [params setObject:data forKey:@"data"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://wxtest.yiyaogo.com/wechatAuth/createCookieFromPartner"]];
    request.HTTPMethod = @"POST";
    //    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString *body = [NSString stringWithFormat: @"appId=%@&siganature=%@&timestamp=%ld&data=%@",appID,sha,time,data];
    //    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:[body dataUsingEncoding: NSUTF8StringEncoding]];
    [_webView loadRequest:request];

}

#pragma mark - delegate
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:@"加载失败！"];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:@"加载失败！"];
    NSLog(@"%@",error);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [SVProgressHUD showSuccessWithStatus:@"加载完成！"];
    NSLog(@"加载完成！");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
