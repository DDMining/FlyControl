//
//  DetrumCalibrationTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumCalibrationTableViewCell.h"

@implementation DetrumCalibrationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (IBAction)checkIMU:(id)sender {
    _event(0);
}

- (IBAction)IMUCompass:(id)sender {
    _event(1);
}

- (IBAction)CompassCalibration:(id)sender {
    _event(2);
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
