//
//  DetrumFlyControlTopView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/16.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^btnClickAction)(NSString *clickName);

@interface DetrumFlyControlTopView : UIView
@property (copy, nonatomic) btnClickAction action;
@end
