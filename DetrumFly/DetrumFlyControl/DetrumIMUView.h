//
//  DetrumIMU.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NSTimer+YYAdd.h>

typedef NS_ENUM(NSInteger, DetrumImuViewColor) {
    DetrumImuViewColorRed,
    DetrumImuViewColorGreen,
    DetrumImuViewColorGray,
    
};

@interface DetrumIMUView : UIView {
    NSTimer *timer;
    NSInteger currenIndex;
    NSInteger lastIndex;
    NSDictionary *viewDic;
}
/** 水平放置正面朝上视图 */
@property (weak, nonatomic) IBOutlet UIView *horizontalView;
/** 水平放置正面朝上 */
@property (weak, nonatomic) IBOutlet UIImageView *horizontalPlace;

/** 翻转水平放置 */
@property (weak, nonatomic) IBOutlet UIView *turnOutView;
@property (weak, nonatomic) IBOutlet UIImageView *turnOut;

/** 机头朝上放置 */
@property (weak, nonatomic) IBOutlet UIView *pointingUpView;
@property (weak, nonatomic) IBOutlet UIImageView *pointingUp;

/** 机头朝下放置 */
@property (weak, nonatomic) IBOutlet UIView *pointingDownView;
@property (weak, nonatomic) IBOutlet UIImageView *pointingDown;

/** 左侧旋转放置 */
@property (weak, nonatomic) IBOutlet UIView *leftSideTurnOutView;
@property (weak, nonatomic) IBOutlet UIImageView *leftSideTurnOut;

/** 右侧旋转放置 */
@property (weak, nonatomic) IBOutlet UIView *rightSideTurnOutViwe;

@property (weak, nonatomic) IBOutlet UIImageView *rightSideTurnOut;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (assign, nonatomic) DetrumImuViewColor imuColor;

@end

