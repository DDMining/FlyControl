//
//  DetrumCollectionViewCell.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <realm/Realm.h>
#import <UIKit/UIKit.h>
#import "DetrumMediaModel.h"
#import "DetrumImageView.h"

@interface DetrumCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet DetrumImageView *photoView;
@property (strong, nonatomic) DetrumMediaModel *model;
@property (strong, nonatomic) RLMObject *dataBaseObj;
- (void)setImgSelected:(BOOL)selected;
@end
