//
//  DetrumFlyMarco.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef DetrumFlyMarco_h
#define DetrumFlyMarco_h
#import "Costants.h"

//文法串
#define Keypath(keypath) (strchr(#keypath, '.') + 1)

#define kNavigationBtnSpaceHorizontal 5.0f
#define kNavigationBtnSpaceVertical 10.0f
#define kNavigationBtnHeight (self.navigationController.navigationBar.frame.size.height - 2 * kNavigationBtnSpaceVertical)

#define DetrumAppNavBarBackBGI @"return"

#define MyColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

#define iOS8Font(fontSize) [UIFont fontWithName:iOS8FontName size:fontSize]
#define iOS8FontName @"Helvetica"
#define kAppDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)
#define RMB @"¥"

#define iOS7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0) && ([[UIDevice currentDevice].systemVersion doubleValue] <= 7.9)

#define iOS8 ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0) && ([[UIDevice currentDevice].systemVersion doubleValue] <= 8.9)

#define iOS9 ([[UIDevice currentDevice].systemVersion doubleValue] >= 9.0) && ([[UIDevice currentDevice].systemVersion doubleValue] <= 9.9)


#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;


#define LongIntConverStr(value)     [NSString stringWithFormat:@"%ld",value]
#define UIntConverStr(value)    [NSString stringWithFormat:@"%zd",value]
#define IntConverStr(value)       [NSString stringWithFormat:@"%d",value]
#define floatConverStr(value)     [NSString stringWithFormat:@"%f",value]

#pragma mark - 文件系统相关
#define ResourceDirectory           [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Resource"]
#define DocumentsDirectory          [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) lastObject]
#define UserDirectory               [DocumentsDirectory stringByAppendingPathComponent:@"Users"]
#define CurrentUserDirectory        [UserDirectory stringByAppendingPathComponent:[Common currentUserName]]
#define CurrentUserInfoDirectory    [CurrentUserDirectory stringByAppendingPathComponent:@"UserInfo"]
#define CurrentMyTaskDirectory      [CurrentUserDirectory stringByAppendingPathComponent:@"MyTask"]
#define SamplePhotoDirectory        [UserDirectory stringByAppendingPathComponent:@"SamplePhoto"]
#define SampleVideoDirectory        [UserDirectory stringByAppendingPathComponent:@"SampleVideo"]


#pragma mark - arc compile flag
#ifndef COMPILE_ARC_MODE
#define COMPILE_ARC_MODE
#endif

#pragma mark - 功能性
#define registCell(nibName,Identifier) [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:Identifier];

#define getStoryOfControllerInstance(value)  [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:value]

#define TOASTSHOW(value) [self.view makeToast:value duration:TOSTEDURATION position:Center_POINT];
#define TOSTEDURATION 1.0f
#define Center_POINT [NSValue valueWithCGPoint:CGPointMake([UIScreen mainScreen].bounds.size.width / 2, ([UIScreen mainScreen].bounds.size.height  / 2.6) )]

#pragma mark -
#pragma mark - GCD

#define CLGcdBack(block) dispatch_async(\
dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define CLGcdMain(block) dispatch_async(dispatch_get_main_queue(),block)

#pragma mark -
#pragma mark - Date

#define CLCurrentTimeInMilliSince1970 ([[NSDate date] timeIntervalSince1970] * 1000)

#define CLLocalDate(gtmTimeInterval) \
([NSDate dateWithTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT] \
sinceDate:[NSDate dateWithTimeIntervalSince1970:gtmTimeInterval]])

#define CLCurrentTimeZeroZoneForSeconds \
([[NSDate date] timeIntervalSince1970] - [[NSTimeZone localTimeZone] secondsFromGMT])


#pragma mark -
#pragma mark - Get image

#define CLImage(name ,ext) \
([UIImage imageWithContentsOfFile: \
[NSString stringWithFormat:@"%@/%@.%@", [[NSBundle mainBundle] resourcePath], name, ext]])

#define CLPNGImage(name) (CLImage(name, @"png"))
#define CLJPGImage(name) (CLImage(name, @"jpg"))

#define CLImageSize(path) ([UIImage imageWithContentsOfFile:path].size)
#define CLImageWidth(image) (image.size.width)
#define CLImageHeight(image) (image.size.height)

#pragma mark -
#pragma mark - String

#define CLStringTrimWhiteSpace(string) \
[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

#define CLStringByAppendingString(first, second) \
[first stringByAppendingString:(second.length > 0)?second:@""]

#define CLStringEqual(nsString1, nsString2) \
(NSOrderedSame == [nsString1 compare:nsString2])

//====================== Notification ================//
#pragma mark - Notification
#define addObserver(_selector,_name)\
([[NSNotificationCenter defaultCenter] addObserver:self selector:_selector name:_name object:nil])

#define removeObserverWithName(_name)\
([[NSNotificationCenter defaultCenter] removeObserver:self name:_name object:nil])

#define removeObserver() ([[NSNotificationCenter defaultCenter] removeObserver:self])

#define postNotification(_name)\
([[NSNotificationCenter defaultCenter] postNotificationName:_name object:nil userInfo:nil])

#define postNotificationWithObject(_name, _obj)\
([[NSNotificationCenter defaultCenter] postNotificationName:_name object:_obj userInfo:nil])

#define postNotificationWithInfo(_name, _obj, _infos)\
([[NSNotificationCenter defaultCenter] postNotificationName:_name object:_obj userInfo:_infos])



//----------------------系统设备相关----------------------------
//获取设备屏幕尺寸
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)//应用尺寸
#define APP_WIDTH [[UIScreen mainScreen]applicationFrame].size.width
#define APP_HEIGHT [[UIScreen mainScreen]applicationFrame].size.height
//获取系统版本
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define CurrentSystemVersion [[UIDevice currentDevice] systemVersion]
#define isIOS4 ([[[UIDevice currentDevice] systemVersion] intValue]==4)
#define isIOS5 ([[[UIDevice currentDevice] systemVersion] intValue]==5)
#define isIOS6 ([[[UIDevice currentDevice] systemVersion] intValue]==6)
#define isAfterIOS4 ([[[UIDevice currentDevice] systemVersion] intValue]>4)
#define isAfterIOS5 ([[[UIDevice currentDevice] systemVersion] intValue]>5)
#define isAfterIOS6 ([[[UIDevice currentDevice] systemVersion] intValue]>6)


#define CLSystemVerison ([[UIDevice currentDevice] systemVersion])
#define CLIsiPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define CL_SYSTEM_VERSION_EQUAL_TO(v) \
([CLSystemVerison compare:v options:NSNumericSearch] == NSOrderedSame)

#define CL_SYSTEM_VERSION_GREATER_THAN(v)\
([CLSystemVerison compare:v options:NSNumericSearch] == NSOrderedDescending)

#define CL_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)\
([CLSystemVerison compare:v options:NSNumericSearch] != NSOrderedAscending)

#define CL_SYSTEM_VERSION_LESS_THAN(v)\
([CLSystemVerison compare:v options:NSNumericSearch] == NSOrderedAscending)

#define CL_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) \
([CLSystemVerison compare:v options:NSNumericSearch] != NSOrderedDescending)

//获取当前语言
#define CurrentLanguage ([[NSLocale preferredLanguages] objectAtIndex:0])

//判断是否 Retina屏、设备是否%fhone 5、是否是iPad
#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
//#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#ifndef iPhone4s
#define iPhone4s CLScreenHeight == 480 ? YES : NO
#endif

#ifndef iPhone5
#define iPhone5  CLScreenHeight == 568 ? YES : NO
#endif

#ifndef iPhone6
#define iPhone6 CLScreenHeight == 667 ? YES : NO
#endif

#ifndef iPhone6P
#define iPhone6P   CLScreenHeight == 736 ? YES : NO
#endif

#ifndef CLScreenHeight
#define CLScreenHeight CGRectGetHeight([UIScreen mainScreen].bounds)
#endif


//判断是真机还是模拟器
#if TARGET_OS_IPHONE
//iPhone Device
#endif
#if TARGET_IPHONE_SIMULATOR
//iPhone Simulator
#endif
//----------------------系统设备相关----------------------------

//----------------------内存相关----------------------------
//使用ARC和不使用ARC
#if __has_feature(objc_arc)
//compiling with ARC
#else
// compiling without ARC
#endif
//释放一个对象
#define SAFE_DELETE(P) if(P) { [P release], P = nil; }
#define SAFE_RELEASE(x) [x release];x=nil
//----------------------内存相关----------------------------

//----------------------图片相关----------------------------
//读取本地图片
#define LOADIMAGE(file,ext) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]
//定义UIImage对象
#define IMAGE(A) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:A ofType:nil]]
//定义UIImage对象
#define ImageNamed(_pointer) [UIImage imageNamed:_pointer]
//可拉伸的图片
#define ResizableImage(name,top,left,bottom,right) [[UIImage imageNamed:name] resizableImageWithCapInsets:UIEdgeInsetsMake(top,left,bottom,right)]
#define ResizableImageWithMode(name,top,left,bottom,right,mode) [[UIImage imageNamed:name] resizableImageWithCapInsets:UIEdgeInsetsMake(top,left,bottom,right) resizingMode:mode]
//建议使用前两种宏定义,性能高于后者
//----------------------图片相关----------------------------

//----------------------颜色相关---------------------------
// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
// 获取RGB颜色
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)
//背景色
#define BACKGROUND_COLOR [UIColor colorWithRed:242.0/255.0 green:236.0/255.0 blue:231.0/255.0 alpha:1.0]
//清除背景色
#define CLEARCOLOR [UIColor clearColor]
//----------------------颜色相关--------------------------

//----------------------其他----------------------------
//方正黑体简体字体定义
#define FONT(F) [UIFont fontWithName:@"FZHTJW--GB1-0" size:F]
//file
//读取文件的文本内容,默认编码为UTF-8
#define FileString(name,ext)            [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:(name) ofType:(ext)] encoding:NSUTF8StringEncoding error:nil]
#define FileDictionary(name,ext)        [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:(name) ofType:(ext)]]
#define FileArray(name,ext)             [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:(name) ofType:(ext)]]
//G－C－D
#define BACK(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define MAIN(block) dispatch_async(dispatch_get_main_queue(),block)
//Alert
#define ALERT(msg) [[[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil] show]

//由角度获取弧度 有弧度获取角度
#define degreesToRadian(x) (M_PI * (x) / 180.0)
#define radianToDegrees(radian) (radian*180.0)/(M_PI)
//----------------------其他-------------------------------

//----------------------视图相关----------------------------
//设置需要粘贴的文字或图片
#define PasteString(string)   [[UIPasteboard generalPasteboard] setString:string];
#define PasteImage(image)     [[UIPasteboard generalPasteboard] setImage:image];

//得到视图的left top的X,Y坐标点
#define VIEW_TX(view) (view.frame.origin.x)
#define VIEW_TY(view) (view.frame.origin.y)

//得到视图的right bottom的X,Y坐标点
#define VIEW_BX(view) (view.frame.origin.x + view.frame.size.width)
#define VIEW_BY(view) (view.frame.origin.y + view.frame.size.height )

//得到视图的尺寸:宽度、高度
#define VIEW_W(view)  (view.frame.size.width)
#define VIEW_H(view)  (view.frame.size.height)
//得到frame的X,Y坐标点
#define FRAME_TX(frame)  (frame.origin.x)
#define FRAME_TY(frame)  (frame.origin.y)
//得到frame的宽度、高度
#define FRAME_W(frame)  (frame.size.width)
#define FRAME_H(frame)  (frame.size.height)
//----------------------视图相关----------------------------

//---------------------打印日志--------------------------
//Debug模式下打印日志,当前行,函数名
#if DEBUG
#define DLog(FORMAT, ...) fprintf(stderr,"\nfunction:%s line:%d content:%s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(FORMAT, ...) nil
#endif
//Debug模式下打印日志,当前行,函数名 并弹出一个警告
#ifdef DEBUG
#   define  WDLog(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }
#else
#   define NSLog(...)
#endif
//打印Frame
#define LogFrame(frame) NSLog(@"frame[X=%.1f,Y=%.1f,W=%.1f,H=%.1f",frame.origin.x,frame.origin.y,frame.size.width,frame.size.height)
//打印Point
#define LogPoint(point) NSLog(@"Point[X=%.1f,Y=%.1f]",point.x,point.y)
//---------------------打印日志--------------------------

#endif /* DetrumFlyMarco_h */
