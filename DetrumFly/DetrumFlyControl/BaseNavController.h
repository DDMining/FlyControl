//
//  BaseNavController.h
//  0元拍
//
//  Created by xcq on 15/1/4.
//  Copyright (c) 2015年 baishan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavController : UINavigationController
- (void)removeSwipeRecognizer;
- (void)addSwipeRecognizer;
+ (void)setupBarButtonItemTheme;
@end
