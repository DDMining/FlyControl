//
//  DetrumTwoInputViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
//#import <UIView+Toast.h>
#import "Common.h"
#import "DetrumMarco.h"
#import "DetrumMavlinkManager.h"
#import "DetrumTwoInputViewCell.h"

@implementation DetrumTwoInputViewCell

- (void)awakeFromNib {
    // Initialization code
    _firstInputTf.delegate = self;
    _secondInputTf.delegate = self;
    
    _firstInputTf.text = [NSString stringWithFormat:@"%.2f",[[NSUserDefaults standardUserDefaults] floatForKey:@"kManP"]];
    _secondInputTf.text = [NSString stringWithFormat:@"%.2f",[[NSUserDefaults standardUserDefaults] floatForKey:@"kManR"]];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    float p = [_firstInputTf.text floatValue];
    float r = [_secondInputTf.text floatValue];
    [[NSUserDefaults standardUserDefaults]setFloat:p forKey:@"kManP"];
    [[NSUserDefaults standardUserDefaults]setFloat:r forKey:@"kManR"];
    
        NSLog(@"%zd",[textField.text integerValue]);
    if ([textField.text integerValue] > 45 && [textField.text integerValue] < 20) {
        return ;
    } else {
        return ;
    }
    return ;
}

- (BOOL)textFieldDidBeginEditing {
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *toBeString = [textField.text
                            stringByReplacingCharactersInRange:range
                            withString:string];
    NSLog(@"%zd",[toBeString integerValue]);
    if (![Common verificationContentIsNum:toBeString] && ![string isEqualToString:@""]) {
        return NO;
    }
    if (toBeString.length == 2 && ![string isEqualToString:@""]) {
    
        if ([toBeString integerValue] > 20 && [toBeString integerValue] < 45 && ![string isEqualToString:@""]) {
            if (textField == _firstInputTf) {
                [[NSUserDefaults standardUserDefaults]setFloat:[toBeString floatValue] forKey:@"kManP"];
                [self sendMavlinkOfP:YES  andValue:[toBeString floatValue]];
            } else {
                [[NSUserDefaults standardUserDefaults]setFloat:[toBeString floatValue] forKey:@"kManP"];
                [self sendMavlinkOfP:NO  andValue:[toBeString floatValue]];
            }
            return YES;
        } else {
            return NO;
        }
    }
//        if (![string isEqualToString:@""]) {
//            return NO;
//        }
////         NO
//    }
    return YES;
}

- (void)sendMavlinkOfP:(BOOL)isP andValue:(CGFloat)value{
    if ([DetrumMavlinkManager shareInstance].isConnect) {
        NSLog(@"发生了");
        if (isP) {
            
            [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROLMAN_P andValue:value];
        } else {
            [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROLMAN_R andValue:value];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
