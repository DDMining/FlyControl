//
//  DetrumLgAfterViewCell.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumLgAfterViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *information;
@property (weak, nonatomic) IBOutlet UILabel *worksLb;
@property (weak, nonatomic) IBOutlet UILabel *fansLb;
@property (weak, nonatomic) IBOutlet UILabel *focusLb;
- (IBAction)changeInfomationEvent:(UIButton *)sender;

@end
