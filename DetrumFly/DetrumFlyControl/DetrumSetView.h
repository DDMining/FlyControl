//
//  DetrumSetView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumSetContentView.h"

@interface DetrumSetView : UIView {
    @private
    DetrumSetContentView *contentView;
}
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *BtnOutlets;
@property (weak, nonatomic) UIButton *seleteBtn;
- (IBAction)exitAction:(id)sender;

@end
