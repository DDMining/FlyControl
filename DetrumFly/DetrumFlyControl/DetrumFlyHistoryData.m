//
//  DetrumFlyHistoryData.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumFlyHistoryData.h"

@implementation DetrumFlyHistoryData

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)initUI {

}

- (void)initData {
    
}

- (IBAction)backHomePage:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)synAction:(id)sender {
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIDeviceOrientationLandscapeLeft | UIDeviceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft;
}
@end
