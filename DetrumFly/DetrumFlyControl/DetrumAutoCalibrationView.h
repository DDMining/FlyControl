//
//  DetrumAutoCalibrationView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumAutoCalibrationView : UIView
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressViewLead;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progerssBtnLead;
@property (weak, nonatomic) IBOutlet UIButton *progressIcon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UIButton *determineBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@end
