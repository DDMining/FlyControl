//
//  DetrumMediaSelectImgViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/15.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
@import AssetsLibrary;
#import <YYKit.h>
#import <Realm/Realm.h>
#import "DetrumMediaModel.h"
#import "XLPlainFlowLayout.h"
#import "DetrumMediaImgModel.h"
#import "DetrumMediaMovieModel.h"
#import "ALAsset+DetrumALAasset.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DetrumCollectionViewCell.h"
#import "DetrumPreviewViewController.h"
#import "DetrumCollectionReusableHeadView.h"
#import "DetrumMediaLibraryViewController.h"
#import "DetrumMediaSelectImgViewController.h"
static NSString *identifier = @"detrumCollectionCell";
static NSString *headViewIdentifer = @"DetrumReusableHeadView";
@interface DetrumMediaSelectImgViewController () <UICollectionViewDataSource,UICollectionViewDelegate>
{
    @private
    NSMutableArray *selectImgs;
    NSMutableArray *fullImgs;
}
@property (nonatomic, strong) ALAssetsLibrary* library ;
@property (nonatomic, strong) MPMoviePlayerViewController *moviePlayer;
@property (nonatomic, strong) NSMutableArray *imagesKey;
@property (nonatomic, strong) NSMutableArray *videosKey;
@property (nonatomic, strong) NSMutableArray *imagesValue;
@property (nonatomic, strong) NSMutableArray *videosValue;
@property (nonatomic, strong) NSMutableArray *videos;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) NSMutableDictionary *imageDic;
@property (nonatomic, strong) NSMutableDictionary *videoDic;
@property (nonatomic, strong) UICollectionView *collectionView;
@end

@implementation DetrumMediaSelectImgViewController
@synthesize images = images;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self showNavBarWithRightTitle:@"保存"
                    andRightAction:@selector(rightAction:)
                     andLeftAction:nil
                 andRightBtnIsShow:YES];
    
    [self initCollectionView];
    [self showSvprogress:nil];
    if (_type == 1) {
        [self reloadImagesFromLibrary];
        self.title = @"图片预览";
    } else {
        [self reloadVideoFromLibrary];
        self.title = @"视频预览";
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)rightAction:(UIButton *)btn {
    if (selectImgs == nil || selectImgs.count == 0 ) {
        [SVProgressHUD showErrorWithStatus:@"请先选择照片"];
        return;
    }
    RLMRealm *realm = [RLMRealm defaultRealm];
    [SVProgressHUD showInfoWithStatus:@"保存中.."];
    if (selectImgs && selectImgs > 0) {
        WS(weakSelf);
        self.token = [[RLMRealm defaultRealm] addNotificationBlock:^(NSString * _Nonnull notification, RLMRealm * _Nonnull realm) {
            [weakSelf transactionFinish];
        }];
        //保持主键唯一性
        int random = [self getRandomNumber:1 to:(INT32_MAX-(int)selectImgs.count)];
        [realm transactionWithBlock:^{
            if (_type == 1) {
                for (int i = 0; i < selectImgs.count; i ++) {
                    DetrumMediaModel *model = selectImgs[i];
                    if ([_urls containsObject:[model.url absoluteString]]) {
                        continue;
                    }
                    DetrumMediaImgModel * dataModel = [DetrumMediaImgModel new];
                    dataModel.id = random + i;
                    dataModel.MediaDuration = model.duration;
                    dataModel.url = [model.url absoluteString];
                    dataModel.dateStr = model.dateStr;
                    dataModel.currenDate = model.camareDate;
                    dataModel.clickType = DetrumImageClickTypeMediaNone;
                    dataModel.type = model.type;
                    dataModel.thumbnail = UIImageJPEGRepresentation(model.thumbnail, 1);
                    UIImage *image = [UIImage imageWithCGImage:model.defaul.fullScreenImage];
                    NSData *data = UIImageJPEGRepresentation(image, 1);
                    dataModel.fullScreen = data;
                    [realm addObject:dataModel];
                }
            } else {
                for (int i = 0; i < selectImgs.count; i ++) {
                    DetrumMediaModel *model = selectImgs[i];
                    if ([_urls containsObject:[model.url absoluteString]]) {
                        continue;
                    }
                    DetrumMediaMovieModel * dataModel = [DetrumMediaMovieModel new];
                    dataModel.duration = model.duration;
                    dataModel.id = random + i;
                    dataModel.mediaUrl = [model.url absoluteString];
                    dataModel.clickType = DetrumImageClickTypeMediaNone;
                    dataModel.type = model.type;
                    dataModel.dateStr = model.dateStr;
                    dataModel.thumbnail = UIImageJPEGRepresentation(model.thumbnail, 1);
                    dataModel.date = model.camareDate;
                    [realm addObject:dataModel];
                }
            }
            [realm commitWriteTransaction];
         }];
    }
}

- (void)transactionFinish {
    NSLog(@"transactionFinish");
    [SVProgressHUD dismiss];
//    [self.view makeToast:@"保存成功" duration:2.f position:@"center"];
    if (_type == 1) {
        _mediaController.isUpdateImg = YES;
    } else {
        _mediaController.isUpdateVedio = YES;
    }
    [selectImgs removeAllObjects];
    postNotification(kMediaNotification);
}

- (void)initCollectionView {
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.headerReferenceSize = CGSizeMake(CGRectGetWidth(self.view.frame), 37);
    flowLayout.minimumLineSpacing = 2;
    flowLayout.minimumInteritemSpacing = 2;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 2, 0, 2);
    flowLayout.itemSize = CGSizeMake((CGRectGetWidth([UIScreen mainScreen].bounds) - (4 * 2)) / 3, (CGRectGetWidth([UIScreen mainScreen].bounds) - (4 * 2)) / 3);
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds) , CGRectGetHeight([UIScreen mainScreen].bounds) - 64) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.scrollEnabled = YES;
    _collectionView.backgroundColor = self.view.backgroundColor;
    [self.collectionView registerNib:[UINib nibWithNibName:@"DetrumCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:identifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"DetrumCollectionReusableHeadView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headViewIdentifer];
    [self.view addSubview:_collectionView];
}

#pragma mark - Collection delegate || DataSouce
#pragma mark -- UICollectionViewDataSource
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (_type == 1) {
        
        return [[_imagesValue objectAtIndex:section] count] != 0 ? [[_imagesValue objectAtIndex:section] count] : 0;
    } else {
        return [[_videosValue objectAtIndex:section] count] != 0 ? [[_videosValue objectAtIndex:section] count] : 0;
    }
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (_type == 1) {
        return _imagesKey.count == 0 ? 0 : _imagesKey.count;
    } else {
        return _videosKey.count == 0 ? 0 : _videosKey.count;
    }
}

//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DetrumCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (_type == 1) {
         cell.model = [[_imagesValue objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    } else {
         cell.model = [[_videosValue objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
    cell.photoView.clickEvent = ^(NSInteger index,
                                  DetrumMediaType type,
                                  DetrumMediaModel *model,
                                  NSObject * dataBaseModel) {
      //TODO:调用视图浏览界面
        if (_type == 1) {
            DetrumPreviewViewController *preview = [[DetrumPreviewViewController alloc] initWithNibName:@"DetrumPreviewViewController" bundle:nil];
            preview.models = images;
            preview.initIndex = [images indexOfObject:model];
            preview.type = JoinPreviewClassTypeSelectMedia;
            [self.navigationController pushViewController:preview animated:NO];
        } else {
            //TODO:调用视频
            [self playMovie:model.url];
        }
    };
    cell.photoView.seleteImgAction = ^(NSInteger index,
                                       DetrumMediaModel *model,
                                       BOOL isSelect) {
        if (selectImgs == nil) {
            selectImgs = [NSMutableArray new];
        }
        if (isSelect) {
            [selectImgs addObject:model];
        } else if (isSelect == NO && selectImgs.count > 0) {
            [selectImgs removeObject:model];
        }
    };
    return cell;
}


#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    DetrumCollectionViewCell * cell = (DetrumCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    DetrumCollectionReusableHeadView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        DetrumCollectionReusableHeadView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headViewIdentifer forIndexPath:indexPath];
        if (_type == 1) {
             [headerView setDateText:((DetrumMediaModel *)[[_imagesValue objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]).dateStr];
        } else {
             [headerView setDateText:((DetrumMediaModel *)[[_videosValue objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]).dateStr];
        }
        reusableview = headerView;
    }
    return reusableview;
}


#pragma mark --UICollectionViewDelegateFlowLayout

//定义每个Item 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(95 , 95);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}


#pragma mark - Utility Method

- (void)reloadDataSource {
//    WS(weakSelf);
    [SVProgressHUD dismiss];
    [self dataSorting];
    @autoreleasepool {
        if (_type == 1) { //照片
            if (_imagesKey == nil) {
                _imagesKey = [NSMutableArray new];
                _imagesValue = [NSMutableArray new];
            }
            for (int i = 0 ; i < images.count; i ++) {
                DetrumMediaModel *model = [images objectAtIndex:i];
                if (![_imagesKey containsObject:model.dateStr]) {
                    [_imagesKey addObject:model.dateStr];
                    NSMutableArray *arr = [NSMutableArray arrayWithArray:@[model]];
                    [_imagesValue addObject:arr];
                } else {
                    NSMutableArray *arr = [_imagesValue objectAtIndex:[_imagesKey indexOfObject:model.dateStr]];
                    [arr addObject:model];
                    [_imagesValue  replaceObjectAtIndex:[_imagesKey indexOfObject:model.dateStr] withObject:arr];
                }
            }
        } else if (_type == 2) { //视频
            if (_videosKey == nil) {
                _videosKey = [NSMutableArray new];
                _videosValue = [NSMutableArray new];
            }
            for (int i = 0 ; i < _videos.count; i ++) {
                DetrumMediaModel *model = [_videos objectAtIndex:i];
                if (![_videosKey containsObject:model.dateStr]) {
                    [_videosKey addObject:model.dateStr];
                    NSMutableArray *arr = [NSMutableArray arrayWithArray:@[model]];
                    [_videosValue addObject:arr];
                } else {
                    NSMutableArray *arr = [_videosValue objectAtIndex:[_videosKey indexOfObject:model.dateStr]];
                    [arr addObject:model];
                    [_videosValue  replaceObjectAtIndex:[_videosKey indexOfObject:model.dateStr] withObject:arr];
                }
            }
            
        }
    }
    [self dismiss];
    [self.collectionView reloadData];
}

- (void)dataSorting {
    WS(weakSelf);
    if (_type == 1) {
        weakSelf.images = [NSMutableArray arrayWithArray:[self.images sortedArrayUsingComparator:[self detrumComparetor]]];
        [weakSelf.images reverse];
        NSLog(@"reverse before %zd , %zd",fullImgs.count,weakSelf.images.count);
    } else {
       weakSelf.videos = [NSMutableArray arrayWithArray:[self.videos sortedArrayUsingComparator:[self detrumComparetor]]];
        [weakSelf.videos reverse];
    }
}


-(int)getRandomNumber:(int)from to:(int)to
{
    return (int)(from + (arc4random() % (to - from + 1)));
}

- (NSComparator)detrumComparetor {
    NSComparator valueCompare = ^(DetrumMediaModel *obj1, DetrumMediaModel *obj2){
        if ([obj1.camareDate year] > [obj2.camareDate year]) {
            return (NSComparisonResult)NSOrderedDescending;
        } else if ([obj1.camareDate year] < [obj2.camareDate year]){
            return (NSComparisonResult)NSOrderedAscending;
        }
        if ([obj1.camareDate month] > [obj2.camareDate month]) {
            return (NSComparisonResult)NSOrderedDescending;
        } else if ([obj1.camareDate month] < [obj2.camareDate month]){
            return (NSComparisonResult)NSOrderedAscending;
        }
        if ([obj1.camareDate day] > [obj2.camareDate day]) {
            return (NSComparisonResult)NSOrderedDescending;
        } else if ([obj1.camareDate day] < [obj2.camareDate day]){
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };

    return valueCompare;
}


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

//这里是会报警告的代码

- (void)reloadVideoFromLibrary {
    [SVProgressHUD showWithStatus:@"加载中.."];
    WS(weakSelf);
    self.videos = [[NSMutableArray alloc] init];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        @autoreleasepool {
            ALAssetsLibraryAccessFailureBlock failureblock = ^(NSError *myerror){
                NSLog(@"媒体库访问失败 =%@", [myerror localizedDescription]);
                if ([myerror.localizedDescription rangeOfString:@"Global denied access"].location!=NSNotFound) {
                    NSLog(@"无法访问媒体库.请在'设置->定位服务'设置为打开状态.");
                }else{
                    NSLog(@"媒体库访问失败.");
                }
            };

            ALAssetsGroupEnumerationResultsBlock groupEnumerAtion = ^(ALAsset *result, NSUInteger index, BOOL *stop){
                if (result!=NULL) {
                    
                    if ([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]) {
                        NSLog(@"vedio");
                        DetrumMediaModel *model = [DetrumMediaModel new];
                        model.type = DetrumMediaTypeVideo;
                        model.clickType = DetrumImageClickTypeMediaSelect;
                        model.isSeleted = NO;
                        model.currenLocation = [result valueForProperty:ALAssetPropertyLocation];
                        model.duration = [[result valueForProperty:ALAssetPropertyDuration] integerValue];
                        model.camareDate = [result valueForProperty:ALAssetPropertyDate];
                        model.representations = [result valueForProperty:ALAssetPropertyRepresentations];
                        model.dic = [result valueForProperty:ALAssetPropertyURLs];
                        model.url = result.defaultRepresentation.url;
                        model.thumbnail = [UIImage imageWithCGImage:result.thumbnail];
                        [weakSelf.videos addObject:model];
                      }
                }
            };
            
            ALAssetsLibraryGroupsEnumerationResultsBlock libraryGroupsEnumeration = ^(ALAssetsGroup* group,
                                                                                      BOOL* stop){
                if (group!=nil) {
                    NSString *g=[NSString stringWithFormat:@"%@",group];//获取相簿的组
                    
                    NSString *g1=[g substringFromIndex:16 ] ;
                    NSArray *arr=[[NSArray alloc] init];
                    arr=[g1 componentsSeparatedByString:@","];
                    NSString *g2=[[arr objectAtIndex:0] substringFromIndex:5];
                    if ([g2 isEqualToString:@"Camera Roll"] || [g2 isEqualToString:@"相机胶卷"]) {
                        [group enumerateAssetsUsingBlock:groupEnumerAtion];
                    }
                    [weakSelf reloadDataSource];
                }
                
            };
            if (_library == nil) {
                _library = [[ALAssetsLibrary alloc] init];
            }
            [_library enumerateGroupsWithTypes:ALAssetsGroupAll
                                    usingBlock:libraryGroupsEnumeration
                                  failureBlock:failureblock];

        }
        
    });
}

/**
 @method 播放电影
 */
-(void)playMovie:(NSURL *)urlStr{
    if (_moviePlayer==nil) {
        _moviePlayer = [[MPMoviePlayerViewController alloc]initWithContentURL:urlStr];
        [self addNotification];
    }
    //    [self presentMoviePlayerViewControllerAnimated:self.moviePlayer];
    [self presentViewController:_moviePlayer animated:YES completion:nil];
}

-(void)addNotification{
    NSNotificationCenter *notificationCenter=[NSNotificationCenter defaultCenter];
    
    
    [notificationCenter addObserver:self
                           selector:@selector(mediaPlayerPlaybackFinished:)
                               name:MPMoviePlayerPlaybackDidFinishNotification
                             object:_moviePlayer.moviePlayer];
}

#pragma mark -------------------视频播放结束委托--------------------
    -(void)mediaPlayerPlaybackFinished:(NSNotification *)notification{
        NSLog(@"播放完成.%li",self.moviePlayer.moviePlayer.playbackState);
        [self dismissViewControllerAnimated:YES completion:nil];
        self.moviePlayer = nil;
        
    }
- (void)reloadImagesFromLibrary
{
    [SVProgressHUD showWithStatus:@"加载中.."];
    self.images = [[NSMutableArray alloc] init];
    fullImgs = [NSMutableArray new];
    WS(weakSelf);
 
        dispatch_async(dispatch_get_main_queue(), ^{
            @autoreleasepool {
                ALAssetsLibraryAccessFailureBlock failureblock = ^(NSError *myerror){
                    NSLog(@"相册访问失败 =%@", [myerror localizedDescription]);
                    if ([myerror.localizedDescription rangeOfString:@"Global denied access"].location!=NSNotFound) {
                        NSLog(@"无法访问相册.请在'设置->定位服务'设置为打开状态.");
                    }else{
                        NSLog(@"相册访问失败.");
                    }
                };
                 ALAssetsGroupEnumerationResultsBlock groupEnumerAtion = ^(ALAsset *result,
                                                                          NSUInteger index,
                                                                          BOOL *stop){
                    if (result!=NULL) {
                        if ([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                            DetrumMediaModel *model = [DetrumMediaModel new];
                            model.type = DetrumMediaTypeImg;
                            model.clickType = DetrumImageClickTypeMediaSelect;
                            model.isSeleted = NO;
                            model.currenLocation = [result valueForProperty:ALAssetPropertyLocation];
                            model.duration = [[result valueForProperty:ALAssetPropertyDuration] integerValue];
                            model.camareDate = [result valueForProperty:ALAssetPropertyDate];
                            model.representations = [result valueForProperty:ALAssetPropertyRepresentations];
                            model.dic = [result valueForProperty:ALAssetPropertyURLs];
                            model.url = result.defaultRepresentation.url;
                            model.thumbnail = [UIImage imageWithCGImage:result.thumbnail];
                            model.defaul = result.defaultRepresentation;
                            [weakSelf.images addObject:model];
                        }
                    }
                };
                ALAssetsLibraryGroupsEnumerationResultsBlock libraryGroupsEnumeration = ^(ALAssetsGroup* group, BOOL* stop){
                    if (group!=nil) {
                        NSString *g=[NSString stringWithFormat:@"%@",group];//获取相簿的组
                        
                        NSString *g1=[g substringFromIndex:16 ] ;
                        NSArray *arr=[[NSArray alloc] init];
                        arr=[g1 componentsSeparatedByString:@","];
                        NSString *g2=[[arr objectAtIndex:0] substringFromIndex:5];
                        NSLog(@"g2%@",g2);
                        if ([g2 isEqualToString:@"Camera Roll"] || [g2 isEqualToString:@"相机胶卷"] || [g2 isEqualToString:@"All Photos"]) {
                            [group enumerateAssetsUsingBlock:groupEnumerAtion];
                            NSLog(@"ALAssetsLibraryGroupsEnumerationResultsBlock");
                            [weakSelf reloadDataSource];
                        }
                    }
                 };
                
                if (_library == nil) {
                    _library = [[ALAssetsLibrary alloc] init];
                }
                [_library enumerateGroupsWithTypes:ALAssetsGroupAll
                                        usingBlock:libraryGroupsEnumeration
                                      failureBlock:failureblock];
            }
            
        });
    
}


#pragma clang diagnostic pop
- (void)dealloc {
    
    NSLog(@"DetrumMediaSelectImgViewController释放了");
}
 
@end
