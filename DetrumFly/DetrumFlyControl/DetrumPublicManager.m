//
//  DetrumPublicManager.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "BaseNavController.h"
#import "DetrumSingletonModel.h"
#import "UIColor+RGBConverHex.h"
#import "DetrumTaBBarContent.h"
#import "DetrumPublicManager.h"
static DetrumPublicManager *public = nil;
@implementation DetrumPublicManager

+ (instancetype)shareInstance {
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        public = [DetrumPublicManager new];
    });
    return public;
}

+ (CYLTabBarController *)createRootController {
    CYLTabBarController *tabbar = [[CYLTabBarController alloc] init];
    [DetrumPublicManager setUpTabBar:tabbar];
    DetrumSingletonModel *model = [DetrumSingletonModel shareInstance];
    model.baseTab = tabbar;
    return tabbar;
}

+ (void)setUpTabBar:(CYLTabBarController *)_tabBarController {
    
    HomePageViewController *home = getStoryOfControllerInstance(@"HomePageViewController");
    BaseNavController *baseNav1 = [[BaseNavController alloc] initWithRootViewController:home];
    
    DetrumMediaLibraryViewController *media = getStoryOfControllerInstance(@"DetrumMediaLibraryViewController");
    BaseNavController *baseNav2 = [[BaseNavController alloc] initWithRootViewController:media];
    
    DetrumMyZoneViewController *myZone = getStoryOfControllerInstance(@"DetrumMyZoneViewController");
    BaseNavController *baseNav4 = [[BaseNavController alloc] initWithRootViewController:myZone];
    
    [DetrumPublicManager customizeTabBarForController:_tabBarController];
  
    // set the text color for unselected state
    // 普通状态下的文字属性
    NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    normalAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHex:0x9C9C9D];
    normalAttrs[NSFontAttributeName] = [UIFont fontWithName:iOS8FontName size:13.];
    
    // set the text color for selected state
    // 选中状态下的文字属性
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSForegroundColorAttributeName] = [UIColor colorWithHex:0x007AFF];
    selectedAttrs[NSFontAttributeName] = [UIFont fontWithName:iOS8FontName size:13.];
    // set the text Attributes
    // 设置文字属性
    UITabBarItem *tabBar = [UITabBarItem appearance];
    [tabBar setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    [tabBar setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    
    [_tabBarController setViewControllers:@[
                                            baseNav1,
                                            baseNav2,
                                            baseNav4,
                                            ]];
    
    
}

+ (void)customizeTabBarForController:(CYLTabBarController *)tabBarController {
    
    NSDictionary *dict1 = @{
                            CYLTabBarItemTitle : @"首页",
                            CYLTabBarItemImage : @"device",
                            CYLTabBarItemSelectedImage : @"device copy",
                            };
    NSDictionary *dict2 = @{
                            CYLTabBarItemTitle : @"媒体库",
                            CYLTabBarItemImage : @"media",
                            CYLTabBarItemSelectedImage : @"media copy",
                            };
    NSDictionary *dict3 = @{
                            CYLTabBarItemTitle : @"我",
                            CYLTabBarItemImage : @"me",
                            CYLTabBarItemSelectedImage : @"me copy",
                            };
    NSArray *tabBarItemsAttributes = @[
                                       dict1,
                                       dict2,
                                       dict3
                                       ];
    tabBarController.tabBarItemsAttributes = tabBarItemsAttributes;

    UIColor *titleHighlightedColor = [UIColor darkGrayColor];
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys: titleHighlightedColor, NSForegroundColorAttributeName,iOS8Font(13.f), NSFontAttributeName, nil] forState:UIControlStateSelected];
    UIImage *tabbarimage=[UIImage imageNamed:@"nav_bg"];
    UIImageView *tabBarBackgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,
                                                                                           tabBarController.tabBar.frame.size.width, tabBarController.tabBar.frame.size.height)];
    tabBarBackgroundImageView.image =tabbarimage;
    
    [tabBarController.tabBar insertSubview:tabBarBackgroundImageView atIndex:1];
}


@end
