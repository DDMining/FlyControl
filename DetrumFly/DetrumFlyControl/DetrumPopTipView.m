//
//  DetrumPopTipView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/19.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "UIView+QuicklyChangeFrame.h"
#import "DetrumPopTipView.h"
#import "DetrumMavlinkManager.h"
#define kTipTitleTakeOff @"请确认是否一键起飞？"
#define kTipTitleLeading @"请确认是否一键降落？"

#define kTipTakeOffContent @"飞行器将自动起飞到1.2米高度，请确保飞行器处于地面，请勿在狭窄空间，人群中或建筑旁起飞，手切勿触碰螺旋桨。"

#define kTipLeadingContent @"飞行器将朝向返航点，根据当前返航设置返航，适当调整遥控方向可避免障碍物，短按遥控器上的返航按钮停止返航。"

@implementation DetrumPopTipView

+ (instancetype)shareInstance {
    DetrumPopTipView *tipView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumPopTipView" owner:self options:nil] lastObject];
    tipView.cancel.layer.borderColor = [UIColor grayColor].CGColor;
    tipView.cancel.layer.borderWidth = 1.f;
    tipView.cancel.layer.cornerRadius = 15.f;
    tipView.sureBtn.layer.borderColor = [UIColor grayColor].CGColor;
    tipView.sureBtn.layer.borderWidth = 1.f;
    tipView.sureBtn.layer.cornerRadius = 15.f;
    return tipView;
}

- (void)setType:(PopViewType)type {
    if (type == PopViewTypeTakeOff) {
        _tipLb.text = kTipTitleTakeOff;
        _tipContent.text = kTipTakeOffContent;
        _tipContent.editable = NO;
    } else if (type == PopViewTypeLeading) {
        _tipLb.text = kTipTitleLeading;
        _tipContent.text = kTipLeadingContent;
        _tipContent.editable = NO;
    }
    _type = type;
}

- (void)show:(UIView *)view {
//    DetrumScrollView *scrollView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumScrollView" owner:self options:nil] lastObject];
//    [scrollView setRectWidth:_scrollConfirmView.frame.size.width];
//    [scrollView setRectHeight:_scrollConfirmView.frame.size.height];
//    [scrollView updateConstraintsIfNeeded];
//    
//    scrollView.event = ^() {
//        if (_action) {
//            _action(_type);
//        }
//    };
//    [self.scrollConfirmView addSubview:scrollView];
    
    
    
    
    CGFloat x = CGRectGetWidth(view.frame) / 2 - CGRectGetWidth(self.frame) / 2;
    CGFloat y = CGRectGetHeight(view.frame) / 2 - CGRectGetHeight(self.frame) / 2;
    self.frame = (CGRect){{x,y},self.frame.size};
    self.transform = CGAffineTransformMakeScale(0.2, 0.2);
    
    [view addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.transform = CGAffineTransformMakeScale(1.f, 1.f);
    }];
}

- (IBAction)flyHight:(id)sender {
    
    self.hightLabel.text = [NSString stringWithFormat:@"%.1f",_hightSlider.value];
    
}


- (IBAction)sure:(id)sender {
    
    if (_action) {
        _action(_type,_hightSlider.value);
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.transform = CGAffineTransformScale(self.transform, 0.2f, 0.2f);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}






- (IBAction)cancel:(id)sender {
    [UIView animateWithDuration:0.3 animations:^{
        self.transform = CGAffineTransformScale(self.transform, 0.2f, 0.2f);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
}

@end
