//
//  DetrumNormalTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumNormalTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
