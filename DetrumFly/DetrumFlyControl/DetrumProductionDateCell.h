//
//  DetrumProductionDateCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/7.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

@interface DetrumProductionDateCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *productionDateLb;
@property (weak, nonatomic) IBOutlet UILabel *serialNumLb;

@end
