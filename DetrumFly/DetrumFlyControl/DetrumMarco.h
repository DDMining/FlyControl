
//
//  DetrumMarco.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/6/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef DetrumMarco_h
#define DetrumMarco_h

#include "mavlink.h"
//uint32_t custom_mode = (main_mode<< 16) | (sub_mode << 24);

#define base_mode 81
#define auto_mode_flags (MAV_MODE_FLAG_AUTO_ENABLED | MAV_MODE_FLAG_STABILIZE_ENABLED | MAV_MODE_FLAG_GUIDED_ENABLED)
#define CUSTOM_MODE(main_mode,sub_mode) ((main_mode << 16) | (sub_mode << 24))

//一键起飞
#define AUTOTAKEOFF CUSTOM_MODE(PX4_CUSTOM_MAIN_MODE_AUTO,PX4_CUSTOM_SUB_MODE_AUTO_TAKEOFF)

//一键降落
#define AUTORTL CUSTOM_MODE(PX4_CUSTOM_MAIN_MODE_AUTO,PX4_CUSTOM_SUB_MODE_AUTO_RTL)

#define BASEMODE (base_mode | auto_mode_flags)

//设置参数
#define FLYCONTROLLIGHT @"LED_RGB_MAXBRT"

//俯仰角限制
#define FLYCONTROLROLL   @"MC_ROLL_TC"
#define FLYCONTROLPITCH  @"MC_PITCH_TC"
#define FLYCONTROLYAW    @"MC_YAW_TC"

//滚动角限制
#define FLYCONTROLMAN_P   @"MPC_MAN_P_MAX"
#define FLYCONTROLMAN_R   @"MPC_MAN_R_MAX"

//限高
#define FLYCONTROLRESHEIGHT @"ALT_LIM"

//飞行速度限制
#define FLYCONTROL_XY_VEL @"MPC_XY_VEL_MAX"
#define FLYCONTROL_Z_VEL @"MPC_Z_VEL_MAX"

#endif /* DetrumMarco_h */
