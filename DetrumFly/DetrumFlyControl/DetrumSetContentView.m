//
//  DetrumSetContentView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/24.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumFlyMarco.h"
#import "DetrumSetContentView.h"
#import "DetrumRadioTableViewCell.h"
#import "DetrumSwitchTableViewCell.h"
#import "DetrumNormalTableViewCell.h"
#import "DetrumSlideAndInputTableViewCell.h"

#import "DetrumLabelAndSlideCell.h"
#import "DetrumPullDownMenuCell.h"
#import "DetrumRemoteControlAdjustingCell.h"
#import "DetrumRrmoteView.h"
#import "DetrumRemoteControlView.h"

#import "DetrumCradleHeadTableViewCell.h"
#import "DetrumCameraTableViewCell.h"
#import "DetrumRestPoseTableViewCell.h"
#import "DetrumFourBtnTableViewCell.h"

#import "DetrumTipView.h"
#import "DetrumAutoCalibrationView.h"

#import "DetrumSeleteChannelCell.h"
#import "DetrumMassTableViewCell.h"
#import "DetrumHaveTableTableViewCell.h"

#import "DetrumContentTableViewCell.h"

#import "DetrumProductionDateCell.h"
#import "DetrumBatteryCapacityCell.h"
#import "DetrumLowBatteryAlarmTableViewCell.h"

#import "DetrumOtherTableViewCell.h"

#import "Interface.h"
#import "NSArray+SecurityGetObj.h"
#import "DetrumMavlinkManager.h"

#define kRadioCellIndentifer @"radio"
#define kSwitchCellIndentifer @"switch"
#define kNormalCellIndentifer @"normal"
#define kSlideAndInputCellIndentifer @"sliderAndInput"

#define kLabelAndSlideIdentifier @"labelAndSlide"
#define kPullDownIdentifier @"pulldown"
#define kRemoteControlAdjustingIdentifier @"remoteControlAdjusting"

#define kCradelHeadIdentifier @"CradleHead"
#define kCameraIdentifier @"camera"
#define kRestPostIdentifier @"RestPose"
#define kFourBtnIdentifier @"fourBtn"

#define kSeleteChannelIdentifier @"seletechannel"
#define kHaveTableIdentifier @"havetable"
#define kMassIdentifier @"mass"

#define kContentIdentifier @"content"

#define kLowBatteryIdentifier @"lowbattery"
#define kProductionIdentifier @"production"
#define kBatteryCapacityIdentifier @"batteryCapacity"

#define kOtherIdentifier @"other"

@interface DetrumSetContentView()



@end

@implementation DetrumSetContentView

- (void)awakeFromNib {
    if (_manager == nil) {
        _manager = [DetrumFlyControlManager shareInstance];
    }
    if (_tableView.delegate == nil) {
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.showsVerticalScrollIndicator = NO;
        [self registCell:_currenType];
    }
}

- (void)registCell:(DetrumSetType)type {
    if (type == DetrumSetTypeFlyControl) {
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumRadioTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kRadioCellIndentifer];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumSwitchTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kSwitchCellIndentifer];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumNormalTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kNormalCellIndentifer];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumSlideAndInputTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kSlideAndInputCellIndentifer];
    } else if (type == DetrumSetTypeRemoteControl) {
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumLabelAndSlideCell"
                                               bundle:nil]
         forCellReuseIdentifier:kLabelAndSlideIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumPullDownMenuCell"
                                               bundle:nil]
         forCellReuseIdentifier:kPullDownIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumRemoteControlAdjustingCell"
                                               bundle:nil]
         forCellReuseIdentifier:kRemoteControlAdjustingIdentifier];
    } else if (type == DetrumSetTypePanTilt) {
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumCradleHeadTableViewCell" bundle:nil] forCellReuseIdentifier:kCradelHeadIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumCameraTableViewCell" bundle:nil] forCellReuseIdentifier:kCameraIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumRestPoseTableViewCell" bundle:nil] forCellReuseIdentifier:kRestPostIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumFourBtnTableViewCell" bundle:nil] forCellReuseIdentifier:kFourBtnIdentifier];
    } else if (type == DetrumSetTypeGraphicCommunication) {
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumSeleteChannelCell" bundle:nil] forCellReuseIdentifier:kSeleteChannelIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumHaveTableTableViewCell" bundle:nil] forCellReuseIdentifier:kHaveTableIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumMassTableViewCell" bundle:nil] forCellReuseIdentifier:kMassIdentifier];
    } else if (type == DetrumSetTypeWifi) {
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumContentTableViewCell" bundle:nil] forCellReuseIdentifier:kContentIdentifier];
    } else if (type == DetrumSetTypeBattery) {
        //电池
        _values = [[DetrumMavlinkManager shareInstance] getDataWithType:PARAM_BETTERY];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumLowBatteryAlarmTableViewCell" bundle:nil] forCellReuseIdentifier:kLowBatteryIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumProductionDateCell" bundle:nil] forCellReuseIdentifier:kProductionIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumBatteryCapacityCell" bundle:nil] forCellReuseIdentifier:kBatteryCapacityIdentifier];
    } else {
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumOtherTableViewCell" bundle:nil] forCellReuseIdentifier:kOtherIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumRadioTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kRadioCellIndentifer];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumSwitchTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kSwitchCellIndentifer];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumNormalTableViewCell"
                                               bundle:nil]
         forCellReuseIdentifier:kNormalCellIndentifer];
    }
}

- (void)setCurrenType:(DetrumSetType)currenType {
    if (_currenType != currenType) {
        [self registCell:currenType];
    }
    if (_popView != nil) {
        [_popView backAnimaiton];
    }
    _currenType = currenType;
    [_manager setCurrenType:currenType];
    _tableView.scrollEnabled = YES;
    [self changeArrowX:currenType];
    [self changeTitle:currenType];
    [self.tableView reloadData];
}

- (void)changeTitle:(NSInteger)index {
    switch (index) {
        case DetrumSetTypeFlyControl:
            _setTypeName.text = @"飞控参数设置";
            break;
        case DetrumSetTypeRemoteControl:
            _setTypeName.text = @"遥控器设置";
            break;
        case DetrumSetTypePanTilt:
            _setTypeName.text = @"云台设置";
            break;
        case DetrumSetTypeGraphicCommunication:
            _setTypeName.text = @"图传设置";
            break;
        case DetrumSetTypeWifi:
            _setTypeName.text = @"wifi设置";
            break;
        case DetrumSetTypeBattery:
            _setTypeName.text = @"电池设置";
            break;
        case DetrumSetTypeOther:
            _setTypeName.text = @"其他设置";
            break;
        default:
            break;
    }
}

- (void)changeArrowX:(NSInteger)index {
    _arrowLeadValue.constant = (index * _BtnWidth) + (_BtnWidth / 2) - 5;
}

- (UITableViewCell *)setCell:(NSIndexPath *)index {
    NSDictionary *dataDic = [_manager.dataSource objectAtIndex:index.row];
    if (_currenType == DetrumSetTypeFlyControl) {
        if ([[dataDic objectForKey:@"type"] intValue] == DetrumContentSubViewTypeSwitch) {
            DetrumSwitchTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kSwitchCellIndentifer forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            cell.onOff.selected = YES;
            return cell;
        } else if ([[dataDic objectForKey:@"type"] intValue] == DetrumContentSubViewTypeSlideAddInputView) {
            DetrumSlideAndInputTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kSlideAndInputCellIndentifer forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            cell.sliderRow = index.row;
            if (index.row == 1) {
                cell.inputTf.text = [NSString stringWithFormat:@"%zd",[[NSUserDefaults standardUserDefaults] integerForKey:kResHeight]];
                cell.slider.value = [cell.inputTf.text intValue];
            }else if (index.row == 3){
                cell.inputTf.text = [NSString stringWithFormat:@"%zd",[[NSUserDefaults standardUserDefaults] integerForKey:kXY_VEL]];
                cell.slider.value = [cell.inputTf.text intValue];
            }else if (index.row == 4){
                cell.inputTf.text = [NSString stringWithFormat:@"%zd",[[NSUserDefaults standardUserDefaults] integerForKey:kZ_VEL]];
                cell.slider.value = [cell.inputTf.text intValue];
            }
            return cell;
        } else if ([[dataDic objectForKey:@"type"] intValue] == DetrumContentSubViewTypeRadiu) {
            DetrumRadioTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kRadioCellIndentifer forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            cell.firstRadioLb.text = [dataDic objectForKey:@"firstRadio"];
            cell.secondRadioLb.text = [dataDic objectForKey:@"secondRadio"];
            return cell;
        } else if ([[dataDic objectForKey:@"type"] intValue] == DetrumContentSubViewTypeIndictor) {
            DetrumNormalTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kNormalCellIndentifer forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            return cell;
        }
    } else if (_currenType == DetrumSetTypeRemoteControl) {
        //遥控设置
        if (index.row == 0) {
            DetrumLabelAndSlideCell *cell = [_tableView dequeueReusableCellWithIdentifier:kLabelAndSlideIdentifier forIndexPath:index];
            return cell;
        } else if(index.row == 1){
            DetrumPullDownMenuCell *cell = [_tableView dequeueReusableCellWithIdentifier:kPullDownIdentifier forIndexPath:index];
            return cell;
        } else {
            DetrumRemoteControlAdjustingCell *cell = [_tableView dequeueReusableCellWithIdentifier:kRemoteControlAdjustingIdentifier forIndexPath:index];
            cell.event = ^(){
                DetrumRrmoteView *view = [[[NSBundle mainBundle] loadNibNamed:@"DetrumRrmoteView" owner:self options:nil] lastObject];
                view.frame = self.bounds;
                [self addSubview:view];
//                DetrumRemoteControlView *view = [[[NSBundle mainBundle] loadNibNamed:@"DetrumRemoteControlView" owner:self options:nil] lastObject];
//                view.frame = self.bounds;
//                [self addSubview:view];
            };
            return cell;
        }
    } else if (_currenType == DetrumSetTypePanTilt) {
        //云台设置
        if (index.row == 0) {
            DetrumCradleHeadTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kCradelHeadIdentifier forIndexPath:index];
            cell.name.text = @"云台模式";
                return cell;
        } else if (index.row == 1) {
            DetrumCameraTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kCameraIdentifier forIndexPath:index];
            cell.type = DetrumCameraTypeOne;
            return cell;
        } else if (index.row == 2) {
            DetrumCameraTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kCameraIdentifier forIndexPath:index];
            cell.type = DetrumCameraTypeTwo;
            return cell;
        } else if (index.row == 3) {
            DetrumRestPoseTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kRestPostIdentifier forIndexPath:index];
            return cell;
        } else {
            DetrumFourBtnTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kFourBtnIdentifier forIndexPath:index];
            cell.callback = ^(DetrumFourBtnclickType type) {
                [self showDetrumTipView:type];
            };
            return cell;
        }
    } else if (_currenType == DetrumSetTypeGraphicCommunication) {
        //图传设置
        if (index.row == 0) {
            DetrumSeleteChannelCell *cell = [_tableView dequeueReusableCellWithIdentifier:kSeleteChannelIdentifier forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            return cell;
        } else if (index.row == 1) {
            DetrumHaveTableTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kHaveTableIdentifier forIndexPath:index];
            
            return cell;
        } else {
            DetrumMassTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMassIdentifier forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            return cell;
        }
        
    } else if (_currenType == DetrumSetTypeWifi) {
        //wifi设置
        DetrumContentTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kContentIdentifier forIndexPath:index];
        _tableView.scrollEnabled = NO;
        return cell;
    } else if (_currenType == DetrumSetTypeBattery) {
        //电池设置
        if (index.row == 0) {
            DetrumLowBatteryAlarmTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kLowBatteryIdentifier forIndexPath:index];
            return cell;
        } else if (index.row == 1) {
            DetrumProductionDateCell *cell = [_tableView dequeueReusableCellWithIdentifier:kProductionIdentifier forIndexPath:index];
            cell.productionDateLb.text = [_values objectSecurityAtIndex:1];
            cell.serialNumLb.text = [_values  objectSecurityAtIndex:0];
            return cell;
        } else {
            DetrumBatteryCapacityCell *cell = [_tableView dequeueReusableCellWithIdentifier:kBatteryCapacityIdentifier forIndexPath:index];
            if (_values.count != 0) {
                cell.batterys = @[[_values objectSecurityAtIndex:9],[_values objectSecurityAtIndex:10],[_values objectSecurityAtIndex:11],[_values objectSecurityAtIndex:12]];
                
                cell.batteryValue = [[_values objectSecurityAtIndex:6] doubleValue] / [[_values objectSecurityAtIndex:5] doubleValue];
                cell.battryLife.text = (NSString *)[_values objectSecurityAtIndex:(int)(_values.count - 1)];
                cell.rechargeNum.text =  FLOATNUMCONVERSTR(_values, 8);
                cell.quantityOfElectricity.text = FLOATNUMCONVERSTR(_values, 6);
            }
            return cell;
        }
    } else {
        //其他设置
        if (index.row == 0) {
            DetrumRadioTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kRadioCellIndentifer forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            return cell;
        } else if (index.row == 1 || index.row == 2 || index.row == 4 || index.row == 5 || index.row == 8) {
            DetrumSwitchTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kSwitchCellIndentifer forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            return cell;
        } else if (index.row == 3 || index.row == 6) {
            DetrumOtherTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kOtherIdentifier forIndexPath:index];
            cell.name.hidden = YES;
            cell.lastName.hidden = YES;
            cell.midName.hidden = NO;
            cell.midName.text = [dataDic objectForKey:@"name"];
            return cell;
        } else if (index.row == 7) {
            DetrumOtherTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kOtherIdentifier forIndexPath:index];
            cell.name.hidden = NO;
            cell.lastName.hidden = YES;
            cell.midName.hidden = YES;
            cell.name.text = [dataDic objectForKey:@"name"];
            return cell;
        } else if (index.row == 9) {
            DetrumNormalTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kNormalCellIndentifer forIndexPath:index];
            cell.name.text = [dataDic objectForKey:@"name"];
            return cell;
        }
    }
    return nil;
}

#pragma mark - tableView Delegate & dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _manager.dataSource.count == 0 ? 0 : _manager.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self setCell:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_currenType == DetrumSetTypeFlyControl) {
        return 36;
    } else if (_currenType == DetrumSetTypeRemoteControl) {
        if (indexPath.row == 0) {
            return 44;
        } else if (indexPath.row == 1){
            return 121;
        } else{
            return 44;
        }
    } else if (_currenType == DetrumSetTypePanTilt) {
        return 38;
    } else if (_currenType == DetrumSetTypeGraphicCommunication) {
        if (indexPath.row == 1) {
            return 123;
        } else {
            return 38;
        }
    } else if (_currenType == DetrumSetTypeWifi) {
        return 500;
    } else if (_currenType == DetrumSetTypeBattery) {
        if (indexPath.row==0 || indexPath.row == 1) {
            return 38;
        } else {
            return 129;
        }
    } else if (_currenType == DetrumSetTypeOther) {
        return 38;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //屏蔽点击事件
    if (_currenType == DetrumSetTypeRemoteControl ||
        _currenType == DetrumSetTypePanTilt ||
        _currenType == DetrumSetTypeGraphicCommunication ||
        _currenType == DetrumSetTypeWifi) {
        return;
    }
    if (![[cell valueForKey:@"_isClick"] boolValue]) {
        return;
    } else {
        if ( [_manager.seleteDatas objectForKey:[cell valueForKeyPath:@"_name.text"]] == nil) {
            return;
        }
    }
    
   
//    DFLog(@"%@",[_manager.seleteDatas objectForKey:[cell valueForKeyPath:@"_name.text"]]);
    DetrumBaseView *view = [[[NSBundle mainBundle] loadNibNamed:[_manager.seleteDatas objectForKey:[cell valueForKeyPath:@"_name.text"]] owner:self options:nil] lastObject];
    [view setValue:[cell valueForKeyPath:@"_name.text"] forKeyPath:@"_name.text"];
    _popView = view;
    [self showSubView:view];
}

- (void)showSubView:(UIView *)view {
    [view setValue:_ContentView forKey:@"_animationViw"];
    view.frame = _ContentView.frame;
    [view updateConstraintsIfNeeded];
    [self addSubview:view];
    [UIView animateWithDuration:0.3f animations:^{
        _ContentView.alpha = 0.1f;
    } completion:^(BOOL finished) {
        _ContentView.hidden = YES;
    }];
}

- (void)showDetrumTipView:(DetrumFourBtnclickType)type {
    if (type == DetrumFourBtnclickTypeAutoCorrection) {
        DetrumAutoCalibrationView *calibration = [[[NSBundle mainBundle] loadNibNamed:@"DetrumAutoCalibrationView" owner:self options:nil] lastObject];
        [self initTipView:calibration];
    } else {
        DetrumTipView *view = [[[NSBundle mainBundle] loadNibNamed:@"DetrumTipView" owner:self options:nil] lastObject];
        if (type == DetrumFourBtnclickTypeRoll) {
            view.title.text = @"横滚轴微调";
        } else if (type == DetrumFourBtnclickTypeLpitch) {
            view.title.text = @"俯仰轴微调";
        } else if (type == DetrumFourBtnclickTypeYaw) {
            view.title.text = @"偏航轴微调";
        }
        [self initTipView:view];
    }
}

- (void)initTipView:(UIView *)view {
    CGFloat x = _ContentView.frame.size.width / 2 - CGRectGetWidth(view.frame) / 2;
    CGFloat y = _ContentView.frame.size.height / 2 - CGRectGetHeight(view.frame) / 2;
    [view setRectX:x];
    [view setRectY:y];
    [view updateConstraintsIfNeeded];
    view.transform = CGAffineTransformMakeScale(0.7, 0.7);
    view.alpha = 0.6;
    [self.ContentView addSubview:view];
    [UIView animateWithDuration:0.3f animations:^{
        view.transform = CGAffineTransformMakeScale(1, 1);
        view.alpha = 1.f;
    } ];
}

@end
