//
//  DetrumCompassCheckView.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/8/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NSTimer+YYAdd.h>

typedef NS_ENUM(NSInteger, DetrumCompassViewColor) {
    DetrumCompassViewColorRed,
    DetrumCompassViewColorGreen,
    DetrumCompassViewColorGray,
    
};

@interface DetrumCompassCheckView : UIView {
    NSTimer *timer;
    NSInteger currenIndex;
    NSInteger lastIndex;
    NSDictionary *viewDic;
}
/** 水平放置正面朝上视图 */
@property (weak, nonatomic) IBOutlet UIView *horizontalView;
/** 水平放置正面朝上 */
@property (weak, nonatomic) IBOutlet UIImageView *horizontalPlace;
@property (weak, nonatomic) IBOutlet UIImageView *horizontaLeftTurn;
@property (weak, nonatomic) IBOutlet UIImageView *horizontaRightTurn;




/** 翻转水平放置 */
@property (weak, nonatomic) IBOutlet UIView *turnOutView;
@property (weak, nonatomic) IBOutlet UIImageView *turnOut;
@property (weak, nonatomic) IBOutlet UIImageView *turnOutLeftTrun;
@property (weak, nonatomic) IBOutlet UIImageView *turnOutRightTurn;




/** 机头朝上放置 */
@property (weak, nonatomic) IBOutlet UIView *pointingUpView;
@property (weak, nonatomic) IBOutlet UIImageView *pointingUp;
@property (weak, nonatomic) IBOutlet UIImageView *pointingUpLeftTurn;
@property (weak, nonatomic) IBOutlet UIImageView *pointingUpRightTurn;



/** 机头朝下放置 */
@property (weak, nonatomic) IBOutlet UIView *pointingDownView;
@property (weak, nonatomic) IBOutlet UIImageView *pointingDown;

@property (weak, nonatomic) IBOutlet UIImageView *pointingDownLeftTurn;

@property (weak, nonatomic) IBOutlet UIImageView *pointingDownRightTurn;


/** 左侧旋转放置 */
@property (weak, nonatomic) IBOutlet UIView *leftSideTurnOutView;
@property (weak, nonatomic) IBOutlet UIImageView *leftSideTurnOut;
@property (weak, nonatomic) IBOutlet UIImageView *leftSideLeftTurn;
@property (weak, nonatomic) IBOutlet UIImageView *leftSideRightTurn;



/** 右侧旋转放置 */
@property (weak, nonatomic) IBOutlet UIView *rightSideTurnOutViwe;

@property (weak, nonatomic) IBOutlet UIImageView *rightSideTurnOut;
@property (weak, nonatomic) IBOutlet UIImageView *rightSideLeftTurn;
@property (weak, nonatomic) IBOutlet UIImageView *rightSideRightTurn;




@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (assign, nonatomic) DetrumCompassViewColor imuColor;



@end
