//
//  DetrumSensorView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseView.h"
#import "DetrumGyroscopeTableViewCell.h"
#import "DetrumBarometerTableViewCell.h"
#import "DetrumCalibrationTableViewCell.h"
@interface DetrumSensorView : DetrumBaseView <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *paramters;
@property (strong, nonatomic) NSMutableArray *tuoluoyiArrs;
@property (strong, nonatomic) NSMutableArray *jisujiArrs;
@property (strong, nonatomic) NSMutableArray *zhinanzhenArrs;
@property (strong, nonatomic) NSString *barometer;
@property (strong, nonatomic) NSTimer *timer;
@end