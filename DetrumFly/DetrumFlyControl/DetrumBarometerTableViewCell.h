//
//  DetrumBarometerTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

@interface DetrumBarometerTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UITextField *barometerTf;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
