//
//  UITextField+Helper.m
//  0元拍
//
//  Created by wangfang on 15/1/5.
//  Copyright (c) 2015年 baishan. All rights reserved.
//

#import "UITextField+Helper.h"

@implementation UITextField (Helper)
+ (instancetype)textFieldWithFrame:(CGRect)frame placeholder:(NSString*)placeholder borderStyle:(UITextBorderStyle)borderStyle keyboardType:(UIKeyboardType)keyboardType clearButtonMode:(UITextFieldViewMode)clearButtonMode delegate:(id<UITextFieldDelegate>)delegate;
{
    UITextField* textField = [[UITextField alloc]init];
    textField.frame = frame;
    textField.placeholder = placeholder;
    textField.borderStyle = borderStyle;
    textField.keyboardType = keyboardType;
    textField.clearButtonMode = clearButtonMode;
    textField.delegate = delegate;
    return textField;
}
@end
