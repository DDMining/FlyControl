//
//  DetrumMediaLibraryViewController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetrumBaseViewController.h"
@interface DetrumMediaLibraryViewController : DetrumBaseViewController

@property (nonatomic, assign) BOOL isUpdateImg;
@property (nonatomic, assign) BOOL isUpdateVedio;
@end
