//
//  DetrumCollectionReusableHeadView.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/19.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumCollectionReusableHeadView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *date;
- (void)setDateText:(NSString *)text;
@end
