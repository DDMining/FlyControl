//
//  DetrumGyroscopeView.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/7/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumGyroscopeView.h"
#import "Interface.h"
#import "DetrumMavlinkManager.h"

@interface DetrumGyroscopeView()

@property (weak, nonatomic) IBOutlet UIProgressView *progress;
@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak, nonatomic) IBOutlet UIButton *back;
@property (weak, nonatomic) IBOutlet UIButton *startbtn;

@end

@implementation DetrumGyroscopeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(progressChange:) name:PARAM_STATUE_TEXT object:nil];
    self.label.text = @"校准进度";
}


- (void)progressChange:(NSNotification *)noti{
    
//    NSLog(@"noti:%@",noti.userInfo);
    NSString *str = noti.userInfo[PARAM_STATUE_TEXT];
    if ([str hasPrefix:@"[cal] progress"]) {
        if (str.length == 18) {
            str = [str substringWithRange:NSMakeRange(str.length - 2, 1)];
        }else if (str.length == 19){
            str = [str substringWithRange:NSMakeRange(str.length - 3, 2)];
        }else if (str.length == 20){
            str = [str substringWithRange:NSMakeRange(str.length - 4, 3)];
        }
        
    }else if ([str isEqualToString:@"[cal] calibration done: gyro"]){
        
        self.label.text = @"陀螺仪校准完成";
        self.progress.progress = 1;
        [[DetrumMavlinkManager shareInstance] stopCalibration];

        
    }
    
    float progressValue = [str floatValue];
    
    self.progress.progress = progressValue / 100;
    
    
//    NSLog(@"str:%@",str);
}

- (IBAction)start:(id)sender {
    
     [[DetrumMavlinkManager shareInstance] startCalibration:1];
    self.label.text = @"校准进度";
}



- (IBAction)backBtn:(id)sender {
    
    [self removeFromSuperview];
    [[DetrumMavlinkManager shareInstance] stopCalibration];
    
}


@end
