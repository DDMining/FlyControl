//
//  DetrumShopListViewController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/20.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseViewController.h"

@interface DetrumShopListViewController : DetrumBaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
