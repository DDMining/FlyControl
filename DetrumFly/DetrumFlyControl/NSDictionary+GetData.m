//
//  NSDictionary+GetData.m
//  JxCarios
//
//  Created by xcq on 15/2/2.
//  Copyright (c) 2015年 xiongchuanqi. All rights reserved.
//

#import "NSDictionary+GetData.h"

@implementation NSDictionary (GetData)

- (id)customObjectForkey:(id)key{
    id obj = [self objectForKey:key];
    if (obj && obj != [NSNull null]) {
        return obj;
    }
    return @"";
}


@end
