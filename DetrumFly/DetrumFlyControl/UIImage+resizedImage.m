//
//  UIImage+resizedImage.m
//  SpellBus
//
//  Created by wangfang on 15/2/3.
//  Copyright (c) 2015年 publicNetwork. All rights reserved.
//

#import "UIImage+resizedImage.h"

@implementation UIImage (resizedImage)

+ (UIImage *)imageWithName:(NSString *)name
{
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7) {
        NSString *newName = [name stringByAppendingString:@"_os7"];
        UIImage *image = [UIImage imageNamed:newName];
        if (image == nil) { // 没有_os7后缀的图片
            image = [UIImage imageNamed:name];
        }
        return image;
    }
    
    // 非iOS7
    return [UIImage imageNamed:name];
}

+ (UIImage *)resizedImageWithName:(NSString *)name
{
    return [self resizedImageWithName:name left:0.5 top:0.5];
}

+ (UIImage *)resizedImageWithName:(NSString *)name left:(CGFloat)left top:(CGFloat)top
{
    UIImage *image = [self imageNamed:name];
    return [image stretchableImageWithLeftCapWidth:image.size.width * left topCapHeight:image.size.height * top];
}

//+ (UIImage *)resizedImageWithName:(NSString *)name top:(CGFloat)top bottom:(CGFloat)bottom
//{
//    UIImage *image = [self imageNamed:name];
//    return [image stretchableImageWith];
//}

+ (UIImage *)resizedImage:(NSString *)name
{
    return [self resizedImageWithName:name left:0.5 top:0.5];
}


@end
