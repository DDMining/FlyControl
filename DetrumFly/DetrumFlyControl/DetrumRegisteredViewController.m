//
//  DetrumRegisteredViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "Common.h"
#import "DetrumRegisteredViewController.h"
#define kGetVerifcationSecond 60

@interface DetrumRegisteredViewController ()
@property (weak, nonatomic) IBOutlet UITextField *emailTf;
@property (weak, nonatomic) IBOutlet UITextField *passWordTf;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwdTf;
@property (weak, nonatomic) IBOutlet UIButton *registeredBtn;
@property (weak, nonatomic) IBOutlet UITextField *emailVerificatoinTf;
@property (weak, nonatomic) IBOutlet UIButton *getVerificationBtn;
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) NSInteger second;

@end

@implementation DetrumRegisteredViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"注册";
    [self showNavBarWithRightTitle:nil
                    andRightAction:nil
                     andLeftAction:nil
                 andRightBtnIsShow:NO];
//    [self initTimer];
}

- (void)initTimer {
    if (_timer == nil) {
        _second = kGetVerifcationSecond;
        _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(refreshVerificationBtn) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
         [_timer fire];
    } else {
        [self destructionTimer];
    }
}


- (void)destructionTimer {
    [_timer invalidate]; _timer = nil;
    _second = kGetVerifcationSecond;
    
}


- (void)refreshVerificationBtn {
    _second--;
    if (_second <= 0) {
        _getVerificationBtn.enabled = YES;
        [_getVerificationBtn setBackgroundColor:[UIColor colorWithHex:0x0A60FE]];
        [_getVerificationBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _getVerificationBtn.userInteractionEnabled = YES;
        [self destructionTimer];
    } else {
        _getVerificationBtn.enabled = NO;
        [_getVerificationBtn setBackgroundColor:[UIColor lightGrayColor]];
             //设置界面的按钮显示 根据自己需求设置
            //NSLog(@"____%@",strTime);
            [_getVerificationBtn setTitle:[NSString stringWithFormat:@"获取中(%zds)",_second] forState:UIControlStateDisabled];
        _getVerificationBtn.userInteractionEnabled = NO;
 
//        _getVerificationBtn.titleLabel.text = [NSString stringWithFormat:@"获取中(%zds)",_second];
    }
}

- (IBAction)getVerificationEvent:(id)sender {
    if (![Common isValidateEmail:_emailTf.text]) {
        [SVProgressHUD showErrorWithStatus:@"请输入正确的邮箱！"];
        return;
    }
    [self initTimer];
    [InterfaceNetManager RequestGetVerifyCode:_emailTf.text compleiton:^(BOOL isSucceed, NSString *message, id data, DFError *error) {
        if (isSucceed) {
            NSLog(@"%@",data);
            [SVProgressHUD showSuccessWithStatus:@"验证码发送成功!"];
        } else {
            [SVProgressHUD showErrorWithStatus:error.failMsg];
        }
    }];
}

- (IBAction)registeredEvent:(id)sender {
    if (![Common isValidateEmail:_emailTf.text]) {
        [SVProgressHUD showErrorWithStatus:@"请输入正确的邮箱！"];
        return;
    }
    
    if (_emailVerificatoinTf.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入验证码！"];
        return;
    }
    if (_emailVerificatoinTf.text.length > 5) {
        [SVProgressHUD showErrorWithStatus:@"验证码只能输入5位！"];
        return;
    }
    if (_passWordTf.text.length == 0 ) {
        [SVProgressHUD showErrorWithStatus:@"请输入密码！"];
        return;
    }
    
    if (_confirmPwdTf.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入确认密码！"];
        return;
    }

    
    [InterfaceNetManager RequestRegisterOfUserName:_emailTf.text password:_passWordTf.text verify:[_emailVerificatoinTf.text integerValue] compleiton:^(BOOL isSucceed, NSString *message, id data, DFError *error) {
        if (isSucceed) {
            [SVProgressHUD showSuccessWithStatus:@"恭喜您！注册成功"];
        } else {
            [SVProgressHUD showErrorWithStatus:error.failMsg];
        }
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
