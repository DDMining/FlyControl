//
//  DetrumPopTipView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/19.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumScrollView.h"

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, PopViewType) {
    PopViewTypeLeading,
    PopViewTypeTakeOff,
};

typedef void(^PopViewScrollAction)(PopViewType type , float hight);

@interface DetrumPopTipView : UIView
@property (assign, nonatomic) PopViewType type;
@property (weak, nonatomic) IBOutlet UILabel *tipLb;
@property (weak, nonatomic) IBOutlet UITextView *tipContent;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UILabel *hight;
@property (weak, nonatomic) IBOutlet UILabel *hightLabel;
@property (weak, nonatomic) IBOutlet UISlider *hightSlider;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;


@property (copy, nonatomic) PopViewScrollAction action;
+ (instancetype)shareInstance;
- (void)show:(UIView *)view ;
@end
