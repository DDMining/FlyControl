//
//  InterfaceNetManager.h
//  JiuLe
//
//  Created by xcq on 15/10/31.
//  Copyright © 2015年 Fine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetManager.h"
#import "NetManagerApi.h"
#import "DFError.h"

typedef void (^interfaceManagerBlock)(BOOL isSucceed, NSString *message, id data ,DFError *error);
typedef BOOL (^interfaceManagerBlockReturnBOl)(BOOL isSucceed, NSString *message, id data ,DFError *error);

@interface InterfaceNetManager : NSObject

/**
 *  @brief  登陆
 *
 *  @param userName   用户名
 *  @param pwd        密码
 *  @param completion 回调函数
 */
+ (void)RequestLogin:(NSString *)userName
            PassWord:(NSString *)pwd
          completion:(interfaceManagerBlock )completion;

/**
 *  @brief  注册
 *
 *  @param userName   用户名
 *  @param pwd        密码
 *  @param completion 回调函数
 */
+ (void)RequestRegisterOfUserName:(NSString *)username
                         password:(NSString *)password
                           verify:(NSInteger)code
                       compleiton:(interfaceManagerBlock)completion;
/**
 *  @brief  注册
 *
 *  @param email   邮箱
 *  @param completion 回调函数
 */
+ (void)RequestGetVerifyCode:(NSString *)email
                  compleiton:(interfaceManagerBlock)completion;

@end
