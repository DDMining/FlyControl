//
//  NSString+Check.m
//  MyCheZan
//
//  Created by lovecar on 14-7-30.
//  Copyright (c) 2014年 chezan. All rights reserved.
//

#import "NSString+Check.h"

@implementation NSString (Check)

-(BOOL)checkPhoneNumInput{
    
    NSString * mobile = @"^1\\d{10}$";
    
//    NSString * cm = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
//    
//    NSString * cu = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
//    
//    NSString * ct = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobile];
//    NSPredicate *regextestCm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cm];
//    NSPredicate *regextestCu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", cu];
//    NSPredicate *regextestCt = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", ct];
    BOOL res1 = [regextestMobile evaluateWithObject:self];
//    BOOL res2 = [regextestCm evaluateWithObject:self];
//    BOOL res3 = [regextestCu evaluateWithObject:self];
//    BOOL res4 = [regextestCt evaluateWithObject:self];
    
    if (res1)
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

- (BOOL)checkEmailInput {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *regexEmail = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];

    return [regexEmail evaluateWithObject:self];
}

@end
