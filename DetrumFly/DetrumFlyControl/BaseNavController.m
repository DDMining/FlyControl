//
//  BaseNavController.m
//  0元拍
//
//  Created by xcq on 15/1/4.
//  Copyright (c) 2015年 baishan. All rights reserved.
//
// 1.判断是否为iOS7



#define iOS7Later ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)

#define iOS8Later ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0)

#define iOS7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0) && ([[UIDevice currentDevice].systemVersion doubleValue] <= 7.9)

#define iOS8 ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0) && ([[UIDevice currentDevice].systemVersion doubleValue] <= 8.9)

#import "BaseNavController.h"
#import "UIColor+RGBConverHex.h"
#import "DetrumPreviewViewController.h"
@interface BaseNavController ()<UIGestureRecognizerDelegate>{
}
@property (nonatomic , retain) UIPanGestureRecognizer *swipeRecognizer;

@end

@implementation BaseNavController

/**
 *  第一次使用这个类的时候会调用(1个类只会调用1次)
 */
+ (void)initialize
{
  
    // 1.设置导航栏主题
    [self setupNavBarTheme];
    
    // 2.设置导航栏按钮主题
    [self setupBarButtonItemTheme];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSwipeRecognizer];
}

#pragma mark 添加右滑手势
- (void)addSwipeRecognizer
{
    // 获取系统自带滑动手势的target对象
//    id target = self.interactivePopGestureRecognizer.delegate;
    
    // 创建全屏滑动手势，调用系统自带滑动手势的target的action方法
//    _swipeRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    
    // 设置手势代理，拦截手势触发
//    _swipeRecognizer.delegate = self;
    
    // 给导航控制器的view添加全屏滑动手势
//    [self.view addGestureRecognizer:_swipeRecognizer];
    
    // 禁止使用系统自带的滑动手势
    
    self.interactivePopGestureRecognizer.enabled = NO;
}

- (void)removeSwipeRecognizer {
//    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
//    [[self view]removeGestureRecognizer:_swipeRecognizer];
}

#pragma mark 返回上一级
- (void)returnLast
{
    // 最低控制器无需返回
    if (self.viewControllers.count <= 1) return;
    
    // pop返回上一级
    [self popViewControllerAnimated:YES];
}
/**
 *  设置导航栏按钮主题
 */
+ (void)setupBarButtonItemTheme
{
    UIBarButtonItem *item = [UIBarButtonItem appearance];
   
    // 设置文字属性
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
    textAttrs[NSFontAttributeName] = iOS8Font((iOS7 ? 18 : 15));
    [item setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    [item setTitleTextAttributes:textAttrs forState:UIControlStateHighlighted];
    
    NSMutableDictionary *disableTextAttrs = [NSMutableDictionary dictionary];
    disableTextAttrs[NSForegroundColorAttributeName] =  [UIColor lightGrayColor];
    [item setTitleTextAttributes:disableTextAttrs forState:UIControlStateDisabled];
}

/**
 *  设置导航栏主题
 */
+ (void)setupNavBarTheme
{//用MJExtension框架，因为需要继承，会和RLMObject发生冲突
    
    // 取出appearance对象
    UINavigationBar *navBar = [UINavigationBar appearance];

    // 设置背景
    if (iOS7Later) {
        [navBar setBackgroundImage:[UIImage imageNamed:@"box"] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
//        navBar.barTintColor = [UIColor colorWithHex:0x0f94cc];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        if(iOS8Later && [UINavigationBar conformsToProtocol:@protocol(UIAppearanceContainer)]) {
//            [[UINavigationBar appearance] setTranslucent:YES];
        }
    }
    [navBar setTitleTextAttributes: @{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // 注意：只有非根控制器才有滑动返回功能，根控制器没有。
    // 判断导航控制器是否只有一个子控制器，如果只有一个子控制器，肯定是根控制器
    if (self.childViewControllers.count == 1) {
        // 表示用户在根控制器界面，就不需要触发滑动手势，
        return NO;
    }
    UIViewController *last  = self.childViewControllers.lastObject;
    if ([last isKindOfClass:[DetrumPreviewViewController class]]) {
        return NO;
    }
//    //地图屏蔽手势
//    if ([last isKindOfClass:[MapSearchViewController class]] || [(UIView *)last.view.subviews.lastObject isKindOfClass:[RouteSearchMapView class]] || [last isKindOfClass:[AddressSeleteBaseMapController class]] || [last isKindOfClass:[S9DemandCommitDetailViewController class]]) {
//        return NO;
//    }
    return YES;
}


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    if([self.topViewController class] == [viewController class]) {
        return;
    }
    [super pushViewController:viewController animated:animated];
}
@end
