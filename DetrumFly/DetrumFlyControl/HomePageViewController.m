//
//  HomePageViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumFlyHistoryData.h"
#import "UIColor+RGBConverHex.h"
#import "HomePageViewController.h"
#import "DetrumShopListViewController.h"
#import "DetrumShopDetailViewController.h"
#import "DetrumFlyControlHomePageController.h"
#import "DetrumTechnologySupportViewController.h"

typedef NS_ENUM(NSInteger,HomePageCurrenMode) {
    /** 用户拥有的机型 */
    HomePageCurrenModeUserHaving,
    /** 更多靓机 */
    HomePageCurrenModeMoreUav,
};

#define kMyImgTag 0x225
#define kMoreImgTag 0x325
#define kImgCount 4

#define kImgWidth \
CGRectGetWidth(_UAVScrollView.frame)

#define kImgHeight \
CGRectGetHeight(_UAVScrollView.frame)

@interface HomePageViewController () <UIScrollViewDelegate,UINavigationControllerDelegate>
@property (nonatomic, strong) NSMutableArray *myHaveSource;
@property (nonatomic, strong) NSMutableArray *myHaveImgSource;
@property (nonatomic, strong) NSMutableArray *moreSource;
@property (nonatomic, strong) NSMutableArray *moreImgSource;
@property (nonatomic, assign) HomePageCurrenMode currenMode;

@end

@implementation HomePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showNavBarLeftImgAndRightTitle:nil
                            andRightImge:nil
                          andRightAction:nil
                           andLeftAction:nil
                             andLeftImge:nil];

    [self initUI];
    [self initRac];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self animationLabel];
}

- (void)initUI {
    
    _checkModelView.layer.masksToBounds = YES;
    _checkModelView.layer.cornerRadius = 15;
    _checkModelView.layer.borderColor = [UIColor colorWithHex:0xCCCCCC].CGColor;
    _checkModelView.layer.borderWidth = 1;
    
    _userHaveDeviceBtn.layer.masksToBounds = YES;
    _userHaveDeviceBtn.layer.cornerRadius = 15;
    [_userHaveDeviceBtn setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
    [_userHaveDeviceBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _moreFlyTypeBtn.layer.masksToBounds = YES;
    _moreFlyTypeBtn.layer.cornerRadius = 15;
    [_moreFlyTypeBtn setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
    [_moreFlyTypeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UILabel *scroll = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, CGRectGetWidth(_UAVDetailView.frame), CGRectGetHeight(_UAVDetailView.frame))];
    _scrollLabel = scroll;
    scroll.text = @"打开IOC模式，锁定航向后，可以拍出一些超出想象的镜头。打开IOC模式，锁定航向后，可以拍出一些超出想象的镜头。";

    CGSize size = [self sizeWithText:scroll.text font:scroll.font];
    scroll.frame = CGRectMake(10, 0, size.width, size.height);
    [_UAVDetailView addSubview:scroll];
    
    [self initTitleView];
}

-(CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxW:(CGFloat)maxW
{
    NSMutableDictionary *attrs=[NSMutableDictionary dictionary];
   attrs[NSFontAttributeName]=font;
        
    CGSize maxSize=CGSizeMake(maxW, MAXFLOAT);
   return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font
{
    return [self sizeWithText:text font:font maxW:MAXFLOAT];
}

- (void)animationLabel {
    if (_scrollLabel.frame.origin.x != 10) {
        _scrollLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width,
                                        0,
                                        CGRectGetWidth(_scrollLabel.frame),
                                        CGRectGetHeight(_scrollLabel.frame));
    }
    CGFloat speed = CGRectGetWidth(_scrollLabel.frame) / 8;
    CGRect frame = _scrollLabel.frame;
    [UIView beginAnimations:@"testAnimation" context:NULL];
    [UIView setAnimationDuration:8];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationRepeatAutoreverses:YES];
    [UIView setAnimationRepeatCount:999999];
    frame = _scrollLabel.frame;
    frame.origin.x = -(speed + 20);
    _scrollLabel.frame = frame;
    [UIView commitAnimations];
}

- (void)initRac {
    [[[self.userHaveDeviceBtn rac_signalForControlEvents:UIControlEventTouchUpInside] filter:^BOOL(id value) {
        return !self.userHaveDeviceBtn.selected;
    }] subscribeNext:^(id x) {
        [self setBtnSelect:_userHaveDeviceBtn andSelect:YES];
        [self setCurrenMode:HomePageCurrenModeUserHaving];
    }];
    
    [[[self.moreFlyTypeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] filter:^BOOL(id value) {
        return !self.moreFlyTypeBtn.selected;
    }] subscribeNext:^(id x) {
        [self setBtnSelect:_moreFlyTypeBtn andSelect:YES];
        [self setCurrenMode:HomePageCurrenModeMoreUav];
    }];
    
    [[self.joinBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self pushFlyControlPage];
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    if (_myHaveImgSource.count == 0 || _moreImgSource.count == 0) {
        self.currenMode = HomePageCurrenModeUserHaving;
    }
    [super viewDidAppear:animated];
}

- (void)initTitleView {
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LOGO-1"]];
    img.frame = CGRectMake(0, 0, 110, 20);
    img.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = img;
}

- (void)rightBtnAction:(UIButton *)sender {
    DetrumTechnologySupportViewController *tech = [[DetrumTechnologySupportViewController alloc] initWithNibName:@"DetrumTechnologySupportViewController" bundle:nil];
    [self.navigationController pushViewController:tech animated:YES];
}

- (void)leftBtnAction:(UIButton *)sender {
    DetrumFlyHistoryData *flyHistory = [[DetrumFlyHistoryData alloc] initWithNibName:@"DetrumFlyHistoryData" bundle:nil];
    [self presentViewController:flyHistory animated:YES completion:nil];
}

- (void)setCurrenMode:(HomePageCurrenMode)currenMode {
    _currenMode = currenMode;
    if (currenMode == HomePageCurrenModeUserHaving) {
        [self setBtnSelect:_userHaveDeviceBtn andSelect:YES];
    } else {
        [self setBtnSelect:_moreFlyTypeBtn andSelect:YES];
    }
    [self netRequest];
}

- (void)netRequest {
    if (_myHaveSource == nil) {
        _myHaveSource = [NSMutableArray new];
        _myHaveImgSource = [NSMutableArray new];
    }
    
    if (_moreSource == nil) {
        _moreSource = [NSMutableArray new];
        _moreImgSource = [NSMutableArray new];
    }
    
    if (_UAVScrollView.subviews.count > 0) {
        [_UAVScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    if (_currenMode == HomePageCurrenModeUserHaving) {
        if (_myHaveImgSource.count > 0) {
            for (int i = 0; i < kImgCount ; i ++) {
                UIImageView *img = _myHaveImgSource[i];
                [_UAVScrollView addSubview:img];
            }
        } else {
             for (int i = 0; i < kImgCount ; i ++) {
                UIImageView *img = [self createImageView:kMyImgTag currenLoop:i];
                 [_UAVScrollView addSubview:img];
                [_myHaveImgSource addObject:img];
            }
        }
         _pageControl.numberOfPages = kImgCount;
         [_UAVScrollView setContentSize:CGSizeMake(CGRectGetWidth(_UAVScrollView.frame) * _myHaveImgSource.count, 0)];
    } else {
        
        if (_moreImgSource.count > 0) {
            for (int i = 0; i < _moreImgSource.count; i ++) {
                UIImageView *img = (UIImageView *)_moreImgSource[i];
                [_UAVScrollView addSubview:img];
            }
        } else {
             for (int i = 0; i < kImgCount ; i ++) {
                 UIImageView *img = [self createImageView:kMoreImgTag currenLoop:i];
                [_UAVScrollView addSubview:img];
                [_moreImgSource addObject:img];
            }
        }
        _pageControl.numberOfPages = kImgCount;
        [_UAVScrollView setContentSize:CGSizeMake(CGRectGetWidth(_UAVScrollView.frame) * _moreImgSource.count, 0)];
    }
    [_UAVScrollView setContentOffset:CGPointMake(0, 0)];
    _pageControl.currentPage = 0;
}

- (UIImageView *)createImageView:(NSInteger)tag currenLoop:(NSInteger)i {
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"medium_%zd",i+1]]];
    img.tag = tag + i;
    img.contentMode = UIViewContentModeScaleAspectFit;
    img.frame = CGRectMake(kImgWidth * i, 0, kImgWidth, kImgHeight);
    [img addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                      action:@selector(UnvShowImgClickAction:)]];
    img.userInteractionEnabled = YES;
    return img;
}

- (void)setBtnSelect:(UIButton *)btn andSelect:(BOOL)isSelect {
    if (btn == _userHaveDeviceBtn) {
        _moreFlyTypeBtn.selected = !isSelect;
        _userHaveDeviceBtn.selected = isSelect;
        _userHaveDeviceBtn.backgroundColor = [UIColor whiteColor];
        _moreFlyTypeBtn.backgroundColor = _checkModelView.backgroundColor;
        _joinBtn.layer.cornerRadius = 16;
        _joinBtn.backgroundColor = [UIColor colorWithHex:0x0A60FE];
        _joinBtn.layer.borderWidth = 0;
        [_joinBtn setTitle:@"进入相机" forState:UIControlStateNormal];
        [_joinBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        _userHaveDeviceBtn.selected = !isSelect;
        _moreFlyTypeBtn.selected = isSelect;
        _moreFlyTypeBtn.backgroundColor = [UIColor whiteColor];
        _userHaveDeviceBtn.backgroundColor =  _checkModelView.backgroundColor;
//        _joinBtn.layer.borderColor = [UIColor blueColor].CGColor;
//        _joinBtn.layer.borderWidth = 1;
//        _joinBtn.layer.cornerRadius = 16;
        _joinBtn.backgroundColor = [UIColor whiteColor];
        [_joinBtn setTitle:@"" forState:UIControlStateNormal];
//        [_joinBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}
//- (void) navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    // 如果进入的是当前视图控制器
//    if (viewController == self) {
//        // 背景设置为黑色
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.000 green:0.000 blue:0.000 alpha:1.000];
//        // 透明度设置为0.3
//        self.navigationController.navigationBar.alpha = 0.300;
//        // 设置为半透明
//        self.navigationController.navigationBar.translucent = YES;
//    } else {
//        // 进入其他视图控制器
//        self.navigationController.navigationBar.alpha = 1;
//        // 背景颜色设置为系统默认颜色
//        self.navigationController.navigationBar.barTintColor = nil;
//        self.navigationController.navigationBar.translucent = NO;
//    }
//}

- (void)UnvShowImgClickAction:(UIGestureRecognizer *)gesture {
    //TODO:进入飞控记录界面
//    DetrumShopDetailViewController *shopDetail = getStoryOfControllerInstance(@"DetrumShopDetailViewController");
//    [self.navigationController pushViewController:shopDetail animated:YES];
}

- (void)pushFlyControlPage {
    if ([[_joinBtn titleForState:UIControlStateNormal] isEqualToString:@"进入相机"]) {
        DetrumFlyControlHomePageController *flyControl = getStoryOfControllerInstance(@"DetrumFlyControlHomePageController");
        [self presentViewController:flyControl animated:YES completion:nil];
    } else {
//        DetrumShopListViewController *detrum = getStoryOfControllerInstance(@"DetrumShopListViewController");
//        [self.navigationController pushViewController:detrum animated:YES];
    }
}

#pragma mark - Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = page;
}

@end
