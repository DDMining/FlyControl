//
//  NSMutableDictionary+DetrumMutableDic.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/19.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (DetrumMutableDic)
- (void)exchangeObjectAtIndex:(NSInteger)index withObjectAtIndex:(NSInteger)toIndex;

@end
