//
//  DetrumBatteryInfoView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/29.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumMavlinkManager.h"
#import "Interface.h"
#import "DetrumBatteryInfoView.h"
#import "DetrumBettryOneTableViewCell.h"
#import "DetrumBettryAlarmTableViewCell.h"
#import "DetrumBettryDetailTableViewCell.h"
#import "DetrumBetteryCapacityTableViewCell.h"
#import "DetrumOnSelfDischargeTableViewCell.h"

#define kOneCellIdentifier @"oneBatteryCell"
#define kTwoCellIdentifier @"twoBatteryCell"
#define kThreeCellIdentifier @"threeBatteryCell"
#define kFourCellIdentifier @"fourBatteryCell"
#define kFiveCellIdentifier @"fiveBatteryCell"


@implementation DetrumBatteryInfoView

- (IBAction)backEvent:(id)sender {
    [self backAnimaiton];
}

- (void)awakeFromNib {
    // Initialization code
    [self viewDidLoad];
}

- (void)viewDidLoad {
    [self initTableView];
    if ([DetrumMavlinkManager shareInstance].isConnect) {
        dataSource = [[DetrumMavlinkManager shareInstance] getDataWithType:PARAM_BETTERY];
    }
}

- (void)initTableView {
    if (_tableView.delegate == nil) {
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumBettryOneTableViewCell" bundle:nil] forCellReuseIdentifier:kOneCellIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumBettryAlarmTableViewCell" bundle:nil] forCellReuseIdentifier:kTwoCellIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumBettryDetailTableViewCell" bundle:nil] forCellReuseIdentifier:kThreeCellIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumBetteryCapacityTableViewCell" bundle:nil] forCellReuseIdentifier:kFourCellIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumOnSelfDischargeTableViewCell" bundle:nil] forCellReuseIdentifier:kFiveCellIdentifier];
        
    }
    
}

#pragma mark - tableView Delegate & dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        DetrumBettryOneTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kOneCellIdentifier forIndexPath:indexPath];
        
        return cell;
    } else if (indexPath.row == 1) {
        DetrumBettryAlarmTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTwoCellIdentifier forIndexPath:indexPath];
        return cell;
    } else if (indexPath.row == 2) {
        DetrumBettryDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kThreeCellIdentifier forIndexPath:indexPath];
        cell.voltageLb.text = FLOATNUMCONVERSTR(dataSource,(int)(dataSource.count - 2));
        cell.currenBatteryLb.text = FLOATNUMCONVERSTR(dataSource, 6);
        cell.batteryCapacityLb.text = FLOATNUMCONVERSTR(dataSource, 4);
        cell.temperatureLb.text = FLOATNUMCONVERSTR(dataSource, 2);
        return cell;
    } else if (indexPath.row == 3){
        DetrumBetteryCapacityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kFourCellIdentifier forIndexPath:indexPath];
        cell.batterys = @[[dataSource objectSecurityAtIndex:9],[dataSource objectSecurityAtIndex:10],[dataSource objectSecurityAtIndex:11],[dataSource objectSecurityAtIndex:12]];
        cell.loopsNum.text = FLOATNUMCONVERSTR(dataSource, 8);
        cell.batteryLife.text = FLOATNUMCONVERSTR(dataSource, (int)(dataSource.count - 1));
        return cell;
    } else {
        DetrumOnSelfDischargeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kFiveCellIdentifier forIndexPath:indexPath];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 75;
    } else if (indexPath.row == 1) {
        return 76;
    } else if (indexPath.row == 2) {
        return 65;
    } else if (indexPath.row == 3){
        return 110;
    } else {
        return 35;
    }
}

@end
