//
//  UserDataMarco.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef UserDataMarco_h
#define UserDataMarco_h

#define DetrumUserFirstRun @"DetrumUserFirstRun"
#define DetrumUserLogin @"DetrumUserLogin"

#define JiuLeMapkey @"0c7df9aa0473fde3c17bca1e4d4219ca"

/** 飞控界面按钮点击key */
#define FlyControlViewBtnEventBackHome @"FlyControlViewBtnEventBackHome"
#define FlyControlViewBtnEventDeviceConnect @"FlyControlViewBtnEventDeviceConnect"
#define FlyControlViewBtnEventJoinClick @"FlyControlViewBtnEventJoinClick"


/** 飞控界面通知 */
#define FlyControlNotificationRemove @"FlyControlNotificationRemove"

/** 媒体库通知 */
#define kMediaNotification @"detrumCollecionCellRefresh"
#endif /* UserDataMarco_h */
