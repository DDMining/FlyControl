//
//  DetrumMediaPopView.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/14.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger , DetrumMediaSelectType) {
    DetrumMediaSelectTypeSdCard,
    DetrumMediaSelectTypePhote,
    DetrumMediaSelectTypeMovie,
};

typedef void (^ClickAction)(DetrumMediaSelectType type);
@interface DetrumMediaPopView : UIView
@property (nonatomic, copy) ClickAction completion;
+ (void)showWithCompletion:(ClickAction)action;
+ (void)dismiss;
@end
