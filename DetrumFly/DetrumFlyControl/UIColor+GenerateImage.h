//
//  UIColor+GenerateImage.h
//
//
//  Created on 14-11-22.
//  Copyright (c) 2014年 All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (GenerateImage)

- (UIImage *)colorToImage;

@end
