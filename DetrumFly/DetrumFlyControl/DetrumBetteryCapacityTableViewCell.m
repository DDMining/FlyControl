//
//  DetrumBetteryCapacityTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/29.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "CapacityView.h"
#import "UIColor+RGBConverHex.h"
#import "DetrumBetteryCapacityTableViewCell.h"
#define kDefaultColor [UIColor greenColor]
#define kAcept 0.081

@implementation DetrumBetteryCapacityTableViewCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setBatterys:(NSArray *)batterys {
    _batterys = batterys;
    [self initCapacityView];
}

- (void)initCapacityView {
    if ([_firstBatteryView viewWithTag:0x521] == nil) {
        CapacityView *firstView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * kAcept, _firstBatteryView.frame.size.height) percen:[[_batterys firstObject] floatValue] fillColor:kDefaultColor radiu:5.f];
        firstView.tag = 0x521;
        [_firstBatteryView addSubview:firstView];
        [self setLayerParameter:_firstBatteryView];

    } else {
        CapacityView *view = [_firstBatteryView viewWithTag:0x521];
        [view setPercent:[[_batterys firstObject] floatValue]];
    }
    _firstBatteryLb.text = [NSString stringWithFormat:@"%@",[_batterys firstObject]];
   
    if ([_secondBatteryView viewWithTag:0x522] == nil) {
        CapacityView *secondView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * kAcept, _secondBatteryView.frame.size.height)  percen:[[_batterys objectAtIndex:1] floatValue] fillColor:kDefaultColor radiu:5.f];
        secondView.tag = 0x522;
        [_secondBatteryView addSubview:secondView];
        [self setLayerParameter:_secondBatteryView];
    } else {
        CapacityView *view = [_secondBatteryView viewWithTag:0x522];
        [view setPercent:[[_batterys objectAtIndex:1] floatValue]];
    }
    _secondBatteryLb.text = [NSString stringWithFormat:@"%@",[_batterys objectAtIndex:1]];

    if ([_threeBatteryView viewWithTag:0x523] == nil) {
        CapacityView *threeView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * kAcept, _threeBatteryView.frame.size.height)  percen:[[_batterys objectAtIndex:2] floatValue] fillColor:kDefaultColor radiu:5.f];
        threeView.tag = 0x523;
        [_threeBatteryView addSubview:threeView];
        [self setLayerParameter:_threeBatteryView];
    } else {
        CapacityView *view = [_threeBatteryView viewWithTag:0x523];
        [view setPercent:[[_batterys objectAtIndex:2] floatValue]];
    }
    _threeBatteryLb.text = [NSString stringWithFormat:@"%@",[_batterys objectAtIndex:2]];
    
    if ([_fourBatteryView viewWithTag:0x524] == nil) {
        CapacityView *fourView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * kAcept, _fourBatteryView.frame.size.height)  percen:[[_batterys lastObject] floatValue] fillColor:kDefaultColor radiu:5.f];
        fourView.tag = 0x524;
        [_fourBatteryView addSubview:fourView];
        [self setLayerParameter:_fourBatteryView];
    } else {
        CapacityView *view = [_fourBatteryView viewWithTag:0x524];
        [view setPercent:[[_batterys objectAtIndex:3] floatValue]];
    }
    _fourBatteryLb.text = [NSString stringWithFormat:@"%@",[_batterys lastObject]];
}

- (void)setLayerParameter:(UIView *)temp {
    temp.layer.borderColor = [UIColor colorWithHex:0xCFCECF].CGColor;
    temp.layer.borderWidth = 1.f;
    temp.layer.masksToBounds = YES;
    temp.layer.cornerRadius = 5.f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
