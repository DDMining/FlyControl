//
//  DetrumCameraTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

typedef NS_ENUM(NSInteger , DetrumCameraType) {
   DetrumCameraTypeOne,
   DetrumCameraTypeTwo,
};

@interface DetrumCameraTableViewCell : DetrumBaseTableViewCell
@property (assign, nonatomic) DetrumCameraType type;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *secondView;

@property (weak, nonatomic) IBOutlet UIButton *toLeft;
@property (weak, nonatomic) IBOutlet UIButton *toFront;
@property (weak, nonatomic) IBOutlet UIButton *toRight;


@property (weak, nonatomic) IBOutlet UIButton *horizontalBtn;
@property (weak, nonatomic) IBOutlet UIButton *toDownBtn;
@end
