//
//  DetrumSwitchTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

@interface DetrumSwitchTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UISwitch *onOff;

@end
