//
//  DetrumAutoCalibrationView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "UIColor+RGBConverHex.h"
#import "DetrumAutoCalibrationView.h"

@implementation DetrumAutoCalibrationView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {
        [self setLayer];
    }
    return self;
}

- (void)setLayer {
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [UIColor colorWithHex:0x979797].CGColor;
    self.layer.borderWidth = 3.f;
}

- (IBAction)determineEvent:(id)sender {
    
}

- (IBAction)closeEvent:(id)sender {
    [UIView animateWithDuration:0.3f animations:^{
        self.transform = CGAffineTransformMakeScale(0.8, 0.8);
        self.alpha = 0.1;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (IBAction)cancelEvent:(id)sender {
    [UIView animateWithDuration:0.3f animations:^{
        self.transform = CGAffineTransformMakeScale(0.8, 0.8);
        self.alpha = 0.1;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
@end
