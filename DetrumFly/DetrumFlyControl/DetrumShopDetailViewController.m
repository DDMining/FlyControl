//
//  DetrumShopDetailViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/20.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumShopDetailViewController.h"

@implementation DetrumShopDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产品参数"; 
    [self showNavBarWithRightTitle:nil
                    andRightAction:@selector(rightNavBtn:)
                     andLeftAction:@selector(leftNavBtn:)
                 andRightBtnIsShow:YES];
}

- (void)leftNavBtn:(UIButton *)btn {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightNavBtn:(UIButton *)btn {
//    self.title = @"";
}

@end
