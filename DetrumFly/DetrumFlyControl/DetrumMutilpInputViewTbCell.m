//
//  DetrumMutilpInputViewTbCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#define kRoll @"kRoll"
#define kPitch @"kPitch"
#define kYaw @"kYaw"
#import "DetrumMarco.h"
#import "DetrumMavlinkManager.h"
#import "DetrumMutilpInputViewTbCell.h"

@implementation DetrumMutilpInputViewTbCell

- (void)awakeFromNib {
    _pitchTf.value = [[NSUserDefaults standardUserDefaults] floatForKey:kPitch];
    _rollTf.value = [[NSUserDefaults standardUserDefaults] floatForKey:kRoll];
    _headingTf.value = [[NSUserDefaults standardUserDefaults] floatForKey:kYaw];

}

- (IBAction)sliderValueEditEnd:(id)sender {
    NSLog(@"%.2f",((UISlider *)sender).value);
    if (sender == _pitchTf) {
        NSLog(@"pitch操作完了");
        if ([DetrumMavlinkManager shareInstance].isConnect) {
            [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROLPITCH andValue:_pitchTf.value];
        }
    } else if (sender == _rollTf) {
        if ([DetrumMavlinkManager shareInstance].isConnect) {
            [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROLROLL andValue:_rollTf.value];
        }
        
        NSLog(@"roll操作完了");
    } else {
        if ([DetrumMavlinkManager shareInstance].isConnect) {
            [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROLYAW andValue:_headingTf.value];
        }

        NSLog(@"heading操作完了");
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
