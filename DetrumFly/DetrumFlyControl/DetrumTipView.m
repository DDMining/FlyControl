//
//  DetrumTipView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "UIColor+RGBConverHex.h"
#import "DetrumTipView.h"

@interface DetrumTipView()

@end

@implementation DetrumTipView

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {
        _num = 0.f;
        _numlb.text = @"0.0";
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [UIColor colorWithHex:0x979797].CGColor;
        self.layer.borderWidth = 3.f;
    }
    return self;
}

- (IBAction)minusEvent:(id)sender {
    if (_num <= 0.0) {
        return;
    }
    _num -= 0.1;
    _numlb.text = [NSString stringWithFormat:@"%.1f",_num];
}

- (IBAction)plusEvent:(id)sender {
    _num += 0.1;
    _numlb.text = [NSString stringWithFormat:@"%.1f",_num];
}

- (IBAction)closeEvent:(id)sender {
    [UIView animateWithDuration:0.3f animations:^{
        self.transform = CGAffineTransformMakeScale(0.8, 0.8);
        self.alpha = 0.1;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
