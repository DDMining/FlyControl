//
//  CQDropDownViewCell.h
//  NIDropDown
//
//  Created by xcq on 16/3/2.
//
//
#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface CQDropDownViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownImg;

@end
