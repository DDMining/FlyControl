//
//  DetrumMediaLibraryViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
@import MediaPlayer;
#import <Realm/Realm.h>
#import "DetrumImageView.h"
#import "DetrumMediaPopView.h"
#import "DetrumMediaImgModel.h"
#import "DetrumMediaMovieModel.h"
#import "CRMediaPickerController.h"
#import "DetrumCollectionViewCell.h"
#import "DetrumPreviewViewController.h"
#import "DetrumCollectionReusableHeadView.h"
#import "DetrumMediaLibraryViewController.h"
#import "DetrumMediaSelectImgViewController.h"


static NSString *Cellidentifier = @"detrumCollectionCell";
static NSString *ReusableViewIdentifer = @"DetrumReusableHeadView";

@interface DetrumMediaLibraryViewController ()<UICollectionViewDataSource,UICollectionViewDelegate> {
    UIButton *movieBtn;
    UIButton *cameraBtn;
    UIFont *font;
}
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *models;
@property (nonatomic, strong) NSMutableArray *mediaModels;
@property (nonatomic, strong) NSMutableArray *modelImgKeys;
@property (nonatomic, strong) NSMutableArray *modelImgValues;
@property (nonatomic, strong) NSMutableArray *modelVedioKeys;
@property (nonatomic, strong) NSMutableArray *modelVedioValues;
@property (nonatomic, strong) MPMoviePlayerViewController *moviePlayer;
@property (nonatomic, strong) NSMutableArray *urlArr;
@property (nonatomic, strong) NSMutableArray *vedioUrlArr;


//@property (nonatomic, strong) NSMutableArray
/** 1:视频 0:图片 */
@property (nonatomic, assign) int type;

@end

@implementation DetrumMediaLibraryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initParameter];
    [self createNavAboutInterface];
    [self initCollection];
    [self initModels:_type];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_isUpdateImg) {
        [self initModels:0];
    } else if (_isUpdateVedio) {
        [self initModels:1];
    }
}

- (void)initParameter {
    _type = 1;
    _isUpdateVedio = NO;
    _isUpdateImg = NO;
}


- (void)initModels:(int)type {
    [self showSvprogress:nil];
    NSLog(@"model:%@",[[RLMRealm defaultRealm] path]);
    RLMResults *result = [[DetrumMediaMovieModel objectsWhere:@"id > 1"]
                          sortedResultsUsingProperty:@"date" ascending:NO];

    if (result.count == 0) {
        [_collectionView reloadData];
        [self dismiss];
        return;
    } else {
        if (type == 1) {
            _models = [NSMutableArray new];
            _mediaModels = [NSMutableArray new];
            _vedioUrlArr = [NSMutableArray new];
        } else {
            _models = [NSMutableArray new];
            _mediaModels = [NSMutableArray new];
            _urlArr = [NSMutableArray new];
        }
    }
    if (type == 1) {
        for (DetrumMediaMovieModel *model in result) {
            [_models addObject:model];
            [_vedioUrlArr addObject:model.mediaUrl];
            [self convertMediaModel:type dataBaseModel:model];
        }
    } else {
        for (DetrumMediaImgModel *model in result) {
            [_models addObject:model];
            [_urlArr addObject:model.url];
            [self convertMediaModel:type dataBaseModel:model];
        }
    }
    [self sortingModels:type];
    _isUpdateVedio = NO;
    _isUpdateImg = NO;
    if (_models.count > 0) {
        [_collectionView reloadData];
    }
    [self dismiss];
}

- (void)convertMediaModel:(int)type dataBaseModel:(RLMObject *)obj{
    DetrumMediaModel *model = [DetrumMediaModel new];
    if (type==0) {
        DetrumMediaImgModel *imgModel = (DetrumMediaImgModel *)obj;
        model.url = [NSURL URLWithString:imgModel.url];
        model.dateStr = imgModel.dateStr;
        model.camareDate = imgModel.currenDate;
        model.duration = imgModel.MediaDuration;
        model.thumbnail = [UIImage imageWithData:imgModel.thumbnail];
        model.fullScreenImage = [UIImage imageWithData:imgModel.fullScreen];
        model.type = DetrumMediaTypeImg;
        model.imgModel = imgModel;
    } else {
        DetrumMediaMovieModel *videoModel = (DetrumMediaMovieModel *)obj;
        model.url = [NSURL URLWithString:videoModel.mediaUrl];
        model.dateStr = videoModel.dateStr;
        model.camareDate = videoModel.date;
        model.duration = videoModel.duration;
        model.thumbnail = [UIImage imageWithData:videoModel.thumbnail];
        model.type = DetrumMediaTypeVideo;
        model.moviewModel = videoModel;
     }
    NSLog(@"%s , %@",__FUNCTION__ , model.url);
    [_mediaModels addObject:model];
}

- (void)sortingModels:(int)type {
    if (type == 0) {
        if (_modelImgKeys == nil || _isUpdateImg) {
            _modelImgKeys = [NSMutableArray new];
            _modelImgValues = [NSMutableArray new];
        } else {
            return;
        }
        for (int i = 0 ; i < _mediaModels.count; i ++) {
            DetrumMediaModel *model = [_mediaModels objectAtIndex:i];
            NSLog(@"index:%zd",model.index);
            if (![_modelImgKeys containsObject:model.dateStr]) {
                [_modelImgKeys addObject:model.dateStr];
                NSMutableArray *arr = [NSMutableArray arrayWithArray:@[model]];
                [_modelImgValues addObject:arr];
            } else {
                NSMutableArray *arr = [_modelImgValues objectAtIndex:[_modelImgKeys indexOfObject:model.dateStr]];
                [arr addObject:model];
                [_modelImgValues  replaceObjectAtIndex:[_modelImgKeys indexOfObject:model.dateStr] withObject:arr];
            }
        }
    } else {
        if (_modelVedioKeys == nil || _isUpdateVedio) {
            _modelVedioKeys = [NSMutableArray new];
            _modelVedioValues = [NSMutableArray new];
        } else {
            return;
        }
        for (int i = 0 ; i < _models.count; i ++) {
            DetrumMediaModel *model = [_mediaModels objectAtIndex:i];
            model.index = i;
            if (![_modelVedioKeys containsObject:model.dateStr]) {
                [_modelVedioKeys addObject:model.dateStr];
                NSMutableArray *arr = [NSMutableArray arrayWithArray:@[model]];
                [_modelVedioValues addObject:arr];
            } else {
                NSMutableArray *arr = [_modelVedioValues objectAtIndex:[_modelVedioKeys indexOfObject:model.dateStr]];
                [arr addObject:model];
                [_modelVedioValues  replaceObjectAtIndex:[_modelVedioKeys indexOfObject:model.dateStr] withObject:arr];
            }
        }
    }
}

- (void)initCollection {
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)]) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.headerReferenceSize = CGSizeMake(CGRectGetWidth(self.view.frame), 37);
    flowLayout.minimumLineSpacing = 2;
    flowLayout.minimumInteritemSpacing = 2;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 2, 0, 2);
    flowLayout.itemSize = CGSizeMake((CGRectGetWidth([UIScreen mainScreen].bounds) - (4 * 2)) / 3, (CGRectGetWidth([UIScreen mainScreen].bounds) - (4 * 2)) / 3);
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds) , CGRectGetHeight([UIScreen mainScreen].bounds) - 64 - 49) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.scrollEnabled = YES;
    _collectionView.backgroundColor = self.view.backgroundColor;
    [self.collectionView registerNib:[UINib nibWithNibName:@"DetrumCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:Cellidentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"DetrumCollectionReusableHeadView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ReusableViewIdentifer];
     [self.view addSubview:_collectionView];
}

- (void)createNavAboutInterface {
    UIButton *_rightNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *leftImg = [UIImage imageNamed:@"import"];
    CGFloat height = kNavigationBtnHeight;
    CGFloat width = height * leftImg.size.width / leftImg.size.height;
    if (height/width < 0.8) {
        CGFloat scale = leftImg.size.height / leftImg.size.width;
        _rightNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       height,
                                       height*scale);
    } else {
        _rightNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       width,
                                       height);
    }
    
    [_rightNavBtn setBackgroundImage:leftImg forState:UIControlStateNormal];
//    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:_rightNavBtn];
//    self.navigationItem.rightBarButtonItem = item;
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *leftBtnImg = [UIImage imageNamed:@"return"];
    CGFloat leftHeight = kNavigationBtnHeight;
    CGFloat leftWidth = height * leftBtnImg.size.width / leftBtnImg.size.height;
    if (leftHeight/leftWidth < 0.8) {
        CGFloat scale = leftImg.size.height / leftBtnImg.size.width;
        leftBtn.frame = CGRectMake(0,
                                   kNavigationBtnSpaceHorizontal,
                                   height,
                                   height*scale);
    } else {
        leftBtn.frame = CGRectMake(0,
                                   kNavigationBtnSpaceHorizontal,
                                   width,
                                   height);
    }
    
    [leftBtn setBackgroundImage:leftBtnImg forState:UIControlStateNormal];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    [[_rightNavBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self popDetailView];
    }];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 44)];
    movieBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [movieBtn setBackgroundImage:[UIImage imageNamed:@"video"] forState:UIControlStateNormal];
    [movieBtn setFrame:CGRectMake(0, 10, 40, 23)];
    movieBtn.imageView.contentMode=UIViewContentModeScaleAspectFill;
    [titleView addSubview:movieBtn];
    [[movieBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        _type = 1;
        [self photographyEvent:movieBtn];
    }];
    [titleView addSubview:movieBtn];
    
    cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cameraBtn setBackgroundImage:[UIImage imageNamed:@"picture"] forState:UIControlStateNormal];
    [cameraBtn setFrame:CGRectMake(CGRectGetWidth(titleView.frame) - 44, 10, 30, 23)];
    cameraBtn.imageView.contentMode=UIViewContentModeScaleAspectFill;
    [[cameraBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        _type = 0;
        [self imageEvent:cameraBtn];
    }];
    
    [cameraBtn setTitleColor:[[UIColor whiteColor]
                              colorWithAlphaComponent:0.5]
                    forState:UIControlStateNormal];
    cameraBtn.transform = CGAffineTransformMakeScale(0.7,0.7);
    font = cameraBtn.titleLabel.font;
    
    [titleView addSubview:cameraBtn];
    self.navigationItem.titleView = titleView;
}

- (void)setBtnStyle:(UIButton *)sender {
    [UIView animateWithDuration:0.4 animations:^{
     [sender setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:1.f]
                  forState:UIControlStateNormal];
      sender.titleLabel.font = [UIFont fontWithName:font.fontName size:font.pointSize];
      sender.transform = CGAffineTransformMakeScale(1.f,1.f);
    }];
    if (sender == movieBtn) {
        [self initModels:1];
        [UIView animateWithDuration:0.4 animations:^{
            [cameraBtn setTitleColor:[[UIColor whiteColor]
                                      colorWithAlphaComponent:0.5]
                            forState:UIControlStateNormal];
            cameraBtn.transform = CGAffineTransformMakeScale(0.7,0.7);
        }];
    } else {
        [self initModels:0];
        [UIView animateWithDuration:0.4
                         animations:^{
        [movieBtn setTitleColor:[[UIColor whiteColor]
                                 colorWithAlphaComponent:0.5]
                       forState:UIControlStateNormal];
         movieBtn.transform = CGAffineTransformMakeScale(0.7,0.7);
        }];
    }
}

- (void)photographyEvent:(UIButton *)btn {
    [self setBtnStyle:btn];
}

- (void)imageEvent:(UIButton *)btn {
    [self setBtnStyle:btn];
}

- (void)popDetailView {
    [DetrumMediaPopView showWithCompletion:^(DetrumMediaSelectType type) {
        if (type == DetrumMediaSelectTypeSdCard) {
            
        } else if (type == DetrumMediaSelectTypePhote) {
            [self pushMediaController:DetrumMediaSelectTypePhote];
        } else {
            [self pushMediaController:DetrumMediaSelectTypeMovie];
        }
    }];
}

- (void)pushMediaController:(DetrumMediaSelectType)type {
    DetrumMediaSelectImgViewController *img = [[DetrumMediaSelectImgViewController alloc] initWithNibName:@"DetrumMediaSelectImgViewController" bundle:nil];
    img.type = type;
    img.urls = (type == DetrumMediaSelectTypePhote ? _urlArr : _vedioUrlArr);
     img.mediaController = self;
    [self.navigationController pushViewController:img animated:YES];
}
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (_type == 0) {
        return [[_modelImgValues objectAtIndex:section] count] != 0 ? [[_modelImgValues objectAtIndex:section] count] : 0;
    } else {
        return [[_modelVedioValues objectAtIndex:section] count] != 0 ? [[_modelVedioValues objectAtIndex:section] count] : 0;
    }
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (_type == 0) {
        return _modelImgKeys.count == 0 ? 0 : _modelImgKeys.count;
    } else {
        return _modelVedioKeys.count == 0 ? 0 : _modelVedioKeys.count;
    }
}

//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DetrumCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:Cellidentifier forIndexPath:indexPath];
    if (_type == 0) {
        cell.model = [[_modelImgValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    } else {
        cell.model = [[_modelVedioValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
    cell.photoView.clickEvent = ^(NSInteger index, DetrumMediaType type , DetrumMediaModel *model , RLMObject *dataBaseModel) {
        //TODO:调用视图浏览界面
        if (type == DetrumMediaTypeImg) {
            DetrumPreviewViewController *preview = [[DetrumPreviewViewController alloc] initWithNibName:@"DetrumPreviewViewController" bundle:nil];
            preview.models = _mediaModels;
            preview.type = JoinPreviewClassTypeLibraryMedia;
            preview.initIndex = [_mediaModels indexOfObject:model];
            preview.action = ^(NSInteger index , DetrumMediaModel *deleteModel) {
                NSInteger keyIndex = [_modelImgKeys indexOfObject:deleteModel.dateStr];
                NSInteger valueIndex = [[_modelImgValues objectAtIndex:keyIndex] indexOfObject:deleteModel];
                [[_modelImgValues objectAtIndex:keyIndex]  removeObjectAtIndex:valueIndex];
                if ([[_modelImgValues objectAtIndex:keyIndex] count] == 0) {
                    [_modelImgKeys removeObject:deleteModel.dateStr];
                    [_modelImgValues removeObjectAtIndex:keyIndex];
                }
                [self.collectionView reloadData];
                RLMRealm *realm = [RLMRealm defaultRealm];
                if (deleteModel.type == DetrumMediaTypeImg) {
                    [_models removeObject:deleteModel.imgModel];
                    [realm beginWriteTransaction];
                    [realm deleteObject:deleteModel.imgModel];
                    [realm commitWriteTransaction];
                } else {
                    [_models removeObject:deleteModel.moviewModel];
                    [realm beginWriteTransaction];
                    [realm deleteObject:deleteModel.moviewModel];
                    [realm commitWriteTransaction];
                }
            };
            [self.navigationController pushViewController:preview animated:NO];
        } else {
            //TODO:调用视频
            [self playMovie:model.url];
        }
    };
    return cell;
}


#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    DetrumCollectionViewCell * cell = (DetrumCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    DetrumCollectionReusableHeadView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        DetrumCollectionReusableHeadView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:ReusableViewIdentifer forIndexPath:indexPath];
        if (_type == 0) {
            [headerView setDateText:((DetrumMediaImgModel *)[[_modelImgValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]).dateStr];
        } else {
            [headerView setDateText:((DetrumMediaMovieModel *)[[_modelVedioValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]).dateStr];
        }        reusableview = headerView;
    }
    return reusableview;
}


#pragma mark --UICollectionViewDelegateFlowLayout


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

//这里是会报警告的代码
/**
 @method 播放电影
 */
-(void)playMovie:(NSURL *)urlStr{
    if (_moviePlayer==nil) {
//        NSString *str = [[NSBundle mainBundle] pathForResource:@"ds_480x272" ofType:@"yuv"];
        
        _moviePlayer = [[MPMoviePlayerViewController alloc]initWithContentURL:urlStr];
        [self addNotification];
    }
//    [self presentMoviePlayerViewControllerAnimated:self.moviePlayer];
    [self presentViewController:_moviePlayer animated:YES completion:nil];
}

-(void)addNotification{
    NSNotificationCenter *notificationCenter=[NSNotificationCenter defaultCenter];

    
    [notificationCenter addObserver:self
                           selector:@selector(mediaPlayerPlaybackFinished:)
                               name:MPMoviePlayerPlaybackDidFinishNotification
                             object:_moviePlayer.moviePlayer];
}

#pragma mark -------------------视频播放结束委托--------------------
-(void)mediaPlayerPlaybackFinished:(NSNotification *)notification{
    NSLog(@"播放完成.%li",self.moviePlayer.moviePlayer.playbackState);
    [self dismissViewControllerAnimated:YES completion:nil];
    self.moviePlayer = nil;
    
}


#pragma clang diagnostic pop

@end
