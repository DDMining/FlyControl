//
//  DetrumImageView.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumImageView.h"
#import "DetrumMediaImgModel.h"
#import "UIColor+RGBConverHex.h"
#import "DetrumMediaMovieModel.h"

#define kSelectImgWidthHeight 22
#define kSelectVideoWidthHeight 30

@implementation DetrumImageView

- (instancetype)initWithType:(DetrumMediaType)type
                    clickType:(DetrumImageClickType)clickType
                        mdoel:(DetrumMediaModel *)model
                        Frame:(CGRect)rects {
    if (self == [super initWithFrame:rects]) {
        self.backgroundColor = [[UIColor colorWithHex:0xCCCCCC] colorWithAlphaComponent:1.];
        [self initWithType:type  clickType:clickType model:model];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {

    }
    return self;
}

- (void)setEdit:(BOOL)isEdit {
    _isEdit = isEdit;
    if (_isEdit) {
        if (_checkImg == nil) {
         }
    } else {
        
    }
}

- (void)resetType:(DetrumMediaType)type
      clickType:(DetrumImageClickType)clickType
          model:(NSObject *)model {
   
    if (_type == DetrumMediaTypeImg) {
        if (_checkImg) {
            [_checkImg removeFromSuperview];
        }
    } else if (_type == DetrumMediaTypeVideo) {
        if (_videoImg) {
            [_videoImg removeFromSuperview];
        }
        if (_bottonBar) {
            [_bottonBar removeFromSuperview];
        }
    }
    if ([model isKindOfClass:[DetrumMediaModel class]]) {
        [self initWithType:type
                 clickType:clickType
                     model:(DetrumMediaModel *)model];
    } else {
        [self initDataBaseModel:type
                 clickType:clickType
                     mdoel:(RLMObject *)model];
    }
}

- (void)initDataBaseModel:(DetrumMediaType)type
                clickType:(DetrumImageClickType)clickType
                    mdoel:(RLMObject *)model {
    self.backgroundColor = [[UIColor colorWithHex:0xCCCCCC] colorWithAlphaComponent:0.7];
    if (_dataBaseModel == nil) {
        _dataBaseModel = model;
    }
    if ([_dataBaseModel isKindOfClass:[DetrumMediaImgModel class]]) {
        DetrumMediaImgModel *model = (DetrumMediaImgModel *)_dataBaseModel;
        [self initUI:model.type
           clickType:model.clickType
           thumbnail:[UIImage imageWithData:model.thumbnail]
            duration:model.MediaDuration];
    } else {
        DetrumMediaMovieModel *model = (DetrumMediaMovieModel *)_dataBaseModel;
        [self initUI:model.type
           clickType:model.clickType
           thumbnail:[UIImage imageWithData:model.thumbnail]
            duration:model.duration];
    }

}

- (void)initWithType:(DetrumMediaType)type
           clickType:(DetrumImageClickType)clickType
               model:(NSObject *)model
{
    self.backgroundColor = [[UIColor colorWithHex:0xCCCCCC] colorWithAlphaComponent:0.7];
    if (model!=nil) {
        _model = (DetrumMediaModel *)model;
    }
    [self initUI:_model.type
       clickType:_model.clickType
       thumbnail:_model.thumbnail
        duration:_model.duration];
}

- (void)initUI:(DetrumMediaType)type
     clickType:(DetrumImageClickType)clickType
     thumbnail:(UIImage *)thumbnail
      duration:(NSInteger) duration {
    _type = type;
    _clickType = clickType;
    if (self.image == nil) {
        self.image = [[UIImageView alloc] initWithImage:thumbnail];
        self.image.frame = self.frame;
        [self addSubview:self.image];
    } else {
        self.image.image = thumbnail;
    }
    if (_maskView == nil) {
        _maskView = [UIView new];
        _maskView.frame = self.image.bounds;
        _maskView.backgroundColor = [UIColor clearColor];
        _maskView.userInteractionEnabled = YES;
        [self.maskView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(imageClick:)]];
        [self addSubview:_maskView];
    }
    if (clickType == DetrumImageClickTypeMediaSelect) {
        if (_checkImg == nil) {
            _checkImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"round"] highlightedImage:[UIImage imageNamed:@"selected"]];
            _checkImg.frame = CGRectMake(CGRectGetMaxX(self.frame) - kSelectImgWidthHeight - 7, 7, kSelectImgWidthHeight, kSelectImgWidthHeight);
            _checkImg.userInteractionEnabled = YES;
            [_checkImg addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setClickImg:)]];
        }
        if (_model.isSeleted) {
            _checkImg.highlighted = YES;
            _image.transform = CGAffineTransformMakeScale(0.8, 0.8);
        } else {
            _checkImg.highlighted = NO;
            _image.transform = CGAffineTransformMakeScale(1.f, 1.f);
        }
        [self addSubview:_checkImg];
    } else if (clickType == DetrumImageClickTypeMediaNewOrOld) {
        
        
    }
    if (type == DetrumMediaTypeImg) {
        
    } else {
        
        _videoImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Play"]];
        _videoImg.frame = CGRectMake((CGRectGetWidth(self.frame) / 2) - kSelectVideoWidthHeight / 2, (CGRectGetHeight(self.frame) / 2) - kSelectVideoWidthHeight / 2, kSelectVideoWidthHeight, kSelectVideoWidthHeight);
        _videoImg.userInteractionEnabled = YES;
        [_videoImg addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(videoClick:)]];
        [self addSubview:_videoImg];
        
        if (_model != nil) {
            if (_bottonBar == nil) {
                _bottonBar = [UIView new];
                _bottonBar.frame = CGRectMake(0, CGRectGetMaxY(self.image.frame) - 10, CGRectGetWidth(self.image.frame), 10);
                _bottonBar.backgroundColor = [[UIColor colorWithHex:0xCCCCCC] colorWithAlphaComponent:0.7];
                
                [self addSubview:_bottonBar];
                UILabel *label = [[UILabel alloc] initWithFrame:_bottonBar.bounds];
                label.textAlignment = NSTextAlignmentCenter;
                label.tag = 0x522;
                label.font = [UIFont fontWithName:iOS8FontName size:13.f];
                label.text = [self timeFormatted:duration];
                label.textColor = [UIColor blackColor];
                [_bottonBar addSubview:label];
            } else {
                UILabel *label = [_bottonBar viewWithTag:0x522];
                label.text = [NSString stringWithFormat:@"%zd:%zd",_model.duration/60/60, (_model.duration / 60) % 60];
            }
        }
    }

}

- (NSString *)timeFormatted:(NSInteger)totalSeconds
{
    NSInteger seconds = totalSeconds % 60;
    NSInteger minutes = (totalSeconds / 60) % 60;
    NSInteger hours = totalSeconds / 3600;
    return [NSString stringWithFormat:@"%02zd:%02zd:%02zd",hours, minutes, seconds];
}

- (void)setClickImg:(UIGestureRecognizer *)gesture {
    _checkImg.highlighted = !_checkImg.highlighted;
    if (_checkImg.highlighted) {
        _model.isSeleted = YES;
        [UIView animateWithDuration:0.3 animations:^{
            _image.transform = CGAffineTransformMakeScale(.8f, 0.8f);
        }];
        _seleteImgAction(_index , _model , YES);
    } else {
        _model.isSeleted = NO;
        [UIView animateWithDuration:0.3 animations:^{
            _image.transform = CGAffineTransformMakeScale(1.f, 1.f);
        }];
        _seleteImgAction(_index , _model , NO);
    }
}

- (void)imgClickFalse {
    _checkImg.highlighted = NO;
    _model.isSeleted = NO;
    [UIView animateWithDuration:0.4 animations:^{
        _image.transform = CGAffineTransformMakeScale(1.f, 1.f);
    }];
}

- (void)imageClick:(UIGestureRecognizer *)gesture {
    //进入照片查看界面
    NSLog(@"照片查看界面");
    [self click];
}

- (void)videoClick:(UIGestureRecognizer *)gesture {
    //进入视频查看界面
    NSLog(@"视频查看界面");
    [self click];
}

- (void)click {
    if (_model != nil) {
        _clickEvent(_model.index,_model.type,_model,_dataBaseModel);
    } else {
        _clickEvent([[_dataBaseModel valueForKey:@"index"] integerValue], [[_dataBaseModel valueForKey:@"type"] integerValue],nil,_dataBaseModel);
    }
}

@end
