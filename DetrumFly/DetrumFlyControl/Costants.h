//
//  Costants.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef Costants_h
#define Costants_h

// 自定义Log
#ifdef DEBUG
#define DFLog(...) NSLog(__VA_ARGS__)
#else
#define DFLog(...)
#endif

//测试环境
#ifdef DEBUG
#define kURL @"http://api.detrumtech.com?m=api"
#else
////正式环境
#define kURL @"http://112.124.35.198:8089"
#endif

#define KBaseURL   [kURL stringByAppendingString:@"&c="]
//#define KBaseURL kURL

// ================================== MD5 =======================
#define MD5Key              @"585bedd4e3d4a3cfa185f710914ad96c"
#define APP_URL             @"http://itunes.apple.com/lookup?id=你的应用程序的ID"
#define APP_DownLoad_URL    @"https://itunes.apple.com/us/app/qun-xiang-dao/id应用程序的ID?ls=1&mt=8"

#define IS_IPHONE_X (Screen_Height == 812.0f) ? YES : NO

#define Height_NavContentBar 44.0f

#define Height_StatusBar (IS_IPHONE_X==YES)?44.0f: 20.0f

#define Height_NavBar   (IS_IPHONE_X==YES)?88.0f: 64.0f

#define Height_TabBar   (IS_IPHONE_X==YES)?83.0f: 49.0f



#endif /* Costants_h */
