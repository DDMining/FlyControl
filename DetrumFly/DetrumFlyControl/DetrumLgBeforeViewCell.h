//
//  DetrumLgBeforeViewCell.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger , DetrumClickType) {
    DetrumClickTypeLogin,
    DetrumClickTypeCreate,
    DetrumClickTypeLogOut,
};

typedef void (^clickEvent)(DetrumClickType type);

@interface DetrumLgBeforeViewCell : UITableViewCell
/** 翔迷论坛 */
@property (weak, nonatomic) IBOutlet UIView *forumView;
/** 翔迷商场 */
@property (weak, nonatomic) IBOutlet UIView *mallView;
/** 官方网站 */
@property (weak, nonatomic) IBOutlet UIView *officialWebsiteView;
/** 创建账号 */
@property (weak, nonatomic) IBOutlet UIButton *createAccountBtn;
/** 登陆 */
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
/** 点击按钮类型 */
@property (copy, nonatomic) clickEvent click;
@end
