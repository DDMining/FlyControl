//
//  UIBarButtonItem+WF.h
//  jyb
//
//  Created by wangfang on 14/12/26.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (helper)
/**
*  快速创建一个显示图片的item
*
*  @param action   监听方法
*/
+ (UIBarButtonItem *)itemWithIcon:(NSString *)icon highIcon:(NSString *)highIcon target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)itemWithTitle:(NSString *)title target:(id)target action:(SEL)action;
@end
