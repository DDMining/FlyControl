//
//  DetrumPullDownMenuCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "CQDropDownView.h"
#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumPullDownMenuCell : DetrumBaseTableViewCell <CQDropDownViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *g1Btn;
@property (weak, nonatomic) IBOutlet UILabel *g2Btn;
@property (strong, nonatomic) CQDropDownView *g1View;
@property (strong, nonatomic) CQDropDownView *g2View;
@end
