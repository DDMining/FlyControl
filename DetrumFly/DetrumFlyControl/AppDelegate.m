//
//  AppDelegate.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/11.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "AppDelegate.h"
#import "UserDataMarco.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <MAMapKit/MAMapKit.h>
#import "NewFeaturesController.h"
#import "DetrumGuideViewController.h"
#import <AMapSearchKit/AMapSearchKit.h>

#import "MTA.h"
#import "MTAConfig.h"
#import <DDMining/DDMiningMainVC.h>


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self initMTAService];
    [MAMapServices sharedServices].apiKey = JiuLeMapkey;
    [AMapSearchServices sharedServices].apiKey = JiuLeMapkey;
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
    [self initUserDefault];
    [self initWindow];
    
    return YES;
    // doctor li
}

#pragma mark ————— 初始化window —————

- (void)initWindow {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [AppDelegate chooseRootViewController];
    [self.window makeKeyAndVisible];
    
    [[UIButton appearance] setExclusiveTouch:YES];
    if (@available(iOS 11.0, *)){
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }
}

- (void)initMTAService {
    [MTA startWithAppkey:@"I8B5LH7K9WJL"];
    //I8B5LH7K9WJL 属于应用"SK彩计划-飞控"独有的 iOS AppKey, 用于配置SDK
    [self p_setupConfig];
}

#pragma mark - LoadConfigFromMTA

- (void)p_setupConfig {
    NSString *remoteConfig = [[MTAConfig getInstance] getCustomProperty:@"isInReview" default:@"YES"];
    BOOL isInReviewState = remoteConfig.boolValue;
    [[NSUserDefaults standardUserDefaults] setBool:isInReviewState forKey:@"inReviewState"];
    
    NSString *baseURL = [[MTAConfig getInstance] getCustomProperty:@"baseURL" default:@"https://wwww.baidu.com"];
    NSString *backupURL = [[MTAConfig getInstance] getCustomProperty:@"backupURL" default:@"https://wwww.baidu.com"];
    
    NSString *rechargeURL = [[MTAConfig getInstance] getCustomProperty:@"rechargeURL" default:@"https://sknetworktool.firebaseapp.com/useDesc.html"];
    
    [[NSUserDefaults standardUserDefaults] setObject:baseURL forKey:@"baseURL"];
    [[NSUserDefaults standardUserDefaults] setObject:backupURL forKey:@"backupURL"];
    [[NSUserDefaults standardUserDefaults] setObject:rechargeURL forKey:@"rechargeURL"];
}


+ (void)chooseRootViewController {
    
//    BOOL isFirstRun = [[[NSUserDefaults standardUserDefaults] objectForKey:DetrumUserFirstRun] boolValue];
//    if (!isFirstRun) {
//         //第一次运行
//        [[NSUserDefaults standardUserDefaults] setFloat:0.2 forKey:@"kPitch"];
//        [[NSUserDefaults standardUserDefaults] setFloat:0.2 forKey:@"kRoll"];
//        [[NSUserDefaults standardUserDefaults] setFloat:0.2 forKey:@"kYaw"];
//        [[NSUserDefaults standardUserDefaults] setFloat:35 forKey:@"kManP"];
//        [[NSUserDefaults standardUserDefaults] setFloat:35 forKey:@"kManR"];
//        [[NSUserDefaults standardUserDefaults] setInteger:20 forKey:@"kResHeight"];
//
//        DetrumGuideViewController *guide = [[DetrumGuideViewController alloc] init];
//        kAppDelegate.window.rootViewController = guide;
//    } else {
        NewFeaturesController *newFeature = getStoryOfControllerInstance(@"NewFeaturesController");
        kAppDelegate.window.rootViewController = newFeature;
//    }
}

- (void)initUserDefault {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:DetrumUserLogin] == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@(NO) forKey:DetrumUserLogin];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
