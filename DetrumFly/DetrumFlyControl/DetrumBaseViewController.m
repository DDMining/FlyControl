//
//  DtFlyBaseViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseViewController.h"

@interface DetrumBaseViewController ()

@end

@implementation DetrumBaseViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)showSvprogress:(NSString *)str {
    [SVProgressHUD showWithStatus:str != nil ? str : @"加载中..."];
}

- (void)dismiss {
    [SVProgressHUD dismiss];
}

- (void)backToTopView {
    if (self.navigationController == nil || self.navigationController.viewControllers.count == 0) {
        [self dismissViewControllerAnimated:YES completion:_backBlock];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

/**
 * 自定义Nav左右按钮(左以图片的形式，右边可图片可文字)
 * @parama 右边按钮的图片或者title，取其一
 * @parama 右边按钮的SEL
 * @parama 左边按钮的SEL
 * @parama 左边按钮的图片
 */
- (void)showNavBarLeftImgAndRightTitle:(NSString*)title
                          andRightImge:(UIImage *)rImg
                        andRightAction:(SEL)raction
                         andLeftAction:(SEL)laction
                           andLeftImge:(UIImage *)img{
    if (img) {
        _leftNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftNavBtn setBackgroundImage:img forState:UIControlStateNormal];
        _leftNavBtn.adjustsImageWhenHighlighted = NO;
        if (laction) {
            [_leftNavBtn addTarget:self action:laction forControlEvents:UIControlEventTouchUpInside];
        }
         CGFloat height = kNavigationBtnHeight;
        CGFloat width = height * img.size.width / img.size.height;
        if (height/width < 0.8) {
            CGFloat scale = img.size.height / img.size.width;
            _leftNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       height,
                                       height*scale);
        } else {
            _leftNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       width,
                                       height);
        }
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBtn];
    }
    if (title || rImg) {
        _rightNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIBarButtonItem *rigthBar = [[UIBarButtonItem alloc] initWithCustomView:_rightNavBtn];
        if (rImg) {
            [_rightNavBtn setBackgroundImage:rImg forState:UIControlStateNormal];
            _rightNavBtn.adjustsImageWhenHighlighted = NO;
            CGFloat height = kNavigationBtnHeight;
            CGFloat width = height * rImg.size.width / rImg.size.height;
            if (height/width < 0.8) {
                CGFloat scale = rImg.size.height / rImg.size.width;
                _rightNavBtn.frame = CGRectMake(0,
                                            kNavigationBtnSpaceHorizontal,
                                            height,
                                            height*scale);
            } else {
                _rightNavBtn.frame = CGRectMake(0,
                                            kNavigationBtnSpaceHorizontal,
                                            width,
                                            height);
            }
        }else {
            [_rightNavBtn setFrame:CGRectMake(15, kNavigationBtnSpaceHorizontal, 80, 30)];
            [_rightNavBtn setTitle:title forState:UIControlStateNormal];
        }
        [_rightNavBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightNavBtn addTarget:self action:raction forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = rigthBar;
    }
    
}
/**
 *  @brief  自定义Nav左右按钮(左以图片的形式，右边可图片可文字，且可以定义选中状态的图片)
 *
 *  @param title      右边控件的文字
 *  @param rImg       右边控件的图片
 *  @param selectImg  右边控件的选中图片
 *  @param raction    右边控件的点击事件
 *  @param laction    左边控件的点击事件
 *  @param img        左边控件的图片
 *  @param selectLimg 左边控件的选中图片
 */
- (void)showNavBarLeftImgAndRightTitle:(NSString*)title
                          andRightImge:(UIImage *)rImg
                     andRightSeleteImg:(UIImage *)selectImg
                        andRightAction:(SEL)raction
                         andLeftAction:(SEL)laction
                           andLeftImge:(UIImage *)img
                      andLeftSeleteImg:(UIImage *)selectLimg {
    if (img) {
        _leftNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftNavBtn setBackgroundImage:img forState:UIControlStateNormal];
        if (selectLimg) {
            [_leftNavBtn setBackgroundImage:selectLimg forState:UIControlStateSelected];
        }
        _leftNavBtn.adjustsImageWhenHighlighted = NO;
        if (laction) {
            [_leftNavBtn addTarget:self action:laction forControlEvents:UIControlEventTouchUpInside];
        }
        CGFloat height = kNavigationBtnHeight;
        CGFloat width = height * img.size.width / img.size.height;
        if (height/width < 0.8) {
            CGFloat scale = img.size.height / img.size.width;
            _leftNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       height,
                                       height*scale);
        } else {
            _leftNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       width,
                                       height);
        }
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBtn];
    }
    if (title || rImg) {
        _rightNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        UIBarButtonItem *rigthBar = [[UIBarButtonItem alloc] initWithCustomView:_rightNavBtn];
        if (rImg) {
            [_rightNavBtn setBackgroundImage:rImg forState:UIControlStateNormal];
            _rightNavBtn.adjustsImageWhenHighlighted = NO;
            _rightNavBtn.contentMode = UIViewContentModeScaleAspectFit;
            CGFloat height = kNavigationBtnHeight;
            CGFloat width = height * rImg.size.width / rImg.size.height;
            if (height/width < 0.8) {
                CGFloat scale = rImg.size.height / rImg.size.width;
                _rightNavBtn.frame = CGRectMake(0,
                                            kNavigationBtnSpaceHorizontal,
                                            height,
                                            height*scale);
            } else {
                _rightNavBtn.frame = CGRectMake(0,
                                            kNavigationBtnSpaceHorizontal,
                                            width,
                                            height);
            }
            if (selectImg) {
                [_rightNavBtn setBackgroundImage:selectImg forState:UIControlStateSelected];
            }
        }else {
            [_rightNavBtn setFrame:CGRectMake(15, kNavigationBtnSpaceHorizontal, 80, 30)];
            [_rightNavBtn setTitle:title forState:UIControlStateNormal];
        }
        [_rightNavBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightNavBtn addTarget:self action:raction forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = rigthBar;
    }
}

/**
 * 自定义Nav左右按钮(左以图片的形式，右边文字,或者右边没有按钮显示)
 * @parama 右边按钮的title
 * @parama 右边按钮的SEsL
 * @parama 左边按钮的SEL
 * @parama 左边按钮的图片
 */
- (void)showNavBarWithRightTitle:(NSString *)rightTitle
                  andRightAction:(SEL)raction
                   andLeftAction:(SEL)laction
               andRightBtnIsShow:(BOOL) isShow{
    //左边返回为箭头，右边是文字
    _leftNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
     if (laction) {
        [_leftNavBtn addTarget:self action:laction forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        [_leftNavBtn addTarget:self action:@selector(backToTopView) forControlEvents:UIControlEventTouchUpInside];
        
    }
    UIImage *leftImg = [UIImage imageNamed:DetrumAppNavBarBackBGI];
     CGFloat height = kNavigationBtnHeight;
    CGFloat width = height * leftImg.size.width / leftImg.size.height;
    if (height/width < 0.8) {
        CGFloat scale = leftImg.size.height / leftImg.size.width;
        _leftNavBtn.frame = CGRectMake(0,
                                   kNavigationBtnSpaceHorizontal,
                                   height,
                                   height*scale);
    } else {
        _leftNavBtn.frame = CGRectMake(0,
                                   kNavigationBtnSpaceHorizontal,
                                   width,
                                   height);
    }
    [_leftNavBtn setBackgroundImage:leftImg forState:UIControlStateNormal];
    UIBarButtonItem *leftBar = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBtn];
    self.navigationItem.leftBarButtonItem = leftBar;
    
    if (isShow) {
        _rightNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightNavBtn setFrame:CGRectMake(15, kNavigationBtnSpaceHorizontal, 60, 30)];
        [_rightNavBtn setTitle:rightTitle forState:UIControlStateNormal];
        [_rightNavBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightNavBtn addTarget:self action:raction forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rigthBar = [[UIBarButtonItem alloc] initWithCustomView:_rightNavBtn];
        self.navigationItem.rightBarButtonItem = rigthBar;
        
    }
    
}

/**
 * 自定义Nav左右按钮(左以图片的形式，右边文字可设置文字大小和宽度)
 * @parama 右边按钮的title
 * @parama 右边按钮的SEL
 * @parama 左边按钮的SEL
 * @parama 左边按钮的图片
 */
- (void)showNavBarWithRightTitle:(NSString *)rightTitle
                  andRightAction:(SEL)raction
                      andLeftImg:(NSString *)imageName
                   andLeftAction:(SEL)laction
                     andFontSize:(CGFloat)size
                   andRightWidth:(CGFloat)width{
    
    //左边返回为箭头，右边是文字
    if(laction != nil){
        _leftNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (imageName == nil) {
            [_leftNavBtn setBackgroundImage:[UIImage imageNamed:DetrumAppNavBarBackBGI] forState:UIControlStateNormal];
        } else {
            [_leftNavBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        }
        if (laction) {
            [_leftNavBtn addTarget:self action:laction forControlEvents:UIControlEventTouchUpInside];
            
        }else{
            [_leftNavBtn addTarget:self action:@selector(backToTopView) forControlEvents:UIControlEventTouchUpInside];
            
        }
        UIImage *leftImg = [UIImage imageNamed:DetrumAppNavBarBackBGI];
        CGFloat height = kNavigationBtnHeight;
        CGFloat width = height * leftImg.size.width / leftImg.size.height;
        if (height/width < 0.8) {
            CGFloat scale = leftImg.size.height / leftImg.size.width;
            _leftNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       height,
                                       height*scale);
        } else {
            _leftNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       width,
                                       height);
        }
        UIBarButtonItem *leftBar = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBtn];
        self.navigationItem.leftBarButtonItem = leftBar;
    }
    
    _rightNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rightNavBtn setFrame:CGRectMake(35, kNavigationBtnSpaceHorizontal, width, 30)];
    [_rightNavBtn setTitle:rightTitle forState:UIControlStateNormal];
    [_rightNavBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_rightNavBtn addTarget:self action:raction forControlEvents:UIControlEventTouchUpInside];
    [_rightNavBtn.titleLabel setFont: [UIFont systemFontOfSize:size]];
    UIBarButtonItem *rigthBar = [[UIBarButtonItem alloc] initWithCustomView:_rightNavBtn];
    self.navigationItem.rightBarButtonItem = rigthBar;
}

/**
 * 自定义Nav左右按钮(左边默认图片，右边文字可设置文字大小和宽度)
 * @parama 右边按钮的title
 * @parama 右边按钮的SEL
 * @parama 左边按钮的SEL
 * @parama 左边按钮的图片
 */
#pragma mark - network reuqest
- (void)showNavBarWithRightTitle:(NSString *)rightTitle
                  andRightAction:(SEL)raction
                   andLeftAction:(SEL)laction
                     andFontSize:(CGFloat)size
                   andRightWidth:(CGFloat)width{
    
    //左边返回为箭头，右边是文字
    if(laction != nil){
        _leftNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftNavBtn setBackgroundImage:[UIImage imageNamed:DetrumAppNavBarBackBGI] forState:UIControlStateNormal];
        if (laction) {
            [_leftNavBtn addTarget:self action:laction forControlEvents:UIControlEventTouchUpInside];
            
        }else{
            [_leftNavBtn addTarget:self action:@selector(backToTopView) forControlEvents:UIControlEventTouchUpInside];
            
        }
        UIImage *leftImg = [UIImage imageNamed:DetrumAppNavBarBackBGI];
        CGFloat height = kNavigationBtnHeight;
        CGFloat width = height * leftImg.size.width / leftImg.size.height;
        if (height/width < 0.8) {
            CGFloat scale = leftImg.size.height / leftImg.size.width;
            _leftNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       height,
                                       height*scale);
        } else {
            _leftNavBtn.frame = CGRectMake(0,
                                       kNavigationBtnSpaceHorizontal,
                                       width,
                                       height);
        }
        UIBarButtonItem *leftBar = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBtn];
        self.navigationItem.leftBarButtonItem = leftBar;
    }
    
    _rightNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rightNavBtn setFrame:CGRectMake(25, kNavigationBtnSpaceHorizontal, width, 30)];
    [_rightNavBtn setTitle:rightTitle forState:UIControlStateNormal];
    [_rightNavBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_rightNavBtn addTarget:self action:raction forControlEvents:UIControlEventTouchUpInside];
    [_rightNavBtn.titleLabel setFont: [UIFont systemFontOfSize:size]];
    UIBarButtonItem *rigthBar = [[UIBarButtonItem alloc] initWithCustomView:_rightNavBtn];
    self.navigationItem.rightBarButtonItem = rigthBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    NSLog(@"%@",NSStringFromClass([self class]));
}

@end
