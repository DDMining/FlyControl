//
//  NSString+MD5.h
//  SpellBus
//
//  Created by wangfang on 15/1/31.
//  Copyright (c) 2015年 publicNetwork. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

/**
 *  32位MD5加密
 *
 *  @return 32位MD5加密结果
 */
- (NSString *)MD5;

/**
 *  私密加密
 *
 */
- (NSString *)myMD5;


@end
