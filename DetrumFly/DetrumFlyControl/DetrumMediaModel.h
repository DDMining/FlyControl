//
//  DetrumMediaModel.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

@import UIKit;
@import AVFoundation;
#import "DataModel.h"
#import "DetrumMediaImgModel.h"
#import "DetrumMediaMovieModel.h"
#import <CoreLocation/CoreLocation.h>
#import <AssetsLibrary/AssetsLibrary.h>
typedef NS_ENUM(NSInteger , DetrumMediaType) {
    DetrumMediaTypeVideo,
    DetrumMediaTypeImg,
};


typedef NS_ENUM(NSInteger , DetrumImageClickType) {
    DetrumImageClickTypeMediaNone,
    DetrumImageClickTypeMediaNewOrOld,
    DetrumImageClickTypeMediaSelect,
};


/**
 *
 1.ALAssetPropertyType 资源的类型（照片，视频）
 2.ALAssetPropertyLocation 资源地理位置（无位置信息返回null）
 3.ALAssetPropertyDuration 播放时长（照片返回ALErorInvalidProperty)
 4.ALAssetPropertyOrientation 方向（共有8个方向，参见：ALAssetOrientation)
 5.ALAssetPropertyDate 拍摄时间（包含了年与日时分秒）
 6.ALAssetPropertyRepresentations 描述（打印看了下，只有带后缀的名称）
 7.ALAssetPropertyURLs（返回一个字典，键值分别是文件名和文件的url）
 8.ALAssetPropertyAssetURL 文件的url )
 */
@interface DetrumMediaModel : DataModel
@property (nonatomic, assign) DetrumMediaType type;
@property (nonatomic, assign) DetrumImageClickType clickType;
@property (nonatomic, assign) BOOL isSeleted;
@property (nonatomic, strong) CLLocation *currenLocation;
@property (nonatomic, assign) NSInteger duration;
@property (nonatomic, assign) ALAssetOrientation orientation;
@property (nonatomic, strong) NSDate *camareDate;
@property (nonatomic, strong) NSString *dateStr;
@property (nonatomic, strong) NSString *representations;
@property (nonatomic, strong) NSDictionary *dic;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, strong) UIImage *fullScreenImage;
@property (nonatomic, strong) ALAssetRepresentation *defaul;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) UInt32 dataBaseId;
@property (nonatomic, strong) DetrumMediaImgModel *imgModel;
@property (nonatomic, strong) DetrumMediaMovieModel *moviewModel;
@end
