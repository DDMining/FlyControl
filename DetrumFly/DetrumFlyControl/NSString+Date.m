//
//  NSString+Date.m
//  IPax
//
//  Created by xcq on 15/4/14.
//  Copyright (c) 2015年 Fine. All rights reserved.
//

#import "NSString+Date.h"

#define kDateTimeFormat                     @"yyyy-MM-dd HH:mm:ss"
#define kDateFormat                         @"yyyy-MM-dd"
#define KTimeFormat                         @"HH:mm"

@implementation NSString (Date)

- (NSString *)getTimeString {
    @try {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:kDateTimeFormat];
        NSDate *date = [format dateFromString:self];
        [format setDateFormat:kDateFormat];
        return [format stringFromDate:date];
    } @catch (NSException *e) {
        return nil;
    }
}

- (NSString *)getDateString {
    @try {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:kDateTimeFormat];
        NSDate *date = [format dateFromString:self];
        [format setDateFormat:kDateFormat];
        return [format stringFromDate:date];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

- (NSDate *)getTime {
    @try {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:KTimeFormat];
        return [format dateFromString:self] != nil ? [format dateFromString:self] : [NSDate date];
    }
    @catch (NSException *exception) {
        return [NSDate date];
    }
}

@end
