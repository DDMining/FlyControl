//
//  UIColor+GenerateImage.m
//
//
//  Created on 14-11-22.
//  Copyright (c) 2014年  All rights reserved.
//

#import "UIColor+GenerateImage.h"

@implementation UIColor (GenerateImage)

- (UIImage *)colorToImage {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [self CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
