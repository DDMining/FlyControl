//
//  CQDropDownView.m
//  NIDropDown
//
//  Created by xcq on 16/3/2.
//
//
#import "CQDropDownViewCell.h"
#import "CQDropDownView.h"
#define kCQCellIdentifier @"dropDown"

#define kCellHeight 23
@interface CQDropDownView ()
{
    BOOL isShow;
    CGRect defaultRect;
}
@property (strong, nonatomic) UITableView *table;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (assign, nonatomic) CQDropDownDirection direction;
@property (strong, nonatomic) UILabel *label;

@end
@implementation CQDropDownView

- (instancetype)initWithFrame:(CGRect)frame andDefaultTitle:(NSArray *)titles {
    
    if (self == [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, kCellHeight)]) {
        if (_table==nil) {
            isShow = NO;
        }
        _direction = CQDropDownDirectionDown;
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        self.layer.borderColor = [UIColor darkGrayColor].CGColor;
        self.layer.borderWidth = 2;
        defaultRect = self.frame;
        self.backgroundColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width-15, kCellHeight)];
        label.userInteractionEnabled = YES;
        [label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(show)]];
        label.text = [titles firstObject];
        label.font = [UIFont systemFontOfSize:15.f];
        [self addSubview:label];
        _label = label;

        
        _table = [[UITableView alloc] initWithFrame:CGRectMake(0, label.frame.size.height, self.frame.size.width, 0)];
        _table.delegate = self;
        _table.dataSource = self;
        _table.layer.cornerRadius = 8;
        _table.tableFooterView = [UIView new];
        _table.backgroundColor = [UIColor whiteColor];
        _table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _table.separatorColor = [UIColor grayColor];
        [_table registerNib:[UINib nibWithNibName:@"CQDropDownViewCell" bundle:nil] forCellReuseIdentifier:kCQCellIdentifier];
        [self addSubview:_table];
        
        _dataSource = [NSMutableArray arrayWithArray:titles];
        [_table reloadData];
    }
    return self;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count == 0 ? 0 : _dataSource.count - 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CQDropDownViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCQCellIdentifier forIndexPath:indexPath];
    cell.name.text = [_dataSource objectAtIndex:indexPath.row+1];
    if (_dataSource.count == 0) {
        cell.dropDownImg.hidden = NO;
    } else {
        cell.dropDownImg.hidden = YES;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CQDropDownViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    _label.text = cell.name.text;
    [self.delegate CQDropDownDelegateMethod:self andSeleteStr:_label.text];
    [self hidden];
}

- (void)show {
    if (isShow) { [self hidden]; return; }
    isShow = YES;
    _table.scrollEnabled = YES;
    if (_direction == CQDropDownDirectionUp) {
        [UIView animateWithDuration:0.4 animations:^{
            self.frame = (CGRect){{self.frame.origin.x,self.frame.origin.y - (_dataSource.count - 1) * kCellHeight},{CGRectGetWidth(self.frame),_dataSource.count * kCellHeight}};
            _table.frame = (CGRect){{0,0},{CGRectGetWidth(self.frame),(_dataSource.count - 1) * kCellHeight}};
            _label.frame = CGRectMake(10, (_dataSource.count - 1) * kCellHeight, CGRectGetWidth(self.frame), kCellHeight);

        }];
    } else {
         [UIView animateWithDuration:0.5 animations:^{
             self.frame = (CGRect){self.frame.origin,{CGRectGetWidth(self.frame),_dataSource.count  * kCellHeight}};
             _table.frame = (CGRect){{0,kCellHeight},{CGRectGetWidth(self.frame),(_dataSource.count - 1) * kCellHeight}};
         }];
    }
}

- (void)hidden {
    isShow = NO;
    _table.scrollEnabled = NO;
    [UIView animateWithDuration:0.4 animations:^{
        if (_direction == CQDropDownDirectionUp) {
            self.frame = defaultRect;
            _table.frame = (CGRect){{0,0},{CGRectGetWidth(self.frame),0}};
            _label.frame = CGRectMake(10, 0, self.frame.size.width-15, kCellHeight);
        } else {
            self.frame = defaultRect;
            _table.frame = (CGRect){{0,kCellHeight},{CGRectGetWidth(self.frame),0}};
        }
    }];
   
}

- (BOOL)getShowStatus {
    return isShow;
}

- (void)setDirection:(CQDropDownDirection)direction {
    _direction = direction;
}


@end
