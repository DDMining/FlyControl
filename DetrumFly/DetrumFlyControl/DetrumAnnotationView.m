//
//  DetrumMannotationView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/1/31.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumAnnotationView.h"

@implementation DetrumAnnotationView

- (id)initWithAnnotation:(id<MAAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.bounds = CGRectMake(0, 0, 50, 50);
        self.annotation = annotation;
        self.annotationImg = [[UIImageView alloc] initWithFrame:self.bounds];
        self.annotationImg.userInteractionEnabled = YES;
        self.gesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                               action:@selector(tapGesture:)];
        self.gesture.delegate = self;
        [self.annotationImg addGestureRecognizer:_gesture];
        [self addSubview:self.annotationImg];
    }
    return self;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}

- (void)tapGesture:(UIGestureRecognizer *)gesture {
    self.canShowCallout = !self.canShowCallout;
}

@end
