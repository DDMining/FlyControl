//
//  DetrumControlDeviceView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/20.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "Costants.h"
#import "UIColor+RGBConverHex.h"
#import "DetrumControlDeviceView.h"
#import "DetrumControlTableViewCell.h"

#define kCellIndentifer @"control"
@implementation DetrumControlDeviceView 

- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithHex:0x696F72];
        [self viewDidLoad:frame];
        [self initParameter];
    }
    return self;
}

- (void)initParameter {
    _dataSource = [NSMutableArray arrayWithArray:@[@"飞行器自检",
                                                   @"飞控状态",
                                                   @"云台状态",
                                                   @"图传状态",
                                                   @"遥控器模式",
                                                   @"飞行器电量",
                                                   @"飞行器电池温度",
                                                   @"SD卡剩余容量",]];
    
    _parameters = [NSMutableArray arrayWithArray:@[@"N/A",
                                                   @"N/A",
                                                   @"N/A",
                                                   @"N/A",
                                                   @"N/A",
                                                   @"N/A",
                                                   @"N/A",
                                                   @"N/A",
                                                   ]];
    [self.tableView reloadData];
}

- (void)viewDidLoad:(CGRect)frame {
    
    _topView = [UIView new];
    _topView.frame = CGRectMake(0, 0, frame.size.width, 37);
    _topView.backgroundColor = [UIColor clearColor];
    [self addSubview:_topView];
    
    UIView *lineView = [UIView new];
    lineView.frame = (CGRect){{0,CGRectGetMaxY(_topView.frame) - 0.5},{CGRectGetWidth(_topView.frame) , 0.5}};
    lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [_topView addSubview:lineView];
    
    _titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(_topView.frame) - 100/2, 0, 100, 37)];
    _titleLb.textColor = [UIColor whiteColor];
    _titleLb.font = [UIFont systemFontOfSize:16];
    _titleLb.text = @"飞行状态信息";
    [_topView addSubview:_titleLb];
    
    _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _closeBtn.frame = CGRectMake(CGRectGetMaxX(_topView.frame) - 34, CGRectGetHeight(_topView.frame) / 2 - 15, 30, 30);
    [_closeBtn setBackgroundImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [_closeBtn addTarget:self action:@selector(exitThisView:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_closeBtn];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_topView.frame), CGRectGetWidth(_topView.frame), CGRectGetHeight(self.frame) - CGRectGetHeight(_topView.frame)) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    [_tableView registerNib:[UINib nibWithNibName:@"DetrumControlTableViewCell"
                                           bundle:nil]
     forCellReuseIdentifier:kCellIndentifer];
    
    [self addSubview:_tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count == 0 ? 0 : _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetrumControlTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIndentifer forIndexPath:indexPath];
    [self setUpCell:cell indexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 39;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _seleteAction([_dataSource objectAtIndex:indexPath.row] , indexPath.row);
}

- (void)exitThisView:(UIButton *)sender {
    _action();
}

- (void)setUpCell:(DetrumControlTableViewCell *)cell
        indexPath:(NSIndexPath *)path {
    cell.name.text = _dataSource[path.row];
    cell.parameterLb.text = _parameters[path.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.layer.cornerRadius = 3.f;
    cell.layer.masksToBounds = YES;
    cell.action = ^() {
        _seleteAction(@"校准", -1);
    };
    DFLog(@"%zd",path.row);
    if (path.row == 1) {
        [cell setControlType:DetrumControlTypeHaveBtn];
    } else if (path.row == 4) {
        [cell setControlType:DetrumControlTypeHaveIndicator];
    } else {
        [cell setControlType:DetrumControlTypeCustom];
    }
}

@end
