//
//  DetrumSetView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#define kBtnCount 7
#define kBtnSetImg(Value) ((UIButton *)_BtnOutlets[Value])
#import "DetrumSetView.h"
#import "UserDataMarco.h"
#import "DetrumFlyMarco.h"
#import "UIColor+RGBConverHex.h"

@implementation DetrumSetView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {
    }
    return self;
}

- (void)awakeFromNib {
    [self viewDidLoad];
    [self setParameter];
}

- (void)setParameter {
    kBtnSetImg(0).selected = YES;
    _seleteBtn = kBtnSetImg(0);
    _seleteBtn.backgroundColor = [UIColor colorWithHex:0x31C6B4];
    [self changeSetView:0];
}

- (void)viewDidLoad {
    for (int i = 0; i < kBtnCount; i ++) {
        UIButton *btn = kBtnSetImg(i);
        btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [btn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"set_%d",i]] forState:UIControlStateNormal];
    }
    _topView.layer.cornerRadius = 10.f;
//    _topView.layer.masksToBounds = YES;
    _contentView.layer.cornerRadius = 10.f;
    _contentView.layer.masksToBounds = YES;
    _backgroundView.layer.cornerRadius = 10.f;
    _backgroundView.layer.masksToBounds = YES;
}

- (IBAction)setEvent:(id)sender {
    UIButton *button = (UIButton *)sender;
    if (_seleteBtn != nil) {
        if (_seleteBtn == button) {
//            button.selected = !button.selected;
//            if (button.selected == NO) {
//                _seleteBtn = nil;
//            }
        } else {
            _seleteBtn.selected = NO;
            _seleteBtn.backgroundColor = [UIColor colorWithHex:0x000000];
            button.selected = !button.selected;
            _seleteBtn = button;
        }
    } else {
        button.selected = !button.selected;
        _seleteBtn = button;
    }
    if (button.selected) {
        button.backgroundColor = [UIColor colorWithHex:0x31C6B4];
    } else {
        button.backgroundColor = [UIColor colorWithHex:0x000000];
    }
    
    switch (button.tag) {
        case 500:
        {
            //飞控设置
            DFLog(@"");
            
        }
            break;
        case 501:
        {
            //遥控器设置
        }
            break;
        case 502:
        {
            //云台设置
        }
            break;
        case 503:
        {
            //图传设置
        }
            break;
        case 504:
        {
            //WIFI设置
        }
            break;
        case 505:
        {
            //电池设置
        }
            break;
        case 506:
        {
            //更多设置
        }
            break;
    }
    [self changeSetView:button.tag % 500];
}

- (IBAction)exitAction:(id)sender {
    postNotification(FlyControlNotificationRemove);
}

- (void)changeSetView:(NSInteger)index {
    if (contentView == nil) {
        contentView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumSetContentView" owner:self options:nil] lastObject];
        contentView.BtnWidth = kBtnSetImg(0).frame.size.width;
        contentView.frame = _contentView.bounds;
        contentView.currenType = index;
        [contentView updateConstraintsIfNeeded];
        [_contentView addSubview:contentView];
    }
    contentView.currenType = index;
}

@end
