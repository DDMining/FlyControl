//
//  DetrumPublicManager.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CYLTabBarController.h"
@interface DetrumPublicManager : NSObject

+ (CYLTabBarController *)createRootController;
+ (instancetype)shareInstance;

@end
