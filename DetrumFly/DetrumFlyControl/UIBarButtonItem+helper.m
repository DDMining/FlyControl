//
//  UIBarButtonItem+helper.m
//  jyb
//
//  Created by wangfang on 14/12/26.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import "UIBarButtonItem+helper.h"

@implementation UIBarButtonItem (helper)

+ (UIBarButtonItem *)itemWithIcon:(NSString *)icon highIcon:(NSString *)highIcon target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:highIcon] forState:UIControlStateHighlighted];
//    button.frame = (CGRect){CGPointZero, button.currentBackgroundImage.size};
    button.frame = CGRectMake(0, 0, button.currentBackgroundImage.size.width*0.8, button.currentBackgroundImage.size.height*0.8);
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)itemWithTitle:(NSString *)title target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    button.frame = CGRectMake(0, 0, 60, 20);
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];

}
@end
