//
//  DetrumAboutView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/8.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseView.h"

@interface DetrumAboutView : DetrumBaseView <UITableViewDataSource,UITableViewDelegate> {
    NSArray *titleArr;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *name;
@end
