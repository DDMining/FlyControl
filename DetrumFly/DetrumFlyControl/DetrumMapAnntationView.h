//
//  DetrumMapAnntationView.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/6/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MAMapKit/MAAnnotation.h>


@interface DetrumMapAnntationView : NSObject  <MAAnnotation>
@property (assign,nonatomic) CLLocationCoordinate2D coordinate;
@end
