//
//  DetrumGyroscopeTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger , DetrumSensorType) {
    DetrumSensorTypeGyroscope, //陀螺仪
    DetrumSensorTypeAccelerometer, //加速计
    DetrumSensorTypeCompass, //指南针
};

@interface DetrumGyroscopeTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detrumLabel;
@property (weak, nonatomic) IBOutlet UITextField *xAxleTf;
@property (weak, nonatomic) IBOutlet UITextField *yAxleTf;
@property (weak, nonatomic) IBOutlet UITextField *zAxleTf;
@property (assign, nonatomic) DetrumSensorType type;

@end
