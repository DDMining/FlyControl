//
//  CapacityView.h
//  DetrumDrawTest
//
//  Created by xcq on 16/3/9.
//  Copyright © 2016年 Fine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CapacityView : UIView {
    CALayer *layer;
}
- (instancetype)initWithFrame:(CGRect)frame
                       percen:(CGFloat)percen
                    fillColor:(UIColor *)fillColor
                        radiu:(CGFloat)radiu;

- (instancetype)initWithFrame:(CGRect)frame
                       percen:(CGFloat)percen
                    fillColor:(UIColor *)fillColor
                        radiu:(CGFloat)radiu
                 isHorizontal:(BOOL)bol;

@property (nonatomic, assign) CGFloat percent;
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, assign) CGFloat radiu;
@property (nonatomic, assign) BOOL isHorizontal;

@end
