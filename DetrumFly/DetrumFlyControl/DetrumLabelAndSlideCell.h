//
//  DetrumLabelAndSlideCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

@interface DetrumLabelAndSlideCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UISlider *slider;

@end
