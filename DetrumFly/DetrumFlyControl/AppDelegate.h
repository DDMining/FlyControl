//
//  AppDelegate.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/11.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumFlyMarco.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

