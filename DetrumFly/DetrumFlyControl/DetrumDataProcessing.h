//
//  DetrumDataProcessing.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/4/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetrumDataProcessing : NSObject
/**
 * @brief 将十六进制的字符串转成NSData
 *
 * @param string 待转换的十六进制字符串
 */
+ (NSMutableData*)stringToByte:(NSString*)string;
+ (NSData *)dataWithHexString:(NSString *)hexStr;

/**
 * @brief 将NSData转成十六进制字符串
 *
 * @param myD NSData
 */
+ (NSString *)hexStringFromData:(NSData *)myD;
@end
