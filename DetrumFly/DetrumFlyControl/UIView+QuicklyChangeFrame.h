//
//  UIView+QuicklyChangeFrame.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/19.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (QuicklyChangeFrame)

- (void)setRectHeight:(CGFloat)height;
- (void)setRectWidth:(CGFloat)width;
- (void)setRectX:(CGFloat)x;
- (void)setRectY:(CGFloat)y;

@end
