//
//  DetrumLoginViewController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseViewController.h"

@interface DetrumLoginViewController : DetrumBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTt;
@property (weak, nonatomic) IBOutlet UITextField *passWordTt;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@end
