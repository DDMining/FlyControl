//
//  DetrumRemoteControlView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseViewController.h"
#import "DetrumRemoteControlView.h"

@implementation DetrumRemoteControlView

- (void)awakeFromNib {
    if (animation == nil) {
        animation = [CATransition animation];
    }
    [animation setDuration:0.35f];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromRight];
    [animation setFillMode:kCAFillModeForwards];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.layer addAnimation:animation forKey:@"detrumRemote"];
    [self addPointImg];
}

- (void)addPointImg {
    _pointImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zoom Button"]];
    _pointImg.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_pointImg];
}

- (void)moveImg:(NSInteger)index {
    if (index <= 0 && index > 6) {
        return;
    }
    if (timer == nil) {
        timer = [NSTimer timerWithTimeInterval:1.f target:self selector:@selector(movePoint) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    }
    [timer fire];
}

- (void)movePoint {
    
}

- (IBAction)backAction:(id)sender {
    [UIView animateWithDuration:0.35f animations:^{
        [self setRectX:CGRectGetWidth(self.frame)];
    } completion:^(BOOL finished) {
        for (int i = 0 ; i < _animationView.subviews.count; i++) {
            UIView *view = _animationView.subviews[i];
            if (![view isKindOfClass:[DetrumRemoteControlView class]]) {
                view.hidden = NO;
            }
        }
        [self removeFromSuperview];
    }];
}


@end
