//
//  DetrumFourBtnTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

typedef NS_ENUM(NSInteger, DetrumFourBtnclickType) {
    DetrumFourBtnclickTypeRoll,
    DetrumFourBtnclickTypeLpitch,
    DetrumFourBtnclickTypeYaw,
    DetrumFourBtnclickTypeAutoCorrection,
};

typedef void(^BtnClick)(DetrumFourBtnclickType type);

@interface DetrumFourBtnTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIButton *rollBtn;
@property (weak, nonatomic) IBOutlet UIButton *lpitchBtn;
@property (weak, nonatomic) IBOutlet UIButton *yawBtn;
@property (weak, nonatomic) IBOutlet UIButton *autocorrectionBtn;
@property (copy, nonatomic) BtnClick callback;
@end
