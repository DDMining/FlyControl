//
//  DetrumClassificationViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumClassificationViewCell : DetrumBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;

@end
