//
//  UserAlerts.m
//  OpenOOIM
//
//  Created on xcq 14-9-25.
//  Copyright (c) 2014年 All rights reserved.
//

#import "UserAlerts.h"

@implementation UserAlerts

+ (UserAlerts *)instance {
    static dispatch_once_t onceToken;
    static UserAlerts *instance = nil;
    
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

/**
 * Show error message with title, message, delegate and tag.
 */
- (void)showErrorMsg:(NSString *)title message:(NSString *)msg delegate:(id<UIAlertViewDelegate>)delegate andTag:(NSInteger)tag {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alert.tag = tag;
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

/**
 * Show error message with title, message and delegate.
 */
- (void)showErrorMsg:(NSString *)title message:(NSString *)msg delegate:(id<UIAlertViewDelegate>)delegate {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

/**
 * Show error message with title and message.
 */
- (void)showErrorMsg:(NSString *)title message:(NSString *)msg {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

/**
 * Show error message with specified message.
 */
- (void)showErrorMsg:(NSString *)msg {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:msg message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

/**
 * Show error message with specified message and tag.
 */
- (void)showErrorMsg:(NSString *)msg tag:(NSInteger)tag target:(id)target {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:msg message:nil delegate:target cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alert.tag = tag;
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

/**
 * Show confirm message with specified message and tag.
 */
- (void)confirmMsg:(NSString *)msg tag:(NSInteger)tag target:(id)target {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:msg message:nil delegate:target cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag = tag;
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

@end
