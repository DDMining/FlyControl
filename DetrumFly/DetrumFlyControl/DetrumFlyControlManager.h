//
//  DetrumFlyControlManager.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumAboutView.h"
#import "DetrumSensorView.h"
#import "DetrumCourseReversalView.h"
#import "DetrumBaseViewController.h"
#import "DetrumGestureSensitivityView.h"
#import <Foundation/Foundation.h>

typedef  NS_ENUM(NSInteger , DetrumSetType) {
    /** 飞控 */
    DetrumSetTypeFlyControl,
     /** 遥控 */
    DetrumSetTypeRemoteControl,
     /** 云台 */
    DetrumSetTypePanTilt,
    /** 图传设置 */
    DetrumSetTypeGraphicCommunication,
     /** WIFI设置 */
    DetrumSetTypeWifi,
    /** 电池设置 */
    DetrumSetTypeBattery,
    /** 其他设置 */
    DetrumSetTypeOther,
};

typedef NS_ENUM(NSInteger, DetrumContentSubViewType) {
    DetrumContentSubViewTypeSwitch,
    DetrumContentSubViewTypeSlideAddInputView,
    DetrumContentSubViewTypeRadiu,
    DetrumContentSubViewTypeIndictor,
};

@interface DetrumFlyControlManager : NSObject
{
    @private
    DetrumSetType currenType;
}
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) NSDictionary *seleteDatas;
+ (instancetype)shareInstance;
- (void)setCurrenType:(DetrumSetType)type;

@end
