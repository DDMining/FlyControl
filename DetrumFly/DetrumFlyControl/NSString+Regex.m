//
//  NSString+Regex.m
//  大博智能
//
//  Created on 14-10-24.
//  Copyright (c) 2014年 All rights reserved.
//

#import "NSString+Regex.h"

@implementation NSString (Regex)

- (NSString *)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}


@end
