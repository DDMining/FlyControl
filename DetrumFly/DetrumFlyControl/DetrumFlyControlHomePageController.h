//
//  DetrumFlyControlHomePageController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/14.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import <MAMapKit/MAMapKit.h>
#import "DetrumBaseViewController.h"
@class DXPopover;
typedef NS_ENUM(NSInteger , DetrumFlyControlCurrenShowPage) {
    DetrumFlyControlCurrenShowPageMovie,
    DetrumFlyControlCurrenShowPageMap,
};

@interface DetrumFlyControlHomePageController : DetrumBaseViewController {
    CGFloat _popoverWidth;
    CGFloat _popoverHeight;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightLayout;
/** 顶部视图 */
@property (weak, nonatomic) IBOutlet UIView *topView;
/** 起飞按钮 */
@property (weak, nonatomic) IBOutlet UIButton *takeOffBtn;
/** 降落按钮的顶部高度布局 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeOffTopLayout;
/** 降落按钮 */
@property (weak, nonatomic) IBOutlet UIButton *landingBtn;
/** 定速巡航 */
@property (weak, nonatomic) IBOutlet UIButton *CruiseControlBtn;
@property (nonatomic, strong) DXPopover *popover;
@property (weak, nonatomic) IBOutlet UILabel *firstParameterLb;
@property (weak, nonatomic) IBOutlet UILabel *secondParameterLb;
@property (weak, nonatomic) IBOutlet UILabel *threeParameterLb;
@property (weak, nonatomic) IBOutlet UILabel *fourParameterLb;
@property (weak, nonatomic) IBOutlet UILabel *fiveParameterLb;

// iphoneX
@end
