//
//  DetrumMyZoneViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "UserDataMarco.h"
#import "DetrumLgAfterViewCell.h"
#import "DetrumLgBeforeViewCell.h"
#import "DetrumBBsViewController.h"
#import "DetrumMallViewController.h"
#import "DetrumLoginViewController.h"
#import "DetrumMyZoneTableViewCell.h"
#import "DetrumMyZoneViewController.h"
#import "DetrumSettingViewController.h"
#import "DetrumFeedBackViewController.h"
#import "DetrumRegisteredViewController.h"

@interface DetrumMyZoneViewController() <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (assign, nonatomic) BOOL isLogin;
@end

@implementation DetrumMyZoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我";
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    _isLogin = [[[NSUserDefaults standardUserDefaults] objectForKey:DetrumUserLogin] boolValue];
    [self initUI];
}

- (void)initUI {
    self.tableView.tableFooterView = [UIView new];
    if (self.tableView.delegate == nil) {
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
    }
    [self.tableView reloadData];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - table delegate 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_isLogin) {
        return 4;
    } else {
        return 4;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_isLogin) {
        if (indexPath.row == 0) {
            if (iPhone5) {
                return 146;
            } else if (iPhone6) {
                return 158;
            } else if (iPhone6P) {
                return 169;
            }
        }
    } else {
        if (indexPath.row == 0) {
            return 233;
        }
    }
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (_isLogin) {
            DetrumLgAfterViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"DetrumLgAfterViewCell" owner:self options:nil] lastObject];
            return cell;
        } else {
            DetrumLgBeforeViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"DetrumLgBeforeViewCell" owner:self options:nil] lastObject];
            cell.click = ^(DetrumClickType type ) {
                if (type == DetrumClickTypeLogin) {
                    [self gotoLogin];
                } else {
                    [self gotoCreateAccount];
                }
            };
            return cell;
        }
    } else {
        DetrumMyZoneTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"DetrumMyZoneTableViewCell" owner:self options:nil] lastObject];
        if (_isLogin) {
            switch (indexPath.row) {
                case 1:{
                    cell.myZoneImg.image = [UIImage imageNamed:@"flyfanmall"];
                    cell.myZoneTitle.text = @"翔迷商场";
                    cell.className = NSStringFromClass([DetrumMallViewController class]);

                }
                    break;
                case 2:{
                    cell.myZoneImg.image = [UIImage imageNamed:@"set"];
                    cell.myZoneTitle.text = @"设置";
                    cell.className = NSStringFromClass([DetrumSettingViewController class]);
                }
                    break;
                case 3:{
                    cell.myZoneImg.image = [UIImage imageNamed:@"Feedback"];
                    cell.myZoneTitle.text = @"用户反馈";
                    cell.className = NSStringFromClass([DetrumFeedBackViewController class]);
                }
                    break;
                default:
                    break;
            }
        } else {
            switch (indexPath.row) {
//                case 1:{
//                    cell.myZoneImg.image = [UIImage imageNamed:@"bbs"];
//                    cell.myZoneTitle.text = @"翔迷论坛";
//                    cell.className = NSStringFromClass([DetrumBBsViewController class]);
//                }
//                    break;
                case 1:{
                    cell.myZoneImg.image = [UIImage imageNamed:@"flyfanmall"];
                    cell.myZoneTitle.text = @"翔迷商场";
                    cell.className = NSStringFromClass([DetrumMallViewController class]);
                }
                    break;
                case 2:{
                    cell.myZoneImg.image = [UIImage imageNamed:@"set"];
                    cell.myZoneTitle.text = @"设置";
                    cell.className = NSStringFromClass([DetrumSettingViewController class]);
                }
                    break;
                case 3:{
                    cell.myZoneImg.image = [UIImage imageNamed:@"Feedback"];
                    cell.myZoneTitle.text = @"用户反馈";
                    cell.className = NSStringFromClass([DetrumFeedBackViewController class]);

                }
                    break;
            }
        }
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 0) {
        DetrumMyZoneTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        Class class = NSClassFromString(cell.className);
        UIViewController * viewController = class.new;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)gotoCreateAccount {
    DetrumRegisteredViewController *regist = [[DetrumRegisteredViewController alloc] initWithNibName:@"DetrumRegisteredViewController" bundle:nil];
    [self.navigationController pushViewController:regist animated:YES];
}

- (void)gotoLogin {
    DetrumLoginViewController *login = [[DetrumLoginViewController alloc] initWithNibName:@"DetrumLoginViewController" bundle:nil];
    [self.navigationController pushViewController:login animated:YES];
    
}

@end
