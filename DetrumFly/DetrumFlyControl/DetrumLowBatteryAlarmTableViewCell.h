//
//  DetrumLowBatteryAlarmTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/7.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

@interface DetrumLowBatteryAlarmTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *redAlarmValueLb;
@property (weak, nonatomic) IBOutlet UILabel *blackAlarmValueLb;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@end
