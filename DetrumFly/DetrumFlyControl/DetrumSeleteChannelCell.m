//
//  DetrumSeleteChannelCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/4.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumSeleteChannelCell.h"

@implementation DetrumSeleteChannelCell

- (void)awakeFromNib {
    [self initDropView];
}

- (void)initDropView {
    if (_dropView == nil) {
        _dropView = [[CQDropDownView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_twoSeleteTitle.frame) + CGRectGetWidth(_twoSeleteTitle.frame) + 60, CGRectGetHeight(self.frame)/2 - 23/2 - 2, CGRectGetWidth(self.frame)-CGRectGetMaxX(_twoSeleteTitle.frame) - 10, 23) andDefaultTitle:@[@"信道1",@"信道2"]];
        NSLog(@"%f",CGRectGetHeight(self.frame)/2 - 23/2);
        _dropView.delegate = self;
        [_dropView setDirection:CQDropDownDirectionDown];
        [self addSubview:_dropView];
    }
}

- (IBAction)radioEvent:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    if (btn == _oneSeleteBtn) {
        _oneSeleteBtn.selected = !btn.selected;
    } else {
        _twoSeleteBtn.selected = !btn.selected;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
