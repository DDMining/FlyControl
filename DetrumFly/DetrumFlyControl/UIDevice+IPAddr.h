//
//  UIDevice+IPAddr.h
//  WiFiTest
//
//  Created by xcq on 16/3/14.
//  Copyright © 2016年 Fine. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <sys/sysctl.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import "IPGetAddress.h"


#if  TARGET_IPHONE_SIMULATOR
//#include <net/route.h>
#elif TARGET_OS_IPHONE
#include "route.h"
#endif

@interface UIDevice (IPAddr)
- (NSString *)localIP;
- (NSString *)getGatewayIPAddress;
@end
