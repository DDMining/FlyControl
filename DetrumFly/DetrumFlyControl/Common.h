//
//  Common.h
//  CityHunter
//
//  Updated by xcq on 2014-10-21.
//  Copyright (c) 2014年 CieNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetrumFlyMarco.h"
#import "UIColor+RGBConverHex.h"
#import "UserDataMarco.h"

#define kAPPID @"1078960669"

#define msgSend(...) ((void (*)(void *, SEL, UIView *))objc_msgSend)(__VA_ARGS__)
#define msgTarget(target) (__bridge void *)(target)


#define TableHeadRefreshDateKey @"tableRefreshDateKey"

#define TICK   NSDate *startTime = [NSDate date];  \
                JLLog(@"begin Time : %@",startTime);

#define TOCK   JLLog(@"stop Time: %f", -[startTime timeIntervalSinceNow]);

//颜色转换
#define RGBCONVERHEX (hexValue) [UIColor colorWithHex:hexValue]

#ifndef GET_VIEW_RIGHT
#define GET_VIEW_RIGHT(view) (view.frame.origin.x + view.frame.size.width)
#endif
#ifndef GET_VIEW_BOTTOM
#define GET_VIEW_BOTTOM(view) (view.frame.origin.y + view.frame.size.height)
#endif
//三目运算符
#define SecurityGet(value,value2) TOP(value,nil,value,value2)
#define TOP(value,key,value1,value2) value != key ? value1 : value2
#define TOPDEFAULT(value) TOPEAQULNIL(value, @"暂无")


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//忽略     [_target performSelector:_action withObject:self]
//方法的警告
#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

#define kAlphaNum  @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789" //只能输入数字和字母
#define kAlpha      @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "         //只能输入字符
#define kNumbers     @"0123456789"                                                   //只能输入数字
#define kNumbersPeriod  @"0123456789."

#define kChinese @"^[\u4e00-\u9fa5]{0,}$"
#define kEnglish @"^[A-Za-z]+$"

typedef void(^completion)(BOOL isSuccess ,
                          BOOL isUpdate ,
                          BOOL isNewfeatures,
                          NSDictionary *updateInfo);

@interface Common : NSObject

+ (void)removeRedPoint:(NSInteger)index;
+ (void)createRedPointToTabBar:(NSInteger)index;

//判断当前时间是白天还是晚上,YES:白天 NO:晚上
+ (BOOL)isBetweenFromHour:(NSInteger)fromHour toHour:(NSInteger)toHour;
//判断传入的时间和当前相差多少
+ (NSArray *)calDateIntervalFromEndDate:(NSString *)paramEndDate;
//检查版本更新
+ (void)checkVersionUpdate:(completion)callback;

//是否安装某个应用
+ (BOOL)judgeAppInstall:(NSString *)urlStr;
//打开第三方应用
+ (void)openThirdParty:(NSString *)urlStr;
+ (BOOL)verificationContentIsNum:(NSString *)string;
+ (void)getAppIcon;
+ (void)getUserInfo;
+ (UIImage *)resizeImage:(UIImage *)imgName;
+ (UIImage *)imageWithImageSimple:(UIImage *)image
                     scaledToSize:(CGSize)newSize;

//设置输入控件UITextField和UITextView的border
+ (void)setInputCustomerStyle:(UIView *)input
                  borderColor:(UIColor *)boderColor
                    textColor:(UIColor *)textColor;

//对于网络返回的数据包做一般性的分析，并根据结果发送通知
+ (void)analyseNetworkResponseAndNotify:(id)rspData
                               moduleID:(NSInteger)moduleID
                              funIDSucc:(NSInteger)funIDSucc
                              funIDFail:(NSInteger)funIDFail;
+ (void)analyseNetworkResponseAndNotify:(id)rspData
                               moduleID:(NSInteger)moduleID
                              funIDSucc:(NSInteger)funIDSucc
                              funIDFail:(NSInteger)funIDFail
                                success:(void (^)(NSDictionary *dic))success
                                failure:(void (^)(NSDictionary *dic))failure;


+ (NSString*)getLocalStr:(NSString*)key;

+ (void)showLocalAlert:(NSString*)title message:(NSString*)message;
+ (void)showLocalAlert:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate;

+ (void)showLocalAlert:(NSString*)title message:(NSString*)message andTag:(NSInteger)tag;
+ (void)showLocalAlert:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate andTag:(NSInteger)tag;

+ (void)showAlert:(NSString*)title message:(NSString*)message;
+ (void)showAlert:(NSString*)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate andAlert:(NSInteger)tag;

//+ (void)showJGActionSheet:(NSString *)title
//                  message:(NSString *)message
//              cancelTitle:(NSString*)cancelTitle
//             buttonTitles:(NSArray *)buttonTitles
//              buttonStyle:(JGActionSheetButtonStyle)buttonStyle
//                 delegate:(id<JGActionSheetDelegate>)delegate
//                      tag:(NSUInteger)tag
//               showInView:(UIView *)view;

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

+ (CGSize)fitSizeForText:(NSString*)text font:(UIFont*)font;
+ (CGSize)fitSizeForText:(NSString*)text font:(UIFont*)font maxWidth:(CGFloat)maxWidth;
+ (CGSize)fitSizeForText:(NSString*)text font:(UIFont*)font maxHight:(CGFloat)maxHeight;
+ (CGSize)fitSizeForText:(NSString*)text font:(UIFont*)font maxWidth:(CGFloat)maxWidth maxHight:(CGFloat)maxHeight;

/**
 *返回随机数
 */
+ (int )returnRandom:(int)max;
/**
 * 返回随机名字
 */
+ (NSString *)randomCreateNSString ;

+ (void)callPhone:(NSString *)phone andControl:(UIView *)View;;

+ (void)printFrame:(CGRect)frame message:(NSString*)msg;

+ (BOOL)isEmptyString:(NSString*)string;

+ (NSString *)md5:(NSString*)string;



//只能输入数字或字母
+ (BOOL)isNumAndLetter:(NSString *)string ;

+ (void)setUserName:(NSString *)name;

+ (BOOL)regexString:(NSString*)string match:(NSString*)match;

+ (BOOL)isMobileNumber:(NSString *)string;

+ (BOOL)isUserName:(NSString*)string;

+ (BOOL)isPassword:(NSString*)string;

+ (BOOL)isRealName:(NSString*)string;

+ (BOOL)isIDNumber:(NSString*)string;

+ (BOOL)isValidateEmail:(NSString *)string;
+ (BOOL)verificationNickName:(NSString *)nickName;
//上传图片
+ (void)upLoadImage:(NSString *)kbaseUrl andFileSuffixRandom:(int)max andFile:(UIImage *)image andSucces:(void (^)(id result))callback andFail:(void (^)(id result))failCallback ;

+ (CGSize)sizeWithContent:(NSString *)content AndFont:(UIFont *)font andSize:(CGSize)size  andLineBreak:(NSLineBreakMode)line;

+ (BOOL)checkIsExistPropertyWithInstance:(id)instance
                      verifyPropertyName:(NSString *)verifyPropertyName;

/**
 *  @brief  读取plist文件
 *
 *  @param plistName 文件名（不包含后缀）
 *
 *  @return 字典
 */
+ (NSMutableDictionary *)loadPlist:(NSString *)plistName;
/**
 *  @brief  写plist文件
 *
 *  @param dic   写入的数据
 *  @param plist 文件名
 *
 *  @return 成功/失败
 */
+ (BOOL)writePlist:(NSMutableDictionary *)dic plistName:(NSString *)plist;

//根据输入的参数，转换成url的参数字符串
+ (NSString*)URLEncodedStringFromKeyAndValue:(NSString*)key value:(id)value;

//将字典的参数转换成字符串的数组，用以在post请求的url中
+ (NSArray*)queryStringPairsFromKeyAndValue:(NSString*)key value:(id)value;

+ (BOOL)copyResourceToDocumentsFolder:(NSString*)fileName;

+ (NSString*)getResourceFilePath:(NSString*)fileName;

+ (NSString*)getErrorDescription:(NSString*)errorCode;

+ (BOOL)isNetWorkReachable:(void (^)(NSInteger status))block;

+ (UIViewController*)viewController:(UIView *)view;

+ (double) folderSizeAtPath:(NSString*) folderPath;

+ (CGFloat)adjustScreenHeight;

/** UTC时间转GMT时间 */
+ (NSDate *)getLocalDateFormateUTCDate:(NSDate *)anyDate;

@end
