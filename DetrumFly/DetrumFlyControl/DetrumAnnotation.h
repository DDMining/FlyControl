//
//  DetrumAnnotation.h
//  DetrumFlyControl
//
//  Created by xcq on 16/1/31.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import <MAMapKit/MAMapKit.h>
#import <Foundation/Foundation.h>

@interface DetrumAnnotation : NSObject <MAAnnotation>
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) NSString *annotationName;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat speed;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;


- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coor;

@end
