//
//  DetrumFlyControlTopView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/16.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumFlyMarco.h"
#import "Interface.h"
#import "DetrumMavlinkManager.h"
#import "UserDataMarco.h"
#import "NSTimer+FastCreate.h"
#import "DetrumFlyControlTopView.h"


@interface DetrumFlyControlTopView ()
@property (weak, nonatomic) IBOutlet UIButton *aroundBtn;
/** GPS 连接情况 */
@property (weak, nonatomic) IBOutlet UILabel *gpsConnectStatus;
@property (weak, nonatomic) IBOutlet UILabel *gpsConnectStatus6;
@property (weak, nonatomic) IBOutlet UILabel *gpsConnectStatus6p;

/** 卫星数量  */
@property (weak, nonatomic) IBOutlet UILabel *gpsSatellite;
@property (weak, nonatomic) IBOutlet UILabel *gpsSatellite6;
@property (weak, nonatomic) IBOutlet UILabel *gpsSatellite6p;

/** 设备连接标签 */
@property (weak, nonatomic) IBOutlet UILabel *connectDeviceStatue;
@property (weak, nonatomic) IBOutlet UILabel *connectDeviceStatue6;
@property (weak, nonatomic) IBOutlet UILabel *conncetDeviceStatue6p;
@property (weak, nonatomic) IBOutlet UIView *connectView;
@property (weak, nonatomic) IBOutlet UIView *remetoControlView;
@property (weak, nonatomic) IBOutlet UIView *hdView;
@property (weak, nonatomic) IBOutlet UIImageView *wifiImg;
@property (weak, nonatomic) IBOutlet UIButton *batteryBtn;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) UILabel *tempLabel;
@property (strong, nonatomic) UILabel *tempGpsStatus;
@property (strong, nonatomic) UILabel *tempGpsSatellite;
@end

@implementation DetrumFlyControlTopView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ([super initWithCoder:aDecoder]) {

    }
    return self;
}

- (void)initTimer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:.2f target:self action:@selector(setData) repeat:YES];
        [_timer startTimer];
    }
    [_timer startTimer];
}

- (void)setData {
    if ([DetrumMavlinkManager shareInstance].isConnect) {
        _tempLabel.text = @"设备已连接";
        NSArray *arr = [[DetrumMavlinkManager shareInstance] getDataWithType:PARAM_GPS];
        NSArray *heartbeatArr = [[DetrumMavlinkManager shareInstance] getDataWithType:PARAM_HEARTBEART];
        
        if (arr.count != 0 && arr != nil) {
            NSInteger fixType = [[arr objectAtIndex:arr.count - 2] integerValue];
            NSInteger satellite = [[arr lastObject] integerValue];
            if (fixType >= 0 && fixType <= 1) {
                _tempGpsStatus.text = @"未锁定";
            } else {
                _tempGpsStatus.text = @"已锁定";
            }
            _tempGpsSatellite.text = [[NSString stringWithFormat:@"%zd",satellite] stringByAppendingString:@"颗卫星"];
        }
        if (heartbeatArr.count != 0 && heartbeatArr != nil) {
            NSInteger customMode = [[heartbeatArr objectAtIndex:0] integerValue];
            if (customMode == 65536) {
                //手动模式
                [_aroundBtn setTitle:@"手动" forState:UIControlStateNormal];
            } else if (customMode == 131072) {
                //定高模式
                [_aroundBtn setTitle:@"定高" forState:UIControlStateNormal];
            } else if (customMode == 196608) {
                //定点模式
                [_aroundBtn setTitle:@"定点" forState:UIControlStateNormal];
            }
        }
    } else {
        _tempLabel.text = @"未连接设备";
    }
}

- (void)awakeFromNib {

    if (iPhone5) {
        _tempLabel = _connectDeviceStatue;
        _tempGpsStatus = _gpsConnectStatus;
        _tempGpsSatellite = _gpsSatellite;
    } else if (iPhone6){
        _tempLabel = _connectDeviceStatue6;
        _tempGpsStatus = _gpsConnectStatus6;
        _tempGpsSatellite = _gpsSatellite6;
    } else if (iPhone6P) {
        _tempLabel = _conncetDeviceStatue6p;
        _tempGpsStatus = _gpsConnectStatus6p;
        _tempGpsSatellite = _gpsSatellite6p;
    }
    _connectView.userInteractionEnabled = YES;
    [_connectView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(connectDevice:)]];
    [self initTimer];

}

- (void)connectDevice:(UIGestureRecognizer *)gesture {
    _action(FlyControlViewBtnEventDeviceConnect);
}

- (IBAction)backHome:(id)sender {
    _action(FlyControlViewBtnEventBackHome);
}

- (IBAction)deviceClickEvent:(id)sender {
}

- (IBAction)setClickEvent:(id)sender {
    _action(FlyControlViewBtnEventJoinClick);
}
@end
