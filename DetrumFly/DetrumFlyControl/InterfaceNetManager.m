//
//  InterfaceNetManager.m
//  JiuLe
//
//  Created by xcq on 15/10/31.
//  Copyright © 2015年 Fine. All rights reserved.
//


#import "InterfaceNetManager.h"
#import "Costants.h"

#define kRequestStatus @"status"
#define kRequestMes @"msg"
#define kRequestResult @"result"

@implementation InterfaceNetManager
+ (void)RequestLogin:(NSString *)userName
            PassWord:(NSString *)pwd
          completion:(interfaceManagerBlock )completion {
    NSDictionary *dic = @{@"username": userName,
                          @"pass":pwd};
    [InterfaceNetManager postHTTPServerWith:dic completion:^(BOOL isSucceed, NSString *message, id data, DFError *error) {
        completion(isSucceed,message,data,error);
    } method:Api_Login];
}

+ (void)RequestRegisterOfUserName:(NSString *)username
                         password:(NSString *)password
                           verify:(NSInteger)code
                       compleiton:(interfaceManagerBlock)completion {
    NSDictionary *dic = @{@"email":username,
                          @"emailVerify":@(code),
                          @"pass":password};
    [InterfaceNetManager postHTTPServerWith:dic completion:^(BOOL isSucceed, NSString *message, id data, DFError *error) {
        completion(isSucceed,message,data,error);
    } method:Api_VerifyCode];
}

+ (void)RequestGetVerifyCode:(NSString *)email
                  compleiton:(interfaceManagerBlock)completion {
    [InterfaceNetManager postHTTPServerWith:@{@"email": email} completion:^(BOOL isSucceed, NSString *message, id data, DFError *error) {
        completion(isSucceed,message,data,error);
    } method:Api_getVerifyCode];
}


#pragma mark - 发送请求的公共方法:POST请求
+ (void) postHTTPServerWith:(NSDictionary *)param completion:(interfaceManagerBlock)completion method:(NSString *)method
{
    DFLog(@"api:%@",method);
    [NetManager postWithURL:[KBaseURL stringByAppendingString:method] params:param success:^(id json) {
        BOOL isSuccess = [[json objectForKey:kRequestStatus] boolValue];
        if (isSuccess) {
            completion(YES, @"成功", [json objectForKey:kRequestResult] , nil);
        } else {
            completion(NO, nil, [json objectForKey:kRequestStatus] , [DFError shareInstanceOfErrorMsg:[json objectForKey:kRequestMes]]);
        }
    } failure:^(NSError *error) {
        completion(NO,
                   nil,
                   nil,
                   [DFError shareInstanceOfCode:error.code]);
    }];
}

+ (void) getHTTPServerWith:(NSDictionary *)param completion:(interfaceManagerBlock)completion method:(NSString *)method {
    [NetManager getWithURL:[KBaseURL stringByAppendingString:method] params:param success:^(id json) {
        BOOL isSuccess = [[json objectForKey:@"success"] boolValue];
        if (isSuccess) {
                completion(YES,
                           @"成功",
                           [json objectForKey:kRequestResult],
                           nil);
        } else {
            completion(NO, nil, [json objectForKey:kRequestResult] , [DFError shareInstanceOfErrorMsg:[json objectForKey:kRequestMes]]);
        }
    } failure:^(NSError *error) {
        completion(NO,
                   nil,
                   nil,
                   [DFError shareInstanceOfCode:error.code]);
    }];
}

@end
