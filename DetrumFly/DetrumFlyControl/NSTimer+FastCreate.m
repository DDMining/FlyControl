//
//  NSTimer+FastCreate.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "NSTimer+FastCreate.h"

@implementation NSTimer (FastCreate)

+ (NSTimer *)timerWithTimeInterval:(NSInteger)timeInterval target:(nonnull id)target action:(nonnull SEL)action repeat:(BOOL)isReqeart {
   NSTimer *timer = [NSTimer timerWithTimeInterval:timeInterval target:target selector:action userInfo:nil repeats:isReqeart];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    return timer;
}

- (void)startTimer {
    if (![self isValid]) {
        [self fire];
    }
}

- (void)destructionTimer {
    if ([self isValid]) {
        [self invalidate];
    }
}

- (void)pause {
    if ([self isValid]) {
        self.fireDate = [NSDate distantFuture];
    }
}

- (void)delayWithTime:(NSTimeInterval)timer {
    if ([self isValid]) {
        self.fireDate = [NSDate dateWithTimeIntervalSinceNow:timer];
    }
}

- (void)resume {
    if ([self isValid]) {
        self.fireDate = [NSDate date];
    }
}

@end

