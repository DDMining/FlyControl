//
//  DetrumShopListViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/20.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumShopListCell.h"
#import "DetrumShopListViewController.h"
#import "DetrumShopDetailViewController.h"

static NSString *cellIdentifier = @"cellIdentifier";
@interface DetrumShopListViewController () <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataSource;
@end

@implementation DetrumShopListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"更多产品"; 
    [self showNavBarWithRightTitle:nil
                    andRightAction:@selector(rightAction:)
                     andLeftAction:@selector(leftAction:)
                 andRightBtnIsShow:YES];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DetrumShopListCell"
                                               bundle:nil]
         forCellReuseIdentifier:cellIdentifier];
    
    [self netRequest];
    

}

- (void)netRequest {
    _dataSource = [NSMutableArray new];
    for (int i = 0; i < 15 ; i ++) {
        [_dataSource addObject:@"1"];
    }
    
    if (self.tableView.delegate == nil) {
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
    }
    [self.tableView reloadData];
}

- (void)rightAction:(UIButton *)btn {
    
}

- (void)leftAction:(UIButton *)btn {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - tableView Delegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetrumShopListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 132;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    DetrumShopDetailViewController *detail = getStoryOfControllerInstance(@"DetrumShopDetailViewController");
//    [self.navigationController pushViewController:detail animated:YES];
}

@end
