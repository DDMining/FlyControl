//
//  DetrumGyroscopeTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumGyroscopeTableViewCell.h"

@implementation DetrumGyroscopeTableViewCell

- (void)awakeFromNib {
    _xAxleTf.enabled = NO;
    _yAxleTf.enabled = NO;
    _zAxleTf.enabled = NO;
}

- (void)setType:(DetrumSensorType)type {
    _type = type;
    if (_type == DetrumSensorTypeAccelerometer) {
        //加速计
        _detrumLabel.text = @"加速计(g)";
    } else if (_type == DetrumSensorTypeCompass) {
        //指南针
        _detrumLabel.text = @"指南针";
    } else {
        //陀螺仪
        _detrumLabel.text = @"陀螺仪(角度/秒)";
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
