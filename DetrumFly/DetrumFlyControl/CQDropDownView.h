//
//  CQDropDownView.h
//  NIDropDown
//
//  Created by xcq on 16/3/2.
//
//

#import <UIKit/UIKit.h>

@class CQDropDownView;
@protocol CQDropDownViewDelegate
@required
- (void) CQDropDownDelegateMethod:(CQDropDownView *)sender andSeleteStr:(NSString *)str;
@end

typedef NS_ENUM(NSInteger, CQDropDownDirection) {
    CQDropDownDirectionUp,
    CQDropDownDirectionDown,
};

@interface CQDropDownView : UIView <UITableViewDelegate,UITableViewDataSource>
{
    
}
@property (weak, nonatomic) id <CQDropDownViewDelegate> delegate;
- (instancetype)initWithFrame:(CGRect)frame andDefaultTitle:(NSArray *)titles;
- (void)showWithDirection:(CQDropDownDirection)direction;
- (void)setDirection:(CQDropDownDirection)direction;
- (void)hidden;
- (BOOL)getShowStatus;
@end
