//
//  DetrumCradleHeadTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumCradleHeadTableViewCell.h"

@implementation DetrumCradleHeadTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self initPullDownView];
}

- (void)initPullDownView {
    if (_dropDown == nil) {
        _dropDown = [[CQDropDownView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.frame)-154, 8, 138, 24) andDefaultTitle:@[@"跟随模式",@"FPV模式"]];
        [_dropDown setDirection:CQDropDownDirectionDown];
        _dropDown.delegate = self;
        [self addSubview:_dropDown];
    }
}

- (void)CQDropDownDelegateMethod:(CQDropDownView *)sender andSeleteStr:(NSString *)str {
    NSLog(@"str");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
