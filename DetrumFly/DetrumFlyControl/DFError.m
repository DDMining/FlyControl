//
//  DFError.m
//  JiuLe
//
//  Created by dynamrc on 16/3/31.
//  Copyright © 2016年 Fine. All rights reserved.
//

#import "DFError.h"

@implementation DFError

+ (instancetype)shareInstanceOfCode:(NSInteger)code {
    DFError *error = [[DFError alloc] initWithDomain:DFErrorDomain code:code userInfo:nil];
    [error analysisErrorCode];
    return error;
}

+ (instancetype)shareInstanceOfErrorMsg:(NSString *)msg {
    DFError *error = [[DFError alloc] initWithDomain:DFErrorDomain code:-1 userInfo:nil];
    error.failMsg = msg;
    return error;
}



- (void)analysisErrorCode {
    NSString *errorMsg = nil;
    switch (self.code) {
        case NSURLErrorCannotFindHost:
//            errorMsg = NSLocalizedString(@"Cannot find specified host. Retype URL.", nil);
//            break;
        case NSURLErrorCannotConnectToHost:
            errorMsg = @"服务器内部错误，请重试！";
            　　　　　break;
        case NSURLErrorNotConnectedToInternet | NSURLErrorNetworkConnectionLost:
            errorMsg = @"无网络，请联网！";
            break;
        case NSURLErrorTimedOut:
            errorMsg = @"请求超时！";
        case NSURLErrorUnknown | NSURLErrorBadURL | NSURLErrorCancelled:
            errorMsg = @"请求失败，请重试！";
        default:
            break;
    }
    self.failMsg = errorMsg;
}

@end
