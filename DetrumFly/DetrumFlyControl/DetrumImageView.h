//
//  DetrumImageView.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>
#import "DetrumFlyMarco.h"
#import "DetrumMediaModel.h"



typedef void (^ImageClick)(NSInteger index , DetrumMediaType type ,DetrumMediaModel *model , RLMObject *dataBaseModel);
typedef void (^selectImgEvent)(NSInteger inde , DetrumMediaModel *model , BOOL isSelect);

@interface DetrumImageView : UIView
/** 照片View Bottom Bar */
@property (nonatomic, strong) UIView *bottonBar;
/** 原图片 */
@property (nonatomic, strong) UIImageView *image;
/** 勾选图片*/
@property (nonatomic, strong) UIImageView *checkImg;
/** 类型 */
@property (nonatomic, assign) DetrumMediaType type;
/** 使用场景 */
@property (nonatomic, assign) DetrumImageClickType clickType;
/** 背景蒙版 */
@property (nonatomic, strong) UIView *maskView;
/** 视频图片 */
@property (nonatomic, strong) UIImageView *videoImg;
/** 右上角原片和新片标志 */
@property (nonatomic, strong) UIImageView *mediaNewOrOld;
/** 标志 */
@property (nonatomic, assign) NSInteger index;
/** 编辑状态 */
@property (nonatomic, assign , setter = setEdit:) BOOL isEdit;
/** model */
@property (nonatomic, strong) DetrumMediaModel *model;
/** DataBase Model */
@property (nonatomic, strong) RLMObject *dataBaseModel;
/** 点击图片 */
@property (nonatomic, copy) ImageClick clickEvent;
/** 选择图片 */
@property (nonatomic, copy) selectImgEvent seleteImgAction;

- (void)initWithType:(DetrumMediaType)type
           clickType:(DetrumImageClickType)clickType
               model:(DetrumMediaModel *)model;

- (instancetype)initWithType:(DetrumMediaType)type
                   clickType:(DetrumImageClickType)clickType
                       Frame:(CGRect)rects
                       mdoel:(DetrumMediaModel *)model;

- (void)resetType:(DetrumMediaType)type
        clickType:(DetrumImageClickType)clickType
            model:(NSObject *)model;

- (void)initDataBaseModel:(DetrumMediaType)type
                clickType:(DetrumImageClickType)clickType
                    mdoel:(RLMObject *)model;

- (void)imgClickFalse;
@end

