//
//  DetrumMediaModel.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import <YYKit.h>
#import "DetrumMediaModel.h"

@implementation DetrumMediaModel

- (void)setCamareDate:(NSDate *)camareDate {
    if (camareDate == nil) {
        return;
    }
    _camareDate = camareDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    _dateStr = [formatter stringFromDate:camareDate];
}

- (NSComparisonResult)compareWithDate:(NSString *)date {
    NSDate *currenDate = [NSDate dateWithString:date format:@"yyyy-MM-dd"];
    NSDate *selfDate = [NSDate dateWithString:self.dateStr format:@"yyyy-MM-dd"];
    return [currenDate compare:selfDate];
}

@end
