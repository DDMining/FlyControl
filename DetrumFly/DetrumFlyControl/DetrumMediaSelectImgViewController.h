//
//  DetrumMediaSelectImgViewController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/15.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseViewController.h"

@interface DetrumMediaSelectImgViewController : DetrumBaseViewController
/** 0:SD卡 1:图片  2:视频 */
@property (nonatomic, assign) NSUInteger type;
@property (nonatomic, strong) RLMNotificationToken *token;
@property (nonatomic, strong) NSMutableArray *urls;
@property (nonatomic, weak) DetrumMediaLibraryViewController *mediaController;
@end
