//
//  DetrumMallViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

@import WebKit;
#import "DetrumMallViewController.h"

#define kUrl @"http://www.baidu.com"
@interface DetrumMallViewController () <WKNavigationDelegate,WKUIDelegate>
@property (strong, nonatomic) WKWebView *webView;
@end

@implementation DetrumMallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"翔迷商场";
    [self showNavBarWithRightTitle:nil andRightAction:nil andLeftAction:nil andRightBtnIsShow:NO];

    self.webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds))];
    [self.view addSubview:self.webView];
    self.webView.UIDelegate = self;
    self.webView.navigationDelegate = self;
    self.webView.allowsBackForwardNavigationGestures = YES;    // Do any additional setup after loading the view from its nib.
    [self.view addSubview:_webView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://puzhou.tmall.com"]]];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    decisionHandler(WKNavigationActionPolicyAllow);
    NSLog(@"开始加载了");
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    [SVProgressHUD showInfoWithStatus:@"加载中.."];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [SVProgressHUD dismiss];
    NSLog(@"%@",self.view.subviews);
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:@"加载失败"];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [SVProgressHUD showErrorWithStatus:@"加载失败2"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
