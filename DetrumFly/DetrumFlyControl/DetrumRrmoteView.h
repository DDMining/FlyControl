//
//  DetrumRrmoteView.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/7/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumRrmoteView : UIView{
    CATransition *animation;
}
@property (strong, nonatomic) UIImageView *pointImg;
@property (weak, nonatomic) UIView *animationView;

@end
