//
//  DetrumInputTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"
@interface DetrumInputTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextField *inputTf;

@end
