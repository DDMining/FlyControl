//
//  FileIO.h
//  CityHunter
//
//  Created by Gary Zhou on 2014.1.16.
//  Copyright (c) 2014 xxx. All rights reserved.
//

@interface FileIO : NSObject

/**
 * @brief get file at path
 *
 * @param [in] file path
 * @param [out]
 * @return file data
 * @note
 */
+ (NSData *)getFileAtPath:(NSString*)path;

/**
 * @brief save file at path
 *
 * @param [in] file path, file data
 * @param [out]
 * @return success or fail
 * @note
 */
+ (BOOL)saveFileAtPath:(NSString*)path data:(NSData *)data;

/**
 * @brief remove item at path
 *
 * @param [in] file path, file data
 * @param [out]
 * @return success or fail
 * @note
 */
+ (BOOL)removeItemAtPath:(NSString*)path;

/**
 * @brief create directory
 *
 * @param [in] file path, file data
 * @param [out]
 * @return success or fail
 * @note
 */
+ (BOOL)createDirectory:(NSString*)path;

/**
 * @brief copy item
 *
 * @param [in] file path, file data
 * @param [out]
 * @return success or fail
 * @note
 */
+ (BOOL)copyItemAtPath:(NSString*)srcPath toPath:(NSString*)dstPath;

@end
