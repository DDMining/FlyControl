//
//  UITextField+Helper.h
//  0元拍
//
//  Created by wangfang on 15/1/5.
//  Copyright (c) 2015年 baishan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Helper)
+ (instancetype)textFieldWithFrame:(CGRect)frame placeholder:(NSString*)placeholder borderStyle:(UITextBorderStyle)borderStyle keyboardType:(UIKeyboardType)keyboardType clearButtonMode:(UITextFieldViewMode)clearButtonMode delegate:(id<UITextFieldDelegate>)delegate;
@end
