//
//  DetrumFlypathSetView.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/8/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CQDropDownView.h"

typedef enum : NSUInteger {
    DetrumFlypathSetStatusThrough,
    DetrumFlypathSetStatusTakeOff,
    DetrumFlypathSetStatusLanding,
} DetrumFlypathSetStatus;

typedef void(^sureBlock)(NSInteger DetrumFlypathSetStatus, NSString *stayTime , NSString *height);
typedef void(^closeBlock)(void);

@interface DetrumFlypathSetView : UIView <CQDropDownViewDelegate>

@property (copy, nonatomic) sureBlock sureAction;
@property (copy, nonatomic) closeBlock closeAction;

@property (weak, nonatomic) IBOutlet UITextField *pointNumTextField;
@property (weak, nonatomic) IBOutlet UITextField *pointStatusTextField;
@property (weak, nonatomic) IBOutlet UITextField *stayTimeTextField;
@property (weak, nonatomic) IBOutlet UITextField *heightTextField;



@property (retain, nonatomic) CQDropDownView *dropView;
@property (assign, nonatomic) NSInteger DetrumFlypathSetStatusInt;
@property (retain, nonatomic) NSString *stayTime;
@property (retain, nonatomic) NSString *height;



+ (instancetype)shareInstance;
- (void)show:(UIView *)view;

@end
