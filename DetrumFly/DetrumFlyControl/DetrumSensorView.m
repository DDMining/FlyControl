//
//  DetrumSensorView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "Interface.h"
#import "NSTimer+FastCreate.h"
#import "DetrumMavlinkManager.h"
#define kGyroscopeIdentifier @"gyroscope"
#define kBarometerIdentifier @"barometer"
#define kCalibrationIdentifier @"Calibration"

#import "DetrumIMUView.h"
#import "DetrumSensorView.h"
#import "DetrumGyroscopeView.h"
#import "DetrumCompassCheckView.h"

@implementation DetrumSensorView

- (void)awakeFromNib {
    [self viewDidLoad];
}

- (void)viewDidLoad {
    [self initTimer];
    [self initTableView];
}

- (void)setUpData {
      _paramters = [NSMutableArray arrayWithArray:[[DetrumMavlinkManager shareInstance] getDataWithType:PARAM_IMU]];
    if (_paramters.count == 0) {
        return;
    }
    _tuoluoyiArrs = [NSMutableArray arrayWithArray:@[_paramters[0],_paramters[1],_paramters[2]]];
    _jisujiArrs = [NSMutableArray arrayWithArray:@[_paramters[3],_paramters[4],_paramters[5]]];
    _zhinanzhenArrs = [NSMutableArray arrayWithArray:@[_paramters[6],_paramters[7],_paramters[8]]];
    _barometer = [NSString stringWithFormat:@"%.5f",[_paramters[9] doubleValue]];
    
}

- (IBAction)backEvent:(id)sender {
    [self backAnimaiton];
}

- (void)initTableView {
    if (_tableView.delegate == nil) {
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        _tableView.backgroundColor = [UIColor clearColor];
        
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumGyroscopeTableViewCell" bundle:nil] forCellReuseIdentifier:kGyroscopeIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumBarometerTableViewCell" bundle:nil] forCellReuseIdentifier:kBarometerIdentifier];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumCalibrationTableViewCell" bundle:nil] forCellReuseIdentifier:kCalibrationIdentifier];
    }
}

#pragma mark - tableView DataSource & Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    if (indexPath.row == 3) {
        DetrumBarometerTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kBarometerIdentifier forIndexPath:indexPath];
        cell.barometerTf.text = _barometer;
        return cell;
    } else if (indexPath.row == 4) {
        __weak typeof(self)weakSelf = self;
        DetrumCalibrationTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kCalibrationIdentifier forIndexPath:indexPath];
        cell.event = ^(NSInteger index) {
            //陀螺仪 1
            if (index == 0) {
                DetrumGyroscopeView *view = [[[NSBundle mainBundle] loadNibNamed:@"DetrumGyroscopeView" owner:self options:nil] firstObject];
                view.frame = weakSelf.bounds;
                [weakSelf addSubview:view];
                
                [[DetrumMavlinkManager shareInstance] startCalibration:1];
                
                
            }
            //加速计 3
            else if (index == 1) {
                DetrumIMUView *view = [[[NSBundle mainBundle] loadNibNamed:@"DetrumIMUView" owner:self options:nil] lastObject];
                view.frame = weakSelf.bounds;
                [view updateConstraintsIfNeeded];
                [weakSelf addSubview:view];
                [[DetrumMavlinkManager shareInstance] startCalibration:3];

            }
            //指南针
            else if (index == 2){
                
                DetrumCompassCheckView *view = [[[NSBundle mainBundle] loadNibNamed:@"DetrumCompassCheckView" owner:self options:nil] firstObject];
                view.frame = weakSelf.bounds;
                [view updateConstraintsIfNeeded];
                [weakSelf addSubview:view];
                [[DetrumMavlinkManager shareInstance] startCalibration:2];
                
            }
        };
        return cell;
    } else  {
        DetrumGyroscopeTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kGyroscopeIdentifier forIndexPath:indexPath];
        NSArray *tempArr;
        if (indexPath.row == 0) {
            tempArr = _tuoluoyiArrs;
        } else if (indexPath.row == 1) {
            tempArr = _jisujiArrs;
        } else {
            tempArr = _zhinanzhenArrs;
        }
        cell.xAxleTf.text = [NSString stringWithFormat:@"%.5f",[tempArr[0] doubleValue]];
        cell.yAxleTf.text = [NSString stringWithFormat:@"%.5f",[tempArr[1] doubleValue]];
        cell.zAxleTf.text = [NSString stringWithFormat:@"%.5f",[tempArr[2] doubleValue]];;
        cell.type = indexPath.row;
        return cell;
    }
}

- (void)initTimer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1.f target:self action:@selector(initData) repeat:YES];
        [_timer startTimer];
    }
    [_timer startTimer];
}

- (void)initData {
    [self setUpData];
    [self.tableView reloadData];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 3) {
        return 36;
    } else if (indexPath.row == 4) {
        return 38;
    } else {
        return 73;
    }
}

@end
