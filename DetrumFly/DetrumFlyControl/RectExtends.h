//
//  RextExtends.h
//  JxCarios
//
//  Created by xcq on 14/7/22.
//  Copyright (c) 2015年 xiongchuanqi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RectExtends : NSObject

+ (CGFloat)getThisViewX:(UIView *)view;
+ (CGFloat)getThisViewY:(UIView *)view;
+ (CGFloat)getThisViewWidth:(UIView *)view;
+ (CGFloat)getThisViewHeight:(UIView *)view;
+ (CGPoint)getThisViewOrigin:(UIView *)view;
+ (CGSize)getThisViewSize:(UIView *)view;
+ (CGRect)getScreenFrame;
+ (CGFloat)getScreenFrameWithHeight;
+ (CGFloat)getScreenFrameWithWidth;

@end
