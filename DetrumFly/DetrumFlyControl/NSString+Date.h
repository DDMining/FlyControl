//
//  NSString+Date.h
//  IPax
//
//  Created by xcq on 15/4/14.
//  Copyright (c) 2015年 Fine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Date)

- (NSString *)getTimeString;
- (NSString *)getDateString;
- (NSDate *)getTime;

@end
