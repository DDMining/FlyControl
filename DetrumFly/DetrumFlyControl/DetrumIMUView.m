//
//  DetrumIMU.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "SVProgressHUD.h"
#import "Costants.h"
#import "DetrumIMUView.h"
#import "Interface.h"
#import "DetrumMavlinkManager.h"


@implementation DetrumIMUView

- (IBAction)closeEvent:(id)sender {
    [self removeFromSuperview];
    [[DetrumMavlinkManager shareInstance] stopCalibration];
}

- (void)flickerImg:(NSInteger) index {
    if (index <= 0 && index > 6) {
        return;
    }
    if (currenIndex != index && currenIndex!=-1) {
        lastIndex = currenIndex;
    }
    currenIndex = index;
    NSLog(@"currenIndex : %zd",currenIndex);
    if (timer == nil) {
        timer = [NSTimer timerWithTimeInterval:1.f target:self selector:@selector(setImgViewBorderColor) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    }
      [timer fire];
}


- (void)setImgViewBorderColor {
    if (lastIndex != -1) {
//        NSLog(@"lastIndex 改变了啊");
        UIImageView *img = [viewDic objectForKey:@(lastIndex)];
        img.layer.borderColor = [[UIColor greenColor] colorWithAlphaComponent:0.7].CGColor;
    }
    
    UIImageView *img = [viewDic objectForKey:@(currenIndex)];
    if (_imuColor == DetrumImuViewColorGray) {
        img.layer.borderColor = [[UIColor redColor] colorWithAlphaComponent:0.7].CGColor;
        _imuColor = DetrumImuViewColorRed;
    } else {
        img.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.7].CGColor;
        _imuColor = DetrumImuViewColorGray;
    }
}

- (void)setCornerRadiu:(UIView *)temp {
    temp.layer.cornerRadius = 6.f;
    temp.layer.borderColor = [UIColor darkGrayColor].CGColor;
    temp.layer.borderWidth = 2.f;
    temp.layer.masksToBounds = YES;
}

- (void)getData {
    [SVProgressHUD showInfoWithStatus:@"校准信息加载中..."];
    for (int i = 0 ; i < 100000; i ++) {
        
    }
    [SVProgressHUD dismiss];
    [self flickerImg:1];
    
//    [NSTimer scheduledTimerWithTimeInterval:10.f block:^(NSTimer *timer) {
//        [self flickerImg:currenIndex + 1];
//    } repeats:YES];
    
}


- (void) adjusting:(NSNotification *)noti{
    
    NSString *str = noti.userInfo[PARAM_STATUE_TEXT];
    NSLog(@"str:%@",str);
    if ([str hasPrefix:@"[cal] down side done"]) {
        [self flickerImg:2];
    }else if ([str hasPrefix:@"[cal] up side done"]) {
        [self flickerImg:3];
    }else if ([str hasPrefix:@"[cal] back side done"]) {
        [self flickerImg:4];
    }else if ([str hasPrefix:@"[cal] front side done"]) {
        [self flickerImg:5];
    }else if ([str hasPrefix:@"[cal] left side done"]) {
        [self flickerImg:6];
    }else if ([str hasPrefix:@"[cal] right side done"]) {
        [timer invalidate];
        UIImageView *img = [viewDic objectForKey:@(6)];
        img.layer.borderColor = [[UIColor greenColor] colorWithAlphaComponent:0.7].CGColor;
    }
    
    // 校验失败
    if ([str hasPrefix:@"[cal] calibration failed"]) {
        [[DetrumMavlinkManager shareInstance] startCalibration:3];
        [self removeFromSuperview];
    }
    
    // 校验成功
    if ([str hasPrefix:@"str:[cal] calibration done: accel"]) {
        
        
    }
    
}

- (void)awakeFromNib {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(adjusting:) name:PARAM_STATUE_TEXT object:nil];
    currenIndex = -1;
    lastIndex = -1;
    _imuColor = DetrumImuViewColorRed;
    
    viewDic = @{@(1) : _horizontalPlace,
                @(2) : _turnOut,
                @(3) : _pointingUp,
                @(4) : _pointingDown,
                @(5) : _leftSideTurnOut,
                @(6) : _rightSideTurnOut};
    
    _turnOut.image = [UIImage imageNamed:@"fly2"];
    _pointingUp.image = [UIImage imageNamed:@"fly3"];
    _pointingDown.image = [UIImage imageNamed:@"fly4"];
    _leftSideTurnOut.image = [UIImage imageNamed:@"fly5"];
    _horizontalPlace.image = [UIImage imageNamed:@"fly1"];
    _rightSideTurnOut.image = [UIImage imageNamed:@"fly6"];
    
    [self setCornerRadiu:_horizontalPlace];
    [self setCornerRadiu:_turnOut];
    [self setCornerRadiu:_pointingUp];
    [self setCornerRadiu:_pointingDown];
    [self setCornerRadiu:_leftSideTurnOut];
    [self setCornerRadiu:_rightSideTurnOut];
    [self getData];
}

@end
