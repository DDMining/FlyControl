//
//  DetrumBatteryCapacityCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/7.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "CapacityView.h"
#import "UIColor+RGBConverHex.h"
#import "DetrumBatteryCapacityCell.h"
#define kAcept 0.08
#define kDefaultColor [UIColor greenColor]
@implementation DetrumBatteryCapacityCell

- (void)awakeFromNib {
    // Initialization code
    self.isClick = NO;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    
}

- (void)setBatterys:(NSArray *)batterys {
    _batterys = batterys;
    [self initCapacityView];
}

- (void)setBatteryValue:(CGFloat)batteryValue {
    _batteryValue = batteryValue;
    [self initBatteryValue];
}

- (void)initBatteryValue {
    if ([_batteryElectricity viewWithTag:0x621] == nil) {
        CapacityView *batteryView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_batteryElectricity.frame) , CGRectGetHeight(_batteryElectricity.frame)) percen:_batteryValue fillColor:kDefaultColor radiu:8.f isHorizontal:YES];
        batteryView.tag = 0x621;
        [_batteryElectricity addSubview:batteryView];
        _batteryElectricity.layer.masksToBounds = YES;
        _batteryElectricity.layer.cornerRadius = 8.f;
        _quantityOfElectricity.text = [[NSString stringWithFormat:@"%0.f",_batteryValue * 100] stringByAppendingString:@"%"];
    } else {
        CapacityView *view = [_batteryElectricity viewWithTag:0x621];
        [view setPercent:_batteryValue];
        _quantityOfElectricity.text = [[NSString stringWithFormat:@"%0.f",_batteryValue * 100] stringByAppendingString:@"%"];
    }
}

- (void)initCapacityView {
    
    if ([_firstBattryView viewWithTag:0x521] == nil) {
        CapacityView *firstView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * kAcept, _firstBattryView.frame.size.height) percen:[[_batterys firstObject] floatValue] fillColor:kDefaultColor radiu:5.f];
        firstView.tag = 0x521;
        [_firstBattryView addSubview:firstView];
        [self setLayerParameter:_firstBattryView];
        
    } else {
        CapacityView *view = [_firstBattryView viewWithTag:0x521];
        [view setPercent:[[_batterys firstObject] floatValue]];
    }
    _firstBatteryValueLb.text = [NSString stringWithFormat:@"%@",[_batterys firstObject]];
    
    if ([_twoBatteryView viewWithTag:0x522] == nil) {
        CapacityView *secondView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * kAcept, _twoBatteryView.frame.size.height)  percen:[[_batterys objectAtIndex:1] floatValue] fillColor:kDefaultColor radiu:5.f];
        secondView.tag = 0x522;
        [_twoBatteryView addSubview:secondView];
        [self setLayerParameter:_twoBatteryView];
    } else {
        CapacityView *view = [_twoBatteryView viewWithTag:0x522];
        [view setPercent:[[_batterys objectAtIndex:1] floatValue]];
    }
    _twoBatteryValueLb.text = [NSString stringWithFormat:@"%@",[_batterys objectAtIndex:1]];
    
    if ([_threeBatteryView viewWithTag:0x523] == nil) {
        CapacityView *threeView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * kAcept, _threeBatteryView.frame.size.height)  percen:[[_batterys objectAtIndex:2] floatValue] fillColor:kDefaultColor radiu:5.f];
        threeView.tag = 0x523;
        [_threeBatteryView addSubview:threeView];
        [self setLayerParameter:_threeBatteryView];
    } else {
        CapacityView *view = [_threeBatteryView viewWithTag:0x523];
        [view setPercent:[[_batterys objectAtIndex:2] floatValue]];
    }
    _threeBatteryValueLb.text = [NSString stringWithFormat:@"%@",[_batterys objectAtIndex:2]];
    
    if ([_fourBatteryView viewWithTag:0x524] == nil) {
        CapacityView *fourView = [[CapacityView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width * kAcept, _fourBatteryView.frame.size.height)  percen:[[_batterys lastObject] floatValue] fillColor:kDefaultColor radiu:5.f];
        fourView.tag = 0x524;
        [_fourBatteryView addSubview:fourView];
        [self setLayerParameter:_fourBatteryView];
    } else {
        CapacityView *view = [_fourBatteryView viewWithTag:0x524];
        [view setPercent:[[_batterys objectAtIndex:3] floatValue]];
    }
    _fourBatteryValueLb.text = [NSString stringWithFormat:@"%@",[_batterys lastObject]];
}

- (void)setLayerParameter:(UIView *)temp {
    temp.layer.borderColor = [UIColor colorWithHex:0xCFCECF].CGColor;
    temp.layer.borderWidth = 1.f;
    temp.layer.masksToBounds = YES;
    temp.layer.cornerRadius = 5.f;
}

@end
