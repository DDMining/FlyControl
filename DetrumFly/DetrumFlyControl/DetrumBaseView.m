//
//  DetrumBaseView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "UIView+QuicklyChangeFrame.h"
#import "DetrumBaseView.h"

@implementation DetrumBaseView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {
        [self setAnimation];
    }
    return self;
}

- (void)setAnimation {
    if (_animation == nil) {
        _animation = [CATransition animation];
    }
    [_animation setDuration:0.35f];
    [_animation setType:kCATransitionPush];
    [_animation setSubtype:kCATransitionFromRight];
    [_animation setFillMode:kCAFillModeForwards];
    [_animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.layer addAnimation:_animation forKey:@"detrumRemote"];
}

- (void)backAnimaiton {
    [UIView animateWithDuration:0.35 animations:^{
        [self setRectX:CGRectGetWidth(self.frame)];
    } completion:^(BOOL finished) {
        _animationViw.hidden = NO;
        [UIView animateWithDuration:0.2 animations:^{
            _animationViw.alpha = 1.f;
        }completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
