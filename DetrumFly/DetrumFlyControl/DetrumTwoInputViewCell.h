//
//  DetrumTwoInputViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumBaseTableViewCell.h"

#import <UIKit/UIKit.h>

@interface DetrumTwoInputViewCell : DetrumBaseTableViewCell <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *firstInpuViewName;
@property (weak, nonatomic) IBOutlet UITextField *firstInputTf;
@property (weak, nonatomic) IBOutlet UILabel *secondInputViewName;
@property (weak, nonatomic) IBOutlet UITextField *secondInputTf;
//@p
@end
