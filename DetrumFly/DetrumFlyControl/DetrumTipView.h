//
//  DetrumTipView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumTipView : UIView
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *minusBtn;
@property (weak, nonatomic) IBOutlet UIButton *plusBtn;
@property (weak, nonatomic) IBOutlet UILabel *numlb;
@property (assign, nonatomic) CGFloat num;
@end
