//
//  DetrumMavlinkManager.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/4/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "UIDevice+IPAddr.h"
#import "GCDAsyncUdpSocket.h"
#import "GCDAsyncSocket.h"

#define FLOATNUMCONVERSTR(arr,value) [NSString stringWithFormat:@"%.2f",[[arr objectSecurityAtIndex:value] floatValue]]

//#de
#define kGetVerifcationSecond 60

typedef NS_ENUM(NSInteger , MavlinkResponseType) {
    MavlinkResponseTypeHeartBeat = 0,   //心跳包
    MavlinkResponseTypeSendSetting = 23, //参数设置
    MavlinkResponseTypeRarameterRequest = 21, //请求所有参数
    MavlinkResponseTypeCalibration, //开始校准
};

typedef void(^DetrumResponse)(NSArray * data , NSInteger msgId);
@interface DetrumMavlinkManager : NSObject {
    GCDAsyncUdpSocket *udpSocket;
    GCDAsyncSocket *tcpSocket;
    NSString *port;
    NSString *ip;
}

@property (nonatomic, assign) BOOL isConnect;
@property (nonatomic, assign) NSInteger currenMsgId;
@property (nonatomic, strong) NSMutableArray *msgs;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger second;
/** 参数列表 */
@property (nonatomic, strong) NSMutableDictionary *parameter;
@property (nonatomic, strong) NSMutableArray *dataSource;
+ (instancetype)shareInstance;
- (void)save;
- (BOOL)connetWifi;
- (void)sendParamer;
- (void)sendParamterSet:(NSString *)str andValue:(CGFloat)value;
- (void)setUpTimer;
- (void)sendSetMode:(int)mode baseMode:(int)baseMode;
- (void)guidedModeTakeoffHeight:(float)height;
- (void)sendQueryList;
- (void)sendkForceSendOrder;
//开始校准
- (void)startCalibration:(int)cal_type;
//结束校准
- (void)stopCalibration;

- (void)sendParamRaw:(int)compID paramName:(NSString *)paramName value:(double)value valueType:(int) valueType;






- (NSArray *)getDataWithType:(NSString *)type;
@end
