//
//  NewFeaturesController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumPublicManager.h"
#import "NewFeaturesController.h"
#import "BottomView.h"
#import <WebKit/WebKit.h>
#import <QMUIKit/QMUIKit.h>
#import <Masonry/Masonry.h>

NSString *const webViewObserverKey = @"estimatedProgress";

#define KShowTime 9

@interface NewFeaturesController ()<WKUIDelegate,WKNavigationDelegate,BottomViewDelegate>

//SK Model
@property (nonatomic,strong) BottomView *bottomView;
@property (nonatomic,strong) WKWebView *webView;
@property (nonatomic,strong) UIProgressView *progressView;
@property (nonatomic,strong) WKWebViewConfiguration *wkConfig;
@property (nonatomic,strong) NSString *urlString;

@end

@implementation NewFeaturesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isValidity = [defaults boolForKey:@"inReviewState"];
    if (!isValidity) {
        //UI Clean for SKModel
        self.navigationController.navigationBar.hidden = YES;
        self.tabBarController.tabBar.hidden = YES;
        [self.featuresImg removeFromSuperview];
        
        self.progressView = [UIProgressView new];
        self.progressView.backgroundColor = [UIColor blueColor];
        //设置进度条的高度，下面这句代码表示进度条的宽度变为原来的1倍，高度变为原来的1.5倍.
        self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
        [self.view addSubview:self.progressView];
        
        __weak typeof(self) weakSelf = self;
        [weakSelf.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.view.mas_left);
            make.right.mas_equalTo(self.view.mas_right);
            make.height.mas_equalTo(@2);
            
            if (@available(iOS 11.0, *)) {
                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            }
            else{
                make.top.mas_equalTo(self.view.mas_top);
            }
        }];
        
        [self.view addSubview:self.webView];
        [self.view addSubview:self.bottomView];
        
        [weakSelf.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.view);
            make.height.mas_equalTo(@44);
            if (@available(iOS 11.0, *)) {
                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            }
            else{
                make.bottom.equalTo(self.view);
            }
        }];
        
        [weakSelf.webView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(self.view);
            make.top.mas_equalTo(self.progressView.mas_bottom);
            make.bottom.mas_equalTo(self.bottomView.mas_top);
        }];
        
        // Do any additional setup after loading the view, typically from a nib.
        
        self.urlString = [[NSUserDefaults standardUserDefaults]objectForKey:@"baseURL"];
        if (self.urlString == nil) {
            self.urlString = [NSString stringWithFormat:@"http://www.cocoachina.com/"];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSURL *url = [NSURL URLWithString:weakSelf.urlString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [self.webView loadRequest:request];
        });
    }else{
        [UIView animateWithDuration:KShowTime/3 animations:^{
            _featuresImg.transform = CGAffineTransformMakeScale(1.05, 1.05);
            [UIView animateWithDuration:KShowTime/3 animations:^{
                _featuresImg.transform = CGAffineTransformMakeScale(1.1, 1.1);
            }];
            [UIView animateWithDuration:KShowTime/3 animations:^{
                _featuresImg.transform = CGAffineTransformMakeScale(1.15, 1.15);
            }];
        } completion:^(BOOL finished) {
            [self skipAction:_skipBtn];
        }];
        
        NSLog(@"1:%f", [[NSDate date] timeIntervalSince1970]);
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (IBAction)skipAction:(id)sender {
    //TODO:跳转首页
    
    NSLog(@"2:%f", [[NSDate date] timeIntervalSince1970]);
    //    [UIView animateWithDuration:0.8 animations:^{
    [UIApplication sharedApplication].keyWindow.rootViewController = [DetrumPublicManager createRootController];
    
}

#pragma mark - SKModel

- (WKWebViewConfiguration *)wkConfig {
    if (!_wkConfig) {
        _wkConfig = [[WKWebViewConfiguration alloc] init];
        _wkConfig.allowsInlineMediaPlayback = YES;
        _wkConfig.preferences = [[WKPreferences alloc] init];
        _wkConfig.preferences.minimumFontSize = 10;
        _wkConfig.processPool = [[WKProcessPool alloc] init];
    }
    return _wkConfig;
}

- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:self.view.bounds
                                      configuration:self.wkConfig];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        
        [_webView addObserver:self
                   forKeyPath:webViewObserverKey
                      options:NSKeyValueObservingOptionNew
                      context:NULL];
    }
    return _webView;
}


- (BottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [BottomView new];
        _bottomView.delegate = self;
    }
    return _bottomView;
}

- (void)cleanWKWebViewCache {
    
    //// All kinds of data
    NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
    //// Date from
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    //// Execute
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
        // Done
        [QMUITips showSucceed:@"清除成功"];
    }];
}

#pragma mark - BottomViewDelegate

- (void)useBackSite {
    NSString *backUpUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"backupURL"];
    self.urlString = backUpUrl;
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

- (void)goHomePage {
    NSString *backUpUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"baseURL"];
    self.urlString = backUpUrl;
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

- (void)goBack {
    if (self.webView.canGoBack) {
        [self.webView goBack];
    }
}
- (void)refresh {
    [self.webView reloadFromOrigin];
}

- (void)recharge {
    NSString *backUpUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"rechargeURL"];
    self.urlString = backUpUrl;
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

- (void)cleanCache {
    [self cleanWKWebViewCache];
}


#pragma mark - 监听

/*
 *4.在监听方法中获取网页加载的进度，并将进度赋给progressView.progress
 */

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:webViewObserverKey]) {
        self.progressView.progress = self.webView.estimatedProgress;
        if (self.progressView.progress == 1) {
            /*
             *添加一个简单的动画，将progressView的Height变为1.4倍
             *动画时长0.25s，延时0.3s后开始动画
             *动画结束后将progressView隐藏
             */
            __weak typeof (self)weakSelf = self;
            [UIView animateWithDuration:0.25f
                                  delay:0.3f
                                options:UIViewAnimationOptionCurveEaseOut animations:^{
                                    weakSelf.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.4f);
                                } completion:^(BOOL finished) {
                                    weakSelf.progressView.hidden = YES;
                                }];
        }
    }
}

#pragma mark - WKWKNavigationDelegate Methods

/*
 *5.在WKWebViewd的代理中展示进度条，加载完成后隐藏进度条
 */

//开始加载
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    NSLog(@"开始加载网页");
    //开始加载网页时展示出progressView
    self.progressView.hidden = NO;
    //开始加载网页的时候将progressView的Height恢复为1.5倍
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    //防止progressView被网页挡住
    [self.view bringSubviewToFront:self.progressView];
}

//加载完成
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSLog(@"加载完成");
    //加载完成后隐藏progressView
    self.progressView.hidden = YES;
}

//加载失败
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    NSLog(@"加载失败");
    //加载失败同样需要隐藏progressView
    self.progressView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
