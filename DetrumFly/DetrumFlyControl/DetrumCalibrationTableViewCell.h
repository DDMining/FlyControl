//
//  DetrumCalibrationTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

typedef void(^IMUCheckEvent)(NSInteger indexz);

@interface DetrumCalibrationTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *checkIMU;
@property (weak, nonatomic) IBOutlet UIButton *IMUCalibration;
@property (weak, nonatomic) IBOutlet UIButton *CompassBtn;
@property (copy, nonatomic) IMUCheckEvent event;
@end
