//
//  DetrumCoordinateCorrect.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/7/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
static double pi = 3.1415926535897932384626;
static double a = 6378245.0;
static double ee = 0.00669342162296594323;

#import "DetrumCoordinateCorrect.h"
#include "math.h"

@implementation DetrumCoordinateCorrect

// 地图经纬度偏移量纠正
- (NSMutableDictionary *)WGS84ToGCJ02:(double)lat lon:(double)lon{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if ([self OutOfChina:lat lon:lon]){
        [dic setObject:[NSString stringWithFormat:@"%f",lat] forKey:@"lat"];
        [dic setObject:[NSString stringWithFormat:@"%f",lon] forKey:@"lon"];
        return dic;
    }
    
    double dLat = [self transformLat:lon - 105.0 y:lat - 35.0];
    double dLon = [self transformLon:lon - 105.0 y:lat - 35.0];
    
    double radLat = lat / 180.0 * pi;
    double magic = sin(radLat);
    magic = 1 - ee * magic * magic;
    double sqrtMagic = sqrt(magic);
    dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
    dLon = (dLon * 180.0) / (a / sqrtMagic * cos(radLat) * pi);
    double mgLat = lat + dLat;
    double mgLon = lon + dLon;
    [dic setObject:[NSString stringWithFormat:@"%f",mgLat] forKey:@"lat"];
    [dic setObject:[NSString stringWithFormat:@"%f",mgLon] forKey:@"lon"];
    return dic;
    
}

-(BOOL)OutOfChina:(double)lat lon:(double)lon {
    if (lon < 72.004 || lon > 137.8347)
        return true;
    if (lat < 0.8293 || lat > 55.8271)
        return true;
    return NO;
}

-(double)transformLat:(double)x y:(double)y{
    double ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y
    + 0.2 * sqrt(fabs(x));//Math.sqrt(Math.abs(x));
    ret += (20.0 * sin(6.0 * x * pi) + 20.0 * sin(2.0 * x * pi)) * 2.0 / 3.0;
    ret += (20.0 * sin(y * pi) + 40.0 * sin(y / 3.0 * pi)) * 2.0 / 3.0;
    ret += (160.0 * sin(y / 12.0 * pi) + 320 * sin(y * pi / 30.0)) * 2.0 / 3.0;
    return ret;
}


-(double)transformLon:(double)x y:(double)y{
    double ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1
    * sqrt(fabs(x));
    ret += (20.0 * sin(6.0 * x * pi) + 20.0 * sin(2.0 * x * pi)) * 2.0 / 3.0;
    ret += (20.0 * sin(x * pi) + 40.0 * sin(x / 3.0 * pi)) * 2.0 / 3.0;
    ret += (150.0 * sin(x / 12.0 * pi) + 300.0 * sin(x / 30.0 * pi)) * 2.0 / 3.0;
    return ret;
}















@end
