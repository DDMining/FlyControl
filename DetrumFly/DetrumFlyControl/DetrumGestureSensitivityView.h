//
//  DetrumGestureSensitivityView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseView.h"

@interface DetrumGestureSensitivityView : DetrumBaseView <UITableViewDelegate,UITableViewDataSource>{
    @private
    NSMutableArray *titleArr;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
