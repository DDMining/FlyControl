//
//  DetrumScrollView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/14.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ScrollEvent)();

@interface DetrumScrollView : UIView {
    UIPanGestureRecognizer *panGesture;
}
@property (weak, nonatomic) IBOutlet UIImageView *rightArrowImg;
@property (weak, nonatomic) IBOutlet UIButton *sliderBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderLeading;
@property (copy, nonatomic) ScrollEvent event;
@end
