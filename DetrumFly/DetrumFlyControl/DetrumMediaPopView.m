//
//  DetrumMediaPopView.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/14.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "RectExtends.h"
#import "DetrumMediaPopView.h"
static DetrumMediaPopView *popView = nil;
@interface DetrumMediaPopView ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentVApartSuperViewBottonLayout;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *selectSdCardBtn;
@property (weak, nonatomic) IBOutlet UIButton *localPhotoBtn;
@property (weak, nonatomic) IBOutlet UIButton *localMovieBtn;
@end

@implementation DetrumMediaPopView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:(CGRect){frame.origin,{CGRectGetWidth([UIScreen mainScreen].bounds),158}}]) {
        
    }
    return self;
}

+ (instancetype)shareInstance {
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        //initWithFrame:CGRectMake(0, 0, [RectExtends getScreenFrameWithWidth], [RectExtends getScreenFrameWithHeight])
        popView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumMediaPopView" owner:self options:nil] lastObject];
        popView.frame = CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds));
        [popView layoutIfNeeded];
        [popView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)]];
        
    });
    return popView;
}

+ (void)showWithCompletion:(ClickAction)action {
    if (popView == nil) {
        [DetrumMediaPopView shareInstance];
    }
    popView.completion = action;
    [popView setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.]];
    [[UIApplication sharedApplication].keyWindow addSubview:popView];
    
    [UIView animateWithDuration:0.4 animations:^{
        [popView setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.3]];
        popView.contentVApartSuperViewBottonLayout.constant = -163;
        [popView layoutIfNeeded];
    }];
}

+ (void)dismiss {
    popView.contentVApartSuperViewBottonLayout.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [popView setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.f]];
        [popView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [popView removeFromSuperview];
    }];
}

- (IBAction)DetrumMediaSelectEvent:(UIButton *)sender {
    switch (sender.tag) {
        case 400:{
//            _completion(DetrumMediaSelectTypeSdCard);
        }
            break;
        case 401:{
//            _completion(DetrumMediaSelectTypePhote);
        }
            break;
        case 402:{
//            _completion(DetrumMediaSelectTypeMovie);
        }
            break;
    }
    [DetrumMediaPopView dismiss];
}

@end
