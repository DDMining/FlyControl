//
//  NewFeaturesController.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewFeaturesController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *skipBtn;
@property (weak, nonatomic) IBOutlet UIImageView *featuresImg;
@property (weak, nonatomic) IBOutlet UILabel *featuresLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icon;

@end
