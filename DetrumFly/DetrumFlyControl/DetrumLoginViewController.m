//
//  DetrumLoginViewController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumLoginViewController.h"


@interface DetrumLoginViewController ()

@end

@implementation DetrumLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"登陆";
    [self showNavBarWithRightTitle:nil
                    andRightAction:nil
                     andLeftAction:nil
                 andRightBtnIsShow:NO];
}

- (IBAction)loginEvent:(UIButton *)sender {
    if (_emailTt.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"邮箱不能为空！"];
        return;
    }
    if (_passWordTt.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"密码不能为空！"];
        return;
    }
    
    [SVProgressHUD showSuccessWithStatus:@"登陆成功"];
    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:DetrumUserLogin];
    [self.navigationController popViewControllerAnimated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
    });
    
//    [InterfaceNetManager RequestLogin:_emailTt.text
//                             PassWord:_passWordTt.text
//                           completion:^(BOOL isSucceed,
//                                        NSString *message,
//                                        id data,
//                                        DFError *error) {
//                               if (isSucceed) {
//                                   DFLog(@"%@",data);
//                                   [SVProgressHUD showSuccessWithStatus:@"登陆成功"];
//                               } else {
//                                   [SVProgressHUD showErrorWithStatus:error.failMsg];
//                               }
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
