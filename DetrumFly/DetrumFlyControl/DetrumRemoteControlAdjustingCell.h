//
//  DetrumRemoteControlAdjustingCell.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/7/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseTableViewCell.h"

typedef void(^RemoteCheckEvent)(void);
@interface DetrumRemoteControlAdjustingCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIButton *adjustingBtn;
@property (copy, nonatomic) RemoteCheckEvent event;

@end
