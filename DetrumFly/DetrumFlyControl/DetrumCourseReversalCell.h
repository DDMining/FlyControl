//
//  DetrumCourseReversalCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

@interface DetrumCourseReversalCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tipImg;

@end
