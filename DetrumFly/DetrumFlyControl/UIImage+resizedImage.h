//
//  UIImage+resizedImage.h
//  SpellBus
//
//  Created by wangfang on 15/2/3.
//  Copyright (c) 2015年 publicNetwork. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (resizedImage)

/**
 *  返回一张自由拉伸的图片
 */
+ (UIImage *)resizedImageWithName:(NSString *)name;
+ (UIImage *)resizedImageWithName:(NSString *)name left:(CGFloat)left top:(CGFloat)top;
+ (UIImage *)resizedImage:(NSString *)name;
+ (UIImage *)imageWithName:(NSString *)name;
@end
