//
//  DetrumBaseTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/1.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetrumBaseTableViewCell : UITableViewCell
@property (nonatomic, assign) BOOL isClick;
@end
