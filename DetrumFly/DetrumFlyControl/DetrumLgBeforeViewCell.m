//
//  DetrumLgBeforeViewCell.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/22.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumLgBeforeViewCell.h"

@implementation DetrumLgBeforeViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (IBAction)createAccount:(UIButton *)sender {
    _click(DetrumClickTypeCreate);
}

- (IBAction)login:(UIButton *)sender {
    _click(DetrumClickTypeLogin);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
