#ifndef PARAM_hpp
#define PARAM_hpp
#include <stdio.h>

class Param
{
public:
    Param();
    virtual float value(int id) = 0;
    int getCount();
    bool getReady();
    void setReady(int ready);

private:
    int count;
    bool _ready;
};

#endif 

