//
//  ParamManualControl.cpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#include "ParamManualControl.hpp"
//#define MAVLINK_MSG_ID_MANUAL_CONTROL 69
//typedef struct __mavlink_manual_control_t
//{
//    int16_t x; /*< X-axis, normalized to the range [-1000,1000]. A value of INT16_MAX indicates that this axis is invalid. Generally corresponds to forward(1000)-backward(-1000) movement on a joystick and the pitch of a vehicle.*/
//    int16_t y; /*< Y-axis, normalized to the range [-1000,1000]. A value of INT16_MAX indicates that this axis is invalid. Generally corresponds to left(-1000)-right(1000) movement on a joystick and the roll of a vehicle.*/
//    int16_t z; /*< Z-axis, normalized to the range [-1000,1000]. A value of INT16_MAX indicates that this axis is invalid. Generally corresponds to a separate slider movement with maximum being 1000 and minimum being -1000 on a joystick and the thrust of a vehicle. Positive values are positive thrust, negative values are negative thrust.*/
//    int16_t r; /*< R-axis, normalized to the range [-1000,1000]. A value of INT16_MAX indicates that this axis is invalid. Generally corresponds to a twisting of the joystick, with counter-clockwise being 1000 and clockwise being -1000, and the yaw of a vehicle.*/
//    uint16_t buttons; /*< A bitfield corresponding to the joystick buttons' current state, 1 for pressed, 0 for released. The lowest bit corresponds to Button 1.*/
//    uint8_t target; /*< The system to be controlled.*/
//} mavlink_manual_control_t;
//

ParamManualControl::ParamManualControl()
{
    
}
float ParamManualControl::value(int id){
    float v = 0.0;
    
    switch(id){
        case 1:
            v = data.x;
            break;
        case 2:
            v = data.y;
            break;
        case 3:
            v = data.z;
            break;
        case 4:
            v = data.r;
            break;
        case 5:
            v = data.buttons;
            break;
        case 6:
            v = data.target;
            break;
        default:
            break;
    }
    
    return v;
}
