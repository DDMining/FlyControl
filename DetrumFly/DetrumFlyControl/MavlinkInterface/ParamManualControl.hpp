//
//  ParamManualControl.hpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef ParamManualControl_hpp
#define ParamManualControl_hpp

#include <stdio.h>
#include "Param.hpp"
#include "mavlink.h"


class ParamManualControl: Param
{
public:
    ParamManualControl();
    
    friend class Mavlib;
    
    float value(int id);
    void setData(const mavlink_manual_control_t& imu)
    {
        data = imu;
    }
    
    mavlink_manual_control_t& getData()
    {
        return data;
    }
private:
    mavlink_manual_control_t data;
    
};



#endif /* ParamManualControl_hpp */
