//
//  ParamSysStatus.hpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/10.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef ParamSysStatus_hpp
#define ParamSysStatus_hpp

#include <stdio.h>
#include "mavlink.h"
#include "Param.hpp"

class ParamSysStatus:Param
{
public:
    ParamSysStatus();
    
    friend class Mavlib;
    
    float value(int id);
    void setData(const mavlink_sys_status_t& imu)
    {
        data = imu;
    }
    
    mavlink_sys_status_t& getData()
    {
        return data;
    }
private:
    mavlink_sys_status_t data;
};



#endif /* ParamSysStatus_hpp */
