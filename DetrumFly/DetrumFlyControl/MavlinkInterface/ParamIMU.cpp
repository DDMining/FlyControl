//
//  ParamIMU.cpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/4/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#include "ParamIMU.hpp"

ParamIMU::ParamIMU()
{
    
}
float ParamIMU::value(int id){
    float v = 0.0;
    
    switch(id){
        case 1:
            v = data.xacc;
            break;
        case 2:
            v = data.yacc;
            break;
        case 3:
            v = data.zacc;
            break;
        case 4:
            v = data.xgyro;
            break;
        case 5:
            v = data.ygyro;
            break;
        case 6:
            v = data.zgyro;
            break;
        case 7:
            v = data.xmag;
            break;
        case 8:
            v = data.ymag;
            break;
        case 9:
            v = data.zmag;
            break;
        case 10:
            v = data.abs_pressure;
            break;
        case 11:
            v = data.diff_pressure;
            break;
        case 12:
            v = data.pressure_alt;
            break;
        case 13:
            v = data.temperature;
            break;
        case 14:
            v = data.fields_updated;
            break;
            
        default:
            break;
    }
    
    return v;
}

