﻿#ifndef SENSORCONTROLLER_H
#define SENSORCONTROLLER_H

#include "interface.h"

typedef enum {
    StartCalibrationRadio,//遥控器
    StartCalibrationGyro,//陀螺仪
    StartCalibrationComp,//指南针
    StartCalibrationAccel,//加速度计
}StartCalibrationType;

// 传感器校准
class SensorController
{
public:
    SensorController();

    void startCalibration(int calType, mavlink_message_t& msg, int sysid, int componentid);
    void stopCalibration(mavlink_message_t& msg, int sysid, int componentid);
};

#endif // SENSORCONTROLLER_H
