//
//  ParamBatteryInfo.hpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/6.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef ParamBatteryInfo_hpp
#define ParamBatteryInfo_hpp

#include "Param.hpp"
#include <stdio.h>
#include "mavlink.h"

class ParamBatteryInfo: Param
{
public:
    ParamBatteryInfo();
    
    friend class Mavlib;
    
//    float value(int id);
//    void setData(const mavlink_smart_battery_status_t& imu)
//    {
//        data = imu;
//    }
//    
//    mavlink_smart_battery_status_t& getData()
//    {
//        return data;
//    }
//private:
////    mavlink_smart_battery_status_t data;
//  mavlink_battery_status_t_t data;
};


#endif /* ParamBatteryInfo_hpp */
