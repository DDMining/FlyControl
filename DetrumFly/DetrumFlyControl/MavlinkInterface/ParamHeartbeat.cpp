//
//  ParamHeartbeat.cpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/4/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#include "mavlink.h"
#include "ParamHeartbeat.hpp"
ParamHeartbeat::ParamHeartbeat()
{
        
}
float ParamHeartbeat::value(int id){
    float v = 0.0;
    
    switch(id){
        case 1:
            v = data.custom_mode;
            break;
        case 2:
            v = data.system_status;
            break;
        case 3:
            v = data.type;
            break;
        case 4:
            v = data.autopilot;
            break;
        case 5:
            v = data.base_mode;
            break;
        case 6:
            v = data.mavlink_version;
            break;
        default:
            break;
    }
    
    return v;
}
