#include "SensorController.h"

SensorController::SensorController()
{

}

void SensorController::startCalibration(int calType, mavlink_message_t& msg, int sysid, int componentid)
{
    float param1 = 0;
    float param2 = 0;
    float param3 = 0;
    float param4 = 0;
    float param5 = 0;
    float param6 = 0;
    float param7 = 0;

    switch (calType) {
    case StartCalibrationGyro:
        param1 = 1;
        break;

    case StartCalibrationComp:
        param2 = 1;
        break;

    case StartCalibrationRadio:
        param4 = 1;
        break;

    case StartCalibrationAccel:
        param5 = 1;
        break;
    }

    int targetSysID = 1;
    int targetComID = 1;

    mavlink_msg_command_long_pack(sysid,//sysid,
                                  componentid,
                                  &msg,
                                  targetSysID,
                                  targetComID,                                // target component
                                  MAV_CMD_PREFLIGHT_CALIBRATION,    // command id
                                  0,                                // 0=first transmission of command
                                  param1,                          // gyro cal
                                  param2,                           // mag cal
                                  param3,                                // ground pressure
                                  param4,                         // radio cal
                                  param5,                         // accel cal
                                  param6,                      // airspeed cal
                                  param7);
}

void SensorController::stopCalibration(mavlink_message_t& msg, int sysid, int componentid)
{
    uint8_t uasid = 1;
    mavlink_msg_command_long_pack(sysid,
                                  componentid,
                                  &msg,
                                  uasid,
                                  0,                                // target component
                                  MAV_CMD_PREFLIGHT_CALIBRATION,    // command id
                                  0,                                // 0=first transmission of command
                                  0,                                // gyro cal
                                  0,                                // mag cal
                                  0,                                // ground pressure
                                  0,                                // radio cal
                                  0,                                // accel cal
                                  0,                                // airspeed cal
                                  0);                               // unused

}
