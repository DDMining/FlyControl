//
//  ParamSetMode.hpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/11.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef ParamSetMode_hpp
#define ParamSetMode_hpp

#include <stdio.h>
#include "mavlink.h"
#include "Param.hpp"

class ParamSetMode:Param
{
public:
    ParamSetMode();
    
    friend class Mavlib;
    
    float value(int id);
    void setData(const mavlink_set_mode_t& imu)
    {
        data = imu;
    }
    
    mavlink_set_mode_t& getData()
    {
        return data;
    }
private:
    //    mavlink_smart_battery_status_t data;
    mavlink_set_mode_t data;
};
#endif /* ParamSetMode_hpp */
