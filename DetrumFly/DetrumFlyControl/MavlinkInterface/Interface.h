#ifndef INTERFACE
#define INTERFACE

#include "mavlink.h"
//#include "mavlink_msg_battery_status.h"

#define PARAM_HEARTBEART  @"PARAM_HEARTBEART"
#define PARAM_IMU         @"PARAM_IMU"
#define PARAM_RC_CHANNELS @"PARAM_RC_CHANNELS"
#define PARAM_BETTERY     @"PARAM_BETTERY"
#define PARAM_LOCAL       @"PARAM_LOCAL"
#define PARAM_VFR_HUD     @"PARAM_VFR_HUD"
#define PARAM_STATUE      @"PARAM_STATUE"
#define PARAM_VALUE_      @"PARAM_VALUE"
#define PARAM_GPS         @"PARAM_GPS"
#define PARAM_STATUE_TEXT @"PARAM_STATUE_TEXT"

/* 心跳*/
#define PARAM_HEARTBEART_FLY_MODEL                  ((MAVLINK_MSG_ID_HEARTBEAT<<8)|0x1)     // 飞行模式
#define PARAM_HEARTBEART_SYSTEM_STATUS              ((MAVLINK_MSG_ID_HEARTBEAT<<8)|0x2)     // 飞行状态
#define PARAM_HEARTBEART_TYPE                       ((MAVLINK_MSG_ID_HEARTBEAT<<8)|0x3)     // 类型
#define PARAM_HEARTBEART_AUTOPILOT                  ((MAVLINK_MSG_ID_HEARTBEAT<<8)|0x4)     // 类型
#define PARAM_HEARTBEART_BASE_MODEL                 ((MAVLINK_MSG_ID_HEARTBEAT<<8)|0x5)
#define PARAM_HEARTBEART_MAVLINK_VERSION            ((MAVLINK_MSG_ID_HEARTBEAT<<8)|0x6)

// 位置信息
//#define MAVLINK_MSG_ID_HOME_POSITION

// IMU
#define PARAM_IMU_XACC                              ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x1)
#define PARAM_IMU_YACC                              ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x2)
#define PARAM_IMU_ZACC                              ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x3)
#define PARAM_IMU_XGYRO                             ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x4)
#define PARAM_IMU_YGYRO                             ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x5)
#define PARAM_IMU_ZGYRO                             ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x6)
#define PARAM_IMU_XMAG                              ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x7)
#define PARAM_IMU_YMAG                              ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x8)
#define PARAM_IMU_ZMAG                              ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0x9)
#define PARAM_IMU_ABS_PRESSURE                      ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0xA)
#define PARAM_IMU_DIFF_PRESSURE                     ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0xB)
#define PARAM_IMU_PRESSURE_ALT                      ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0xC)
#define PARAM_IMU_TEMPERATURE                       ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0xD)
#define PARAM_IMU_FIELDS_UPDATED                    ((MAVLINK_MSG_ID_HIGHRES_IMU<<8)|0xE)

// 遥控
#define PARAM_RC_CHANNELS_CHAN1_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x1)
#define PARAM_RC_CHANNELS_CHAN2_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x2)
#define PARAM_RC_CHANNELS_CHAN3_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x3)
#define PARAM_RC_CHANNELS_CHAN4_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x4)
#define PARAM_RC_CHANNELS_CHAN5_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x5)
#define PARAM_RC_CHANNELS_CHAN6_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x6)
#define PARAM_RC_CHANNELS_CHAN7_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x7)
#define PARAM_RC_CHANNELS_CHAN8_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x8)
#define PARAM_RC_CHANNELS_CHAN9_RAW                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x9)
#define PARAM_RC_CHANNELS_CHAN10_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0xA)
#define PARAM_RC_CHANNELS_CHAN11_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0xB)
#define PARAM_RC_CHANNELS_CHAN12_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0xC)
#define PARAM_RC_CHANNELS_CHAN13_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0xD)
#define PARAM_RC_CHANNELS_CHAN14_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0xE)
#define PARAM_RC_CHANNELS_CHAN15_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0xF)
#define PARAM_RC_CHANNELS_CHAN16_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x10)
#define PARAM_RC_CHANNELS_CHAN17_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x11)
#define PARAM_RC_CHANNELS_CHAN18_RAW                ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x12)
#define PARAM_RC_CHANNELS_CHANCOUNT                 ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x13)
#define PARAM_RC_CHANNELS_RSSI                      ((MAVLINK_MSG_ID_RC_CHANNELS<<8)|0x14)

// 电池
#define PARAM_BETTERY_ID                            ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x1)
#define PARAM_BETTERY_PRODUCTION_DATE               ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x2)
#define PARAM_BETTERY_TEMPERATURE                   ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x3)
#define PARAM_BETTERY_DESIGN_VOLTAGE                ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x4)
#define PARAM_BETTERY_DESIGN_CAPACITY               ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x5)
#define PARAM_BETTERY_FULL_CAPACITY                 ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x6)
#define PARAM_BETTERY_AVAILABLE_BATTERY             ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x7)
#define PARAM_BETTERY_REMAINING_BATTERY             ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x8)
#define PARAM_BETTERY_CYCLE_COUNT                   ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0x9)
#define PARAM_BETTERY_VOLTAGE_SINGLE_1              ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0xA)
#define PARAM_BETTERY_VOLTAGE_SINGLE_2              ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0xB)
#define PARAM_BETTERY_VOLTAGE_SINGLE_3              ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0xC)
#define PARAM_BETTERY_VOLTAGE_SINGLE_4              ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0xD)
#define PARAM_BETTERY_VOLTAGE                       ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0xE)
#define PARAM_BETTERY_LIFE_TIME                     ((MAVLINK_MSG_ID_SMART_BATTERY_STATUS<<8) | 0xF)

//local position
#define PARAM_LOCAL_POSITION_TIMEBOOTMS             ((MAVLINK_MSG_ID_LOCAL_POSITION_NED<<8) | 0x1)
#define PARAM_LOCAL_POSITION_X                      ((MAVLINK_MSG_ID_LOCAL_POSITION_NED<<8) | 0x2)
#define PARAM_LOCAL_POSITION_Y                      ((MAVLINK_MSG_ID_LOCAL_POSITION_NED<<8) | 0x3)
#define PARAM_LOCAL_POSITION_Z                      ((MAVLINK_MSG_ID_LOCAL_POSITION_NED<<8) | 0x4)
#define PARAM_LOCAL_POSITION_VX                     ((MAVLINK_MSG_ID_LOCAL_POSITION_NED<<8) | 0x5)
#define PARAM_LOCAL_POSITION_VY                     ((MAVLINK_MSG_ID_LOCAL_POSITION_NED<<8) | 0x6)
#define PARAM_LOCAL_POSITION_VZ                     ((MAVLINK_MSG_ID_LOCAL_POSITION_NED<<8) | 0x7)

//HUD
#define PARAM_VFR_HUD_AIRSPEED                      ((MAVLINK_MSG_ID_VFR_HUD<<8) | 0x1)
#define PARAM_VFR_HUD_GROUNDSPEED                   ((MAVLINK_MSG_ID_VFR_HUD<<8) | 0x2)
#define PARAM_VFR_HUD_ALT                           ((MAVLINK_MSG_ID_VFR_HUD<<8) | 0x3)
#define PARAM_VFR_HUD_CLIMB                         ((MAVLINK_MSG_ID_VFR_HUD<<8) | 0x4)
#define PARAM_VFR_HUD_HEADING                       ((MAVLINK_MSG_ID_VFR_HUD<<8) | 0x5)
#define PARAM_VFR_HUD_THROTTLE                      ((MAVLINK_MSG_ID_VFR_HUD<<8) | 0x6)


//sys status
#define PARAM_SYS_STATUS_ONBOARD_SENSORS_PRESENT    ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x1)
#define PARAM_SYS_STATUS_ONBOARD_SENSORS_ENABLED    ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x2)
#define PARAM_SYS_STATUS_ONBOARD_SENSORS_HEALTH     ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x3)
#define PARAM_SYS_STATUS_LOAD                       ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x4)
#define PARAM_SYS_STATUS_VOLTAGE_BATTERY            ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x5)
#define PARAM_SYS_STATUS_CURRENT_BATTERY            ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x6)
#define PARAM_SYS_STATUS_DROP_RATE_COMM             ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x7)
#define PARAM_SYS_STATUS_ERRORS_COMM                ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x8)
#define PARAM_SYS_STATUS_ERRORS_COUNT1              ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0x9)
#define PARAM_SYS_STATUS_ERRORS_COUNT2              ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0xA)
#define PARAM_SYS_STATUS_ERRORS_COUNT3              ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0xB)
#define PARAM_SYS_STATUS_ERRORS_COUNT4              ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0xC)
#define PARAM_SYS_STATUS_BATTERY_REMAINING          ((MAVLINK_MSG_ID_SYS_STATUS<<8) | 0xD)

//set mode
#define PARAM_CUSTOM_MODE                           ((MAVLINK_MSG_ID_SET_MODE<<8) | 0x1)
#define PARAM_TARGET_SYSTEM                         ((MAVLINK_MSG_ID_SET_MODE<<8) | 0x2)
#define PARAM_BASE_MODE                             ((MAVLINK_MSG_ID_SET_MODE<<8) | 0x3)

//param value
#define PARAM_VALUE                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x1)
#define PARAM_COUNT                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x2)
#define PARAM_INDEX                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x3)
#define PARAM_ID_1                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x4)
#define PARAM_ID_2                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x5)
#define PARAM_ID_3                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x6)
#define PARAM_ID_4                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x7)
#define PARAM_ID_5                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x8)
#define PARAM_ID_6                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x9)
#define PARAM_ID_7                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0xA)
#define PARAM_ID_8                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0xB)
#define PARAM_ID_9                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0xC)
#define PARAM_ID_10                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0xD)
#define PARAM_ID_11                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0xE)
#define PARAM_ID_12                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0xF)
#define PARAM_ID_13                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x10)
#define PARAM_ID_14                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x11)
#define PARAM_ID_15                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x12)
#define PARAM_ID_16                                 ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x13)
#define PARAM_TYPE                                  ((MAVLINK_MSG_ID_PARAM_VALUE<<8) | 0x14)

//GPS
#define PARAM_GPS_TIMEUSEC                          ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x1)
#define PARAM_GPS_LAT                               ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x2)
#define PARAM_GPS_LON                               ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x3)
#define PARAM_GPS_ALT                               ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x4)
#define PARAM_GPS_EPH                               ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x5)
#define PARAM_GPS_EPV                               ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x6)
#define PARAM_GPS_VEL                               ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x7)
#define PARAM_GPS_COG                               ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x8)
#define PARAM_GPS_FIXTYPE                           ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0x9)
#define PARAM_GPS_SATELLITESVISIBLE                 ((MAVLINK_MSG_ID_GPS_RAW_INT<<8) | 0xA)

#endif // INTERFACE

