#include "Mavlib.hpp"

Mavlib::Mavlib():
    sysid(255),
    componentid(1),
    vehicleid(1),
    mavlinkChannel(1)
{
    for (int i = 0; i < 256; i++)
    {
        for (int j = 0; j < 256; j++)
        {
            lastIndex[i][j] = -1;
        }
    }
}

//int Mavlib::ReceivedBytes(char* b, int len)
//{
//    mavlink_message_t message;
//    mavlink_status_t status;
//
//    static int nonmavlinkCount = 0;
//    static bool decodedFirstPacket = false;
//
//    for (int position = 0; position < len; position++) {
//        unsigned int decodeState = mavlink_parse_char(mavlinkChannel, (uint8_t)(b[position]), &message, &status);
//        if (decodeState == 0 && !decodedFirstPacket)
//        {
//            nonmavlinkCount++;
//            if (nonmavlinkCount > 2000 )
//            {
//                if (message.msgid == 1) {
//                    printf(" --------------------------msgID _ 1 crc 失败  -------------------------- \n");
//                }
//                //2000 bytes with no
//            }
//        }
//        else if (decodeState == 1)
//        {
//            if (message.msgid == 1) {
//                printf(" --------------------------msgID _ 1 crc 成功  -------------------------- \n");
//            }
//            decodedFirstPacket = true;
//
//            switch (message.msgid){
//
//            case MAVLINK_MSG_ID_GPS_RAW_INT:
//                _handleParamGPS(message);
//                    break;
//            // 位置消息
//            case MAVLINK_MSG_ID_HOME_POSITION:
//                _handleHomePosition(message);
//                break;
//            case MAVLINK_MSG_ID_VFR_HUD:
//                _handleHUD(message);
//                break;
//            // 心跳包
//            case MAVLINK_MSG_ID_HEARTBEAT:
//                _handleHeartbeat(message);
//                break;
//
//            // 参数消息
//            case MAVLINK_MSG_ID_PARAM_VALUE:
//                _handleParamvalue(message);
//                break;
//
//            // 遥控通道
//            case MAVLINK_MSG_ID_RC_CHANNELS:
//                _handleRcChannels(message);
//                break;
//
//            // IMU
//            case MAVLINK_MSG_ID_HIGHRES_IMU:
//                _handleHighresImu(message);
//                break;
//
//            // 系统状态:设备姿态, CPU等实时信息
//            case MAVLINK_MSG_ID_SYS_STATUS:
//                _handleSysStatus(message);
//                break;
//                    
//            //
//            case MAVLINK_MSG_ID_COMMAND_ACK:
//                _handleSysCommandAck(message);
//                break;
//
//            case MAVLINK_MSG_ID_LOG_ENTRY:
//                _handleLogEntry(message);
//                break;
//
//            case MAVLINK_MSG_ID_LOG_DATA:
//                _handleLogData(message);
//                break;
//                    
////            case MAVLINK_MSG_ID_SMART_BATTERY_STATUS:
////                _handleBattery(message);
////                break;
//            
//            case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
//                _handelLocalPosition(message);
//                break;
//             
//            case MAVLINK_MSG_ID_SET_MODE:
//                //设置飞控模式（比如一键起飞，一键返航）
//                _handleSetMode(message);
//                break;
//                    
//            default:
//                    
//                break;
//            }
//
//            int lastSeq = lastIndex[message.sysid][message.compid];
//            int expectedSeq = (lastSeq == -1) ? message.seq : (lastSeq + 1);
//
//            if (message.seq != expectedSeq)
//            {
//                int lostMessages = message.seq - expectedSeq;
//
//                // 顺序发生错误, 忽略
//                lostMessages = (lostMessages < 0) ? 0 : lostMessages;
//            }
//
//            lastIndex[message.sysid][message.compid] = expectedSeq;
//
//            return message.msgid;
//        }
//    }
//
//    return -1;
//}

int Mavlib::ReceivedBytes(char b) {
    mavlink_message_t message;
    mavlink_status_t status;
    
    static int nonmavlinkCount = 0;
    static bool decodedFirstPacket = false;
    
    unsigned int decodeState = mavlink_parse_char(mavlinkChannel,
                                                  (uint8_t)b, &message, &status);
    
    if (decodeState == 0 && !decodedFirstPacket) {
        nonmavlinkCount++;
        if (nonmavlinkCount > 2000) {
            //2000 bytes with no
        }
    } else if (decodeState == 1) {
        decodedFirstPacket = true;
        
        switch (message.msgid) {
                
                // 位置消息
            case MAVLINK_MSG_ID_HOME_POSITION:
                _handleHomePosition(message);
                break;
                
                // 心跳包
            case MAVLINK_MSG_ID_HEARTBEAT:
                _handleHeartbeat(message);
                break;
                
                // 参数消息
            case MAVLINK_MSG_ID_PARAM_VALUE:
                _handleParamvalue(message);
                break;
                
                // 遥控通道
            case MAVLINK_MSG_ID_RC_CHANNELS:
                _handleRcChannels(message);
                break;
                
                // IMU
            case MAVLINK_MSG_ID_HIGHRES_IMU:
                _handleHighresImu(message);
                break;
                
                // 系统状态:设备姿态, CPU等实时信息
            case MAVLINK_MSG_ID_SYS_STATUS:
                _handleSysStatus(message);
                break;
                
            case MAVLINK_MSG_ID_COMMAND_ACK:
                _handleSysCommandAck(message);
                break;
                
            case MAVLINK_MSG_ID_LOG_ENTRY:
                _handleLogEntry(message);
                break;
                
            case MAVLINK_MSG_ID_LOG_DATA:
                _handleLogData(message);
                break;
                
                // 自定义电池信息
                /*case MAVLINK_MSG_ID_SMART_BATTERY_STATUS:
                 _handleBattery(message);
                 break;*/
                
            case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
                _handelLocalPosition(message);
                break;
                
            case MAVLINK_MSG_ID_SET_MODE:
                //设置飞控模式（比如一键起飞，一键返航）
                _handleSetMode(message);
                break;
            case MAVLINK_MSG_ID_GPS_RAW_INT:
                _handleParamGPS(message);
                break;
            case MAVLINK_MSG_ID_STATUSTEXT:
                _handleStatusText(message);
                break;
                
                // Mission
            case MAVLINK_MSG_ID_MISSION_COUNT:
            case MAVLINK_MSG_ID_MISSION_ITEM:
            case MAVLINK_MSG_ID_MISSION_REQUEST:
            case MAVLINK_MSG_ID_MISSION_ACK:
            case MAVLINK_MSG_ID_MISSION_ITEM_REACHED:
            case MAVLINK_MSG_ID_MISSION_CURRENT:
                //missionController.handleMessage(message.msgid, message);
                break;
                // Mission end
                
            default:
                break;
        }
        
        int lastSeq = lastIndex[message.sysid][message.compid];
        int expectedSeq = (lastSeq == -1) ? message.seq : (lastSeq + 1);
        
        if (message.seq != expectedSeq) {
            int lostMessages = message.seq - expectedSeq;
            
            // 顺序发生错误, 忽略
            lostMessages = (lostMessages < 0) ? 0 : lostMessages;
        }
        
        lastIndex[message.sysid][message.compid] = expectedSeq;
        
        return message.msgid;
    }
    
    return -1;
}







int Mavlib::_getMessageBytes(mavlink_message_t message, char* buff)
{
    if(!buff)
        return 0;

    static uint8_t messageKeys[256] = MAVLINK_MESSAGE_CRCS;
    mavlink_finalize_message_chan(&message, sysid, componentid, 1, message.len, messageKeys[message.msgid]);

    // Write message into buffer, prepending start sign
    int len = mavlink_msg_to_send_buffer((uint8_t*)buff, &message);
    return len;
}

float Mavlib::getParameterValue(int id)
{
    int msg_id = id>>8;
    int param_id = (((id<<8)&0xFFFF)>>8);
    float value = 0.0;

    switch(msg_id)
    {
    // 位置消息
    case MAVLINK_MSG_ID_HOME_POSITION:

        break;
    case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
        value = param_loction.value(param_id);
        break;
    case MAVLINK_MSG_ID_HEARTBEAT:
        // 心跳包
        value =  param_heartbeat.value(param_id);
        break;
    case MAVLINK_MSG_ID_SET_MODE:
            //设置一键起飞等等
        value = param_setModel.value(param_id);
        break;
    // 参数消息
    case MAVLINK_MSG_ID_PARAM_VALUE:
            //
        
        break;
    case MAVLINK_MSG_ID_GPS_RAW_INT:
        value = param_gps.value(param_id);
        break;
    // 遥控通道
    case MAVLINK_MSG_ID_RC_CHANNELS:
        value = param_rc_channels.value(param_id);
        break;
    // IMU
    case MAVLINK_MSG_ID_HIGHRES_IMU:
        value = param_IMU.value(param_id);
        break;
    case MAVLINK_MSG_ID_VFR_HUD:
        value = param_hud.value(param_id);
            break;
    // 系统状态:设备姿态, CPU等实时信息
    case MAVLINK_MSG_ID_SYS_STATUS:
        value = param_sys.value(param_id);
        break;
//    case MAVLINK_MSG_ID_SMART_BATTERY_STATUS:
//            value = param_battery.value(param_id);
//            break;
    default:
        break;
    }

    return value;
}

// 刷新组件所有参数, 当componentID为0时, 为更新所有参数
int Mavlib::sendParametersRequest(char* buff, uint8_t componentID)
{
    mavlink_message_t msg;
    mavlink_msg_param_request_list_pack(sysid, componentid, &msg, vehicleid, componentID);
    return _getMessageBytes(msg, buff);
}

// 发送心跳
int Mavlib::sendHeartbeat(char* buff)
{
    mavlink_message_t message;
    mavlink_msg_heartbeat_pack(sysid,
                               componentid,
                               &message,
                               MAV_TYPE_GCS,            // MAV_TYPE
                               MAV_AUTOPILOT_INVALID,   // MAV_AUTOPILOT
                               MAV_MODE_MANUAL_ARMED,   // MAV_MODE
                               0,                       // custom mode
                               MAV_STATE_ACTIVE);       // MAV_STATE

    return _getMessageBytes(message, buff);
}


//发送设置模式
//int Mavlib::sendSetMode(char* buff ,
//                        uint32_t customMode)
//{
//    mavlink_message_t message;
//    mavlink_msg_set_mode_pack(sysid,
//                              componentid,
//                              &message,
//                              targetSys,
//                              baseMode,
//                              customMode);
//    
//    return _getMessageBytes(message, buff);
//}

//发送设置模式
int Mavlib::sendSetMode(char* buff, uint8_t targetSystem, uint8_t baseMode,
                        uint32_t customMode) {
    mavlink_message_t message;
    mavlink_msg_set_mode_pack(sysid, componentid, &message, targetSystem,
                              baseMode, customMode);
    
    return _getMessageBytes(message, buff);
}



// 设置参数
int Mavlib::sendParameterSetting(char* buff, int componentId, char* paramName, double value)
{
    mavlink_param_set_t     p;
    mavlink_param_union_t   union_value;
   // 根据类型设置数据

    p.param_value = union_value.param_float;
    p.target_system = (uint8_t)vehicleid;
    p.target_component = (uint8_t)componentid;
    p.param_value = value;
    strncpy(p.param_id, paramName, sizeof(p.param_id));

    mavlink_message_t msg;
    mavlink_msg_param_set_encode(sysid, componentId, &msg, &p);

    return _getMessageBytes(msg, buff);    return 0;
}

int Mavlib::startCalibration(char* buff, int cal_type)
{
    mavlink_message_t msg;
    
    sensorController.startCalibration(cal_type, msg, sysid, componentid);
    
    return _getMessageBytes(msg, buff);
}

int Mavlib::stopCalibration(char* buff)
{
    mavlink_message_t msg;
    
    sensorController.stopCalibration(msg, sysid, componentid);
    
    return _getMessageBytes(msg, buff);
}

int Mavlib::sendParamRaw(char *buff, int compID, const char *paramName, float value, int valueType)
{
    if(!buff)
        return 0;
    
    mavlink_param_set_t     p;
    mavlink_param_union_t   paramVal;
    
    switch(valueType)
    {
        case 1:
            paramVal.param_uint8 = (unsigned char)value;
            break;
            
        case 2:
            paramVal.param_int8 = (char)value;
            break;
            
        case 3:
            paramVal.param_uint16 = (unsigned short)value;
            break;
            
        case 4:
            paramVal.param_int16 = (short)value;
            break;
            
        case 5:
            paramVal.param_uint32 = (unsigned int)value;
            break;
            
        case 6:
            paramVal.param_int32 = (int)value;
            break;
            
        case 7:
            paramVal.param_float = value;
            break;
            
        default:
            break;
    }
    
    int targetSysID = 1;
    p.param_type = valueType;
    p.param_value = paramVal.param_float;
    p.target_system = targetSysID;
    p.target_component = compID;
    
    strncpy(p.param_id, paramName, sizeof(p.param_id));
    
    mavlink_message_t msg;
    mavlink_msg_param_set_encode(sysid, componentid, &msg, &p);
    
    return _getMessageBytes(msg, buff);
}

void Mavlib::getStatusText(char *buff)
{
    if(!buff)
        return;
    
    strncpy(buff, statusText, MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN+1);
}

float Mavlib::getParaValueInfo(char *valueType, char *buff)
{
    *valueType = paraValueType;
    
    if(buff)
    {
        strncpy(buff, paraName, sizeof(paraName));
    }
    
    mavlink_param_union_t paramVal;
    paramVal.param_float = paraValue;
    
    switch(*valueType)
    {
        case 1:
            paraValue = paramVal.param_uint8;
            break;
            
        case 2:
            paraValue = paramVal.param_int8;
            break;
            
        case 3:
            paraValue = paramVal.param_uint16;
            break;
            
        case 4:
            paraValue = paramVal.param_int16;
            break;
            
        case 5:
            paraValue = paramVal.param_uint32;
            break;
            
        case 6:
            paraValue = paramVal.param_int32;
            break;
            
        default:
            break;
    }
    
    return paraValue;
}


int Mavlib::guidedModeTakeoff(char *buff, float altitudeRel)
{
    mavlink_message_t msg;
    mavlink_command_long_t cmd;
    
    cmd.command = (uint16_t)MAV_CMD_NAV_TAKEOFF;
    cmd.confirmation = 0;
    cmd.param1 = -1.0f;
    cmd.param2 = 0.0f;
    cmd.param3 = 0.0f;
    cmd.param4 = NAN;
    cmd.param5 = NAN;
    cmd.param6 = NAN;
    cmd.param7 = NAN;
    cmd.target_system = vehicleid;
    cmd.target_component = 0;
    mavlink_msg_command_long_encode(sysid, componentid, &msg, &cmd);
    
    return _getMessageBytes(msg, buff);
}



// 处理位置消息
void Mavlib::_handleHomePosition(mavlink_message_t msg)
{
    
}

void Mavlib::_handleHUD(mavlink_message_t msg) {
    mavlink_vfr_hud_t hud_t;
    mavlink_msg_vfr_hud_decode(&msg, &hud_t);
    param_hud.setData(hud_t);
}

// 处理心跳消息
void Mavlib::_handleHeartbeat(mavlink_message_t msg)
{
//    mavlink_heartbeat_t state;
//    mavlink_msg_heartbeat_decode(&msg, &state);
//
//    sysid = msg.sysid;
//    baseMode = state.base_mode;
//    param_heartbeat.setData(state);

    // We got the mode
    //receivedMode = true ;
    
    mavlink_heartbeat_t state;
    mavlink_msg_heartbeat_decode(&msg, &state);
    
    vehicleid = msg.sysid;
    
    param_heartbeat.setData(state);
}

// 处理参数消息
void Mavlib::_handleParamvalue(mavlink_message_t msg)
{
    mavlink_param_value_t rawValue;
    mavlink_msg_param_value_decode(&msg, &rawValue);
    
    paraValueType = rawValue.param_type;
    paraValue = rawValue.param_value;
    strncpy(paraName, rawValue.param_id, sizeof(paraName));

}

// 处理遥控
void Mavlib::_handleRcChannels(mavlink_message_t msg)
{
    mavlink_rc_channels_t rcChannels;
    mavlink_msg_rc_channels_decode(&msg, &rcChannels);
    param_rc_channels.setData(rcChannels);
}

void Mavlib::_handelLocalPosition(mavlink_message_t msg) {
    mavlink_local_position_ned_t local;
    mavlink_msg_local_position_ned_decode(&msg, &local);
    param_loction.setData(local);
}

// IMU
void Mavlib::_handleHighresImu(mavlink_message_t msg)
{
    mavlink_highres_imu_t imu_t;
    mavlink_msg_highres_imu_decode(&msg, &imu_t);
    
    param_IMU.setData(imu_t);
    
}

void Mavlib::_handleSetMode(mavlink_message_t msg)
{
    mavlink_set_mode_t mode_t;
    mavlink_msg_set_mode_decode(&msg, &mode_t);
    targetSys = mode_t.target_system;
    baseMode = mode_t.base_mode;
    param_setModel.setData(mode_t);
}


void Mavlib::_handleParamGPS(mavlink_message_t msg) {
    mavlink_gps_raw_int_t gps;
    mavlink_msg_gps_raw_int_decode(&msg,&gps);
    param_gps.setData(gps);
}

// 系统状态:设备姿态, CPU等实时信息
void Mavlib::_handleSysStatus(mavlink_message_t msg)
{
    mavlink_sys_status_t sys_status_t;
    mavlink_msg_sys_status_decode(&msg, &sys_status_t);
    
}

// 电池信息:
void Mavlib::_handleBattery(mavlink_message_t msg)
{
//    mavlink_smart_battery_status_t battery_status_t;
//    mavlink_msg_smart_battery_status_decode(&msg, &battery_status_t);
}


// 执行命令, 确认消息
void Mavlib::_handleSysCommandAck(mavlink_message_t msg)
{
    
}
// 日志消息
void Mavlib::_handleLogEntry(mavlink_message_t msg)
{

}

// 日志消息
void Mavlib::_handleLogData(mavlink_message_t msg)
{

}

// 收到状态消息
void Mavlib::_handleStatusText(mavlink_message_t msg)
{
    mavlink_msg_statustext_get_text(&msg, statusText);
    
    statusText[MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN] = '\0';
    
    //qDebug("AA: %s", statusText);
}







