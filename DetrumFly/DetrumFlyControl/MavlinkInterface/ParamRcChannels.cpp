//
//  ParamRcChannels.cpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/4/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#include <stdio.h>
#include "ParamRcChannels.hpp"

ParamRcChannels::ParamRcChannels()
{
    
}


float ParamRcChannels::value(int id){
        float v = 0.0;
        switch(id){
            case 1:
                v = data.chan1_raw;
                break;
            case 2:
                v = data.chan2_raw;
                break;
            case 3:
                v = data.chan3_raw;
                break;
            case 4:
                v = data.chan4_raw;
                break;
            case 5:
                v = data.chan5_raw;
                break;
            case 6:
                v = data.chan6_raw;
                break;
            case 7:
                v = data.chan7_raw;
                break;
            case 8:
                v = data.chan8_raw;
                break;
            case 9:
                v = data.chan9_raw;
                break;
            case 10:
                v = data.chan10_raw;
                break;
            case 11:
                v = data.chan11_raw;
                break;
            case 12:
                v = data.chan12_raw;
                break;
            case 13:
                v = data.chan13_raw;
                break;
            case 14:
                v = data.chan14_raw;
                break;
            case 15:
                v = data.chan15_raw;
                break;
            case 16:
                v = data.chan16_raw;
                break;
            case 17:
                v = data.chan17_raw;
                break;
            case 18:
                v = data.chan18_raw;
                break;
            case 19:
                v = data.chancount;
                break;
            case 20:
                v = data.rssi;
                break;
        }
        return v;
}
