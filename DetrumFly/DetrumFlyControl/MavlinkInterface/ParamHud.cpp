//
//  ParamHud.cpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/10.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#include "ParamHud.hpp"
ParamHud::ParamHud()
{
    
}

float ParamHud::value(int id){
    float v = 0.0;
    
    switch(id){
        case 1:
            return data.airspeed;
            break;
        case 2:
            return data.groundspeed;
            break;
            
        case 3:
            return data.alt;
            break;
            
        case 4:
            return data.climb;
            break;
        case 5:
            return data.heading;
            break;
        case 6:
            return data.throttle;
            break;
            
        default:
            break;
    }
    
    return v;
}