#ifndef PARAMHEARTBEAT_H
#define PARAMHEARTBEAT_H

#include "Param.hpp"
#include <stdio.h>

class Mavlib;
class ParamHeartbeat: Param
{
public:
    ParamHeartbeat();

    friend class Mavlib;

    float value(int id);
    
    
    void setData(mavlink_heartbeat_t _data)
    {
        data = _data;
    }
    
    mavlink_heartbeat_t& getData()
    {
        return data;
    }

private:
    int  custom_mode;
    char base_mode;
    char system_status;
    char mavlink_version;
    mavlink_heartbeat_t data;
};

#endif // PARAMHEARTBEAT_H
