#ifndef PARAMRCCHANNELS_H
#define PARAMRCCHANNELS_H

#include "Param.hpp"
#include <stdio.h>
#include "mavlink.h"

class ParamRcChannels:Param
{
public:
    ParamRcChannels();

    friend class Mavlib;

    float value(int id);
    void setData(const mavlink_rc_channels_t& imu)
    {
        data = imu;
    }
    
    mavlink_rc_channels_t& getData()
    {
        return data;
    }
private:
    mavlink_rc_channels_t data;
};

#endif // PARAMRCCHANNELS_H
