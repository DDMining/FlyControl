#ifndef MISSIONCONTROLLER_H
#define MISSIONCONTROLLER_H

#include "interface.h"

class MissionController
{
public:
    MissionController();

    void handleMessage(int msgId, const mavlink_message_t& message);

private:
    typedef enum {
            AckNone,            ///< State machine is idle
            AckMissionCount,    ///< MISSION_COUNT message expected
            AckMissionItem,     ///< MISSION_ITEM expected
            AckMissionRequest,  ///< MISSION_REQUEST is expected, or MISSION_ACK to end sequence
            AckGuidedItem,      ///< MISSION_ACK expected in reponse to ArduPilot guided mode single item send
        } AckType_t;

    void _handleMissionCount(const mavlink_message_t& message);

    void _handleMissionItem(const mavlink_message_t& message);

    void _handleMissionRequest(const mavlink_message_t& message);

    void _handleMissionAck(const mavlink_message_t& message);

    void _handleMissionCurrent(const mavlink_message_t& message);

    void  sendMissionCount(const mavlink_message_t& message);

    void sendMissionItem(const mavlink_message_t& message);

    void sendMissionRequest(const mavlink_message_t& message);

    void sendMissionAck(const mavlink_message_t& message);

    void sendMissionCurrent(const mavlink_message_t& message);
};

#endif // MISSIONCONTROLLER_H
