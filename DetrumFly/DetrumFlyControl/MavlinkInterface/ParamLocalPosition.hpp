//
//  ParamLocalPosition.hpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/10.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef ParamLocalPosition_hpp
#define ParamLocalPosition_hpp

#include <stdio.h>
#include "mavlink.h"
#include "Param.hpp"

class ParamLocalPosition:Param
{
public:
    ParamLocalPosition();
    
    friend class Mavlib;
    
    float value(int id);
    void setData(const mavlink_local_position_ned_t& imu)
    {
        data = imu;
    }
    
    mavlink_local_position_ned_t& getData()
    {
        return data;
    }
private:
    mavlink_local_position_ned_t data;
};


#endif /* ParamLocalPosition_hpp */
