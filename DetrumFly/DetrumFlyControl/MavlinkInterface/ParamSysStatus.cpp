//
//  ParamSysStatus.cpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/10.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#include "ParamSysStatus.hpp"
ParamSysStatus::ParamSysStatus()
{
    
}
float ParamSysStatus::value(int id){
    float v = 0.0;
    
    switch(id){
        case 1:
            return data.onboard_control_sensors_present;
            break;
        case 2:
            return data.onboard_control_sensors_enabled;
            break;
        case 3:
            return data.onboard_control_sensors_health;
            break;
        case 4:
            return data.load;
            break;
        case 5:
            return data.voltage_battery;
            break;
        case 6:
            return data.current_battery;
            break;
        case 7:
            return data.drop_rate_comm;
            break;
        case 8:
            return data.errors_comm;
            break;
        case 9:
            return data.errors_count1;
            break;
        case 10:
            return data.errors_count2;
            break;
        case 11:
            return data.errors_count3;
            break;
        case 12:
            return data.errors_count4;
            break;
        case 13:
            return data.battery_remaining;
            break;
            
        default:
            break;
    }
    
    return v;
}