#ifndef MAVLIB_hpp
#define MAVLIB_hpp

#include "interface.h"
#include "ParamIMU.hpp"
#include "ParamRcChannels.hpp"
#include "ParamHeartbeat.hpp"
#include "parseutils.h"
#include <stdio.h>
#include "ParamSysStatus.hpp"
#include "ParamHud.hpp"
#include "ParamBatteryInfo.hpp"
#include "ParamSetMode.hpp"
#include "ParamLocalPosition.hpp"
#include "ParamGPS.hpp"
#include "ParamManualControl.hpp"
#include "SensorController.h"
#include "MissionController.h"

class Mavlib
{
public:
    Mavlib();

public:
    // 收到消息后处理, 返回要处理的消息类型
//    int ReceivedBytes(char* buff, int len);
    int ReceivedBytes(char b);
    
    /** 只设置customModel
     0:手动
     1:定高
     2:定点
     3:自动模式
     4:自动悬停
     5:自动返航
     6:特技
     7:OFF board
     8:自稳
     9:翻滚（模式）
     10:自动起飞
     11:自动降落
     */
    int sendSetMode(char* buff , uint8_t targetSystem, uint8_t baseMode , uint32_t customMode);
    // 发送获取参数请求包
    int sendParametersRequest(char* buff, uint8_t componentID);
    // 发送心跳包
    int sendHeartbeat(char* buff);
    // 发送设置参数包
    int sendParameterSetting(char* buff, int componentId, char* paramName, double value);
    // 开始校准
    int startCalibration(char* buff, int cal_type);
    // 结束校准
    int stopCalibration(char* buff);
    // 根据id值 获取参数值
    float getParameterValue(int id);
    //
    int sendParamRaw(char *buff, int compID, const char *paramName, float value, int valueType);
    
    void getStatusText(char *buff);
    
    float getParaValueInfo(char *valueType, char *buff);
    //一键起飞
    int guidedModeTakeoff(char *buff, float altitudeRel);
    
private:
    // 处理位置信息包
    void _handleHomePosition(mavlink_message_t msg);
    // 处理心跳包
    void _handleHeartbeat(mavlink_message_t msg);
    // 处理参数信息包
    void _handleParamvalue(mavlink_message_t msg);
    // 处理遥控
    void _handleRcChannels(mavlink_message_t msg);
    // IMU
    void _handleHighresImu(mavlink_message_t msg);
    // 系统状态:设备姿态, CPU等实时信息
    void _handleSysStatus(mavlink_message_t msg);
    // 执行命令, 确认消息
    void _handleSysCommandAck(mavlink_message_t msg);
    // 日志消息
    void _handleLogEntry(mavlink_message_t msg);
    // 日志消息
    void _handleLogData(mavlink_message_t msg);
    //电池信息
    void _handleBattery(mavlink_message_t msg);
    //位置信息
    void _handelLocalPosition(mavlink_message_t msg);
    //设置各种模式
    void _handleSetMode(mavlink_message_t msg);
    //gps信息
    void _handleParamGPS(mavlink_message_t msg);
    // 收到状态消息
    void _handleStatusText(mavlink_message_t msg);
    // 由消息, 生成要发送的字节流
    
    void _handleHUD(mavlink_message_t msg);
    int _getMessageBytes(mavlink_message_t message, char* buff);

private:
    
    ParamIMU param_IMU;
    ParamRcChannels param_rc_channels;
    ParamHeartbeat param_heartbeat;
//    ParamBatteryInfo param_battery;
    ParamLocalPosition param_loction;
    ParamSysStatus param_sys;
    ParamHud param_hud;
    ParamSetMode param_setModel;
    ParamGPS param_gps;
    ParamManualControl param_manualControl;
    SensorController sensorController;
    int targetSys;
    int baseMode;
    
    int sysid;          // 自己的ID, 255
    int componentid;    // 自己的组件号,默认1
    int mavlinkChannel; // 分配的mavlink通道, 这里设置成1

    int vehicleid;      // 飞机的id

    int lastIndex[256][256];
    
    char statusText[MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN+1];
    int paraValueType;
    double paraValue;
    char paraName[MAVLINK_MSG_PARAM_VALUE_FIELD_PARAM_ID_LEN];
};

#endif // MAVLIB_H
