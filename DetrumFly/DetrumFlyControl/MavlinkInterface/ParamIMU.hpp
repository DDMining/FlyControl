#ifndef PARAMIMU_H
#define PARAMIMU_H

#include "Param.hpp"
#include <stdio.h>
#include "mavlink.h"

class ParamIMU: Param
{
public:
    ParamIMU();

    friend class Mavlib;

    float value(int id);
    void setData(const mavlink_highres_imu_t& imu)
    {
        data = imu;
    }
    
    mavlink_highres_imu_t& getData()
    {
        return data;
    }
private:
    mavlink_highres_imu_t data;

};

#endif // PARAMIMU_H
