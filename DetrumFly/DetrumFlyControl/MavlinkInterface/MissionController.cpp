#include "MissionController.h"

MissionController::MissionController()
{

}
void MissionController::_handleMissionCount(const mavlink_message_t& message)
{
}

void MissionController::_handleMissionItem(const mavlink_message_t& message)
{

}

void MissionController::_handleMissionRequest(const mavlink_message_t& message)
{
}

void MissionController::_handleMissionAck(const mavlink_message_t& message)
{

}

void MissionController::_handleMissionCurrent(const mavlink_message_t& message)
{
}

void MissionController::sendMissionCount(const mavlink_message_t& message)
{
}

void MissionController::sendMissionItem(const mavlink_message_t& message)
{
}

void MissionController::sendMissionRequest(const mavlink_message_t& message)
{
}

void MissionController::sendMissionAck(const mavlink_message_t& message)
{
}

void MissionController::sendMissionCurrent(const mavlink_message_t& message)
{
}


void MissionController::handleMessage(int msgId, const mavlink_message_t& message)
{
    switch(msgId)
    {
    case MAVLINK_MSG_ID_MISSION_COUNT:

        _handleMissionCount(message);
        break;

    case MAVLINK_MSG_ID_MISSION_ITEM:
        _handleMissionItem(message);
        break;

    case MAVLINK_MSG_ID_MISSION_REQUEST:
        _handleMissionRequest(message);
        break;

    case MAVLINK_MSG_ID_MISSION_ACK:
        _handleMissionAck(message);
        break;

    case MAVLINK_MSG_ID_MISSION_ITEM_REACHED:
        // FIXME: NYI
        break;

    case MAVLINK_MSG_ID_MISSION_CURRENT:
        _handleMissionCurrent(message);
        break;
    default:
        ;
    }
}
