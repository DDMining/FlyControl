//
//  ParamHud.hpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/10.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef ParamHud_hpp
#define ParamHud_hpp

#include <stdio.h>
#include "Param.hpp"
#include "mavlink.h"


class ParamHud:Param {
public:
    ParamHud();
    friend class Mavlib;
//    MAVLINK_MSG_ID_VFR_HUD
    float value(int id);
    void setData(const mavlink_vfr_hud_t& imu)
    {
        data = imu;
    }
    
    mavlink_vfr_hud_t& getData()
    {
        return data;
    }
private:
    //    mavlink_smart_battery_status_t data;
    mavlink_vfr_hud_t data;
};

#endif /* ParamHud_hpp */
