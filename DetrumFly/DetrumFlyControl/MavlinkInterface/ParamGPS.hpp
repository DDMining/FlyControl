//
//  ParamGPS.hpp
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/17.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#ifndef ParamGPS_hpp
#define ParamGPS_hpp

#include <stdio.h>
#include "mavlink.h"
#include "Param.hpp"


class ParamGPS:Param
{
public:
    ParamGPS();
    
    friend class Mavlib;
    
    float value(int id);
    void setData(const mavlink_gps_raw_int_t& imu)
    {
        data = imu;
    }
    
    mavlink_gps_raw_int_t& getData()
    {
        return data;
    }
private:
    //    mavlink_smart_battery_status_t data;
    mavlink_gps_raw_int_t data;
};

#endif /* ParamGPS_hpp */
