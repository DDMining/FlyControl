
//
//  DetrumFourBtnTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/3.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumFourBtnTableViewCell.h"

@implementation DetrumFourBtnTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (IBAction)rollEvent:(id)sender {
    _callback(DetrumFourBtnclickTypeRoll);
}

- (IBAction)pitchEvent:(id)sender {
    _callback(DetrumFourBtnclickTypeLpitch);
}

- (IBAction)yawEvent:(id)sender {
    _callback(DetrumFourBtnclickTypeYaw);
}

- (IBAction)autoCalibrationEvent:(id)sender {
    _callback(DetrumFourBtnclickTypeAutoCorrection);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
