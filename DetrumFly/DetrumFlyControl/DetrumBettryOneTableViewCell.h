//
//  DetrumBettryOneTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/29.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumBettryOneTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *bettryOnOff;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *sliderValue;

@end
