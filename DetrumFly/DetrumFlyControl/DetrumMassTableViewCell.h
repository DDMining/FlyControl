//
//  DetrumMassTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/4.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

@interface DetrumMassTableViewCell : DetrumBaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *massValueLb;

@end
