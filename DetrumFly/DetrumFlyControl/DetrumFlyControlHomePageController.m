//
//  DetrumFlyControlHomePageController.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/14.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumMarco.h"
#import <MAMapKit/MAMapKit.h>
#import <MAMapKit/MAAnnotation.h>
#import <Masonry.h>
#import "DXPopover.h"
#include "avformat.h"
#import "DetrumMapAnntationView.h"
#import "Interface.h"
#include "swscale.h"
#import "DetrumSetView.h"
#import "DetrumAnnotation.h"
#import "DetrumPopTipView.h"
#import "NSTimer+FastCreate.h"
#import "DetrumMavlinkManager.h"
#import "DetrumAnnotationView.h"
#import "UIColor+RGBConverHex.h"
#import "DetrumFlyControlTopView.h"
#import "DetrumControlDeviceView.h"
#import "DetrumRemoteControlView.h"
#import "DetrumFlyControlHomePageController.h"
#import "DetrumCoordinateCorrect.h"
#import "DetrumFlypathAlterView.h"
#import "DetrumFlypathSetView.h"

#define kSpace 10
#define kBtnOriginTag 0x190
#define kCruisePopViewWidth 258

@interface DetrumFlyControlHomePageController () <MAMapViewDelegate,UIGestureRecognizerDelegate>
@property (assign, nonatomic) DetrumFlyControlCurrenShowPage currenShowPage;
//600
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewHeightLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewWidthLayout;
//700
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ratioLayout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewEqualTopBarWidth;

@property (weak, nonatomic) IBOutlet UIView *movieView;
@property (weak, nonatomic) IBOutlet UIView *mapContentView;
@property (weak, nonatomic) IBOutlet UIView *parameterView;
@property (strong, nonatomic) UIView *tempView;
@property (strong, nonatomic) MAMapView *mapView;
//@property (strong, nonatomic)
@property (strong, nonatomic) NSMutableArray *annotations;

@property (strong, nonatomic) DetrumFlyControlTopView *controlTopView;
@property (weak, nonatomic) IBOutlet UIView *zoomView;

@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *aeBtn;
@property (weak, nonatomic) IBOutlet UIView *photoSetView;

/** gesture */
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) UITapGestureRecognizer *mapTap;
@property (strong, nonatomic) UITapGestureRecognizer *flyPathTap;
// 路径
@property (assign, nonatomic) int pointNumber;
@property (strong, nonatomic) MAPolyline *flyPathLine;
@property (strong, nonatomic) NSMutableArray *flyPathLineArr;

@property (strong, nonatomic) UIView *CruiseControlPopView;
@property (assign, nonatomic) BOOL isPopAnimation;

@property (strong, nonatomic) DetrumPopTipView *popTipView;
@property (strong, nonatomic) DetrumControlDeviceView *device;
@property (strong, nonatomic) DetrumSetView *setView;
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) BOOL isShowMap;
@property (strong, nonatomic) DetrumMapAnntationView *annotation;
@property (strong, nonatomic) MAOfflineItem *offlineItem;
@property (nonatomic, strong) NSArray *cities;
@property (nonatomic, strong) NSArray *provinces;
@property (strong, nonatomic) DetrumFlypathAlterView *flypathAlterView;
@property (strong ,nonatomic) UIButton *clearAllPathBtn;
@property (strong ,nonatomic) UIButton *sureAllPathBtn;
@property (strong ,nonatomic) UIButton *deleteAllPathBtn;

@end

@implementation DetrumFlyControlHomePageController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isShowMap = NO;
    [self initParameter];
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [self initMavlinkManager];
    [self initTimer];
}

- (void)initUI {
    [self initMap];
    [self initTopView];
    [self initTempView];
}

- (void)initTimer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1.f target:self action:@selector(initData) repeat:YES];
        [_timer startTimer];
    }
    [_timer startTimer];
}

- (void)initData {
    
    NSArray *localArray = [[DetrumMavlinkManager shareInstance] getDataWithType:PARAM_LOCAL];
    NSArray *hudArray = [[DetrumMavlinkManager shareInstance] getDataWithType:PARAM_VFR_HUD];
    if (localArray.count != 0) {
        NSString *d = nil;
        CGFloat x = [localArray[1] floatValue] * [localArray[1] floatValue];
        CGFloat y = [localArray[2] floatValue] * [localArray[2] floatValue];
        
        CGFloat sqrtfXy = sqrtf(x + y);
        d = [NSString stringWithFormat:@"%.2f",sqrtfXy];
        _secondParameterLb.text = [@"D:" stringByAppendingString:d];
        _firstParameterLb.text = [@"H:" stringByAppendingString:[NSString stringWithFormat:@"%.2f",fabs([localArray[3] floatValue])]];
 
    }
    if (hudArray.count != 0) {
        _threeParameterLb.text = [@"VS:" stringByAppendingString:[NSString stringWithFormat:@"%.2f",[hudArray[3] floatValue]]];
        _fourParameterLb.text = [@"HS:" stringByAppendingString:[NSString stringWithFormat:@"%.2f",[hudArray[1] floatValue]]];
    }
    NSArray *gps = [[DetrumMavlinkManager shareInstance] getDataWithType:PARAM_GPS];
    if (gps == nil || gps.count == 0) {
        return;
    }  else {
        double lat = [gps[1] doubleValue] / 10000000;
        double lon = [gps[2] doubleValue] / 10000000;
//        NSLog(@"lat:%f-----lon:%f",lat,lon);
        if (lat == 0.00000 || lon == 0.0000) {
            return;
        }
        DetrumCoordinateCorrect *coo = [[DetrumCoordinateCorrect alloc]init];
        NSDictionary * Coodic = [coo WGS84ToGCJ02:lat lon:lon];
        if (_annotation) {
            [self.mapView removeAnnotation:_annotation];
        }
        _annotation = [[DetrumMapAnntationView alloc] init];
        
        _annotation.coordinate = CLLocationCoordinate2DMake([[Coodic valueForKey:@"lat"] doubleValue], [Coodic[@"lon"] doubleValue]);
        NSLog(@"%f,%f",[[Coodic valueForKey:@"lat"] doubleValue], [Coodic[@"lon"] doubleValue]);
        [self.mapView addAnnotation:_annotation];
        [_mapView setCenterCoordinate:_annotation.coordinate animated:YES];
    }
}



- (void)initMavlinkManager {
    if ([[DetrumMavlinkManager shareInstance] connetWifi]) {
        [[DetrumMavlinkManager shareInstance] setUpTimer];
        [SVProgressHUD showSuccessWithStatus:@"成功连接飞控"];
    } else {
        DFLog(@"连接失败");
    }
}

- (void)initTopView {    
    if (iPhone5) {
        _controlTopView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumFlyControlTopView"
                                                         owner:self
                                                       options:nil]
                           firstObject];
    } else if (iPhone6) {
        _controlTopView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumFlyControlTopView"
                                                        owner:self
                                                       options:nil] objectAtIndex:1];
    } else if (iPhone6P) {
        _controlTopView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumFlyControlTopView"
                                                         owner:self
                                                       options:nil]
                           lastObject];
    }

    
    WS(weakSelf);
    [SVProgressHUD showSuccessWithStatus:@""];
    _controlTopView.action = ^(NSString *seleteName) {
        if ([FlyControlViewBtnEventBackHome isEqualToString:seleteName]) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        } else if ([FlyControlViewBtnEventDeviceConnect isEqualToString:seleteName]) {
            [weakSelf connectDevice];
        } else if ([FlyControlViewBtnEventJoinClick isEqualToString:seleteName]) {
            [weakSelf joinSetInterface];
        }
    };

    _topView.userInteractionEnabled = YES;
    [_topView addSubview:_controlTopView];
    _topViewHeightLayout.constant = _controlTopView.frame.size.height;
}


- (void)initParameter {
    _currenShowPage = DetrumFlyControlCurrenShowPageMovie;
    addObserver(@selector(hiddenSetViewAnimation), FlyControlNotificationRemove);
}

- (void)initTempView {
    if (_tempView == nil) {
        _tempView = [[UIView alloc] initWithFrame:_mapContentView.frame];
        _tempView.alpha = 0.0f;
        _tempView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:_tempView];
    }
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                          action:@selector(exchangeMapViewAndMovieView:)];
    [_tempView addGestureRecognizer:_tapGesture];
    _tempView.userInteractionEnabled = YES;
}

- (void)initCruiseControlView {
    if (_CruiseControlPopView == nil) {
        _CruiseControlPopView = [[UIView alloc]initWithFrame:(CGRect){{CGRectGetMinX(_CruiseControlBtn.frame),CGRectGetMinY(_CruiseControlBtn.frame)},{0,CGRectGetHeight(_CruiseControlBtn.frame)}}];
        _CruiseControlPopView.backgroundColor = [UIColor redColor];
        UIImageView *img = [[UIImageView alloc] initWithFrame:_CruiseControlPopView.frame];
        [_CruiseControlPopView addSubview:img];
        img.image = [UIImage imageNamed:@"A box"];
        [img  mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.left.right.equalTo(_CruiseControlPopView);
        }];
        _CruiseControlPopView.layer.cornerRadius = 21;
        _CruiseControlPopView.layer.masksToBounds = YES;
        img.layer.cornerRadius = _CruiseControlPopView.layer.cornerRadius;
        _isPopAnimation = NO;
        [self.view addSubview:_CruiseControlPopView];
        [self.view insertSubview:_CruiseControlPopView belowSubview:_CruiseControlBtn];
        CGFloat lastX = 0.f;
        CGFloat width = kCruisePopViewWidth - CGRectGetWidth(_CruiseControlBtn.frame);
        CGFloat btnWidth = (width - (5 * kSpace)) / 5;
        for (int i = 1; i < 6; i ++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            if (i == 1) {
                [btn setFrame:CGRectMake(i*kSpace + CGRectGetWidth(_CruiseControlBtn.frame) , (CGRectGetHeight(_CruiseControlPopView.frame) - btnWidth) / 2, btnWidth, btnWidth)];
                lastX = CGRectGetMaxX(btn.frame);
            } else {
                [btn setFrame:CGRectMake(lastX + kSpace,  (CGRectGetHeight(_CruiseControlPopView.frame) - btnWidth) / 2, btnWidth, btnWidth)];
                lastX = CGRectGetMaxX(btn.frame);
            }
            btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [btn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d",i]] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(MultifunctionEvent:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = kBtnOriginTag + i;
            [_CruiseControlPopView addSubview:btn];
//            NSLog(@"btn rect : %@",NSStringFromCGRect(btn.frame));
        }
    }
}

- (void)initMap {
    
    // offlineMap
    self.cities = [MAOfflineMap sharedOfflineMap].cities;
    self.provinces = [MAOfflineMap sharedOfflineMap].provinces;
    MAOfflineProvince *pro = self.provinces[10];
    MAOfflineItem *item = pro.cities[0];
// 离线下载武汉市地图
//    [self download:item];
    
    if (_mapView==nil) {
        _mapView = [[MAMapView alloc] initWithFrame:_mapContentView.bounds];
        _mapView.delegate = self;
        [self.mapContentView addSubview:_mapView];
    }
    _mapView.showsCompass = NO;
    _mapView.zoomLevel = 14;
    _mapView.showsScale = NO;
    _mapView.mapType = MAMapTypeStandard;
//    _mapView.showsUserLocation = YES;
    _mapView.userTrackingMode = MAUserTrackingModeFollow;

    _mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exchangeMapViewAndMovieView:)];
    _mapTap.delegate = self;
    [_mapView addGestureRecognizer:_mapTap];
}


- (void)exchangeMapViewAndMovieView:(UIGestureRecognizer *)gesture {
    
    if (_currenShowPage == DetrumFlyControlCurrenShowPageMovie) {
        _isShowMap = NO;
        [UIView animateWithDuration:0.4 animations:^{
            _tempView.hidden = NO;
            _mapViewHeightLayout.priority = 800;
            _mapViewWidthLayout.priority = 800;
            _tempView.alpha = 1.f;
        } completion:nil];
        _currenShowPage = DetrumFlyControlCurrenShowPageMap;
    } else {
        _isShowMap = YES;

        if (gesture == _tapGesture) {
            [UIView animateWithDuration:0.4 animations:^{
                _mapViewHeightLayout.priority = 600;
                _mapViewWidthLayout.priority = 600;
                _tempView.alpha = 0.f;
            } completion:^(BOOL finished) {
                _tempView.hidden = YES;
            }];
            _currenShowPage = DetrumFlyControlCurrenShowPageMovie;
            return;
        }
//        if (_annotations == nil) {
//            _annotations = [NSMutableArray new];
//        }
//        _currenShowPage = DetrumFlyControlCurrenShowPageMovie;
//        CGPoint point = [gesture locationInView:_mapContentView];
//        CLLocationCoordinate2D coor = [_mapView convertPoint:point
//                                        toCoordinateFromView:_mapContentView];
//        DetrumAnnotation *annotationView = [[DetrumAnnotation alloc] initWithCoordinate:coor];
//        annotationView.index = [_annotations count];
//        annotationView.annotationName = [NSString stringWithFormat:@"编号%zd",annotationView.index];
//        annotationView.title = annotationView.annotationName;
//        [self.mapView addAnnotation:annotationView];
//        [_annotations addObject:annotationView];
//        if (_annotations.count >= 2) {
//            [self DrawLine];
//        }
    }
    [self.view setNeedsLayout];
}


#pragma mark 路径绘制
- (void)initFlypathView{
    self.menuView.hidden = YES;
    self.menuBtn.hidden = YES;
    self.aeBtn.hidden = YES;
    self.photoSetView.hidden = YES;
    self.zoomView.hidden = YES;
    self.pointNumber = 0;
    if (_currenShowPage == DetrumFlyControlCurrenShowPageMovie) {
        _isShowMap = NO;
        [UIView animateWithDuration:0.4 animations:^{
            _tempView.hidden = NO;
            _mapViewHeightLayout.priority = 800;
            _mapViewWidthLayout.priority = 800;
            _tempView.alpha = 1.f;
        } completion:nil];
        _currenShowPage = DetrumFlyControlCurrenShowPageMap;
    }
    
    _flyPathTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showFlyPathPoint:)];
    [_mapView addGestureRecognizer:_flyPathTap];
    
    if (_clearAllPathBtn == nil) {
        _clearAllPathBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 90, self.topView.frame.size.height + 15, 80, 20)];
        _clearAllPathBtn.backgroundColor = [UIColor colorWithHexString:@"25CEC1"];
        [_clearAllPathBtn setTitle:@"清除全部点" forState:UIControlStateNormal];
        _clearAllPathBtn.titleLabel.textColor = [UIColor whiteColor];
        [_clearAllPathBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
        _clearAllPathBtn.layer.cornerRadius = 10;
        [_clearAllPathBtn addTarget:self action:@selector(clearAllPathBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_clearAllPathBtn];
    }
   
    if (_sureAllPathBtn == nil) {
        _sureAllPathBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 90, self.topView.frame.size.height + 45, 80, 20)];
        _sureAllPathBtn.backgroundColor = [UIColor colorWithHexString:@"25CEC1"];
        [_sureAllPathBtn setTitle:@"确认路径点" forState:UIControlStateNormal];
        _sureAllPathBtn.titleLabel.textColor = [UIColor whiteColor];
        [_sureAllPathBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
        _sureAllPathBtn.layer.cornerRadius = 10;
        [_sureAllPathBtn addTarget:self action:@selector(sureAllPathBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_sureAllPathBtn];
    }
    
    if (_deleteAllPathBtn == nil) {
        _deleteAllPathBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 90, self.topView.frame.size.height + 75, 80, 20)];
        _deleteAllPathBtn.backgroundColor = [UIColor colorWithHexString:@"25CEC1"];
        [_deleteAllPathBtn setTitle:@"退出路径设置" forState:UIControlStateNormal];
        _deleteAllPathBtn.titleLabel.textColor = [UIColor whiteColor];
        [_deleteAllPathBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
        _deleteAllPathBtn.layer.cornerRadius = 10;
        [_deleteAllPathBtn addTarget:self action:@selector(deleteAllPathBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_deleteAllPathBtn];
    }
    
    _clearAllPathBtn.hidden = NO;
    _sureAllPathBtn.hidden = NO;
    _deleteAllPathBtn.hidden = NO;
    
}

- (void)showFlyPathPoint:(UIGestureRecognizer*)gesture{
    self.pointNumber++;
    DetrumFlypathSetView *flypathSetView = [DetrumFlypathSetView shareInstance];
    flypathSetView.pointNumTextField.text = [NSString stringWithFormat:@"%d",self.pointNumber];
    [flypathSetView show:self.view];
    
    flypathSetView.closeAction = ^(){
        self.pointNumber--;
        return;
    };
    
    CGPoint touchPoint = [gesture locationInView:_mapView];
    CLLocationCoordinate2D touchMapCoordinate = [_mapView convertPoint:touchPoint toCoordinateFromView:_mapView];
    MAPointAnnotation *flyPathAnnotation = [[MAPointAnnotation alloc]init];
    flyPathAnnotation.coordinate = touchMapCoordinate;
    if (_annotations == nil) {
        _annotations = [NSMutableArray new];
    }
    
    flypathSetView.sureAction = ^(NSInteger DetrumFlypathSetStatus, NSString *stayTime , NSString *height){
        [_mapView addAnnotation:flyPathAnnotation];
        [_annotations addObject:flyPathAnnotation];
        NSLog(@"DetrumFlypathSetStatus:%ld,stayTime:%@,height:%@",(long)DetrumFlypathSetStatus,stayTime,height);
        [self DrawLine];
    };
    
}

- (void)clearAllPathBtn:(UIButton *)sender{
    self.pointNumber = 0;
    [_mapView removeOverlays:_flyPathLineArr];
    [_mapView removeAnnotations:_annotations];
    _annotations = nil;
    _flyPathLineArr = nil;
    
}

- (void)sureAllPathBtn:(UIButton *)sender{
    
    
    
}

- (void)deleteAllPathBtn:(UIButton *)sender{
    
    self.pointNumber = 0;
    _clearAllPathBtn.hidden = YES;
    _sureAllPathBtn.hidden = YES;
    _deleteAllPathBtn.hidden = YES;
    
    self.menuView.hidden = NO;
    self.menuBtn.hidden = NO;
    self.aeBtn.hidden = NO;
    self.photoSetView.hidden = NO;
    self.zoomView.hidden = NO;
    [_mapView removeOverlays:_flyPathLineArr];
    [_mapView removeAnnotations:_annotations];
    _annotations = nil;
    _flyPathLineArr = nil;
    
    [_mapView removeGestureRecognizer:_flyPathTap];
    
}


- (void)DrawLine {
    if (_annotations.count < 2) {
        return;
    }
    CLLocationCoordinate2D currenCoor = ((DetrumAnnotation *)[_annotations lastObject]).coordinate;
    CLLocationCoordinate2D OnACurrenCoor = ((DetrumAnnotation *)[_annotations objectAtIndex:_annotations.count - 2]).coordinate;
    CLLocationCoordinate2D points[2];
    points[0] = currenCoor;
    points[1] = OnACurrenCoor;
    if (_flyPathLineArr == nil) {
        _flyPathLineArr = [NSMutableArray new];
    }
    MAPolyline *line = [MAPolyline polylineWithCoordinates:points count:2];
    [_flyPathLineArr addObject:line];
    [self.mapView addOverlay:line];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}


- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation
 updatingLocation:(BOOL)updatingLocation {
    if (updatingLocation) {
//        _mapView.centerCoordinate = userLocation.coordinate;
    }
}

#pragma mark - MAMapViewDelegate

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[DetrumAnnotation class]])
    {
        static NSString *animatedAnnotationIdentifier = @"AnimatedAnnotationIdentifier";
        
        DetrumAnnotationView *annotationView = (DetrumAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:animatedAnnotationIdentifier];
        
        if (annotationView == nil)
        {
            annotationView = [[DetrumAnnotationView alloc] initWithAnnotation:annotation
                                                              reuseIdentifier:animatedAnnotationIdentifier];
            annotationView.annotationImg.image = [UIImage imageNamed:@"bbs"];
            annotationView.canShowCallout   = NO;
            annotationView.draggable        = YES;
        }
        return annotationView;
    } else if ([annotation isKindOfClass:[DetrumMapAnntationView class]]) {
        static NSString *AnnotationIdentifier = @"AnnotationIdentifier";
        MAAnnotationView *mapAnntationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
        if (mapAnntationView == nil)
        {
            mapAnntationView = [[MAAnnotationView alloc] initWithAnnotation:annotation
                                                                  reuseIdentifier:AnnotationIdentifier];
            mapAnntationView.image = [UIImage imageNamed:@"无人机图标-1"];
            mapAnntationView.canShowCallout   = NO;
            mapAnntationView.draggable        = YES;
        }
        return mapAnntationView;
    }else if ([annotation isKindOfClass:[MAPointAnnotation class]]){
        static NSString *flyPathAnnotationIdentifier = @"flyPathAnnotationIdentifier";
        MAAnnotationView *flyPathAnnotationView = (MAPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:flyPathAnnotationIdentifier];
        if (flyPathAnnotationView == nil)
        {
            flyPathAnnotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:flyPathAnnotationIdentifier];
            
        }
        return flyPathAnnotationView;
        
    }
    
    return nil;
}

- (MAOverlayView *)mapView:(MAMapView *)mapView viewForOverlay:(id <MAOverlay>)overlay
{
    if ([overlay isKindOfClass:[MAPolyline class]])
    {
        MAPolylineView *polylineView = [[MAPolylineView alloc] initWithPolyline:overlay];
        
        polylineView.lineWidth = 4.f;
        polylineView.strokeColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.6];
        polylineView.lineJoinType = kMALineJoinRound;//连接类型
        polylineView.lineCapType = kMALineCapRound;//端点类型
        
        return polylineView;
    }
    return nil;
}

//- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay {
//    MAPolylineRenderer *polylineRenderer = [[MAPolylineRenderer alloc] initWithPolyline:overlay];
//    polylineRenderer.lineWidth   = 2.f;
//    polylineRenderer.fillColor = [UIColor redColor];
//    return polylineRenderer;
//}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeLeft;
}

- (BOOL)shouldAutorotate
{
     return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

// 离线下载
- (void)download:(MAOfflineItem *)item
{
    if (item == nil || item.itemStatus == MAOfflineItemStatusInstalled)
    {
        return;
    }
    
    NSLog(@"download :%@", item.name);
    
    [[MAOfflineMap sharedOfflineMap] downloadItem:item shouldContinueWhenAppEntersBackground:YES downloadBlock:^(MAOfflineItem *downloadItem, MAOfflineMapDownloadStatus downloadStatus, id info) {
            /* Manipulations to your application’s user interface must occur on the main thread. */
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (downloadStatus == MAOfflineMapDownloadStatusWaiting)
                {
                    NSLog(@"状态为: %@", @"等待下载");
                }
                else if(downloadStatus == MAOfflineMapDownloadStatusStart)
                {
                    NSLog(@"状态为: %@", @"开始下载");
                }
                else if(downloadStatus == MAOfflineMapDownloadStatusProgress)
                {
                    NSLog(@"状态为: %@", @"正在下载");
                }
                else if(downloadStatus == MAOfflineMapDownloadStatusCancelled) {
                    NSLog(@"状态为: %@", @"取消下载");
                }
                else if(downloadStatus == MAOfflineMapDownloadStatusCompleted) {
                    NSLog(@"状态为: %@", @"下载完成");
                }
                else if(downloadStatus == MAOfflineMapDownloadStatusUnzip) {
                    NSLog(@"状态为: %@", @"下载完成，正在解压缩");
                }
                else if(downloadStatus == MAOfflineMapDownloadStatusError) {
                    NSLog(@"状态为: %@", @"下载错误");
                }
                else if(downloadStatus == MAOfflineMapDownloadStatusFinished) {
                    NSLog(@"状态为: %@", @"全部完成");
                    [self.mapView reloadMap];              //激活离线地图
                }
            });
        
    }];
}

- (void)pause:(MAOfflineItem *)item
{
    NSLog(@"pause :%@", item.name);
    
    [[MAOfflineMap sharedOfflineMap] pauseItem:item];
}



#pragma mark 点击事件
- (IBAction)CruiseControlClickEvent:(id)sender {

    if (_flypathAlterView == nil) {
        _flypathAlterView = [DetrumFlypathAlterView shareInstance];
        [_flypathAlterView show:self.view];
        
        [self initFlypathView];
        
    }else{
        [_flypathAlterView removeFromSuperview];
        _flypathAlterView = nil;
    }
    
    if (_CruiseControlPopView == nil) {
        [self initCruiseControlView];
    }
    if (_isPopAnimation == YES) {
        return;
    }
    if (CGRectGetWidth(_CruiseControlPopView.frame) == 0) {
        [UIView animateWithDuration:0.8 animations:^{
            _isPopAnimation = YES;
            [_CruiseControlPopView setRectWidth:kCruisePopViewWidth];
        }completion:^(BOOL finished) {
            _isPopAnimation = NO;
        }];
    } else {
        [_CruiseControlPopView setRectWidth:0];
    }
}

- (void)MultifunctionEvent:(UIButton *)btn {
   
}

//一键起飞
- (IBAction)takeOffClickEvent:(id)sender {
    _popTipView = [DetrumPopTipView shareInstance];
    _popTipView.type = PopViewTypeTakeOff;
    _popTipView.action = ^(PopViewType type , float height ) {
        
        NSLog(@"height:%f",height);
        
        
        
        [[DetrumMavlinkManager shareInstance] sendParamRaw:1 paramName:@"MIS_TAKEOFF_ALT" value:height valueType:7];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [[DetrumMavlinkManager shareInstance] guidedModeTakeoffHeight:height];
            [[DetrumMavlinkManager shareInstance] sendSetMode:AUTOTAKEOFF baseMode:157];
        });
        
        
    };
    [_popTipView show:self.view];
}

- (IBAction)landingEvent:(id)sender {
//    [[DetrumMavlinkManager shareInstance] sendParamterSet:FLYCONTROLLIGHT andValue:0];
    
    [[DetrumMavlinkManager shareInstance] sendSetMode:AUTORTL baseMode:157];
    NSLog(@"AUTORTL:%d,BASEMODE:%d",AUTORTL,BASEMODE);
    
//    _popTipView = [DetrumPopTipView shareInstance];
//    _popTipView.type = PopViewTypeLeading;
//    _popTipView.action = ^(PopViewType type) {
//        //一键返航
//        [[DetrumMavlinkManager shareInstance] sendSetMode:11];
//    };
//    [_popTipView show:self.view];
}

- (void)connectDevice {
    DFLog(@"点击了连接设备");
    if ([UIScreen mainScreen].bounds.size.width == 568) {
        _popoverWidth = 384;
        _popoverHeight = 267;
    } else if ([UIScreen mainScreen].bounds.size.width == 667) {
        _popoverWidth = 455;
        _popoverHeight = 300;
    } else {
        _popoverWidth = 500;
        _popoverHeight = 370;
    }
    
    WS(weakSelf);
    if (_device == nil) {
        _device = [[DetrumControlDeviceView alloc] initWithFrame:CGRectMake(0, 0, _popoverWidth, _popoverHeight)];
      
    }
    
    _device.backgroundColor = [UIColor clearColor];
    _device.action = ^() {
        [weakSelf.popover dismiss];
    };
    _device.seleteAction = ^(NSString *seleteTypeName , NSInteger index) {
        if ([seleteTypeName isEqualToString:@"遥控器模式"]) {
            DetrumRemoteControlView *controlView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumRemoteControlView" owner:weakSelf options:nil] lastObject];
            controlView.frame = CGRectMake(0, 0, weakSelf.device.frame.size.width, weakSelf.device.frame.size.height);
            controlView.animationView = weakSelf.device;
            for (int i = 0 ; i < weakSelf.device.subviews.count; i++) {
                UIView *view = weakSelf.device.subviews[i];
                if (![view isKindOfClass:[DetrumRemoteControlView class]]) {
                    view.hidden = YES;
                }
            }
            [weakSelf.device addSubview:controlView];
        }
    };
    if (_popover == nil) {
        self.popover = [DXPopover new];
    }
    self.popover.arrowColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Rectangle 45 Copy"]];
    self.popover.backgroundImg = [UIImage imageNamed:@"Rectangle 45 Copy"];
    [self.popover showAtPoint:CGPointMake(CGRectGetMidX(_controlTopView.frame), CGRectGetMaxY(_controlTopView.frame))
               popoverPostion:DXPopoverPositionDown
              withContentView:_device
                       inView:self.view];
    self.popover.didDismissHandler = ^{
        DFLog(@"取消了");
    };
}

- (void)joinSetInterface {
    _setView = [[[NSBundle mainBundle] loadNibNamed:@"DetrumSetView" owner:self options:nil] lastObject];
    [_setView setRectY:_controlTopView.frame.size.height];
    [_setView setRectX:CGRectGetMaxX(_landingBtn.frame) + 5];
    [_setView setRectWidth:[RectExtends getScreenFrameWithWidth] - (CGRectGetMaxX(_landingBtn.frame) + 5) - (([RectExtends getScreenFrameWithWidth] - CGRectGetMinX(_zoomView.frame)) )];
    [_setView setRectHeight:[RectExtends getScreenFrameWithHeight] - (2 * _topView.frame.size.height) + 10];
    [_setView updateConstraintsIfNeeded];
    _setView.alpha = 0.4;
    [self.view addSubview:_setView];
    [self showSetViewAnimation];
}

- (void)showSetViewAnimation {
    [UIView animateWithDuration:0.3 animations:^{
        _topView.transform = CGAffineTransformMakeTranslation(0, -CGRectGetHeight(_topView.frame));
        _mapContentView.transform = CGAffineTransformMakeTranslation(0,CGRectGetMinY(_mapContentView.frame) + CGRectGetHeight(_mapContentView.frame));
        _parameterView.transform = CGAffineTransformMakeTranslation(0,CGRectGetHeight(self.view.frame));
        _menuView.alpha = 0.0;
        _menuBtn.alpha = 0.0;
        _aeBtn.alpha = 0.0f;
        _setView.alpha = 1.f;
    }];
}

- (void)hiddenSetViewAnimation {
    [UIView animateWithDuration:0.3 animations:^{
        _topView.transform = CGAffineTransformIdentity;
        _mapContentView.transform = CGAffineTransformIdentity;
        _parameterView.transform = CGAffineTransformIdentity;
        _menuView.alpha = 1.f;
        _menuBtn.alpha = 1.f;
        _aeBtn.alpha = 1.f;
        _setView.alpha = 0.1f;
    } completion:^(BOOL finished) {
        [_setView removeFromSuperview];
    }];
}

- (void)dealloc {
    removeObserver();
}

@end
