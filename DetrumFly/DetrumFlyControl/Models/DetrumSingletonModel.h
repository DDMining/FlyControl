//
//  DetrumSingletonModel.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "CYLTabBarController.h"
#import <Foundation/Foundation.h>

@interface DetrumSingletonModel : NSObject <NSCoding>
@property (strong, nonatomic) CYLTabBarController *baseTab;
+ (instancetype)shareInstance;
@end
