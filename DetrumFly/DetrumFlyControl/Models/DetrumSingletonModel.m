//
//  DetrumSingletonModel.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/12.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumSingletonModel.h"
static DetrumSingletonModel *_detrumModel = nil;
@implementation DetrumSingletonModel

//TODO:在更新资料后请把对象存到本地刷新
+ (instancetype)shareInstance {
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken, ^{
        _detrumModel = [DetrumSingletonModel new];
    });
    return _detrumModel;
}



@end
