//
//  RextExtends.m
//  JxCarios
//
//  Created by xcq on 14/7/22.
//  Copyright (c) 2015年 xiongchuanqi. All rights reserved.
//

#import "RectExtends.h"

@implementation RectExtends

+ (CGFloat)getThisViewX:(UIView *)view {
    return CGRectGetMinX(view.frame);
}

+ (CGFloat)getThisViewY:(UIView *)view {
    return CGRectGetMinY(view.frame);
}

+ (CGFloat)getThisViewWidth:(UIView *)view {
    return CGRectGetWidth(view.frame);
}

+ (CGFloat)getThisViewHeight:(UIView *)view {
    return CGRectGetHeight(view.frame);
}

+ (CGPoint)getThisViewOrigin:(UIView *)view {
    return view.frame.origin;
}

+ (CGSize)getThisViewSize:(UIView *)view {
    return view.frame.size;
}

+ (CGRect)getScreenFrame {
    return [UIScreen mainScreen].bounds;
}


+ (CGFloat)getScreenFrameWithHeight {
    return CGRectGetHeight([RectExtends getScreenFrame]);
}

+ (CGFloat)getScreenFrameWithWidth {
    return CGRectGetWidth([RectExtends getScreenFrame]);
}

@end
