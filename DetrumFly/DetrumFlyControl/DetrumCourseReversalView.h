//
//  DetrumCourseReversalView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/26.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseView.h"
#import <UIKit/UIKit.h>

@interface DetrumCourseReversalView : DetrumBaseView <UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *titleArr;
}
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *backTo;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
