//
//  DetrumBettryAlarmTableViewCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/29.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseTableViewCell.h"
#import <UIKit/UIKit.h>

@interface DetrumBettryAlarmTableViewCell : DetrumBaseTableViewCell
/** 严重低电量报警 */
@property (weak, nonatomic) IBOutlet UILabel *seriousLowBatteryLb;
@property (weak, nonatomic) IBOutlet UISlider *seriousLowBatteryAlarmSld;
/** 低电量报警 */
@property (weak, nonatomic) IBOutlet UILabel *lowBatteryLb;
@property (weak, nonatomic) IBOutlet UISlider *lowBatteryAlarmSld;
@property (weak, nonatomic) IBOutlet UILabel *flightTimeLb;

@end
