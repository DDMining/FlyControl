//
//  DetrumAboutView.m
//  DetrumFlyControl
//
//  Created by xcq on 16/3/8.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumAboutView.h"
#import "DetrumOtherTableViewCell.h"

#define kOtherIdentifier @"other"

@implementation DetrumAboutView

- (void)awakeFromNib {
    [self viewDidLoad];
}

- (void)viewDidLoad {
    if (_tableView.delegate == nil) {
        titleArr = @[@"版本",@"APP",@"遥控器",@"固件",@"网站"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView registerNib:[UINib nibWithNibName:@"DetrumOtherTableViewCell" bundle:nil] forCellReuseIdentifier:kOtherIdentifier];
    }
}

#pragma mark - tableView delegate & dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return titleArr.count == 0 ? 0 : titleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        DetrumOtherTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kOtherIdentifier forIndexPath:indexPath];
        cell.lastName.hidden = YES;
        cell.midName.hidden = YES;
        cell.name.hidden = NO;
        cell.name.textColor = [UIColor darkGrayColor];
        cell.name.text = titleArr[indexPath.row];
        return cell;
    } else {
        DetrumOtherTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kOtherIdentifier forIndexPath:indexPath];
        cell.midName.hidden = YES;
        cell.lastName.hidden = NO;
        cell.name.hidden = NO;
        cell.name.text = titleArr[indexPath.row];
        if (indexPath.row == titleArr.count - 1) {
            cell.lastName.text = @"www.detrumtech.com";
        } else {
            cell.lastName.text = @"N/A";
        }
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 38;
}

- (IBAction)backEvent:(id)sender {
    [self backAnimaiton];
}

@end
