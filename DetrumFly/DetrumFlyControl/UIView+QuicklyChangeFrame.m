//
//  UIView+QuicklyChangeFrame.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/19.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "UIView+QuicklyChangeFrame.h"

@implementation UIView (QuicklyChangeFrame)

- (void)setRectHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (void)setRectWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (void)setRectX:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (void)setRectY:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}


@end
