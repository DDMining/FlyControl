//
//  FileIO.m
//  CityHunter
//
//  Created by Gary Zhou on 2014.1.16.
//  Copyright (c) 2014 xxx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileIO.h"

@implementation FileIO

/**
 * @brief get file at path
 *
 * @param [in] file path
 * @param [out]
 * @return file data
 * @note
 */
+ (NSData *)getFileAtPath:(NSString*)path {
    if (nil == path) {
        return nil;
    }
    
    BOOL isDirectory;
    BOOL isFileExist = [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDirectory];
    if (!(isFileExist && !isDirectory)) {
        return nil;
    }
    
    return [[NSFileManager defaultManager] contentsAtPath:path];
}

/**
 * @brief save file at path
 *
 * @param [in] file path, file data
 * @param [out]
 * @return success or fail
 * @note
 */
+ (BOOL)saveFileAtPath:(NSString*)path data:(NSData *)data {
    if (nil == path) {
        return NO;
    }
    
    BOOL isFileExist = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (!isFileExist) {
        return [[NSFileManager defaultManager] createFileAtPath:path contents:data attributes:nil];
    } else {
        return [data writeToFile:path atomically:YES];
    }
}

/**
 * @brief remove item at path
 *
 * @param [in] file path, file data
 * @param [out]
 * @return success or fail
 * @note
 */
+ (BOOL)removeItemAtPath:(NSString*)path {
    if (nil == path || ![[NSFileManager defaultManager] fileExistsAtPath:path
                                                             isDirectory:NO]) {
        return YES;
    }
    
    NSError *error = nil;
    BOOL ret = [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    if (ret) {
        NSLog(@"remove item success, path: %@", path);
    } else {
        NSLog(@"remove item fail, path: %@, error: %@ ", path, error.userInfo);
    }
    
    return ret;
}

/**
 * @brief create directory
 *
 * @param [in] file path, file data
 * @param [out]
 * @return success or fail
 * @note
 */
+ (BOOL)createDirectory:(NSString*)path {
    //先判断文件夹是否存在
    BOOL isDir = NO;
    if ([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir]) {
        if (isDir) {
            return YES;
        } else {
            NSLog(@"path is not Directory: %@", path);
            return NO;
        }
    }
    
    //递归创建
    NSString* superDir = [path stringByDeletingLastPathComponent];
    if ([FileIO createDirectory:superDir]) {
        NSError *error = nil;
        BOOL ret = [[NSFileManager defaultManager] createDirectoryAtPath:path
                                             withIntermediateDirectories:NO
                                                              attributes:nil
                                                                   error:&error];
        if (!ret) {
            NSLog(@"create directory fail, error: %@", error.userInfo);
        }
        
        return ret;
    } else {
        return NO;
    }
}

/**
 * @brief copy item
 *
 * @param [in] file path, file data
 * @param [out]
 * @return success or fail
 * @note
 */
+ (BOOL)copyItemAtPath:(NSString*)srcPath toPath:(NSString*)dstPath {
    NSError *error = nil;
    BOOL ret = [[NSFileManager defaultManager] copyItemAtPath:srcPath toPath:dstPath error:&error];
    if (!ret) {
        NSLog(@"error: %@", error.userInfo);
    }
    
    return ret;
}

@end
