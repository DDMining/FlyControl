//
//  ALAsset+DetrumALAasset.m
//  DetrumFlyControl
//
//  Created by xcq on 16/1/27.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "ALAsset+DetrumALAasset.h"

@implementation ALAsset (DetrumALAasset)

- (NSComparisonResult)assertComparetor:(ALAsset *)asset {
    NSComparisonResult result = [[asset valueForProperty:@"ALAssetPropertyDate"] compare:[self valueForProperty:@"ALAssetPropertyDate"]];
    return result;
}

@end
