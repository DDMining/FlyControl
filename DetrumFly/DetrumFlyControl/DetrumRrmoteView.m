//
//  DetrumRrmoteView.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/7/18.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#define RC1_MIN @"RC1_MIN"
#define RC1_TRIM @"RC1_TRIM"
#define RC1_MAX @"RC1_MAX"

#define RC2_MIN @"RC2_MIN"
#define RC2_TRIM @"RC2_TRIM"
#define RC2_MAX @"RC2_MAX"

#define RC3_MIN @"RC3_MIN"
#define RC3_TRIM @"RC3_TRIM"
#define RC3_MAX @"RC3_MAX"

#define RC4_MIN @"RC4_MIN"
#define RC4_TRIM @"RC4_TRIM"
#define RC4_MAX @"RC4_MAX"


#import "DetrumRrmoteView.h"
#import "DetrumMavlinkManager.h"
#import "Interface.h"
#include <math.h>


@interface DetrumRrmoteView()

@property (weak, nonatomic) IBOutlet UIButton *japanHand;

@property (weak, nonatomic) IBOutlet UIButton *americaHand;
@property (weak, nonatomic) IBOutlet UIButton *starAdjusting;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *picView;
@property (weak, nonatomic) IBOutlet UIButton *chooseBackBtn;
@property (weak, nonatomic) IBOutlet UIButton *quitAdjusting;
@property (assign, nonatomic) int count;
@property (retain, nonatomic) NSTimer *timer;
@property (retain, nonatomic) NSMutableArray *channelArr;
@property (assign, nonatomic) int adjustingCount;
@property (retain, nonatomic) NSMutableDictionary *adjustingDic;
@property (retain, nonatomic) NSArray *paramInfoArr;

@end

@implementation DetrumRrmoteView

- (NSMutableArray *)channelArr{
    if (!_channelArr) {
        _channelArr = [[NSMutableArray alloc]init];
    }
    return _channelArr;
}

-(NSMutableDictionary *)adjustingDic{
    if (!_adjustingDic) {
        _adjustingDic = [[NSMutableDictionary alloc]init];
    }
    return _adjustingDic;
}

- (void)awakeFromNib {
    if (animation == nil) {
        animation = [CATransition animation];
    }
    [animation setDuration:0.35f];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromRight];
    [animation setFillMode:kCAFillModeForwards];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.layer addAnimation:animation forKey:@"detrumRemote"];
    self.picView.hidden = YES;
    
    self.paramInfoArr = [NSArray arrayWithObjects:RC1_TRIM,RC1_MAX,RC1_MIN,RC2_TRIM,RC2_MAX,RC2_MIN,RC3_TRIM,RC3_MAX,RC3_MIN,RC4_MAX,RC4_MIN,RC4_TRIM, nil];
    
    //        CAL: Disabling RC IN
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(adjusting:) name:PARAM_STATUE_TEXT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(channels:) name:PARAM_RC_CHANNELS object:nil];
}

- (void)addPointImg {
    _pointImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zoom Button"]];
    _pointImg.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_pointImg];
}


- (IBAction)backBtn:(id)sender {
    [UIView animateWithDuration:0.35f animations:^{
//        [self setRectX:CGRectGetWidth(self.frame)];
        CGRect frame = self.frame;
        frame.origin.x = CGRectGetWidth(self.frame);
        self.frame = frame;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


- (IBAction)japanHandBtn:(id)sender {
    if (self.japanHand.selected) {
        self.japanHand.selected = NO;
    }else{
        self.japanHand.selected = YES;
        self.americaHand.selected = NO;
    }
    
}


- (IBAction)americaHandBtn:(id)sender {
    if (self.americaHand.selected) {
        self.americaHand.selected = NO;
    }else{
        self.americaHand.selected = YES;
        self.japanHand.selected = NO;
    }
}

- (IBAction)chooseBack:(id)sender {
    
    [[DetrumMavlinkManager shareInstance] stopCalibration];
}


- (IBAction)starAdjustingBtn:(id)sender {
    if (self.japanHand.selected || self.americaHand.selected) {
        [[DetrumMavlinkManager shareInstance] startCalibration:0];
        self.adjustingCount = 0;
    }
    else
    {
        UIAlertView *alterView = [[UIAlertView alloc]initWithTitle:@"请选择遥控器模式" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alterView show];
    }
}

- (IBAction)quitAdjustingBtn:(id)sender {
    [[DetrumMavlinkManager shareInstance] stopCalibration];
}



// 校准监听方法
- (void)adjusting:(NSNotification *)noti{
    
//    NSLog(@"NSNotification:%@",noti.userInfo);
    
    if ([[noti.userInfo objectForKey:PARAM_STATUE_TEXT] isEqualToString:@"CAL: Disabling RC IN"]) {
        //开始校准验证通过
        self.picView.hidden = NO;
        [self startToAdjusting];
    }
    else if ([[noti.userInfo objectForKey:PARAM_STATUE_TEXT] isEqualToString:@"CAL: Re-enabling RC IN"]){
        self.picView.hidden = YES;
    }
    
}

// 接受的遥控器数据通知
- (void)channels:(NSNotification*)noti{

}

- (void)startToAdjusting{
    self.adjustingCount++;
    self.channelArr = nil;
    
    // 设置图片
    if (self.adjustingCount < 10) {
        if (self.americaHand.selected) {
            NSMutableString *imageStr = [[NSMutableString alloc]initWithString:@"mode1Step"];
            [imageStr appendString:[NSString stringWithFormat:@"%d",self.adjustingCount]];
            self.picView.image = [UIImage imageNamed:imageStr];
        }else if (self.japanHand.selected){
            NSMutableString *imageStr = [[NSMutableString alloc]initWithString:@"mode2Step"];
            [imageStr appendString:[NSString stringWithFormat:@"%d",self.adjustingCount]];
            self.picView.image = [UIImage imageNamed:imageStr];
        }
    }else if (self.adjustingCount == 10){
        //校准完成 发送数据
        [[DetrumMavlinkManager shareInstance] sendParamRaw:1 paramName:@"RC_MAP_ROLL" value:1 valueType:6];
        [[DetrumMavlinkManager shareInstance] sendParamRaw:1 paramName:@"RC_MAP_YAW" value:4 valueType:6];
        
        // 美国手
        if (self.americaHand.selected) {
            [[DetrumMavlinkManager shareInstance] sendParamRaw:1 paramName:@"RC_MAP_PITCH" value:2 valueType:6];
            [[DetrumMavlinkManager shareInstance] sendParamRaw:1 paramName:@"RC_MAP_THROTTLE" value:3 valueType:6];
        }
        // 日本手
        else if (self.japanHand.selected){
            [[DetrumMavlinkManager shareInstance] sendParamRaw:1 paramName:@"RC_MAP_PITCH" value:3 valueType:6];
            [[DetrumMavlinkManager shareInstance] sendParamRaw:1 paramName:@"RC_MAP_THROTTLE" value:2 valueType:6];
        }
        
        
        for (int i = 0; i<12; i++) {
            [[DetrumMavlinkManager shareInstance] sendParamRaw:1 paramName:self.paramInfoArr[i] value:[[self.adjustingDic valueForKey:self.paramInfoArr[i]] doubleValue] valueType:7];
        }
        
        NSLog(@"===========校准完成 ============");
        [[DetrumMavlinkManager shareInstance] stopCalibration];
        
        return;
    }
    

    NSLog(@"===========开始校准步骤 %d ============",self.adjustingCount);
    self.count = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(reperat) userInfo:nil repeats:YES];
    
}

-(void)reperat{
    NSArray *channelsArr = [[DetrumMavlinkManager shareInstance].parameter valueForKey:PARAM_RC_CHANNELS];
    if (channelsArr) {
        [self.channelArr addObject:channelsArr];
    }
//    NSLog(@"channelArr:%@",channelsArr);
    if (self.count > 9){
//        NSLog(@"channelArr:%@",channelsArr);
        if (!([self compair:self.count j:0] && [self compair:self.count j:1] && [self compair:self.count j:2] && [self compair:self.count j:3])) {
            NSLog(@"===========校准失败============");
            [[DetrumMavlinkManager shareInstance] stopCalibration];
            [self.timer invalidate];
            self.channelArr = nil;
            self.adjustingDic = nil;
            return;
        }
        NSLog(@"===========校验中============");
    }
    
    self.count++;
    if (self.count == 20) {
        [self.timer invalidate];
        NSLog(@"===========结束校准步骤 %d ============",self.adjustingCount);
        [self startToAdjusting];
        NSLog(@"adjustingDic:%@",self.adjustingDic);
    }
    
    
}

- (BOOL) compair:(int)i j:(int)j{
    BOOL res = NO;
    NSString *temp1 = self.channelArr[i-1][j];
    NSString *temp2 = self.channelArr[i][j];
//    NSLog(@"temp1:%@----temp2:%@",temp1,temp2);
    switch (self.adjustingCount) {
        case 1:
            res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            if (res) {
                [self.adjustingDic setObject:self.channelArr[i][0] forKey:RC1_TRIM];
                [self.adjustingDic setObject:self.channelArr[i][1] forKey:RC2_TRIM];
                [self.adjustingDic setObject:self.channelArr[i][2] forKey:RC3_TRIM];
                [self.adjustingDic setObject:self.channelArr[i][3] forKey:RC4_TRIM];
            }
            break;
        case 2:{
            if (j == 3) {
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1000];
                if (res) {
                    [self.adjustingDic setObject:self.channelArr[i][3] forKey:RC4_MIN];
                }
            }else{
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            }
            break;
        }
        case 3:{
            if (j == 3) {
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:2000];
                if (res) {
                    [self.adjustingDic setObject:self.channelArr[i][3] forKey:RC4_MAX];
                }
            }else{
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            }
            break;
        }
        case 4:{
            if (j == 1) {
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1000];
                if (res) {
                    [self.adjustingDic setObject:self.channelArr[i][1] forKey:RC2_MIN];
                }
            }else{
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            }
            break;
        }
        case 5:{
            if (j == 1) {
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:2000];
                if (res) {
                    [self.adjustingDic setObject:self.channelArr[i][1] forKey:RC2_MAX];
                }
            }else{
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            }
            break;
        }
        case 6:{
            if (j == 0) {
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1000];
                if (res) {
                    [self.adjustingDic setObject:self.channelArr[i][0] forKey:RC1_MIN];
                }
            }else{
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            }
            break;
        }
        case 7:{
            if (j == 0) {
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:2000];
                if (res) {
                    [self.adjustingDic setObject:self.channelArr[i][0] forKey:RC1_MAX];
                }
            }else{
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            }
            break;
        }
        case 8:{
            if (j == 2) {
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1000];
                if (res) {
                    [self.adjustingDic setObject:self.channelArr[i][2] forKey:RC3_MIN];
                }
            }else{
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            }
            break;
        }
        case 9:{
            if (j == 2) {
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:2000];
                if (res) {
                    [self.adjustingDic setObject:self.channelArr[i][2] forKey:RC3_MAX];
                }
            }else{
                res = [self compairFourElementFirst:temp1 second:temp2 OfError:10 Range:1500];
            }
            break;
        }
            
        default:
            return NO;
            break;
    }
    
    return res;
    
}

- (BOOL) compairFourElementFirst:(NSString*)temp1 second:(NSString*)temp2 OfError:(int)error Range:(int)range{
    
    BOOL res = ((abs([temp1 intValue] - [temp2 intValue]) < error) && ([temp1 intValue] < range + 50) && ([temp1 intValue] > range - 50) && ([temp2 intValue] < range + 50) && ([temp2 intValue] > range - 50));
    return res;
    
}













@end
