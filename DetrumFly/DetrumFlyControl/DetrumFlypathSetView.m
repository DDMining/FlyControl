//
//  DetrumFlypathSetView.m
//  DetrumFlyControl
//
//  Created by dynamrc on 16/8/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumFlypathSetView.h"


@implementation DetrumFlypathSetView

+(instancetype)shareInstance{
    DetrumFlypathSetView *setView = [[[NSBundle mainBundle]loadNibNamed:@"DetrumFlypathSetView" owner:self options:nil] firstObject];
    setView.pointNumTextField.enabled = NO;
    CGRect dropViewFrame = CGRectMake(setView.pointStatusTextField.frame.origin.x , setView.pointStatusTextField.frame.origin.y + 7, setView.pointStatusTextField.frame.size.width, setView.pointStatusTextField.frame.size.height * 3);
    setView.dropView = [[CQDropDownView alloc]initWithFrame:dropViewFrame andDefaultTitle:@[@"路径点",@"路径点",@"起飞点",@"降落点"]];
    setView.dropView.delegate = setView;
    [setView.dropView setDirection:CQDropDownDirectionDown];
    [setView addSubview:setView.dropView];
    setView.pointStatusTextField.hidden = YES;
    setView.DetrumFlypathSetStatusInt = DetrumFlypathSetStatusThrough;
    setView.stayTimeTextField.text = @"10";
    setView.heightTextField.text = @"10";
    return setView;
}

- (void)CQDropDownDelegateMethod:(CQDropDownView *)sender andSeleteStr:(NSString *)str{
    if ([str isEqualToString:@"路径点"]) {
        _DetrumFlypathSetStatusInt = DetrumFlypathSetStatusThrough;
    }else if ([str isEqualToString:@"起飞点"]){
        _DetrumFlypathSetStatusInt = DetrumFlypathSetStatusTakeOff;
    }else if ([str isEqualToString:@"降落点"]){
        _DetrumFlypathSetStatusInt = DetrumFlypathSetStatusLanding;
    }
}


-(void)show:(UIView *)view{
    
    self.center = view.center;
    self.transform = CGAffineTransformMakeScale(0.2, 0.2);
    [view addSubview:self];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
    
}
- (IBAction)sure:(id)sender {
    _sureAction(_DetrumFlypathSetStatusInt,self.stayTimeTextField.text,self.heightTextField.text);
    [UIView animateWithDuration:0.2 animations:^{
        self.transform = CGAffineTransformMakeScale(0.1, 0.1);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}

- (IBAction)close:(id)sender {
    _closeAction();
    [UIView animateWithDuration:0.2 animations:^{
        self.transform = CGAffineTransformScale(self.transform, 0.1, 0.1);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
