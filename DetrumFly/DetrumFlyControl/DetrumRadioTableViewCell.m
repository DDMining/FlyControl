//
//  DetrumRadioTableViewCell.m
//  DetrumFlyControl
//
//  Created by xcq on 16/2/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumRadioTableViewCell.h"

@implementation DetrumRadioTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _firstRadio.selected = YES;
}

- (IBAction)radioBtn:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    if (btn == _firstRadio) {
        _lastRadio.selected = !btn.selected;
    } else {
        _firstRadio.selected = !btn.selected;
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
