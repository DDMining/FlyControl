//
//  ALAsset+DetrumALAasset.h
//  DetrumFlyControl
//
//  Created by xcq on 16/1/27.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAsset (DetrumALAasset)
- (NSComparator)assertComparetor:(ALAsset *)asset;
@end
