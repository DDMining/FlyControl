//
//  DetrumSetTableViewCell.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/23.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, DetrumSetType) {
    DetrumSetTypeHaveSwitch,
    DetrumSetTypeHaveBtn,
    DetrumSetTypeHaveIndicator,
};
@interface DetrumSetTableViewCell : UITableViewCell
@property (assign, nonatomic) DetrumSetType type;

@property (weak, nonatomic) IBOutlet UILabel *setItemNameTx;
@property (weak, nonatomic) IBOutlet UISwitch *switch3g;
@property (weak, nonatomic) IBOutlet UIImageView *nextImg;
@property (weak, nonatomic) IBOutlet UIButton *exitBtn;
@end
