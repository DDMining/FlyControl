//
//  DetrumSeleteChannelCell.h
//  DetrumFlyControl
//
//  Created by xcq on 16/3/4.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "CQDropDownView.h"
#import <UIKit/UIKit.h>
#import "DetrumBaseTableViewCell.h"

@interface DetrumSeleteChannelCell : DetrumBaseTableViewCell <CQDropDownViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *oneSeleteBtn;
@property (weak, nonatomic) IBOutlet UILabel *onSeleteTitle;
@property (weak, nonatomic) IBOutlet UIButton *twoSeleteBtn;
@property (weak, nonatomic) IBOutlet UILabel *twoSeleteTitle;
@property (strong, nonatomic) CQDropDownView *dropView;
@end
