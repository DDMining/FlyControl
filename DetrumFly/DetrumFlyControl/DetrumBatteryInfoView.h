//
//  DetrumBatteryInfoView.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/29.
//  Copyright © 2016年 dynamrc. All rights reserved.
//
#import "UIView+QuicklyChangeFrame.h"
#import <UIKit/UIKit.h>
#import "NSArray+SecurityGetObj.h"
#import "DetrumBaseView.h"

@interface DetrumBatteryInfoView : DetrumBaseView<UITableViewDataSource,UITableViewDelegate> {
    NSArray *dataSource;
}

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
