//
//  NSArray+SecurityGetObj.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/5/9.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (SecurityGetObj)
- (id)objectSecurityAtIndex:(int)index;
@end
