//
//  DetrumFlyHistoryData.h
//  DetrumFlyControl
//
//  Created by xcq on 16/2/2.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import "DetrumBaseViewController.h"

@interface DetrumFlyHistoryData : DetrumBaseViewController
@property (weak, nonatomic) IBOutlet UIButton *homeBtn;
@property (weak, nonatomic) IBOutlet UIButton *synchronousBtn;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *flyTime;
@property (weak, nonatomic) IBOutlet UILabel *flyMileage;
@property (weak, nonatomic) IBOutlet UILabel *flyNum;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
