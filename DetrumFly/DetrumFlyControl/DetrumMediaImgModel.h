//
//  DetrumMediaImgModel.h
//  DetrumFlyControl
//
//  Created by dynamrc on 16/1/25.
//  Copyright © 2016年 dynamrc. All rights reserved.
//

#import <Realm/Realm.h>

@interface DetrumMediaImgModel : RLMObject

@property  NSInteger id;
@property  NSString *url;
@property  NSString *dateStr;
@property  NSDate *currenDate;
@property  NSString *Location;
@property  NSInteger MediaDuration;
@property  NSData *thumbnail;
@property  NSData *fullScreen;
@property  NSInteger index;
@property  NSInteger type;
@property  NSInteger clickType;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<DetrumMediaImgModel>
RLM_ARRAY_TYPE(DetrumMediaImgModel)
