# DDMiningLib

#### 项目介绍
DDMining,a free time project to make money.

#### 软件架构
iOS MVC


#### 安装教程

1. 编辑Podfile文件，注意：DDMining pod组件已经依赖了 "Masonry", "1.1.0" ，"QMUIKit", "2.7.5" 这两个组件请不要在你的Podfile里边重复编写。
2. 添加 pod 'DDMining', :git => 'https://gitee.com/DDMining/DDMiningLib.git'
3. 执行pod install --verbose --no-repo-update ，这里的技巧是如果你在科学上网的环境下面可以加上 --repo-update，此时会更新你本地pod repo list下面的左右Repo库索引，
如果没有处在科学上网环境 请添加--no-repo-update参数，这样不会检查更新你的pod repo list下面的库索引,--verbose为在Terminal打印输出进度参数.

#### App名称列表
SK娱乐 SK国际娱乐 SK客户端 SK旗舰版  SK彩  SK专业版 彩虹娱乐 彩虹国际娱乐 彩虹客户端 彩虹旗舰版 彩虹专业版


#### 已发布列表

SK国际版 姚新超 2018-09-23


#### 使用说明

1. 在所需要的模式下面直接#import "DDMiningMainVC.h"即可，可以继承，可以直接初始化使用。
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)