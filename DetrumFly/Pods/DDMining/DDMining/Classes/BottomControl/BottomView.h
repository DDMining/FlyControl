//
//  BottomView.h
//  VIPClient
//
//  Created by ChaiJun on 2018/7/2.
//  Copyright © 2018年 Vanson App. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BottomViewDelegate<NSObject>
@optional
- (void)goHomePage;
- (void)goBack;
- (void)refresh;
- (void)recharge;
- (void)cleanCache;
- (void)useBackSite;
@end

@interface BottomView : UIView

@property (nonatomic,assign)id<BottomViewDelegate>delegate;

@end
