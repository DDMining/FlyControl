//
//  BottomView.m
//  VIPClient
//
//  Created by ChaiJun on 2018/7/2.
//  Copyright © 2018年 Vanson App. All rights reserved.
//

#import "BottomView.h"
#import <QMUIKit/QMUIKit.h>
#import <Masonry/Masonry.h>

@interface BottomView ()
@property (nonatomic,strong) QMUIButton *homePage;
@property (nonatomic,strong) QMUIButton *goBack;
@property (nonatomic,strong) QMUIButton *refresh;
@property (nonatomic,strong) QMUIButton *recharge;
@property (nonatomic,strong) QMUIButton *moreAction;
@property(nonatomic, strong) QMUIPopupMenuView *menuView;

@property (nonatomic,strong) NSMutableArray *masonryViewArray;
@end

@implementation BottomView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        QMUICMI.buttonTintColor = [UIColor colorWithRed:70/255.0 green:70/255.0 blue:70/255.0 alpha:1];
        // ButtonTintColor : QMUIButton 默认的 tintColor，不影响系统的 UIButton
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self addSubview:self.homePage];
    [self addSubview:self.goBack];
    [self addSubview:self.refresh];
    [self addSubview:self.recharge];
    [self addSubview:self.moreAction];
    
    [self.masonryViewArray addObject:self.homePage];
    [self.masonryViewArray addObject:self.goBack];
    [self.masonryViewArray addObject:self.refresh];
    [self.masonryViewArray addObject:self.recharge];
    [self.masonryViewArray addObject:self.moreAction];
    
    // 实现masonry水平固定间隔方法
    [self.masonryViewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
    
    // 设置array的垂直方向的约束
    [self.masonryViewArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.top.mas_equalTo(0);
    }];
}

- (NSMutableArray *)masonryViewArray {
    if (!_masonryViewArray) {
        _masonryViewArray = [NSMutableArray array];
    }
    return _masonryViewArray;
}

- (QMUIButton *)homePage {
    if (!_homePage) {
        _homePage = [QMUIButton new];
        [_homePage setTitle:@"首页" forState:UIControlStateNormal];
        _homePage.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
        [_homePage setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_homePage addTarget:self action:@selector(showHomePageAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _homePage;
}

- (QMUIButton *)goBack {
    if (!_goBack) {
        _goBack = [QMUIButton new];
        [_goBack setTitle:@"返回" forState:UIControlStateNormal];
        
        _goBack.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
        [_goBack setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_goBack addTarget:self action:@selector(goBackAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _goBack;
}

- (QMUIButton *)refresh {
    if (!_refresh) {
        _refresh = [QMUIButton new];
        [_refresh setTitle:@"刷新" forState:UIControlStateNormal];
        _refresh.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
        [_refresh setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_refresh addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _refresh;
}

- (QMUIButton *)recharge {
    if (!_recharge) {
        _recharge = [QMUIButton new];
        [_recharge setTitle:@"充值" forState:UIControlStateNormal];
        
        _recharge.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
        [_recharge setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_recharge addTarget:self action:@selector(rechargeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _recharge;
}
- (QMUIButton *)moreAction {
    if (!_moreAction) {
        _moreAction = [QMUIButton new];
        [_moreAction setTitle:@"更多" forState:UIControlStateNormal];
        
        _moreAction.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
        [_moreAction setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_moreAction addTarget:self action:@selector(showMoreAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreAction;
}

- (QMUIPopupMenuView *)menuView {
    if (!_menuView) {
        _menuView = [[QMUIPopupMenuView alloc] init];
        _menuView.automaticallyHidesWhenUserTap = YES;// 点击空白地方消失浮层
        _menuView.maskViewBackgroundColor = UIColorMaskWhite;// 使用方法 2 并且打开了 automaticallyHidesWhenUserTap 的情况下，可以修改背景遮罩的颜色
        _menuView.maximumWidth = 180;
        _menuView.shouldShowItemSeparator = YES;
        _menuView.separatorInset = UIEdgeInsetsMake(0, _menuView.padding.left, 0, _menuView.padding.right);
        
        __weak BottomView *weakSelf = self;
        _menuView.items = @[
                            [QMUIPopupMenuItem itemWithImage:nil title:@"备用线路" handler:^(QMUIPopupMenuView *aMenuView, QMUIPopupMenuItem *aItem) {
                                [aItem.menuView hideWithAnimated:YES];
                                [weakSelf useBackSiteUrl];
                            }],
                            [QMUIPopupMenuItem itemWithImage:nil title:@"清除缓存" handler:^(QMUIPopupMenuView *aMenuView, QMUIPopupMenuItem *aItem) {
                                [aItem.menuView hideWithAnimated:YES];
                                if (weakSelf.delegate&&[weakSelf.delegate respondsToSelector:@selector(cleanCache)]) {
                                    [weakSelf.delegate cleanCache];
                                }
                            }],
                            [QMUIPopupMenuItem itemWithImage:nil title:@"退出App" handler:^(QMUIPopupMenuView *aMenuView, QMUIPopupMenuItem *aItem) {
                                [aItem.menuView hideWithAnimated:YES];
                                 exit(0);
                            }],
                            [QMUIPopupMenuItem itemWithImage:nil title:@"取消" handler:^(QMUIPopupMenuView *aMenuView, QMUIPopupMenuItem *aItem) {
                                [aItem.menuView hideWithAnimated:YES];
                                [weakSelf useBackSiteUrl];
                            }]];
        _menuView.didHideBlock = ^(BOOL hidesByUserTap) {
            
        };
    }
    return _menuView;
}

- (void)useBackSiteUrl {
    if (self.delegate && [self.delegate respondsToSelector:@selector(useBackSite)]) {
        [self.delegate useBackSite];
    }
}

- (void)showHomePageAction:(QMUIButton *)sender {
    if (self.delegate&&[self.delegate respondsToSelector:@selector(goHomePage)]) {
        [self.delegate goHomePage];
    }
}
- (void)goBackAction:(QMUIButton *)sender {
    if (self.delegate&&[self.delegate respondsToSelector:@selector(goBack)]) {
        [self.delegate goBack];
    }
    
}
- (void)refreshAction:(QMUIButton *)sender {
    if (self.delegate&&[self.delegate respondsToSelector:@selector(refresh)]) {
        [self.delegate refresh];
    }
    
}
- (void)rechargeAction:(QMUIButton *)sender {
    if (self.delegate&&[self.delegate respondsToSelector:@selector(recharge)]) {
        [self.delegate recharge];
    }
}

- (void)showMoreAction:(QMUIButton *)sender {
    [self.menuView layoutWithTargetView:self.moreAction];
    [self.menuView showWithAnimated:YES];
}

@end
